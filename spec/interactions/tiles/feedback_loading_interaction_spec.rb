require 'spec_helper'

describe Tiles::FeedbackLoadingInteraction do
  it "should load feedback of a random user" do
    serialized_attrs = [:status, :track_uri]
    user = Persistence::UserTrackFeedback.last.user
    ids = Persistence::UserTrackFeedback.where(user_uri: user.uri).pluck :track_uri
    loader = described_class.new user: user, ids: ids,  country: 'gb'
    res = loader.as_json
    expect(res.any?).to be true
    expect(res.first.keys).to include *serialized_attrs
  end

  it "sould raise error if no user is specified" do
    expect {described_class.new(user: nil)}.to raise_error(InteractionErrors::WrongArgument)
  end

  it "sould raise error if no ids specified" do
    user = Persistence::UserTrackFeedback.last.user
    expect {described_class.new user: user}.to raise_error(InteractionErrors::WrongArgument)
  end
end

