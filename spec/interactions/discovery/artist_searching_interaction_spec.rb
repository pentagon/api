require 'spec_helper'

describe Discovery::ArtistSearchingInteraction do
  it "should find some artists by query"

  it "should raise exception if 'q' parameter is not specified" do
    expect {described_class.new({})}.to raise_error InteractionErrors::WrongArgument
  end

  it "should raise exception if 'q' is blank" do
    expect {described_class.new q: ''}.to raise_error InteractionErrors::WrongArgument
  end
end
