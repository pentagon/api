require 'spec_helper'

describe Discovery::TrackLoadingInteraction do
  it "should load some tracks"

  it "should raise exception if 'id' parameter is not specified" do
    expect {described_class.new({})}.to raise_error InteractionErrors::WrongArgument
  end
end
