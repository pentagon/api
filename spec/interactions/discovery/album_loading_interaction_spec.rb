require 'spec_helper'

describe Discovery::AlbumLoadingInteraction do
  it "should load album by ID"

  it "should raise exception if 'album_id' parameter is not specified" do
    expect {described_class.new({})}.to raise_error InteractionErrors::WrongArgument
  end

  it "should raise exception if album doesn't exist" do
    expect {described_class.new album_id: 'fake'}.to raise_error InteractionErrors::NotFound
  end
end
