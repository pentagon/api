require 'spec_helper'

describe Discovery::ArtistLoadingInteraction do
  it "should load artist by ID"

  it "should raise exception if 'artist_id' parameter is not specified" do
    expect {described_class.new({})}.to raise_error InteractionErrors::WrongArgument
  end

  it "should raise exception if artist doesn't exist" do
    expect {described_class.new artist_id: 'fake'}.to raise_error InteractionErrors::NotFound
  end
end
