require 'spec_helper'

describe Discovery::TracksLoadingInteraction do
  it "should load some tracks"

  it "should raise exception if 'ids' parameter is not specified" do
    expect {described_class.new({})}.to raise_error InteractionErrors::WrongArgument
  end
end
