require 'spec_helper'

describe HitMaker::ReportUpdatingInteraction do
  before(:all) do
    @data = YAML.load_file File.join Rails.root, 'spec', 'hss3_response.yml'
    @track_uri = @data['track_uri']
  end

  it "should load data from YAML" do
    attrs = ["track_uri", "priority", "track_url", "job", "error_callback", "success_callback", "message", "status", "results"]
    expect(@data.keys).to include *attrs
  end

  it "should apply HSS for UK" do
    report = Persistence::HitMakerReport.find_or_create_by territory: 'uk', track_uri: @track_uri, is_processed: false
    described_class.new @data
    report.reload
    expect(report.track_uri).to be == @track_uri
    expect(report.territory).to be == 'uk'
    expect(report.color.keys).to include 'white', 'yellow', 'grey', 'red', 'purple', 'orange', 'green', 'blue', 'black'
    expect(report.energy).to be == @data['results']['energy']
    expect(report.genre_cloud.any?).to be true
    expect(report.genre_cloud.first.keys).to include 'genre_id', 'genre_name', 'probability'
    expect(report.hss_rating).to be == 173
    expect(report.hss_score).to be == 248
    expect(report.hss_avg_year).to be == 2006.2
    expect(report.similar_hits.any?).to be true
    expect(report.similar_hits.count).to be == 20
    report.destroy
  end

  it "should apply HSS for US" do
    report = Persistence::HitMakerReport.find_or_create_by territory: 'us', track_uri: @track_uri, is_processed: false
    described_class.new @data
    report.reload
    expect(report.track_uri).to be == @track_uri
    expect(report.territory).to be == 'us'
    expect(report.color.keys).to include 'white', 'yellow', 'grey', 'red', 'purple', 'orange', 'green', 'blue', 'black'
    expect(report.energy).to be == @data['results']['energy']
    expect(report.genre_cloud.any?).to be true
    expect(report.genre_cloud.first.keys).to include 'genre_id', 'genre_name', 'probability'
    expect(report.hss_rating).to be == 794
    expect(report.hss_score).to be == 490
    expect(report.hss_avg_year).to be == 2007.1
    expect(report.similar_hits.any?).to be true
    expect(report.similar_hits.count).to be == 20
    report.destroy
  end
end
