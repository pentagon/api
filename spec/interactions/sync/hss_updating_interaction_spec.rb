require 'spec_helper'

describe Sync::HssUpdatingInteraction do
  before(:all) do
    @data = YAML.load_file File.join Rails.root, 'spec', 'hss3_response.yml'
    @track_uri = @data['track_uri']
  end

  it "should load data from YAML" do
    attrs = ["track_uri", "priority", "track_url", "job", "error_callback", "success_callback", "message", "status", "results"]
    expect(@data.keys).to include *attrs
  end

  it "should apply hss" do
    hss = described_class.new @data
    res = hss.as_json
    expect(res['track_uri']).to be == @track_uri
    expect(res['hss_score']['uk']).to be == 248
    expect(res['hss_score']['us']).to be == 490
    # expect(res['similar_tracks'].any?).to be true
    # expect(res['similar_tracks'].first).to include *%w(track_uri title artist distance)
    hss.destroy_record @track_uri
  end
end
