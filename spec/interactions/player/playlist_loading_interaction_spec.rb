require 'spec_helper'

describe Player::PlaylistLoadingInteraction do
  it "should load a playlist for a user by playlist_id" do
    loader = described_class.new user: $user, playlist_id: $user.user_playlists.first.uri
    res = loader.as_json
    expect(res).to include :id, :title, :track_uris, :position
  end

  it "should rise error if no user specified" do
    expect {described_class.new user: nil}.to raise_error(InteractionErrors::WrongArgument)
  end

  it "should rise error if no playlist_id specified" do
    expect {described_class.new user: $user}.to raise_error(InteractionErrors::WrongArgument)
  end
end
