require 'spec_helper'

describe Player::PlaylistsLoadingInteraction do
  it "should load playlists for a user" do
    loader = described_class.new user: $user
    res = loader.as_json
    expect(res.any?).to be true
    expect(res.first).to include :id, :title, :track_uris, :position
  end

  it "should rise error if no user specified" do
    expect {described_class.new user: nil}.to raise_error(InteractionErrors::WrongArgument)
  end
end
