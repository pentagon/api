require 'spec_helper'

describe Player::VideoCreatingInteraction do
  it "should create video from params"

  it "should rise error if no user specified" do
    expect {described_class.new user: nil}.to raise_error(InteractionErrors::WrongArgument)
  end
end
