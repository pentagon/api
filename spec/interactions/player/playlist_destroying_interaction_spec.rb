require 'spec_helper'

describe Player::PlaylistDestroyingInteraction do
  it "should delete user's playlist by id"

  it "should rise error if no user specified" do
    expect {described_class.new user: nil}.to raise_error(InteractionErrors::WrongArgument)
  end
end
