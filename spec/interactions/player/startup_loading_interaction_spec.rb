require 'spec_helper'

describe Player::StartupLoadingInteraction do
  it "should load startup object for some user" do
    startup = described_class.new user: $user, user_country: 'gb'
    res = startup.as_json
    expect(res).to include(:track_pool, :user_playlists, :favourite_playlist, :history_playlist, :video_playlist)
  end

  it "should rise NotFound exception if no user specified" do
    expect {described_class.new user: nil}.to raise_error(InteractionErrors::WrongArgument)
  end
end
