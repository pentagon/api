require 'spec_helper'

describe Player::VideoGalleryLoadingInteraction do
  it "should load video gallery" do
    loader = described_class.new track_uri: $user.tracks.first.uri
    res = loader.as_json
    expect(res).to include(:linked_videos, :artist_videos)
  end

  it "should rise error if wrong track_uri specified" do
    expect {described_class.new track_uri: 'fake'}.to raise_error(InteractionErrors::NotFound)
  end
end
