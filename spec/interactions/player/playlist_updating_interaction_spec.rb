require 'spec_helper'

describe Player::PlaylistUpdatingInteraction do

  it "should rise error if no user specified" do
    expect {described_class.new user: nil}.to raise_error(InteractionErrors::WrongArgument)
  end

  it "should rise error if no playlist_id specified" do
    expect {described_class.new user: $user}.to raise_error(InteractionErrors::WrongArgument)
  end

  it "should update playlist title" do
    playlist = $user.user_playlists.first
    user_playlist = {title: 'Ololo'}
    Player::PlaylistUpdatingInteraction.new({playlist_id: playlist.uri, user_playlist: user_playlist, user: $user})
    updated_playlist = Persistence::UserPlaylist.find_by(uri: playlist.uri)
    updated_playlist.title.should == user_playlist[:title]
  end

  it "should switch playlist position" do
    playlist_count = $user.user_playlists.count
    playlist0 = FactoryGirl.create(:persistence_user_playlist, user_uri: $user.uri, position: playlist_count)
    playlist1 = FactoryGirl.create(:persistence_user_playlist, user_uri: $user.uri, position: playlist_count + 1)
    playlist2 = FactoryGirl.create(:persistence_user_playlist, user_uri: $user.uri, position: playlist_count + 2)
    user_playlist = {update_position: 2}
    Player::PlaylistUpdatingInteraction.new({playlist_id: playlist0.uri, user_playlist: user_playlist, user: $user})
    updated_playlist = Persistence::UserPlaylist.find_by(uri: playlist0.uri)
    updated_playlist.position.should == user_playlist[:update_position]
  end

  it "should update playlist track_uris" do
    playlist = $user.user_playlists.first
    user_playlist = {track_uris: [$user.tracks.first.uri, $user.tracks.last.uri]}
    Player::PlaylistUpdatingInteraction.new({playlist_id: playlist.uri, user_playlist: user_playlist, user: $user})
    updated_playlist = Persistence::UserPlaylist.find_by(uri: playlist.uri)
    updated_playlist.track_uris.should == user_playlist[:track_uris]
  end

  it "should empty playlist track_uris" do
    playlist = $user.user_playlists.first
    user_playlist = {empty_uris: true}
    Player::PlaylistUpdatingInteraction.new({playlist_id: playlist.uri, user_playlist: user_playlist, user: $user})
    updated_playlist = Persistence::UserPlaylist.find_by(uri: playlist.uri)
    updated_playlist.track_uris.should == []
  end

  it "shouldn't switch playlist position if new position is wrong" do
    playlist = $user.user_playlists.first
    user_playlist = {update_position: 1000}
    expect {Player::PlaylistUpdatingInteraction.new(playlist_id: playlist.uri, user_playlist: user_playlist,
      user: $user)}.to raise_error(InteractionErrors::WrongArgument)
  end
end
