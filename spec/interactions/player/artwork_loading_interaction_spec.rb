require 'spec_helper'

describe Player::ArtworkLoadingInteraction do
  it "should load artwork for VICE" do
    loader = described_class.new artist_id: $user.artist.uri
    res = loader.as_json
    expect(res[:artist_id]).to match $user.artist.uri
    expect(res).to include :artist_id, :galleries
  end

  it "should rise NotFound exception if no artist found" do
    expect {described_class.new artist_id: 'fake'}.to raise_error(InteractionErrors::NotFound)
  end
end
