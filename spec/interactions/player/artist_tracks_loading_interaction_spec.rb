require 'spec_helper'

describe Player::ArtistTracksLoadingInteraction do
  it "should load tracks of unsigned artist" do
    artist = $user.artist
    loader = described_class.new artist_id: artist.uri, country: 'uk'
    res = loader.as_json
    expect(res).to include(:artist_id, :artist_name, :tracks)
    expect(res[:artist_id]).to match(artist.uri)
    expect(res[:tracks].any?).to be true
  end

  it "sould raise error if no artist_id is specified" do
    expect {described_class.new({})}.to raise_error(InteractionErrors::WrongArgument)
  end

  it "sould raise error if no artist found" do
    expect {described_class.new artist_id: 'fake'}.to raise_error(InteractionErrors::NotFound)
  end
end
