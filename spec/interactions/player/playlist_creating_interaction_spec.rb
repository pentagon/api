require 'spec_helper'

describe Player::PlaylistCreatingInteraction do
  it "should create playlist from params" do
    params = {title: "New playlist", track_uris: []}
    playlist = Player::PlaylistCreatingInteraction.new(user_playlist: params, user: $user)
    res = playlist.as_json
    expect(res).to include :title, :track_uris, :position
    expect(res[:title]).to match params[:title]
    expect(res[:track_uris].any?).to be false
  end

  it "should rise error if no user specified" do
    expect {described_class.new user: nil}.to raise_error(InteractionErrors::WrongArgument)
  end

  it "should rise error if no user_playlist params specified" do
    expect {described_class.new user: $user}.to raise_error(InteractionErrors::WrongArgument)
  end
end
