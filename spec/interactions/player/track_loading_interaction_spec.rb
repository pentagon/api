require 'spec_helper'

describe Player::TrackLoadingInteraction do
  it "should load a track of a user" do
    serialized_attrs = [:id, :album_uri, :album_name, :allowed, :artist_name, :artist_uri, :cover_uri, :rights,
      :status, :track_name, :track_url, :user_uri]
    track_feedback = FactoryGirl.create(:persistence_track_feedback, user_uri: $user.uri, track_uri: $user.tracks.first.uri)
    loader = described_class.new track_id: $user.tracks.first.uri, country: 'gb', user: $user
    res = loader.as_json
    expect(res).to include *serialized_attrs
  end

  it "sould raise error if no track_id is specified" do
    expect {described_class.new({})}.to raise_error(InteractionErrors::WrongArgument)
  end

  it "sould raise error if no track found" do
    expect {described_class.new track_id: 'fake'}.to raise_error(InteractionErrors::NotFound)
  end
end

