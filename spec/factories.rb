FactoryGirl.define do

  factory :artist do
    trait :basic_feature do
      title 'Bread'
    end

    trait :unsigned do
      id 'fms:artist:1361372470879993'
      image_url 'http://storage.qa.vocvox.com/public/fms/artist/1361372470879993.jpg'
      user_uri 'fms:user:1360696664851283'
      popularity_rank ''
      tracks {[build(:track, id:'rrmusic:track:25830', title:'Angel', genre:'Rock', duration: 220, label:'unsigned',
          image_url:'', music_url:'', release_date:'2013-02-15')]}
      albums {[build(:album, id:'rrmusic:album:3767', title:'My Music', release_date: '2010-09-07', image_url: '')]}
    end

    trait :labeled do
      id 'medianet:artist:12009'
      type 'artist'
      tracks {[build(:track, id: "medianet:track:15747", title: "It's About Time")]}
      albums {[build(:album, id: 'medianet:album:15717', title: 'Anthology Of Bread')]}
    end

    factory :unsigned_artist, traits: [:basic_feature, :unsigned]
    factory :labeled_artist,  traits: [:basic_feature, :labeled]
  end

  factory :album do
    trait :basic_feature do
      title 'Anthology Of Bread'
      image_url ''
      release_date ''
    end

    trait :unsigned do
      id 'uplaya:album:13798'
      artist { build(:artist, id:'uplaya:artist:14660', title: 'lexyonce', image_url: '', user_uri: 'uplaya:user:16297') }
    end

    trait :labeled do
      id 'medianet:album:15717'
      type ''
      genre ''
      contributors { build(:artist, id: 'medianet:artist:12009', title: 'Tracy Lawrence', type: 'ARTIST' ) }
      tracks_count 12
      label ''
      duration ''
      artist {build(:artist, id: 'medianet:artist:25977', title:'Paul Weston')}
      tracks {[build(:track, id: 'medianet:track:75112503', title: 'Dardanella')]}
      rights {}
    end

    factory :unsigned_album, traits: [:basic_feature,:unsigned]
    factory :labeled_album,  traits: [:basic_feature,:labeled]
  end

  factory :track do
    trait :basic_feature do
      title "It's About Time"
      image_url 'http://storage.qa.vocvox.com/public/fms/track/1362160603053093.jpg'
      genre 'Rap/Hip Hop'
      release_date '2003-03-20'
      music_url 'http://storage.qa.vocvox.com/public/fms/track/1362160603053093.mp3'
      label ''
      duration 182
      allowed false
      rights Hash.new
    end

    trait :unsigned do
      id 'rrmusic:track:31824'
      artist { build(:artist, id: 'fms:artist:1361372470879993', title: 'Bread', image_url: '', user_uri: 'fms:user:1360696664851283' ) }
      album  { build(:album, id: 'uplaya:album:13798', title: 'Anthology Of Bread', release_date: '', image_url: '' )  }
     # hss3  { hss_rating: 999 }
    end

    trait :labeled do
      id "medianet:track:15747"
      type ''
      item_number ''
      contributors { build(:artist, id: 'medianet:artist:12009', title: 'Bread', type: 'PERFORMER') }
      artist { build(:artist, id: 'medianet:artist:12009', title: 'Bread') }
      album  { build(:album, id: 'medianet:album:15717', title: 'Anthology Of Bread') }
      #association :artist, factory: [:artist, :labeled], strategy: :build #vs. :artist, :labeled, strategy: :build
      user_uri 'rrmusic:user:18212'
    end

    factory :unsigned_track, traits: [:basic_feature,:unsigned]
    factory :labeled_track,  traits: [:basic_feature,:labeled]
    factory :basic_track, traits: [:basic_feature]
  end
end

#TODO: UPDATE TRACKS TO RESEMBLE TO EACH OTHER, BOTH LABELED AND UNSIGNED
