module CoreExt
  module Json
    def parsed
      ActiveSupport::JSON.decode(self)
    end
  end
end


String.send :include, CoreExt::Json
