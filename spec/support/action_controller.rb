module ActionController
  class TestCase
    module Behavior
      def raw_post(action, raw_data = '', parameters = nil, session = nil, flash = nil)
        @request.env['RAW_POST_DATA'] = raw_data
        result = post action, parameters, session, flash
        @request.env['RAW_POST_DATA'] = ''
        result
      end
    end
  end
end
