module StorageHelper
  def get_keys
   YAML.load(File.read(Rails.root.join('spec', 'fixtures', 'keys_storage.yml')))
  end
end

RSpec.configure do |c|
  c.include StorageHelper 
end

