require 'rubygems'
require 'spork'

Spork.prefork do
  ENV["RAILS_ENV"] ||= 'test'
  require File.expand_path("../../config/environment", __FILE__)
  require 'rspec/rails'
  require 'rspec/autorun'
  require 'active_support/core_ext/hash/keys'
  require 'json'
  require 'benchmark'
  # Requires supporting ruby files with custom matchers and macros, etc,
  # in spec/support/ and its subdirectories.
  Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

  RSpec.configure do |config|
    config.infer_base_class_for_anonymous_controllers = false
    config.order = 'random'
    config.before(:suite) do
      $user = FactoryGirl.create :persistence_user
    end
    config.after(:suite) do
      Mongoid.default_session.collections.select {|c| c.name !~ /system/ }.each(&:drop)
    end
  end
end

Spork.each_run do
  Dir["#{Rails.root}/app/**/*.rb"].each {|file| load file }
  load "#{Rails.root}/config/routes.rb"
end


ENV["RAILS_ENV"] ||= 'test'
require 'simplecov'
if ENV['COVERAGE']
  SimpleCov.start :rails do
    coverage_dir "coverage/simplecov"
  end
end

require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/autorun'
require 'active_support/core_ext/hash/keys'
require 'json'
require 'benchmark'

Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

RSpec.configure do |config|
  config.alias_example_to :it
  config.alias_example_to :given
  config.infer_base_class_for_anonymous_controllers = false
  config.order = 'default'
end

class Hash
  alias :wia :with_indifferent_access
end
