FactoryGirl.define do
  factory :persistence_track_feedback, class: Persistence::UserTrackFeedback do
    user_uri '111'
    track_uri '222'
  end
end
