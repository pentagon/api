FactoryGirl.define do
  factory :persistence_user_playlist, class: Persistence::UserPlaylist do
    title "New playlist"
    position 0
  end
end
