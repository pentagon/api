FactoryGirl.define do
  factory :persistence_user, class: Persistence::User do
    sequence(:email) { |n| "test_user#{n}@example.com" }
    password "12345678"
    after(:create) do |u|
      u.artist = create :persistence_artist, user_uri: u.uri
      create :persistence_user_playlist, track_uris: u.tracks.pluck(:uri), user_uri: u.uri
    end
  end
end
