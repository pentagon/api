FactoryGirl.define do
  factory :persistence_artist, class: Persistence::Artist do
    stage_name 'Some Artist'
    after(:create) do |artist|
      create :persistence_track, artist_uri: artist.uri, user_uri: artist.user_uri, title: 'Track 1'
      create :persistence_track, artist_uri: artist.uri, user_uri: artist.user_uri, title: 'Track 2'
      create :persistence_album, artist_uri: artist.uri, title: 'Album 1'
      create :persistence_album, artist_uri: artist.uri, title: 'Album 2'
    end
  end
end
