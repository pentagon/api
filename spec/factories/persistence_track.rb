FactoryGirl.define do
  factory :persistence_track, class: Persistence::Track do
    title 'track_name'
    duration 240
    genre 'Rock'
  end
end
