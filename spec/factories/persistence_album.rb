FactoryGirl.define do
  factory :persistence_album, class: Persistence::Album do
    title 'Some album'
    after(:create) do |album|
      create :persistence_track, artist_uri: album.artist_uri, user_uri: album.artist.user_uri,
        album_uri: album.uri, title: 'Track 1'
      create :persistence_track, artist_uri: album.artist_uri, user_uri: album.artist.user_uri,
        album_uri: album.uri, title: 'Track 2'
    end
  end
end
