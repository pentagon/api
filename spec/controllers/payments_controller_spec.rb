require 'spec_helper'
require 'action_controller'
require 'json'

describe PaymentsController do

  def sign_in(user = double('user'))
    if user.nil?
      request.env['warden'].stub(:authenticate!).
        and_throw(:warden, {scope: :user})
      controller.stub current_user: nil
    else
      request.env['warden'].stub authenticate!: user
      controller.stub current_user: user
    end
  end

  let(:auth_token) { "TEST_AUTH_TOKEN" }
  let(:paypal_token) { "TEST_PAYPAL_TOKEN" }
  let(:paypal_payer_id) { "TEST_PAYPAL_PAYER_ID" }
  let(:paypal_transaction_id) { "TEST_PAYPAL_TRANSACTION_ID" }
  let(:error_url) { "http://example.com/error" }
  let(:ip_address) { "212.58.244.18" }
  let(:success_url) { "http://example.com/success" }
  let(:test_track_id) { "unsigned:track:123" }
  let :track_to_sell do
    track = Track.new({
      id: test_track_id,
      title: "Test title"
    })
    track.stub(:digital_goods_price) { {amount: 100, currency:"GBP"} }
    track.stub(:artist_name) { "Test artist" }
    track
  end
  let :valid_user do
    user = FactoryGirl.create(:persistence_user)
    user.set_billing_address({
      country: "GB",
      city: "London",
      postal_code: "W1F 8GW"
    }).save
    user
  end
  let :invalid_user do
    user = FactoryGirl.create(:persistence_user)
    user.set_billing_address({
      country: "INVALID_COUNTRY",
      city: "London",
      postal_code: "W1F 8GW"
    }).save
    user
  end
  let :temporary_payment do
    payment = ::Persistence::PaymentOrder.create_temporary_payment track_to_sell, valid_user, success_url: success_url, error_url: error_url
    payment.stub(:item) { track_to_sell }
    payment
  end
  let :success_response do
    double(:success_response, success?: true, params: {"transaction_id" => paypal_transaction_id, "payment_status" => "Completed"})
  end

  describe '#process_ipn' do
    let(:notification_class) { OffsitePayments::Integrations::Paypal::Notification }

    it "should verify if the IPN was received from PayPal" do
      expect_any_instance_of(notification_class).to receive(:acknowledge)
      raw_post :process_ipn, "IPN_TEST=1"
    end

    it "should respond with status 200 OK and no body" do
      raw_post :process_ipn, "IPN_TEST=1"
      expect(response).to be_success
      expect(response.body).to be_blank
    end

    it "should process the IPN if it was related to subscription" do
      recurring_payment_id = 'TEST_RECURRING_PAYMENT_ID'
      subscription_stub = double 'subscription'
      notification_class.any_instance.stub(:acknowledge) { true }
      notification_class.any_instance.stub :test? do
        Settings.paypal.use_sandbox
      end
      expect_any_instance_of(notification_class).to receive(:params).at_least(:once) do
        {'recurring_payment_id' => recurring_payment_id}
      end
      expect(Persistence::Subscription).to(receive(:find_by).with({payment_prov_id: recurring_payment_id})) { subscription_stub }
      expect(subscription_stub).to receive(:process_ipn)
      raw_post :process_ipn, "IPN_TEST=1"
    end
  end

  describe '#buy' do
    it "should redirect to error url on item not found" do
      params = {
        type: "track",
        id: test_track_id,
        error_url: error_url,
        ip_address: ip_address
      }
      sign_in valid_user
      get :buy, params
      expect(response).to redirect_to(error_url)
      expect(response.headers["X-Payments-Error"]).to eq("item_not_found")
    end

    it "should create temporary payment if everything is fine" do
      params = {
        type: "track",
        id: test_track_id,
        error_url: error_url,
        ip_address: ip_address
      }
      Track.stub(:digital_goods_find_by_id) { track_to_sell }
      expect(::Persistence::PaymentOrder).to receive(:create_temporary_payment).and_call_original
      sign_in valid_user
      get :buy, params
    end

    it "should redirect to PayPal if everything is correct" do
      params = {
        type: "track",
        id: test_track_id,
        error_url: error_url,
        ip_address: ip_address
      }
      Track.stub(:digital_goods_find_by_id) { track_to_sell }
      sign_in valid_user
      get :buy, params
      expect(response).to be_redirect
      expect(response.headers["Location"]).to match("paypal.com")
    end
  end

  describe '#confirm_purchase' do
    it "should redirect to error url on payment not found" do
      ::Persistence::PaymentOrder.stub(:find_by) { nil }
      sign_in valid_user
      get :confirm_purchase, id: temporary_payment.id
      expect(response.headers["Location"]).to match(/\/api\/payments\/result_error\?reason=internal_payment_error/)
    end

    it "should fix ambigious tokens" do
      params = {
        id: temporary_payment.uri,
        token: paypal_token,
        th_token: auth_token
      }
      get :confirm_purchase, params
      expect(request.params[:token]).to eq(auth_token)
      expect(request.params[:paypal_token]).to eq(paypal_token)
    end

    it "should commit payment and redirect to success url" do
      ::Persistence::PaymentOrder.stub(:find_by) { temporary_payment }
      sign_in valid_user
      params = {
        id: temporary_payment.uri,
        th_token: 'present',
        token: paypal_token,
        :PayerID => paypal_payer_id
      }

      expect(temporary_payment).to receive(:do_purchase).with(paypal_token, paypal_payer_id).and_call_original
      allow_any_instance_of(ActiveMerchant::Billing::PaypalExpressGateway).to receive(:purchase) do
        success_response
      end
      temporary_payment.stub(:deliver_item) { {success: true} }

      get :confirm_purchase, params
      expect(response).to redirect_to(success_url)
    end

    it "should redirect back to PayPal on funding failure error" do
      ::Persistence::PaymentOrder.stub(:find_by) { temporary_payment }
      sign_in valid_user
      params = {
        id: temporary_payment.uri,
        th_token: 'present',
        token: paypal_token,
        :PayerID => paypal_payer_id
      }
      temporary_payment.stub(:do_purchase).and_raise ::Persistence::PaymentOrder::PaymentProviderFundingFailureError

      get :confirm_purchase, params
      expect(response).to be_redirect
      expect(response.headers["Location"]).to match("paypal.com")
    end

    it "should redirect to error url on delivery error" do
      ::Persistence::PaymentOrder.stub(:find_by) { temporary_payment }
      sign_in valid_user
      params = {
        id: temporary_payment.uri,
        th_token: 'present',
        token: paypal_token,
        :PayerID => paypal_payer_id
      }
      allow_any_instance_of(ActiveMerchant::Billing::PaypalExpressGateway).to receive(:purchase) do
        success_response
      end
      temporary_payment.stub(:deliver_item) { {success: false} }

      get :confirm_purchase, params
      expect(response).to redirect_to(error_url)
      expect(response.headers["X-Payments-Error"]).to eq("delivery_error")
    end
  end

  describe '#get_price' do
    it "should return 404 if item not found" do
      params = {
        type: "track",
        id: test_track_id
      }

      get :get_price, params
      expect(response).to be_not_found
    end

    it "should return price if item found" do
      price = track_to_sell.digital_goods_price
      Track.stub(:digital_goods_find_by_id) { track_to_sell }
      params = {
        type: "track",
        id: track_to_sell.id
      }

      get :get_price, params
      expect(response.body.parsed["price"]["amount"]).to eq(price[:amount])
      expect(response.body.parsed["price"]["currency"]).to eq(price[:currency])
    end
  end

  describe '#check_billing_address' do
    it "should return error on invalid address" do
      sign_in invalid_user
      get :check_billing_address, ip_address: ip_address
      expect(response.body.parsed["result"]["success"]).to be_false
      expect(response.body.parsed["result"]["errors"]).to eq({"country" => "invalid"})
    end

    it "should return success on valid address" do
      sign_in valid_user
      get :check_billing_address, ip_address: ip_address
      expect(response.body.parsed["result"]["success"]).to be_true
      expect(response.body.parsed["result"]["errors"]).to be_blank
    end

    it "should allow to override address fields" do
      sign_in invalid_user
      get :check_billing_address, ip_address: ip_address, country: "GB"
      expect(response.body.parsed["result"]["success"]).to be_true
      expect(response.body.parsed["result"]["errors"]).to be_blank
    end
  end

  describe '#update_billing_address' do
    it "should return error on invalid address" do
      sign_in valid_user
      put :update_billing_address, ip_address: ip_address, country: "INVALID"
      expect(response.status).to eq(422)
      expect(response.body.parsed["result"]["success"]).to be_false
      expect(response.body.parsed["result"]["errors"]).to eq({"country" => "invalid"})
    end

    it "update user on valid address" do
      sign_in valid_user
      expect(valid_user).to receive(:set_billing_address).and_return(valid_user)
      expect(valid_user).to receive(:save).and_call_original
      put :update_billing_address, ip_address: ip_address, city: "Cardiff"
      expect(response.body.parsed["result"]["success"]).to be_true
      expect(response.body.parsed["result"]["errors"]).to be_blank
    end
  end

  describe '#result' do
    it "should return result JSON if popup parameter not passed" do
      get :result, {status: :success, token: auth_token}, format: :json
      expected_json = {
        "result" => {
          "type" => "payments",
          "token" => auth_token,
          "result" => "success"
        }
      }
      expect(response.body.parsed).to eq(expected_json)
    end

    it "should render 'close_popup' template if popup parameter passed" do
      get :result, {status: :success, token: auth_token, popup: 1}, format: :json
      expect(response).to render_template('close_popup')
    end
  end

  describe "#countries_list" do
    it "should return a list of valid countries" do
      get :countries_list, format: :json
      expect(response.body.parsed).to eq(::Persistence::PaymentOrder.send(:valid_country_states).wia)
    end
  end

end
