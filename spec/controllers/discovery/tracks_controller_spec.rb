require 'spec_helper'
require 'json'
require 'benchmark'

describe Discovery::TracksController do
  let(:labeled_track) { FactoryGirl.build(:labeled_track) }

  describe '#INDEX' do
    context 'existing track record' do
      params = {ids: ['medianet:track:15261','medianet:track:15735']}

      it 'responds with 200 OK' do
        response = get :index, params, format: :json
        expect(response.status).to eq(200)
      end

      it 'renders an expected json' do
        response = get :index, params, format: :json
        response.body.parsed.each do |actual|
        expect(actual.keys).to eq(labeled_track.as_json_for_discovery.wia.keys)
        end
      end
    end

    context 'non-existing track record' do
      it 'responds with 404 Not Found' do
        response = get :index, {ids: nil}, format: :json
        expect(response.status).to eq(404)
      end

      it 'renders nothing' do
        response = get :index, {ids: nil}, format: :json
        response.body.strip.should be_blank
      end
    end
  end

  describe '#SHOW' do
    context 'existing track record' do
      before(:each) do
        ApiBase.stub(:fetch) { labeled_track }
        Track.stub(:document_type) { 'track' }
      end

      it 'responds with 200 OK' do
        p 'TIME ELAPSED: ' + (Benchmark.realtime {response = get :show, :id => labeled_track.id, format: :json}).to_s
        expect(response.status).to eq(200)
      end

      it 'renders an expected json' do
        response = get :show, :id => labeled_track.id, format: :json
        expect(response.body.parsed.keys).to eq(labeled_track.as_json_for_discovery.wia.keys)
      end
    end

    context 'non-existing track record' do
      before(:each) do
        ApiBase.stub(:fetch) { nil }
      end

      it 'responds with 404 Not Found' do
        p 'TIME ELAPSED: ' + (Benchmark.realtime {response = get :show, :id => 'random', format: :json}).to_s
        expect(response.status).to eq(404)
      end

      it 'renders nothing' do
        response = get :show, :id => 'random', format: :json
        response.body.strip.should be_blank
      end
    end
  end

  describe '#SEARCH' do
    context 'existing track record' do
      params = {q: 'Frozen', per_page: '10', remote_ip: '94.100.180.199'} #user-country cannot be set by 'before_filter :set_country_for_request' call

      it 'responds with 200 status code' do
        response = get :search,  params, format: :json
        expect(response.status).to eq(200)
      end

      it 'renders a json' do
        response = get :search,  params, format: :json
        actual = response.body.parsed.wia
        actual[:data] = actual[:data][0]
        tire_meta = {current_page: '', per_page: '' , country: '' , total: {count: '', pages: ''}}
        expect(actual.keys).to eq(tire_meta.merge({data: [labeled_track]}).wia.keys)
        expect(actual[:data].keys).to eq(labeled_track.as_json_for_discovery.wia.keys)
      end
    end

    context 'non-existing track record' do
      params = {q: '~~~~'}

      it 'responds with 404 status code' do
        response = get :search,  params, format: :json
        expect(response.status).to eq(404)
      end

      it 'renders an empty body' do
        response = get :search,  params, format: :json
        response.body.strip.should be_blank
      end
    end
  end
end


