require 'spec_helper'

describe Discovery::StartController do
  let(:samples) {get_keys.wia}

  def stub_out_load time_ref
    pending "Needs update."
    StartWheels.stub(:load) do
      ::Settings.discovery.start_wheels[time_ref].map do |track_id|
        res = samples[:no_track_hashed_wheel].id = track_id
        res
      end
    end
  end

  describe '#SHOW' do
    it 'responds with 200 OK' do
      response = get :show, format: :json
      response.status.should == 200
    end

    it 'renders expected json for discovery wheels' do
      response = get :show, format: :json
      [:future, :present, :past].each do |time_ref|
        stub_out_load time_ref
        response.body.parsed.wia[:wheels][time_ref].each{ |track| track.keys.should == samples[:no_track_hashed_wheel].keys}
      end
    end

    it 'renders expected jsonPi for discovery wheels', p: {callback: 'wrapper_function_for'} do
      response = get :show, example.metadata[:p], format: :json
      [:future, :present, :past].map {|time_ref| stub_out_load time_ref}
      response.body.should match(/^#{example.metadata[:p][:callback]}\(.+\)/)
    end
  end
end
