require 'spec_helper'

describe Mailing::ClickController do

  describe "GET 'open'" do
    it "returns http success" do
      get 'open', token: 'cnJtdXNpYzp1c2VyOjcwNjg2', campaign: 'test'
      response.should be_success
    end

    it "creates activity" do
      pending "Can't be tested without an isolated environment"
      expect {
        get 'open', token: 'cnJtdXNpYzp1c2VyOjcwNjg2', campaign: 'test'
      }.to change(Persistence::Activity, :count).by 1
    end

    it "creates a 'newsletter opened' activity" do
      pending "Can't be tested without an isolated environment"
      get 'open', token: 'cnJtdXNpYzp1c2VyOjcwNjg2', campaign: 'test'
      activity = Persistence::Activity.last
      expect(activity.key).to eq('newsletter')
      expect(activity.parameters['event']).to eq('opened')
    end
  end

  describe "GET 'visit'" do
    it "returns http success" do
      get 'visit', token: 'cnJtdXNpYzp1c2VyOjcwNjg2', campaign: 'test', url: 'http://example.com'
      expect(response).to redirect_to('http://example.com')
    end

    it "creates activity" do
      expect {
        get 'visit', token: 'cnJtdXNpYzp1c2VyOjcwNjg2', campaign: 'test'
      }.to change(Persistence::Activity, :count).by 1
    end

    it "creates a 'newsletter visited' activity" do
      pending "Can't be tested without an isolated environment"
      get 'visit', token: 'cnJtdXNpYzp1c2VyOjcwNjg2', campaign: 'test'
      activity = Persistence::Activity.last
      expect(activity.key).to eq('newsletter')
      expect(activity.parameters['event']).to eq('visited')
    end
  end

end
