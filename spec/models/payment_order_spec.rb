require "spec_helper"

describe Persistence::PaymentOrder do

  let :item_to_sell do
    track = Track.new({
      id: "unsigned:track:123",
      title: "Test title"
    })
    track.stub(:digital_goods_price) { {amount: 100, currency: "GBP"} }
    track.stub(:artist_name) { "Test artist" }
    track
  end

  let :user do
    user = FactoryGirl.create(:persistence_user)
    user.set_billing_address({
      country: "GB",
      city: "London",
      postal_code: "W1F 8GW"
    }).save
    user
  end

  let(:paypal_token) { "TEST_PAYPAL_TOKEN" }
  let(:paypal_transaction_id) { "TEST_PAYPAL_TRANSACTION_ID" }
  let(:paypal_payer_id) { "TEST_PAYPAL_PAYER_ID" }

  describe "class" do

    context "#create_temporary_payment" do
      it "should not create if incorrect item" do
        expect do
          result = described_class.create_temporary_payment [], user, {}
          expect(result).to be_false
        end.not_to change(described_class, :count)
      end

      it "should create if correct" do
        payment = described_class.create_temporary_payment item_to_sell, user, {}
        expect(payment).to be_an_instance_of(described_class)
        expect(payment).to be_persisted
      end
    end

    context "#create_temporary_payment!" do
      it "should be a wrapper that raises on error" do
        expect do
          described_class.create_temporary_payment! [], user, {}
          expect(described_class).to receive(:create_temporary_payment)
        end.to raise_error(described_class::PaymentError)
      end
    end

    context "#check_billing_address" do

      it "should require valid country" do
        blank_result   = described_class.check_billing_address country: ""
        invalid_result = described_class.check_billing_address country: "invalid"
        valid_result_1 = described_class.check_billing_address country: "GB"
        valid_result_2 = described_class.check_billing_address country: "gb"
        expect(blank_result[:country]).to eq(:blank)
        expect(invalid_result[:country]).to eq(:invalid)
        [valid_result_1, valid_result_2].each do |result|
          expect(result[:country]).to be_nil
        end
      end

      it "should require city to be set" do
        blank_result    = described_class.check_billing_address city: ""
        nonblank_result = described_class.check_billing_address city: "non_blank"
        expect(blank_result[:city]).to eq(:blank)
        expect(nonblank_result[:city]).to be_nil
      end

      it "should require valid postal code for some countries" do
        postal_code = "W1F 8GW"
        blank_country_result = described_class.check_billing_address country: "", postal_code: postal_code
        invalid_result = described_class.check_billing_address country: "GB", postal_code: "invalid"
        valid_result = described_class.check_billing_address country: "GB", postal_code: postal_code
        expect(blank_country_result[:postal_code]).to be_nil
        expect(invalid_result[:postal_code]).to eq(:invalid)
        expect(valid_result[:postal_code]).to be_nil
      end

      it "should require valid state for some countries" do
        country_states = Persistence::Helpers::COUNTRIES_STATES.with_indifferent_access
        valid_country = country_states.keys.first.to_s
        valid_state = country_states[valid_country].keys.first.to_s
        invalid_country = "INVALID"
        invalid_state = "TEST"
        valid_result = described_class.check_billing_address country: valid_country, state: valid_state
        invalid_result = described_class.check_billing_address country: valid_country, state: invalid_state
        not_validatable_result = described_class.check_billing_address country: invalid_country, state: invalid_state
        expect(valid_result[:state]).to be_nil
        expect(invalid_result[:state]).to eq(:invalid)
        expect(not_validatable_result[:state]).to be_nil
      end
    end

  end

  it "should return correct purchase country" do
    payment = described_class.create_temporary_payment(item_to_sell, user)
    expect(payment.purchase_country.downcase).to eq(user.billing_address_hash[:country].downcase)
  end

  it "should be able to fetch transaction details" do
    payment = described_class.create_temporary_payment(item_to_sell, user)
    payment.update_attributes payment_provider_payment_id: paypal_transaction_id
    expect_any_instance_of(ActiveMerchant::Billing::PaypalExpressGateway.as_null_object).to receive(:transaction_details).once do |arg|
      expect(arg).to eq(paypal_transaction_id)
      double(:response, success?: true).as_null_object
    end
    payment.fetch_payment_provider_transaction_details
    payment.fetch_payment_provider_transaction_details
    payment.fetch_payment_provider_transaction_details
  end

  it "should update status with one fetched from from PayPal on status refresh" do
    payment = described_class.create_temporary_payment(item_to_sell, user)
    payment.payment_provider_payment_id = paypal_transaction_id
    payment.save
    expect_any_instance_of(ActiveMerchant::Billing::PaypalExpressGateway.as_null_object).to receive(:transaction_details).with(paypal_transaction_id) do
      double "paypal_status_response", success?: true, params: {"payment_status" => described_class::STATUSES.values.first}
    end
    payment.refresh_status
    expect(payment.status).to eq(described_class::STATUSES.keys.first)
  end

  it "should return correct redirect url" do
    return_url = "http://example.com"
    Track.stub(:digital_goods_find_by_id) { item_to_sell }
    payments = {
      payment_web: described_class.create_temporary_payment(item_to_sell, user),
      payment_mobile: described_class.create_temporary_payment(item_to_sell, user, mobile: true),
      payment_popup: described_class.create_temporary_payment(item_to_sell, user, popup: true)
    }
    urls = {}
    payments.each do |type, payment|
      url = payment.purchase_redirect_url return_url, return_url
      expect(url).to match("paypal.com")
      expect(url).to match("token=")
      urls[type] = url
    end
    expect(urls[:payment_web]).to match("cmd=_express-checkout")
    expect(urls[:payment_mobile]).to match("cmd=_express-checkout-mobile")
    expect(urls[:payment_popup]).to match("incontext")
  end

  it "should return correct funding error recover redirect url" do
    return_url = "http://example.com"
    payments = {
      payment_web: described_class.create_temporary_payment(item_to_sell, user),
      payment_mobile: described_class.create_temporary_payment(item_to_sell, user, mobile: true),
      payment_popup: described_class.create_temporary_payment(item_to_sell, user, popup: true)
    }
    urls = {}
    payments.each do |type, payment|
      url = payment.funding_error_recover_redirect_url paypal_token
      expect(url).to match("paypal.com")
      expect(url).to match("token=#{paypal_token}")
      urls[type] = url
    end
    expect(urls[:payment_web]).to match("cmd=_express-checkout")
    expect(urls[:payment_mobile]).to match("cmd=_express-checkout-mobile")
    expect(urls[:payment_popup]).to match("incontext")
  end

  describe "#do_purchase" do

    it "should return payment instance if everything is correct" do
      described_class.send(:paypal_gateway, item_to_sell.class.digital_goods_use_payment_account).stub(:purchase) do
        double "paypal_success_response", success?: true, params: {"transaction_id" => "TEST_TRANSACTION"}
      end
      payment = described_class.create_temporary_payment item_to_sell, user
      Track.stub(:digital_goods_find_by_id) { item_to_sell }

      expect do
        payment.do_purchase paypal_token, paypal_payer_id
      end.not_to raise_error

      expect(payment).to be_persisted
    end

    it "should raise on error conditions" do
      payment = described_class.create_temporary_payment item_to_sell, user
      Track.stub(:digital_goods_find_by_id) { item_to_sell }

      described_class.send(:paypal_gateway, item_to_sell.class.digital_goods_use_payment_account).stub(:purchase) do
        double "paypal_success_response_2", success?: true, params: {"payment_status" => "Completed", "transaction_id" => nil}
      end
      expect do
        payment.do_purchase paypal_token, paypal_payer_id
      end.to raise_error ::Persistence::PaymentOrder::InternalPaymentError

      described_class.send(:paypal_gateway, item_to_sell.class.digital_goods_use_payment_account).stub(:purchase) do
        double "paypal_error_response", success?: false, params: {}
      end
      expect do
        payment.do_purchase paypal_token, paypal_payer_id
      end.to raise_error ::Persistence::PaymentOrder::PaymentProviderError

      described_class.send(:paypal_gateway, item_to_sell.class.digital_goods_use_payment_account).stub(:purchase) do
        double "paypal_error_response_2", success?: false, params: {'error_code' => 10486}
      end
      expect do
        payment.do_purchase paypal_token, paypal_payer_id
      end.to raise_error ::Persistence::PaymentOrder::PaymentProviderFundingFailureError

    end

  end

  it "may be successful or not, depending on status" do
    successful_statuses = [:canceled_reversal, :completed, :processed]
    unsuccessful_statuses = [:created, :denied, :expired, :failed, :pending, :refunded, :reversed, :voided]
    successful_statuses.each do |status|
      order = described_class.new status: status
      expect(order).to be_successful
    end
    unsuccessful_statuses.each do |status|
      order = described_class.new status: status
      expect(order).not_to be_successful
    end
  end

  it "may be temporary, depending on status" do
    order = described_class.new status: described_class::STATUS_TEMPORARY
    expect(order).to be_temporary
    described_class::STATUSES.keys.each do |status|
      order = described_class.new status: status
      expect(order).not_to be_temporary
    end
  end

end
