require "spec_helper"

describe Persistence::Subscription do

  let(:paypal_token) { "123TEST_TOKEN123" }

  describe "class" do
    describe "#load_from_params" do
      it "should return Mongoid criteria for lazy load" do
        result = described_class.load_from_params
        expect(result).to be_instance_of(Mongoid::Criteria)
      end
      it "should accept user_uri" do
        test_user_uri = "test:user:123"
        result = described_class.load_from_params user_uri: test_user_uri
        expect(result.selector["user_uri"]).to eq(test_user_uri)
      end
      it "should accept type" do
        test_type = "test"
        result = described_class.load_from_params type: test_type
        expect(result.selector["type"]).to eq(test_type)
      end
      it "should accept status" do
        test_type_included = "test_include"
        test_type_excluded = "test_exclude"
        described_class.any_instance.stub :refresh_status
        result = described_class.load_from_params status: {test_type_included => true, test_type_excluded => false}
        expect(result.selector["status"]).to eq({"$in" => [test_type_included], "$nin" => [test_type_excluded]})
      end
      it "should accept limits" do
        limit = 2
        result = described_class.load_from_params limit: limit
        expect(result.entries.length).to be <= limit
      end
      it "should accept sorting" do
        desc = described_class.load_from_params
        asc = described_class.load_from_params ascending: true
        expect(desc.first).to eq(asc.last)
        expect(desc.last).to eq(asc.first)
      end
    end

    describe "#get_latest_of_type_for_user" do
      it "should return latest subscription" do
        external_payment_id = "external_id"
        type = "test"
        s1 = described_class.create({
          user_uri: $user.uri,
          payment_prov_id: external_payment_id,
          repeat_period: described_class::REPEAT_PERIODS.keys.first,
          repeat_frequency: 1,
          type: type
        })
        s2 = described_class.create({
          user_uri: $user.uri,
          payment_prov_id: external_payment_id,
          repeat_period: described_class::REPEAT_PERIODS.keys.first,
          repeat_frequency: 1,
          type: type
        })
        result = described_class.get_latest_of_type_for_user type, $user.uri
        expect(result).to eq(s2)
      end
      it "should return subscription of correct type" do
        external_payment_id = "external_id"
        type1 = "test1"
        type2 = "test2"
        s1 = described_class.create({
          user_uri: $user.uri,
          payment_prov_id: external_payment_id,
          repeat_period: described_class::REPEAT_PERIODS.keys.first,
          repeat_frequency: 1,
          type: type1
        })
        s2 = described_class.create({
          user_uri: $user.uri,
          payment_prov_id: external_payment_id,
          repeat_period: described_class::REPEAT_PERIODS.keys.first,
          repeat_frequency: 1,
          type: type2
        })
        result = described_class.get_latest_of_type_for_user type1, $user.uri
        expect(result).to eq(s1)
      end
      it "should return subscription of correct user" do
        external_payment_id = "external_id"
        type = "test"
        s1 = described_class.create({
          user_uri: $user.uri,
          payment_prov_id: external_payment_id,
          repeat_period: described_class::REPEAT_PERIODS.keys.first,
          repeat_frequency: 1,
          type: type
        })
        s2 = described_class.create({
          user_uri: FactoryGirl.create(:persistence_user).uri,
          payment_prov_id: external_payment_id,
          repeat_period: described_class::REPEAT_PERIODS.keys.first,
          repeat_frequency: 1,
          type: type
        })
        result = described_class.get_latest_of_type_for_user type, $user.uri
        expect(result).to eq(s1)
      end
    end

    describe "#auth_redirect_url" do
      it "should return correct redirect url" do
        return_url = "http://example.com"
        template = SubscriptionTemplate.all.first
        web_url = described_class.auth_redirect_url template, return_url, return_url
        mobile_url = described_class.auth_redirect_url template, return_url, return_url, true
        [web_url, mobile_url].each do |url|
          expect(url).to match("paypal.com")
          expect(url).to match("token=")
        end
        expect(web_url).to match("cmd=_express-checkout")
        expect(mobile_url).to match("cmd=_express-checkout-mobile")
      end
    end

    describe "#setup_profile" do
      it "should send request to PayPal" do
        template = SubscriptionTemplate.all.first
        expect(described_class.send(:paypal_gateway)).to receive(:recurring) do
          double("result").as_null_object
        end
        described_class.setup_profile template, paypal_token
      end
    end
  end

  it "should return correct next payment date" do
    date1 = Time.now
    date2 = date1 + 1.month
    date3 = date2 + 1.month
    s1 = described_class.new({
      repeat_period: "month",
      repeat_frequency: 1
    })
    s2 = described_class.new({
      repeat_period: "month",
      repeat_frequency: 1
    })
    s1.created_at = date1
    s2.created_at = date1
    expect(s1.next_payment_date.to_datetime.to_time_in_current_zone).to eq(date2.to_datetime.to_time_in_current_zone)
    expect(s2.next_payment_date.to_datetime.to_time_in_current_zone).to eq(date3.to_datetime.to_time_in_current_zone)
  end

  it "may be usable or not" do
    subscription = described_class.new
    subscription.status = "active"
    expect(subscription).to be_usable

    subscription = described_class.new repeat_period: "month", repeat_frequency: 1
    subscription.status = "cancelled"
    expect(subscription).to be_usable

    subscription = described_class.new repeat_period: "month", repeat_frequency: 1
    subscription.status = "compatibility"
    subscription.created_at = Time.now
    expect(subscription).to be_usable

    subscription = described_class.new repeat_period: "day", repeat_frequency: 1
    subscription.status = "cancelled"
    expect(subscription).not_to be_usable

    subscription = described_class.new repeat_period: "day", repeat_frequency: 1
    subscription.status = "compatibility"
    subscription.created_at = Time.now - 1.month
    expect(subscription).not_to be_usable

    ["suspended", "pending"].each do |status|
      subscription = described_class.new
      subscription.status = status
      expect(subscription).not_to be_usable
    end
  end

  describe "#cancel", focus: true do
    it "should succeed for compatibility subscriptions" do
      pending "Work in progress"
      subscription = described_class.new
      subscription.stub :save
    end
  end

end
