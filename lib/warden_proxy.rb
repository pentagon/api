class WardenProxy
  delegate :cookies, :env, :to => :@warden

  def initialize(warden, options)
    @warden = warden
    @options = options
  end

  def request
    @request ||= ActionDispatch::Request.new(env)
  end

  def authenticate
    @warden.authenticate(@options)
  end

  def method_missing(method, resource, *args)
    resource ||= authenticate
    resource.send(method, *args) if resource.respond_to?(method)
  end
end
