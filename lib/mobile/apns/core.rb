module Mobile
  module APNS
    require 'socket'
    require 'openssl'
    require 'json'

    @host = 'gateway.sandbox.push.apple.com'
    @port = 2195
    # openssl pkcs12 -in mycert.p12 -out client-cert.pem -nodes -clcerts
    @pem = nil # this should be the path of the pem file not the contentes
    @pass = nil

    class << self
      attr_accessor :host, :pem, :port, :pass
    end

    def self.send_notification device_token, message
      n = Mobile::APNS::Notification.new device_token, message
      self.send_notifications [n]
    end

    def self.send_notifications notifications
      with_connection(NotificationConnection) {|conn| conn.send_notifications packed_nofications notifications}
    end

    def self.feedback
      with_connection(FeedbackConnection) {|conn| conn.feedback}
    end

    def self.packed_nofications(notifications)
      bytes = ''
      notifications.each do |notification|
        # Each notification frame consists of
        # 1. (e.g. protocol version) 2 (unsigned char [1 byte])
        # 2. size of the full frame (unsigend int [4 byte], big endian)
        pn = notification.packaged_notification
        bytes << ([2, pn.bytesize].pack('CN') + pn)
      end
      bytes
    end

    protected

    def self.with_connection(type)
      raise "The path to your pem file is not set. (APNS.pem = /path/to/cert.pem)" unless self.pem
      connection = type.new host: self.host, port: self.port, pem: self.pem, pass: self.pass
      yield connection
    ensure
      connection.close if connection
    end

    def self.feedback_host
      self.host.gsub 'gateway','feedback'
    end
  end
end
