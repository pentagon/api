# require 'queue'

module Rack
  module Mongoid
    module Middleware
      class Sessions

        def initialize(app)
          @app = app
          @sessions = Queue.new
        end

        def call(env)
          session = checkout_session
          ::Mongoid::Threaded.sessions[:default] = session
          @app.call(env)
        ensure
          release_session session
        end

        def checkout_session
          @sessions.pop(true)
        rescue ThreadError
          ::Mongoid::Sessions::Factory.default
        end

        def release_session(session)
          @sessions.push session
        end
      end
    end
  end
end
