require 'quick_magick'
require 'tempfile'

module WaveformBuilder
  extend self

  # build png-waveform from audio-file
  def build file, background_color = 'white', width = 2400, height = 120
    bytes = sox_get_bytes file
    peak = bytes.max
    buckets = fill_buckets bytes, width
    transparent_color = %w(#000 #000000 black).include?(background_color) ? :white : :black

    image = QuickMagick::Image::solid(width, height, background_color)
    image.fill = transparent_color

    # draw waveform graph
    midpoint = height / 2
    scale = peak / midpoint
    (0...buckets.size).each do |i|
      low = buckets[i][0]
      high = buckets[i][1]
      image.draw_line(i, (midpoint + low / scale).round, i, (midpoint + high / scale).round)
    end
    wave_file = Tempfile.new %w(waveform .png), Settings.tmp_path
    begin
      image.save wave_file.path
      image.transparent = transparent_color
      image.save wave_file.path
      wave_file.read
    ensure
      wave_file.close
      wave_file.unlink
    end
  end

  def build_json file, width
    bytes = sox_get_bytes file
    offset = (bytes.length / width.to_f).ceil
    results = bytes.each_slice(offset).map { |a| a.map(&:abs).compact.max }
    peak = results.max
    results.map { |e| e.to_f / peak }
  end

  # fill the buckets
  def fill_buckets bytes, width
    bucket_size = ((bytes.length - 1).to_f / width).to_i + 1
    bytes.in_groups_of(bucket_size).map {|a| [a.compact.min, a.compact.max]}
  end

  # return a byte array with waveform information
  def sox_get_bytes file
    tmp_file = Tempfile.new %w(waveform .dat), Settings.tmp_path
    `sox #{file} -t raw -r 20000 -c 1 -V1 -e signed-integer -L #{tmp_file.path}`
    bytes = tmp_file.read
    raise "sox returned no data" if bytes.size.zero?
    bytes.unpack("s*")
  end
end

