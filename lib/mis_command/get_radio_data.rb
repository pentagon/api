module MisCommand
  class GetRadioData < Base
    def as_json params = {}
      super.merge tube: "#{@data[:station_id]}@#{callback_tube}"
    end
  end
end
