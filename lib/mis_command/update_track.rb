module MisCommand
  class UpdateTrack < TrackStatusCommand
    def as_json params = {}
      super.merge action: 'update'
    end
  end
end
