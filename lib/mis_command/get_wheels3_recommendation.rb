module MisCommand
  class GetWheels3Recommendation < Base
    def as_json params = {}
      super.merge error_callback: "#{callback_protocol}://#{callback_tube}",
        success_callback: "#{callback_protocol}://#{callback_tube}"
    end
  end
end
