module MisCommand
  class GetTrackStatus < TrackStatusCommand
    def as_json params = {}
      super.merge action: 'check'
    end
  end
end
