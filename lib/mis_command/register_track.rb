module MisCommand
  class RegisterTrack < TrackStatusCommand
    def as_json params = {}
      super.merge action: 'add'
    end
  end
end
