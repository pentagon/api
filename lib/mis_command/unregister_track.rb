module MisCommand
  class UnregisterTrack < TrackStatusCommand
    def as_json params = {}
      super.merge action: 'delete'
    end
  end
end
