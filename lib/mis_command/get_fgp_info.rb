module MisCommand
  class GetFgpInfo < Base
    def as_json params = {}
      super.merge response_tube: callback_tube
    end
  end
end
