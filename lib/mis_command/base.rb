module MisCommand
  class Base
    attr_accessor :callback_class, :callback_method, :data, :priority
    delegate :tube, :job_name, :callback_tube, :callback_protocol, to: 'self.class'

    class << self
      attr_accessor :callback_tube, :tube, :job_name, :priority, :callback_protocol, :processing_time_limit

      def configure; yield self end
      def priority; @priority or superclass.priority end
    end

    def initialize callback_class, callback_method, data = {}, priority = nil
      @callback_class = callback_class.is_a?(String) ? callback_class : callback_class.name
      @callback_method = callback_method
      @priority = (priority or self.class.priority).to_i
      @data = data
    end

    def callback_data
      {handler_class: callback_class, method: callback_method, type: self.class.name,
        created_at: DateTime.now.strftime('%Y-%m-%d %H:%M:%S:%L')}
    end

    def as_json params = {}
      {job: job_name}.merge @data
    end
  end
end

# command = MisCommand::GetRecommendation.new self.class.name, :process_results, track_uri: 'xxxxx', track_url: 'http://fdfdf'
# MisService.enqueue command
