class CookieFilter
  def initialize(app)
    @app = app
  end

  def call(env)
    status, headers, body = @app.call(env)
    if headers['Set-Cookie'].respond_to?(:include?) && headers['Set-Cookie'].include?('_session_id')
      headers.delete('Set-Cookie')
    end
    # Rack::Utils.delete_cookie_header!(headers, '_session_id')
    [status, headers, body]
  end
end
