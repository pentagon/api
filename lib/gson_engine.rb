require 'gson'

class GsonEngine
  class << self
    def encoder
      @@encoder ||= Gson::Encoder.new
    end

    def decoder
      @@decoder ||= Gson::Decoder.new
    end

    def encode arg
      encoder.encode arg
    end

    def decode arg
      decoder.decode arg
    end
  end
end
