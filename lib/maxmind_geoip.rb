require 'net/http'
require 'open-uri'
require 'uri'
require 'geoip'

class MaxmindGeoip
  include Singleton
  FIELDS = %w(country_code region_name city latitude longitude error)
  delegate :logger, :url, :geoip_file, :license_key, :cache_time, to: 'self.class'

  class << self
    attr_accessor :logger, :url, :geoip_file, :license_key, :cache_time
    delegate :get_geolocation_from_ip, to: :instance
    def configure; yield self end
  end

  def initialize
    @geoip = GeoIP.new geoip_file
  end

  def name; self.class.name end

  def cache_key_for_ip ip
    "geoip_ip_#{ip}"
  end

  def load_from_cache ip
    cache_key = cache_key_for_ip ip
    res = RedisDb.client.get cache_key
    logger.debug "#{name} :: load_from_cache got data: '#{res}' for IP: '#{ip}' :: cache_key: '#{cache_key}'"
    JSON.parse res if res
  rescue Exception => e
    logger.error "#{name} :: load_from_cache got exception '#{e}'"
    nil
  end

  def store_to_cache ip, res
    cache_key = cache_key_for_ip ip
    logger.debug "#{name} :: store_to_cache storing data: '#{res}' for IP: '#{ip}' :: cache_key: '#{cache_key}'"
    RedisDb.client.setex cache_key, cache_time, res.to_json
  end

  def load_from_file ip
    if (c = @geoip.city ip)
      Hash[FIELDS.zip [c.country_code2, c.region_name, c.city_name, c.latitude, c.longitude]]
    end
  rescue Exception => e
    logger.error "#{name} :: load_from_file got exception '#{e}'"
    nil
  end

  def load_from_service ip
    res = {}
    uri = URI.parse url
    uri.query = URI.encode_www_form l: license_key, i: ip
    response = Net::HTTP.get_response uri
    if response.is_a? Net::HTTPSuccess
      res = Hash[FIELDS.zip response.body.split ',']
      if res['error']
        logger.error "#{name} :: MaxMind returned an error code for the request: #{res[:error]}"
      else
        logger.info "#{name} :: MaxMind data for #{ip} :: #{res}"
        res['latitude'] = res['latitude'].to_f
        res['longitude'] = res['longitude'].to_f
      end
    else
      logger.error "#{name} :: Request to Maxmid failed with '#{response.code}'"
    end
    res.blank? ? nil : res
  rescue Exception => e
    logger.error "#{name} :: load_from_service got exception '#{e}'"
    nil
  end

  def get_geolocation_from_ip ip
    return {} if ip.blank? or ip.start_with?('127.')
    res = (load_from_cache(ip) or load_from_service(ip) or load_from_file(ip)).with_indifferent_access
    unless res.blank?
      res.select! {|_, v| v}
      store_to_cache ip, res
    end
    logger.info "#{name} :: responding with '#{res}'"
    res
  end
end
