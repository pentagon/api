
#########################################################################################################
#                          Restrictions for creating radio playlist:                                    #
#                                                                                                       #
# it shall not Webcast (or authorise the Webcast) in any 3-hour period:                                 #
#                                                                                                       #
# 1)  more than 3 different Tracks from a particular album, including no more than 2 consecutively, or  #
#                                                                                                       #
# 2)  more than 4 different Tracks by a particular artist or from any compilation of Tracks, including  #
#     no more than 3 consecutively;                                                                     #
#                                                                                                       #
# 3)  the same Track repeated during any 1-hour period                                                  #
#                                                                                                       #
#########################################################################################################

module RadioMixer

  RESTRICTIONS = [:three_tracks_from_album_and_two_consecutively, :four_tracks_from_artist_and_three_consecutively, :same_track_during_one_hour]

  def self.create_playlist_from_tracks tracks_to_use, previous_played_tracks
    tracks = tracks_to_use.shuffle
    playlist, temp_queue, size, i = [], [], tracks_to_use.size, 0
    previous_played_tracks_local = previous_played_tracks.dup

    while tracks.size > 0
      track = tracks.shift
      if restrictions_passed?(previous_played_tracks_local, track)
        playlist << track.id
        previous_played_tracks_local << track
      else
        temp_queue.unshift track
      end
      temp_queue.each do |track|
        if restrictions_passed?(previous_played_tracks, track)
          playlist << track.id
          previous_played_tracks_local << track
          temp_queue.delete track
        end
      end
    end
    playlist
  end

  def self.create_playlist_from_seeds seed_tracks, size, country, user, previous_played_tracks = []
    return [] if seed_tracks.empty?
    playlist, per_page, energy_range = [], 50, 20
    energy = seed_tracks.first.energy || rand(180)
    options = {
      genre: seed_tracks.first.genre,
      energy_from: energy - energy_range,
      energy_to: energy + energy_range
    }
    previous_played_tracks_local = previous_played_tracks.dup

    search_interaction = Radio::TrackSearchingInteraction.new(search_params: options, country: country, user: user)
    total_count = search_interaction.results.total
    page = rand((total_count / per_page).floor)
    page = page / 10 while page > 100 # Avoid deep paging for better performance.
    tracks = Radio::TrackSearchingInteraction.new(search_params: options, page: page, country: country, user: user).results
    tracks.to_a.shuffle.each do |track|
      if restrictions_passed?(previous_played_tracks_local, track)
        playlist << track.id
        previous_played_tracks_local << track
      end
      break if playlist.size == size
    end
    playlist
  end

  def self.restrictions_passed? previous_played_tracks, track
    RESTRICTIONS.all? do |restriction|
      !!send(restriction, previous_played_tracks, track)
    end
  end

  private

  def self.three_tracks_from_album_and_two_consecutively previous_played_tracks, track
    summary = previous_played_tracks.inject({}) do |memo, track|
      memo[track.album_uri] ||= 0
      memo[track.album_uri] += 1
      memo
    end
    last_two_tracks = previous_played_tracks[-2, 2] || []
    return false if summary[track.album_uri].present? && (summary[track.album_uri] > 3 || last_two_tracks.map(&:album_uri).uniq == [track.album_uri])
    true
  end

  def self.four_tracks_from_artist_and_three_consecutively previous_played_tracks, track
    summary = previous_played_tracks.inject({}) do |memo, track|
      memo[track.artist_uri] ||= 0
      memo[track.artist_uri] += 1
      memo
    end
    last_three_tracks = previous_played_tracks[-3, 3] || []
    return false if summary[track.artist_uri].present? && (summary[track.artist_uri] > 4 || last_three_tracks.map(&:artist_uri).uniq == [track.artist_uri])
    true
  end

  def self.same_track_during_one_hour previous_played_tracks, track
    one_hour_tracks, time = [], 0
    previous_played_tracks.each do |track|
      time += track.duration.to_i
      one_hour_tracks << track
      break if time > 4200 # not 3600, because we want to be sure about 5 skips
    end
    not one_hour_tracks.map(&:id).include? track.id
  end

end
