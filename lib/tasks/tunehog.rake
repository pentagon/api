require 'rake_logger'

# $logger ||= ActiveSupport::TaggedLogging.new(RakeLogger.new)

def log_debug(message)
  # $logger.debug message
  Rails.logger.debug message
end

def log_info(message)
  # $logger.info message
  Rails.logger.info message
end

def log_error(message)
  # $logger.error message
  Rails.logger.error message
end

def post_send_mails(options)
  payload = {uris: options[:uris]} if options.key?(:uris)
  payload = {emails: options[:emails]} if options.key?(:emails)
  payload[:test] = true if $test_mode
  puts RestClient.post "http://#{Settings.domain}/api/mailing/mails/#{$campaign}",
    payload.to_json,
    content_type: :json,
    accept: :json,
    authorization: "Token token=#{$token}"
end

def print_mailing_params
  puts "test mode: #{$test_mode}"
  puts "campaign: #{$campaign}"
  puts "token: #{$token}"
  puts "per_page: #{$per_page}"
  puts "left: #{$left}"
  puts "right: #{$right}"
  puts "-"*40
end

namespace :tunehog do
  task setup: :environment do
  end

  desc "Assign role from emails seperated by comma"
  task assign_role: :setup do |t, args|
    $stderr.puts "emails environment variable requried" and exit 1 if ENV['emails'].blank?
    emails = Array.wrap(ENV['emails'].chomp.split(','))
    role = ENV['role'].chomp
    $stderr.puts "role environment variable is required" and exit 2 if role.blank?
    $stderr.puts "role is invalid" and exit 3 unless Persistence::User::ROLES.include?(role)
    users = Persistence::User.where(:email.in => emails)
    users.add_to_set(:roles, role)
    log_info "Updated %d documents with role #{role.inspect}" % users.count
  end

  desc "Revoke role from emails seperated by comma"
  task revoke_role: :setup do |t, args|
    $stderr.puts "emails environment variable requried" and exit 1 if ENV['emails'].blank?
    emails = Array.wrap(ENV['emails'].chomp.split(','))
    role = ENV['role'].chomp
    $stderr.puts "role environment variable is required" and exit 2 if role.blank?
    $stderr.puts "role is invalid" and exit 3 unless Persistence::User::ROLES.include?(role)
    users = Persistence::User.where(:email.in => emails)
    users.pull(:roles, role)
    log_info "Updated %d documents with role #{role.inspect}" % users.count
  end

  desc "Migrate roles"
  task migrate_roles: :setup do
    superusers = Persistence::User.where(superuser: true)
    superusers.add_to_set(:roles, 'superuser')
    log_info "Updated %d superusers" % superusers.count
    developers = Persistence::User.where(email: /@(randrmusic|tunehog)\.com/i)
    developers.add_to_set(:roles, 'developer')
    log_info "Updated %d developers" % developers.count
    admins = Persistence::User.where(admin: true)
    admins.add_to_set(:roles, 'admin')
    log_info "Updated %d admins" % admins.count
  end

  desc "Setup slugs"
  task setup_slugs: :setup do
    relations = %w(artist.albums tracks)
    users = Persistence::User.find(['singleton'])
    users.map.with_index do |user,users_index|
      user.slug_builder
      user.save
      puts "[#{users_index+1}/#{users.count}] For user #{user.uri} (user.full_name) created slug - #{user.slug}"
      relations.map do |relation|
        objects = relation.split('.').inject(user) {|o,m| o.send m}
        puts "   - #{relation} (#{objects.count})"
        objects.map.with_index do |object,object_index|
          object.slug_builder
          object.save
          puts "      * [#{object_index+1}/#{objects.count}] Created slug - #{object.slug}"
        end
      end
    end
  end

  namespace :mailing do
    task setup: :environment do
      $per_page = ENV['per_page'].to_i
      $per_page = 1000 if $per_page.zero?
      $left = ENV['left'].to_i
      $right = ENV['right'].to_i
      $token = ENV['token'] || Persistence::User.asu.authentication_token
      $test_mode = ENV['RAILS_ENV'] == 'test' || ENV['test'].present?
    end

    desc "Send mails for creatives"
    task creatives: :setup do
      $campaign = ENV['campaign'] || 'creatives_1'
      print_mailing_params
      (left..right).map do |page|
        scoped = Persistence::User.subscribed
        scoped = scoped.where(:source.in => %w(rrmusic uplaya), encrypted_password: '')
        scoped = scoped.page(page).per(per_page)
        uris = scoped.pluck(:uri)
        puts "page: #{page}, count: #{uris.count}"
        post_send_mails uris: uris
        puts "-"*40
      end
    end

    desc "Send mails for ptp2 members"
    task ptp2_members: :setup do
      $campaign = 'polling'
      print_mailing_params
      Persistence::Ptp2Member.establish_connection_to_recommender
      (left..right).map do |page|
        scoped = Persistence::Ptp2Member.where{email != ''}
        scoped = scoped.page(page).per(per_page)
        emails = scoped.pluck(:email)
        puts "page: #{page}, count: #{emails.count}"
        post_send_mails emails: emails
        puts "-"*40
      end
    end

    desc "Send mails for hitmaker NY2014"
    task hitmaker_ny2014: :setup do
      $campaign = 'hitmaker_ny2014'
      print_mailing_params
      all_uris = Persistence::Activity.by_key('signed_in').distinct(:trackable_uri)
      delivered_uris = Persistence::Activity.by_key('newsletter').where('parameters.event'=>'delivered', 'parameters.campaign'=>$campaign).distinct(:trackable_uri)
      uris = all_uris - delivered_uris
      puts "all count: #{all_uris.count}"
      puts "delivered count: #{delivered_uris.count}"
      puts "count: #{uris.count}"
      post_send_mails uris: uris
      puts "-"*40
    end
  end
end
