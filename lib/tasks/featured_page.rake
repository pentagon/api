namespace :featured do
  desc "Fill in Emerging artists, Trending tracks and Top Radio sections"
  task seed: :environment do
    block = Persistence::MagazineBlock
    block.delete_all
    puts "[Trending tracks]"
    b = block.create title: 'Trending Tracks', type: 'track',
      description: 'Discover great new music from emerging, current and established artists',
      action_link_url: ::Settings.urls.chart.tracks,
      position: 0,
      method_type: 2
    ids = %w(medianet:track:93016231 medianet:track:88440249 medianet:track:90233445 medianet:track:87401343 medianet:track:93197937 medianet:track:78190411 medianet:track:89095985 medianet:track:94360281 medianet:track:82394547 medianet:track:96080897 medianet:track:96080811 medianet:track:94751745 uplaya:track:91429 uplaya:track:95841 fms:track:1391087335664308 sync:track:1384366910670000 uplaya:track:87220 uplaya:track:96344 uplaya:track:41160 fms:track:1369654339539163 fms:track:1365548437779385 uplaya:track:95865 fms:track:1367353730681956 uplaya:track:72299)
    Track.find_for_catalog(ids).map do |track|
      puts "  #{track.title} - #{track.id}"
      b.items.create uri: track.id, cover: track.image_url,
        name: track.title,
        name_url: (File.join(::Settings.musicmanager_url, 'users', track.artist.user_uri, 'music', track.id) if track.artist.user_uri),
        subname: track.artist.title,
        subname_url: (File.join(::Settings.musicmanager_url, 'users', track.artist.user_uri) if track.artist.user_uri),
        url: File.join(::Settings.storage_url, track.id.split(':')) + '.' + 'mp3'
    end

    puts "[Emerging artists]"
    b = block.create title: 'Emerging Artists', type: 'artist',
      description: "Here, you'll find the most popular artists on tunehog",
      action_link_url: ::Settings.urls.chart.artists,
      position: 2
    ids = %w(uplaya:artist:42390 uplaya:artist:37058 uplaya:artist:23692 rrmusic:artist:2642 uplaya:artist:34593 uplaya:artist:33378 uplaya:artist:45229 uplaya:artist:44980 uplaya:artist:19102 uplaya:artist:42796 uplaya:artist:32725 uplaya:artist:37649 uplaya:artist:44485 uplaya:artist:47309 accounts:artist:1367983371788436 accounts:artist:1391890152651935 uplaya:artist:31763 uplaya:artist:37736 accounts:artist:1391354635562443 accounts:artist:1378086955343538 accounts:artist:1389896048019444 uplaya:artist:27469 uplaya:artist:39892 uplaya:artist:47524)
    Persistence::Artist.in(uri: ids).all.map do |artist|
      puts "  #{artist.display_name} - #{artist.user_uri}"
      b.items.create uri: artist.uri, cover: artist.image_url,
        name: artist.display_name,
        name_url: File.join(::Settings.musicmanager_url, 'users', artist.user_uri)
    end

    puts "[Top Radio]"
    b = block.create title: 'Top Radio Seeds', type: 'radio',
      description: 'Some of the most popular seed tracks from all over the world',
      action_link_url: ::Settings.urls.radio.stations.popular,
      position: 1,
      method_type: 2
    ids = %w(medianet:track:81281433 medianet:track:32620123 medianet:track:138977 medianet:track:69886949 medianet:track:40723427 medianet:track:30583959 medianet:track:30235289 medianet:track:2020433 medianet:track:4943445 medianet:track:60882191 medianet:track:74538219 medianet:track:4678923 medianet:track:85386071 medianet:track:10383251 medianet:track:9664463 medianet:track:12936685 medianet:track:8145231 medianet:track:74640177 medianet:track:45961117 medianet:track:11337699 medianet:track:6910555 medianet:track:5021583 medianet:track:6985461 medianet:track:65102569)
    Track.find_for_catalog(ids).map do |track|
      b.items.create uri: track.id, cover: track.image_url,
        name: track.title,
        name_url: (File.join(::Settings.musicmanager_url, 'users', track.artist.user_uri, 'music', track.id) if track.artist.user_uri),
        subname: track.artist.title,
        subname_url: (File.join(::Settings.musicmanager_url, 'users', track.artist.user_uri) if track.artist.user_uri),
        url: File.join(::Settings.storage_url, track.id.split(':')) + '.' + 'mp3'
    end

    puts 'DONE!'
  end
end
