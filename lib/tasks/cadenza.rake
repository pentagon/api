require 'rake'
require 'optparse'

namespace :cadenza do |args|
  desc 'Add credits to user'
  task convertcredits: :environment do
    options = {}
    OptionParser.new(args) do |opts|
      opts.banner = "Usage: rake cadenza:convertcredits [options]"
      opts.on("-u", "--user {username}","User's email address", String) do |user|
        options[:user] = user
      end
      opts.on("-a", "--all","Update all available users") do
        options[:all] = true
      end
    end.parse!
    convert options[:user] if options[:user]
    if options[:all]
      puts "WARNING!!! Process start in 10 sec...."
      sleep 10
      users = Persistence::User.where(:available_reports.gt => 0)
      users.each {|u| convert u.email}
    end
    exit 0
  end

  def convert user
    u = Persistence::User.find_by email: user

    unless u
      puts "User was not found, exiting..."
      return false
    end

    available_reports = u.available_reports

    if available_reports.zero?
      puts "User have not available reports, exiting..."
      return false
    end

    if u.hitlogic_credits_bundles.where(type: 'bundle_convert').exists?
      puts "User #{u.full_name} #{u.email} - has already converted"
    end

    credits = (available_reports * 1.8) / 0.222
    rounded_credits = (credits.to_i/5+1) * 5

    b=Persistence::HitlogicCreditsBundle.digital_goods_find_by_id('bundle_3')
    b.update_attributes({amount: rounded_credits.to_i, user_uri: u.uri, type: "bundle_convert"})

    puts "User #{u.full_name} (#{u.email}): available reports are #{available_reports} were converted to credits in amount #{rounded_credits}"
  end
end
