namespace :elastic do
  namespace :update do
    PER = 100
    INDEX_NAME = 'unsigned'

    def log_progress progress
      puts "[#{DateTime.now.strftime('%Y-%m-%d %H:%M:%S')}] DONE #{progress}%"
    end

    def convert_and_send_data_bulk hashes
      data = convert_to_elastic_bulk_format_string hashes
      tries = 3
      begin
        RestClient.post "#{Settings.elastic_search.url}/_bulk", data, content_type: :json, accept: :json
      rescue e
        tries -= 1
        puts "Got exception: #{e}, sleeping for 1 second..."
        sleep 1
        tries > 0 ? retry : raise(e)
      end
    end

    def push_mapping klass
      type = klass.name.demodulize.underscore
      data = GsonEngine.encode({type => {properties: klass.tire.mapping}})
      RestClient.put "#{Settings.elastic_search.url}/#{INDEX_NAME}/#{type}/_mapping", data, content_type: :json, accept: :json
    end

    def convert_to_elastic_bulk_format_string hashes
      hashes.inject('') do |a, h|
        a << GsonEngine.encode({index: {_index: INDEX_NAME, _type: h[:type], _id: h[:id]}}) << "\n"
        a << GsonEngine.encode(h) << "\n"
      end
    end

    def index_klass klass, criteria = nil
      cr = criteria || klass.all
      count = cr.count / PER
      i = 0
      puts "Putting mapping..."
      push_mapping klass
      puts "...done"
      puts "Staring indexing...."
      cr.in_batches_by PER do |t|
        convert_and_send_data_bulk t.map &:to_indexed_hash
        log_progress (i += 1) * 100 / count
      end
      puts "...done"
    end

    desc 'Update ElasticSearch indexes for tracks'
    task tracks: :environment do
      index_klass Persistence::Track, Persistence::Track.indexable
    end

    desc 'Update ElasticSearch indexes for albums'
    task albums: :environment do
      index_klass Persistence::Album
    end

    desc 'Update ElasticSearch indexes for artists'
    task artists: :environment do
      index_klass Persistence::Artist
    end

    desc 'Update ElasticSearch indexes for users'
    task users: :environment do
      index_klass Persistence::User
    end
  end
end
