# encoding: utf-8
namespace :migration do
  desc "Migrate first pack of available charts"
  task hitlogic_first_charts: :environment do
    charts = [
      {title: "Canada's Top 100 Pop Songs", name: 'ca', priority: 1, description: "Curious about how your track stacks up against some of the biggest songs in the Canadian Hot 100? We've analysed every Canadian Hot 100 song from 2007 to 2014 so we can identify the ones with points of sonic similarity to yours. This enables us to create a rating out of 10 based on the level of shared sonic properties, and a five track list of the most sonically similar chart songs.", tooltip: "<p>We have analysed every pop song from the Canadian Top 100, between 2007 and 2014 - and identified tracks with points of sonic similarity to your track. This enables us to create a rating out of 10 based on how many chart tracks are sonically similar, and the extent to which your track has shared sonic properties with them. The rating also takes into account the highest chart position these hit tracks achieved.</p><p>The track list shows the five most sonically similar hit tracks, and their similarity level (expressed as a percentage), to your track. Any percentage over 70% suggests there are significant sonic similarities.</p>", year: "2007-2014", sub_description: "Find out if your music has what it takes to climb the Canadian Hot 100."},
      {title: "UK Top 40 Pop Songs", name: 'uk', priority: 10, description: "Curious to see how your track stacks up against some of the biggest songs from the the UK Top 40 Pop charts? We've analysed every song from the UK Singles chart all the way from 1960 right up to 2014 and can identify the hit tracks with points of sonic similarity to your track. This enables us to create a rating out of 10 based on the level of shared sonic properties, and a five track list of the most sonically similar chart songs.", tooltip: "<p>We have analysed every song from the UK Top 40 Pop charts, from 1960 to 2014, and identified tracks with points of sonic similarity to your track. This enables us to create a rating out of 10 based on how many chart tracks are sonically similar, and the extent to which your track has shared sonic properties with them. The rating also takes into account the highest chart position these hit tracks achieved.</p><p>The track list shows the five most sonically similar hit tracks, and their similarity level (expressed as a percentage), to your track. Any percentage over 70% suggests there are significant sonic similarities.</p>", year: "1960-2014", sub_description: "Find out if your music has what it takes to climb the UK Top 40."},
      {title: "US Top 100 Pop Songs", name: 'us', priority: 20, description: "Curious about how your track stacks up against some of the biggest songs in the US Pop Charts? We have analysed every song from the US Pop charts all the way from 1958 to 2014 so we can identify the hit tracks with points of sonic similarity to your track. This enables us to create a rating out of 10 based on the level of shared sonic properties, and a five track list of the most sonically similar pop chart songs.", tooltip: "<p>We have analysed every song from the US Top 100 Pop chart from 1988 to 2014 and identified tracks with points of sonic similarity to yours. This enables us to create a rating out of 10 based on how many chart tracks are sonically similar, and the extent to which your track has shared sonic properties with them. The rating also takes into account the highest chart position these hit tracks achieved.</p><p>The track list shows the five most sonically similar hit tracks, and their similarity level (expressed as a percentage), to your track. Any percentage over 70% suggests there are significant sonic similarities.</p>", year: "1958-2014", sub_description: 'Find out if your music has what it takes to climb the Hot 100.'},
      {title: "US Rock Songs", name: 'us_rock_combined', priority: 30, description: "Curious about how your track stacks up against some of the biggest songs in the US Rock charts? We've analysed every song from the US Rock charts from 2009 to 2014 and can identify the hit rock tracks with points of sonic similarity to your track. This enables us to create a rating out of 10 based on the level of shared sonic properties, and a five track list of the most sonically similar chart songs.", tooltip: "<p>We have analysed every song from theUS Rock chart, between 2009 and 2014, and identified tracks with points of sonic similarity to your track. This enables us to create a rating out of 10 based on how many chart tracks are sonically similar, and the extent to which your track has shared sonic properties with them. The rating also takes into account the highest chart position these hit tracks achieved.</p><p>The track list shows the five most sonically similar hit tracks, and their similarity level (expressed as a percentage), to your track. Any percentage over 70% suggests there are significant sonic similarities.</p>", year: "2009-2014", sub_description: "Let Cadenza's sonic analysis rock your world."},
      {title: "US Dance Songs", name: 'us_dance', priority: 40 , description: "Curious about how your track stacks up against some of the biggest songs in the US Dance charts? We've analysed every song from the US Dance chart from 1985 to 2014 and can identify the tracks with points of sonic similarity to yours. This enables us to create a rating out of 10 based on the level of shared sonic properties, and a five track list of the most sonically similar tracks.", tooltip: "<p>We have analysed every song from the US Dance charts, between 1985 and 2014, and identified tracks with points of sonic similarity to your track. This enables us to create a rating out of 10 based on how many chart tracks are sonically similar, and the extent to which your track has shared sonic properties with them. The rating also takes into account the highest chart position these hit tracks achieved.</p><p>The track list shows the five most sonically similar hit tracks, and their similarity level (expressed as a percentage), to your track. Any percentage over 70% suggests there are significant sonic similarities.</p>", year: "1985-2014", sub_description: "Feel the beat of your track compared to the top dance tunes of the day."},
      {title: "US R&B/Hip-Hop Songs", name: 'us_hip_hop', priority: 50 , description: "Curious about how your track stacks up against some of the biggest songs in the US Hip Hop charts? We have analysed every song from the US Hip Hop chart from as far back as 1985 to 2014 and can identify the hit tracks with points of sonic similarity to your track. This enables us to create a rating out of 10 based on the level of shared sonic properties, and a five track list of the most sonically similar chart songs.", tooltip: "<p>We've analysed every pop song from the US R&B/Hip Hop charts from 1985 to 2014 and identified tracks with points of sonic similarity to yours. This enables us to create a rating out of 10 based on how many chart tracks are sonically similar, and the extent to which your track has shared sonic properties with them. The rating also takes into account the highest chart position these hit tracks achieved.</p><p>The track list shows the five most sonically similar hit tracks, and their similarity level (expressed as a percentage), to your track. Any percentage over 70% suggests there are significant sonic similarities.</p>", year: "1985-2014", sub_description: "Hip hop and you don't stop (until you get the answers you need)"},
      {title: "US Rap Songs", name: 'us_rap_combined', priority: 60 , description: "Curious about how your track stacks up against some of the biggest songs in the US Rap charts? We've analysed every song from the US Rap chart from 1989 to 2014 and can identify the tracks with points of sonic similarity to yours. This enables us to create a rating out of 10 based on the level of shared sonic properties, and a five track list of the most sonically similar songs.", tooltip: "<p>We have analysed every song from the US Rap chart, between 1989 and 2014, and identified tracks with points of sonic similarity to your track. This enables us to create a rating out of 10 based on how many chart tracks are sonically similar, and the extent to which your track has shared sonic properties with them. The rating also takes into account the highest chart position these hit tracks achieved.</p><p>The track list shows the five most sonically similar hit tracks, and their similarity level (expressed as a percentage), to your track. Any percentage over 70% suggests there are significant sonic similarities.</p>", year: "1989-2014", sub_description: "Find out if you're a rap genius with our data."},
      {title: "US Latin Songs", name: 'us_latin', priority: 70 , description: "Curious about how your track stacks up against some of the biggest songs in the US Latin Charts? We've analysed every song from the US Latin chart from 1986 to 2014 and can identify those tracks with points of sonic similarity to yours. This enables us to create a rating out of 10 based on the level of shared sonic properties, and a five track list of the most sonically similar chart songs.", tooltip: "<p>We have analysed every song from the US Latin charts between 1986 and 2014 and identified tracks with points of sonic similarity to yours. This enables us to create a rating out of 10 based on how many chart tracks are sonically similar, and the extent to which your track has shared sonic properties with them. The rating also takes into account the highest chart position these hit tracks achieved.</p><p>The track list shows the five most sonically similar hit tracks, and their similarity level (expressed as a percentage), to your track. Any percentage over 70% suggests there are significant sonic similarities.</p>", year: "1986-2014", sub_description: 'Compare your music to popular Spanish-language songs. ¡Bailamos!'},
      {title: "US Country Songs", name: 'us_country' , priority: 80, description: "Curious about how your track stacks up against some of the biggest songs in the US Country charts? We've analysed every song from the US Country chart from way back when in 1985 up to 2013 so we can identify those songs with points of sonic similarity to yours. This enables us to create a rating out of 10 based on the level of shared sonic properties, and a five track list of the most sonically similar US Country chart songs.", tooltip: "<p>We have analysed every song from the US Country charts, between 1985 and 2013, and identified tracks with points of sonic similarity to your track. This enables us to create a rating out of 10 based on how many chart tracks are sonically similar, and the extent to which your track has shared sonic properties with them. The rating also takes into account the highest chart position these hit tracks achieved.</p><p>The track list shows the five most sonically similar hit tracks, and their similarity level (expressed as a percentage), to your track. Any percentage over 70% suggests there are significant sonic similarities.</p>", year: "1985-2013", sub_description: "Cadenza knows if you're a lil bit country."},
      {title: "US Adult Pop Songs", name: 'us_adult_pop', priority: 90 , description: "Curious about how your track stacks up against some of the biggest songs in the US Adult Pop Songs? We have analysed every US adult pop song between 1995 and 2014 and can identify the hit tracks with points of sonic similarity to your track. This enables us to create a rating out of 10 based on the level of shared sonic properties, and a five track list of the most sonically similar chart songs.", tooltip: "<p>We have analysed every pop song from the US Adult Pop charts, between 1995 and 2014, and identified tracks with points of sonic similarity to yours. This enables us to create a rating out of 10 based on how many chart tracks are sonically similar, and the extent to which your track has shared sonic properties with them. The rating also takes into account the highest chart position these hit tracks achieved.</p><p>The track list shows the five most sonically similar hit tracks, and their similarity level (expressed as a percentage), to your track. Any percentage over 70% suggests there are significant sonic similarities.</p>", year: "1995-2014", sub_description: "How're you gonna get to the toppermost of the US Adult poppermost?"},
      {title: "US Alternative Songs", name: 'us_alternative', priority: 100 , description: "Curious about how your track stacks up against some of the biggest songs in the US Alternative charts? We've analysed every song from the US Alternative chart from 1988 to 2014 and can identify the tracks with points of sonic similarity to yours. This enables us to create a rating out of 10 based on the level of shared sonic properties, and a five track list of the most sonically similar songs.", tooltip: "<p>We have analysed every song from the US Alternative charts, between 2005 and 2014, and identified tracks with points of sonic similarity to your track. This enables us to create a rating out of 10 based on how many chart tracks are sonically similar, and the extent to which your track has shared sonic properties with them. The rating also takes into account the highest chart position these hit tracks achieved.</p><p>The track list shows the five most sonically similar hit tracks, and their similarity level (expressed as a percentage), to your track. Any percentage over 70% suggests there are significant sonic similarities.</p>", year: "1988-2014", sub_description: "Get the alternative lowdown you need for your music."},
      {title: "US Digital Download/Stream Chart", name: 'us_digital', priority: 110 , description: "Curious about how your track stacks up against some of the biggest songs in the US Digital Charts? We've analysed every song from the US Digital chart from 2005 to 2014 and can identify the tracks with points of sonic similarity to yours. This enables us to create a rating out of 10 based on the level of shared sonic properties, and a five track list of the most sonically similar digital songs.", tooltip: "<p>We have analysed tracks from the US Digital Download/Streaming chart, between 2005 and 2014 and identified tracks with points of sonic similarity to yours. This enables us to create a rating out of 10 based on how many tracks are sonically similar, and the extent to which your track has shared sonic properties with them. The rating also takes into account the highest chart position these tracks achieved.</p><p>The track list shows the five most sonically similar hit tracks, and their similarity level (expressed as a percentage), to your track. Any percentage over 70% suggests there are significant sonic similarities.</p>", year: "2005-2014", sub_description: 'Download the insight you need to be a digital dream.'}
      # {title: "UK Indie Singles & Albums", name: 'uk_independent_combined', priority: , description: "",  },
    ]

    charts.map do |a|
      catalog = Persistence::Hitlogic::Catalog.find_or_initialize_by name: a[:name]
      if catalog.persisted?
        catalog.update_attributes(a)
      else
        catalog.assign_attributes(a)
        catalog.save
      end
    end
    prototypes = [
      {
        :value_attribute=>"similar_hits",
        :module_type=>"similarity",
        :title=>"Chart analysis",
        :description=>"Curious about how your tracks stack up against some of the biggest songs in the <chart_Name> charts of the last 50 years? We can trawl through an entire universe of songs to show you your songs' sonic similarities.",
        :sub_description=>"",
        :category=>"",
        :is_prototype=>true,
        :is_multiple_charts=>true,
        :cost_of_credit=>1,
        :priority=>1
      },
      {
        :value_attribute=>"color",
        :module_type=>"sonic_mood",
        :title=>"Mood level",
        :description=>"There is a scientific correlation between the sonic mark-up of a track and the type of mood it evokes. Based on extensive research, Cadenza identifies the moods that your track might evoke in a listener. You can use this data to order, prioritise and categorise your tracks to fit your listener's mood.",
        :sub_description=>"See the moods your song evokes, the way it makes the listener feel.",
        :category=>"",
        :is_prototype=>true,
        :is_multiple_charts=>false,
        :cost_of_credit=>1,
        :priority=>500
      },
      {
        :value_attribute=>"genre_cloud",
        :module_type=>"genre_competition",
        :title=>"Genre matcher",
        :description=>"We collate and analyse genre data from tracks that are sonically similar to your own. You get to view the type of genre tags that have historically attracted listeners to tracks like yours, enabling you to better categorise your music.",
        :sub_description=>"The sonic similarities between your track and other genres can open your eyes to new possibilities.",
        :category=>"",
        :is_prototype=>true,
        :is_multiple_charts=>false,
        :cost_of_credit=>1,
        :priority=>1000
      }
    ]
    prototypes.each do |a|
      m = Persistence::Hitlogic::DataModule.prototypes.find_or_initialize_by module_type: a[:module_type]
      if m.persisted?
        m.update_attributes(a)
      else
        m.assign_attributes(a)
        m.save
      end
    end
  end

  desc "Migrate player"
  task player: :environment do
    playlists = Persistence::UserPlaylist.all.to_a
    playlists.each do |playlist|
      if playlist[:items].count == 0
        playlist[:track_uris].each do |uri|
          playlist.items << {track_id: uri, type: 'track'}
        end
        playlist.save
        p playlist._id
      end
    end
  end

  desc "Migrate user feedbacks"
  task feedbacks: :environment do
    feedbacks = Persistence::UserTrackFeedback.where(:track_duration.exists => false).to_a
    feedbacks.each do |feedback|
      track = case feedback.track_uri
                when /medianet/
                  Track.load_from_params(ids: feedback.track_uri).try(:first)
                else
                  Persistence::Track.find_by(uri: feedback.track_uri)
                end
      puts "track #{feedback.track_uri} absent" if track.blank?
    end
  end

  desc "Update track durations"
  task update_track_durations: :environment do
    require 'timeout'

    def get_duration_for_track_file path
      tries = 3
      begin
        Timeout::timeout(4) {`soxi -D #{path}`}.to_i
      rescue Timeout::Error => e
        if tries > 0
          puts "Timeout! Retrying..."
          tries -= 1
          retry
        else
          raise e
        end
      end
    end

    Persistence::Track.where(duration: 0, is_deleted: false).each_by(100) do |t|
      print "Picking track '#{t.uri}'..."
      if (d = get_duration_for_track_file(t.track_file_path).to_i) > 0
        print " got duration: #{d} ... saving ..."
        t.duration = d.to_i
        t.save
        print 'done!'
      else
        print 'error!'
      end
      puts
    end
  end
end
