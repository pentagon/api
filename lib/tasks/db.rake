namespace :db do
  desc "Destroy all OAuth clients and run db:seed"
  task reset_clients: :environment do
    Persistence::Client.destroy_all
    Rake::Task['db:seed'].invoke
  end
end

namespace :files do
  desc 'Remove Static Folder'
  task :clean_static do
    time = Benchmark.measure do
      FileUtils.remove_dir("public/static", true)
    end
    puts "(II) Remove Static (#{"%.4fs"%time.real})"
  end
end

