require 'redis'
require 'redis-sentinel'
class RedisDb
  include Singleton

  def client
    @client ||= Redis.new
  end

  # :host, :port, :password, :db are options
  def connect options = {}
    @client = Redis.new options
  end

  def self.method_missing method, *args
    instance.send method, *args
  end
end
