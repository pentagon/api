module Utils
  module CarrierWave
    module Crop
      extend ActiveSupport::Concern
      def self.included(base)
        base.class_eval do
          attr_accessor :crop_x, :crop_y, :crop_w, :crop_h
          attr_accessible :crop_x, :crop_y, :crop_w, :crop_h
        end
      end
    end
  end
end
