require 'mime/types'
require 'mini_magick'

module Utils
  module CarrierWave
    class BaseUploader < ::CarrierWave::Uploader::Base
      include ::CarrierWave::MiniMagick

      storage :file

      def store_dir
        path = model.uri.split(':')
        File.join(Settings.storage_path, path[0..-2])
      end

      def cache_dir
        File.join(Settings.storage_path, 'tmp')
      end

      def quality(percentage)
        manipulate! do |img|
          img.quality(percentage.to_s)
          img = yield(img) if block_given?
          img
        end
      end

      def crop
        manipulate! do |img|
          return img unless cropping?
          img.crop "#{model.crop_w}x#{model.crop_h}+#{model.crop_x}+#{model.crop_y}"
          img
        end
      end

      def cropping?
        %w|x y w h|.detect do |v|
          if model.respond_to?(:"crop_#{v}")
            !model.send(:"crop_#{v}").blank?
          end
        end
      end

      def watermark(path_to_file)
        manipulate! do |img|
          logo = ::MiniMagick::Image.open(path_to_file)
          img.composite(logo) { |c| c.gravity "SouthEast" }
        end
      end

      def present?
        file.present?
      end

      def url *args
        path = model.uri.split(':')
        path[-1] = "#{args.first}_#{path.last}" if args.first.respond_to?(:to_sym)
        file_path = File.join(Settings.storage_path, path)
        image_extensions = %w|jpg png jpeg bmp gif|
        (image_extensions + image_extensions.map(&:upcase)).each do |ext|
          file_name = "#{file_path}.#{ext}"
          return File.join(Settings.storage_url, path.first(2), "#{path.last}.#{ext}") if File.exists?(file_name)
        end
        default_url
      end
    end
  end
end
