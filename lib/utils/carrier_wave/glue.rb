module Utils
  module CarrierWave
    module Glue
      extend ActiveSupport::Concern
      module ClassMethods
        def has_attachment(attr_name = :data, uploader = nil, options = {}, &block)
          attr_accessible attr_name
          exts = Array(options.delete(:extensions))
          exts.each do |ext|
            include "Utils::CarrierWave::#{ext.to_s.classify}".constantize
          end if exts.present?
          mount_uploader(attr_name, uploader, options, &block)
          validates_processing_of attr_name
        end

        def valudates_filesize_of(*attr_names)
          validates_with FileSizeValidator, _merge_attributes(attr_names)
        end
      end
    end
  end
end
