class SimpleRange
  attr_accessor :start_date, :end_date

  def initialize(date1, date2)
    @start_date, @end_date = date1, date2
  end

  def days
    (start)..(endd)
  end

  private

  def start
    @start_date ||= start_date
    return @start_date.to_datetime if @start_date.respond_to? :to_datetime
    @start_date
  end

  def endd
    @end_date ||= end_date
    return @end_date.to_datetime if @end_date.respond_to? :to_datetime
    @end_date
  end
end
