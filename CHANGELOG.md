####2013-05-06 - 2013-05-13

* Added start wheels for Discovery project. URL **/api/discovery/start.json** returns tracks sets for *future|present|past* wheels. Track sets for start wheels configure through settings.yml.
* Track files URLs now point to TH Storage service.
* Added support for HitMaker.
* Added **SerilizeDSL** mixin for simple json responses generation. So, having a definition like:
`serialize_for hitmaker: %w(id waveform_base images_base image_urls is_available artist_name)`
in a model results in a new method
`as_json_for_hitmaker` that returns a JSON filled with `id waveform_base images_base image_urls is_available artist_name` values.
* Added **wave_form generation**. To get a wave_form for a track there is /api/tracks/<track_id>/wave URL. So, calling `/api/tracks/rrmusic:track:10008/wave` will results in a binary stream with an image data. I.e. it's possible to add HTML like like: `<img src='http://restapi.tunehog.com/api/tracks/rrmusic:track:10008/wave'>` to get a wave_form embedded into page. Also it's possible to supply additional params `background_color` and `size` to get a desired results. So, URL like `http://restapi.tunehog.com/api/tracks/rrmusic:track:10008/wave?background_color=black&size=1200x120` returns a wafe_form of size 1200x120 painted on black. Currently there are only two colors available: `black` & `white`
