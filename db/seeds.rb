include Term::ANSIColor

def info(message)
  puts green "(II) %s" % message
end

def warning(message)
  puts yellow "(WW) %s" % message
end

def error(message)
  puts red "(EE) %s" % message
end

puts ("="* 20)+"[Begin Load Seeds]"+("="* 20)

ADMIN_EMAIL = "support@tunehog.com"

admin = Persistence::User.admins.first
if admin && admin.email != ADMIN_EMAIL
  admin.update_attribute(:email, ADMIN_EMAIL)
  warning "Admin's emails was changed to #{ADMIN_EMAIL}"
end

admin = Persistence::User.find_or_initialize_by(email: ADMIN_EMAIL)
if admin.persisted?
  warning "Admin already created with email %s" % admin.email
else
  admin.email = ADMIN_EMAIL
  admin.password = 'VHbVZ0lB'
  admin.admin = true
  admin.slug = 'admin'
  if admin.save
    info "Admin with email #{admin.email} has been saved"
  else
    error admin.errors.map{|k, v| [k,v].join(" ")}.join("\n")
  end
end

def define_client(attributes={}, &block)
  admin = Persistence::User.find_by(email: ADMIN_EMAIL)
  client = admin.clients.build(attributes)
  yield(client) if block_given?
  found_client = admin.clients.find_or_initialize_by(identifier: client.identifier)
  if found_client.persisted?
    warning "Client %s already registered" % found_client.name
  else
    if client.save
      info "Client %s registred for %s" % [client.name, admin]
    else
      error client.errors.map{|k, v| [k,v].join(" ")}.join("\n")
    end
  end
end

# FMS

define_client do |c|
  c.name = "Tunehog MM Local"
  c.redirect_uri = "http://musicmanager.tunehog.local/auth/randr/callback"
  c.sign_out_uri = "http://musicmanager.tunehog.local/sign_out"
  c.source_label = "fms"
  c.website = "fms.tunehog.local"
  c.support_email = "oleksandr.u@randrmusic.com"
  c.contact_email = "oleksandr.u@randrmusic.com"
  c.app_description = "Future Music Storage is a BIG company of the future"
  c.identifier = "be627cfbbcbe3bf94b6571900fa4e29d"
  c.secret = "9388ea835b6f76cc896a0eee10b0943d"
end

define_client do |c|
  c.name = "Tunehog MM QA"
  c.redirect_uri = "https://musicmanager.qa.vocvox.com/auth/randr/callback"
  c.sign_out_uri = "https://musicmanager.qa.vocvox.com/sign_out"
  c.source_label = "fms"
  c.website = "musicmanager.qa.vocvox.com"
  c.support_email = "oleksandr.u@randrmusic.com"
  c.contact_email = "oleksandr.u@randrmusic.com"
  c.app_description = "Future Music Storage is a BIG company of the future"
  c.identifier = "a7088841dccc45a0b77d1acd9e36a14d"
  c.secret = "3153a3e9f2d5f272402bf980578e68a2"
end

define_client do |c|
  c.name = "Tunehog Music Manager"
  c.redirect_uri = "https://musicmanager.tunehog.com/auth/randr/callback"
  c.sign_out_uri = "https://musicmanager.tunehog.com/sign_out"
  c.source_label = "fms"
  c.website = "musicmanager.tunehog.com"
  c.support_email = "oleksandr.u@randrmusic.com"
  c.contact_email = "oleksandr.u@randrmusic.com"
  c.app_description = "Future Music Storage is a BIG company of the future"
  c.identifier = "eb9bf36a5b141d74c95e3a9129beac23"
  c.secret = "4618fd5eb036e98515d7f2b5e55d392b"
end

# Discovery

define_client do |c|
  c.name = "Discovery Local"
  c.redirect_uri = "http://discovery/auth/callback"
  c.sign_out_uri = "http://discovery/auth/logout"
  c.source_label = "discovery"
  c.website = "discovery"
  c.support_email = "nikolay.m@randrmusic.com"
  c.contact_email = "nikolay.m@randrmusic.com"
  c.identifier = "2c3024079aea943a9f41c97620a3955b"
  c.secret = "ac274b42fc6345aaf79e1af81b861ffc"
end

define_client do |c|
  c.name = "Discovery QA"
  c.redirect_uri = "https://discovery.qa.vocvox.com/auth/callback"
  c.sign_out_uri = "https://discovery.qa.vocvox.com/auth/logout"
  c.source_label = "discovery"
  c.website = "discovery.qa.vocvox.com"
  c.support_email = "nikolay.m@randrmusic.com"
  c.contact_email = "nikolay.m@randrmusic.com"
  c.identifier = "1e04b7cc1df830b8dc031b74a56afbf0"
  c.secret = "42192340796fb5274c3cf24cb740b416"
end

define_client do |c|
  c.name = "Discovery"
  c.redirect_uri = "https://discovery.tunehog.com/auth/callback"
  c.sign_out_uri = "https://discovery.tunehog.com/auth/logout"
  c.source_label = "discovery"
  c.website = "discovery.tunehog.com"
  c.support_email = "nikolay.m@randrmusic.com"
  c.contact_email = "nikolay.m@randrmusic.com"
  c.identifier = "3f00320e7c0a49e3c2de5cc0e542c266"
  c.secret = "1e3871518283f9e546b393b71f6a5d9e"
end

# Academy

define_client do |c|
  c.name = "Academy Loc"
  c.redirect_uri = "http://academy.loc/auth/callback"
  c.sign_out_uri = "http://academy.loc/auth/logout"
  c.source_label = "academy"
  c.website = "academy.loc"
  c.support_email = "nikolay.m@randrmusic.com"
  c.contact_email = "nikolay.m@randrmusic.com"
  c.identifier = "7a6716d54b4f0e39c5e4e42684b30f35"
  c.secret = "a4274b4108355e8a3842478bd002d6a4"
end

define_client do |c|
  c.name = "Academy Local"
  c.redirect_uri = "http://academy.local/auth/callback"
  c.sign_out_uri = "http://academy.local/auth/logout"
  c.source_label = "academy"
  c.website = "academy.local"
  c.support_email = "nikolay.m@randrmusic.com"
  c.contact_email = "nikolay.m@randrmusic.com"
  c.identifier = "33ec96e55e08a52906dba2672042d6e3"
  c.secret = "94d3fc89a47102aefd0bcfdf0e3fb349"
end

define_client do |c|
  c.name = "Academy QA"
  c.redirect_uri = "https://academy.qa.vocvox.com/auth/callback"
  c.sign_out_uri = "https://academy.qa.vocvox.com/auth/logout"
  c.source_label = "academy"
  c.website = "academy.qa.vocvox.com"
  c.support_email = "nikolay.m@randrmusic.com"
  c.contact_email = "nikolay.m@randrmusic.com"
  c.identifier = "ac2fce2d80ce08caa6d8d0d844c01b31"
  c.secret = "69ee494bc9f99e8a01c631cc0f080f1a"
end

define_client do |c|
  c.name = "Academy"
  c.redirect_uri = "https://academy.tunehog.com/auth/callback"
  c.sign_out_uri = "https://academy.tunehog.com/auth/logout"
  c.source_label = "academy"
  c.website = "academy.tunehog.com"
  c.support_email = "nikolay.m@randrmusic.com"
  c.contact_email = "nikolay.m@randrmusic.com"
  c.identifier = "6091e2b25e992c8fe0e43793e56062fd"
  c.secret = "9ebcedb5daaf35245d349e27ff682ebe"
end

# Syncability

define_client do |c|
  c.name = "Syncability Local"
  c.redirect_uri = "http://syncability.local/user/login"
  c.sign_out_uri = "http://syncability.local/user/logout"
  c.source_label = "sync"
  c.website = "syncability.local"
  c.support_email = "support@syncability.local"
  c.contact_email = "support@syncability.local"
  c.identifier = "fd68009582b7da9333893b13c1ae551c"
  c.secret = "1b83ac6c82c60c4e99ba3d7188d8e84b"
end

define_client do |c|
  c.name = "Syncability QA"
  c.redirect_uri = "https://syncability.qa.vocvox.com/user/login"
  c.sign_out_uri = "https://syncability.qa.vocvox.com/user/logout"
  c.source_label = "sync"
  c.website = "syncability.qa.vocvox.com"
  c.support_email = "support@syncability.local"
  c.contact_email = "support@syncability.local"
  c.identifier = "9a3a02c368345a363f8a3e1c9c80a870"
  c.secret = "d1c79a6991145f023d2b038440ba18b5"
end

define_client do |c|
  c.name = "Syncability QA Local"
  c.redirect_uri = "https://syncability-stage.test.vocvox.com/user/login"
  c.sign_out_uri = "https://syncability-stage.test.vocvox.com/user/logout"
  c.source_label = "sync"
  c.website = "syncability-stage.test.vocvox.com"
  c.support_email = "support@syncability.local"
  c.contact_email = "support@syncability.local"
  c.identifier = "ac065ed43e0992398d0a89d141c82081"
  c.secret = "9f9f00e888413518f1357548f0810c8e"
end

define_client do |c|
  c.name = "Syncability Staging"
  c.redirect_uri = "https://syncability-stage.qa.vocvox.com/user/login"
  c.sign_out_uri = "https://syncability-stage.qa.vocvox.com/user/logout"
  c.source_label = "sync"
  c.website = "syncability-stage.qa.vocvox.com"
  c.support_email = "support@syncability.local"
  c.contact_email = "support@syncability.local"
  c.identifier = "f2bc004e552cbf05dd64257d51b4d61f"
  c.secret = "b9b796e71bc9a6537fda1f09516b3ce2"
end

define_client do |c|
  c.name = "Syncability"
  c.redirect_uri = "https://sync.tunehog.com/user/login"
  c.sign_out_uri = "https://sync.tunehog.com/user/logout"
  c.source_label = "sync"
  c.website = "sync.tunehog.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "76771d4ebabe872d710b71b38a391131"
  c.secret = "845d01f75735f61afb95b449b90904df"
end

# TuneHog blog
define_client do |c|
  c.name = "TuneHog Blog"
  c.redirect_uri = "http://blog.tunehog.com/auth/randr/callback"
  c.sign_out_uri = "http://blog.tunehog.com/sign_out"
  c.source_label = "blog"
  c.website = "blog.tunehog.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "54f92048e5fe58921ff559657953d5c0"
  c.secret = "9002f8b96c420ee6efc11b721de960f3"
end

define_client do |c|
  c.name = "TuneHog Blog QA"
  c.redirect_uri = "http://blog.qa.vocvox.com/auth/randr/callback"
  c.sign_out_uri = "http://blog.qa.vocvox.com/sign_out"
  c.source_label = "blog"
  c.website = "blog.qa.vocvox.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "a45e47de98426aab8d3c4f7416e8720a"
  c.secret = "3f0570491a3b418dc140dac6de500407"
end

define_client do |c|
  c.name = "TuneHog Blog Local"
  c.redirect_uri = "http://blog.tunehog.local/auth/randr/callback"
  c.sign_out_uri = "http://blog.tunehog.local/sign_out"
  c.source_label = "blog"
  c.website = "blog.tunehog.local"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "a45e47deasfasflj4353256hlagdssgasasa"
  c.secret = "ewtye47dea46363acasfagjsgjh780gkdsa"
end

# Radio
define_client do |c|
  c.name = "Radio Local"
  c.redirect_uri = "http://radio.tunehog.local/auth/randr/callback"
  c.sign_out_uri = "http://radio.tunehog.local/sign_out"
  c.source_label = "radio"
  c.website = "radio.tunehog.local"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "1af44dc006954eb7c36f9698a712ba3f"
  c.secret = "d11003e71df261d2e78811ab943810ef"
end

define_client do |c|
  c.name = "Radio QA"
  c.redirect_uri = "https://radio.qa.vocvox.com/auth/randr/callback"
  c.sign_out_uri = "https://radio.qa.vocvox.com/sign_out"
  c.source_label = "radio"
  c.website = "radio.qa.vocvox.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "3ebe10bd4c0c4fd4f84927166df036e2"
  c.secret = "55c270f51f5ad2ddfa2c879e848cd0fb"
end

define_client do |c|
  c.name = "Radio QA2"
  c.redirect_uri = "https://radio2.qa.vocvox.com/auth/randr/callback"
  c.sign_out_uri = "https://radio2.qa.vocvox.com/sign_out"
  c.source_label = "radio"
  c.website = "radio2.qa.vocvox.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "df94030b0c0c725a170422440025f111"
  c.secret = "700a0baf6dd8fd4c8a464c48ca85f0ce"
end

define_client do |c|
  c.name = "Radio"
  c.redirect_uri = "https://radio.tunehog.com/auth/randr/callback"
  c.sign_out_uri = "https://radio.tunehog.com/sign_out"
  c.source_label = "radio"
  c.website = "radio.tunehog.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "1ea548457925db0d90bfa54c0ebf46db"
  c.secret = "a5af06b3e6252616a51624695541fa1e"
end

# Hitmaker

define_client do |c|
  c.name = "Hitmaker Local"
  c.redirect_uri = "http://hitmaker.tunehog.local/auth/randr/callback"
  c.sign_out_uri = "http://hitmaker.tunehog.local/sign_out"
  c.source_label = "hitmaker"
  c.website = "hitmaker.tunehog.local"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "676669838fab40adf89fa5a31cdd1408"
  c.secret = "d9d2e6fbd4dd644d9c1e4970352f6d97"
end

define_client do |c|
  c.name = "Hitmaker Jam"
  c.redirect_uri = "https://hitmaker.jam.vocvox.com/auth/randr/callback"
  c.sign_out_uri = "https://hitmaker.jam.vocvox.com/sign_out"
  c.source_label = "hitmaker"
  c.website = "hitmaker.jam.vocvox.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "e0da7fcace425945e5f5a251c6916f29"
  c.secret = "0db23bc69dcddbab3b88322df03381d2"
end

define_client do |c|
  c.name = "Hitmaker QA"
  c.redirect_uri = "https://hitmaker.qa.vocvox.com/auth/randr/callback"
  c.sign_out_uri = "https://hitmaker.qa.vocvox.com/sign_out"
  c.source_label = "hitmaker"
  c.website = "hitmaker.qa.vocvox.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "af32a443c24e8cc22590f1ceacd997f9"
  c.secret = "5a07e30bfa335d78c9b40de892b535d9"
end

define_client do |c|
  c.name = "Hitmaker"
  c.redirect_uri = "https://hitmaker.tunehog.com/auth/randr/callback"
  c.sign_out_uri = "https://hitmaker.tunehog.com/sign_out"
  c.source_label = "hitmaker"
  c.website = "hitmaker.tunehog.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "67a28ca2ce1e73c03c3870d0d600f05f"
  c.secret = "e8b8f38373c2ad71fbdfaed0dd8fd068"
end

# Gigs Exchange

define_client do |c|
  c.name = "Gigs Exchange"
  c.redirect_uri = "http://gigs.tunehog.local:3001/auth/randr/callback"
  c.sign_out_uri = "http://gigs.tunehog.local:3001/sign_out"
  c.source_label = "gigs"
  c.website = "gigs.tunehog.local:3001"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Gigs Exchange (GEX)"
  c.identifier = "a5d3a9405a4dba633f7100711db6f8dc"
  c.secret = "ae8bb85d9af5fcba4e40e90919d44641"
end

# Recommender

define_client do |c|
  c.name = "Recommender Local"
  c.redirect_uri = "http://recommender.tunehog.local:3100/auth/randr/callback"
  c.sign_out_uri = "http://recommender.tunehog.local:3100/sign_out"
  c.source_label = "recommender"
  c.website = "recommender.tunehog.local:3100"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Recommender"
  c.identifier = "dcd701458c33c75dd022dd09a9c873a3"
  c.secret = "8933fcf46fd2b8f9ebdc3cb6ed3878af"
end

define_client do |c|
  c.name = "Recommender QA"
  c.redirect_uri = "https://recommender.qa.vocvox.com/auth/randr/callback"
  c.sign_out_uri = "https://recommender.qa.vocvox.com/sign_out"
  c.source_label = "recommender"
  c.website = "recommender.qa.vocvox.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Recommender"
  c.identifier = "a0cc89f27919caea7dba7514a855dae9"
  c.secret = "f91ecbf73dd7c027b06da9ef25e0acd6"
end

define_client do |c|
  c.name = "Recommender"
  c.redirect_uri = "https://recommender.tunehog.com/auth/randr/callback"
  c.sign_out_uri = "https://recommender.tunehog.com/sign_out"
  c.source_label = "recommender"
  c.website = "recommender.tunehog.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Recommender"
  c.identifier = "ee4cbadea8ee2fd3496775bca2f41785"
  c.secret = "cdaf5716a79229021a499566c73c175f"
end

# Locate

define_client do |c|
  c.name = "Locate Local"
  c.redirect_uri = "http://locate.tunehog.local/auth/randr/callback"
  c.sign_out_uri = "http://locate.tunehog.local/sign_out"
  c.source_label = "locate"
  c.website = "locate.tunehog.local"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Locate"
  c.identifier = "d65edea2e4e75eb8048f977ba0f63d09"
  c.secret = "cac39481ae27fb41fdd1738eb5543e47"
end

define_client do |c|
  c.name = "Locate QA"
  c.redirect_uri = "https://locate.qa.vocvox.com/auth/randr/callback"
  c.sign_out_uri = "https://locate.qa.vocvox.com/sign_out"
  c.source_label = "locate"
  c.website = "locate.qa.vocvox.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Locate"
  c.identifier = "ae5f6ba0d0c26fb13961ffefcb483fd7"
  c.secret = "bc5e7b682d98ed3f5e35c456488661e9"
end

define_client do |c|
  c.name = "Locate"
  c.redirect_uri = "https://locate.tunehog.com/auth/randr/callback"
  c.sign_out_uri = "https://locate.tunehog.com/sign_out"
  c.source_label = "locate"
  c.website = "locate.tunehog.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Locate"
  c.identifier = "6febf81157bd5b74b86340db9ff59cbc"
  c.secret = "d3b53de73c57b81f5f50292d1abf18e7"
end

# Gifts

define_client do |c|
  c.name = "Gifts Ruby Local"
  c.redirect_uri = "http://gifts.tunehog.local:3001/auth/randr/callback"
  c.sign_out_uri = "http://gifts.tunehog.local:3001/sign_out"
  c.source_label = "gifts"
  c.website = "gifts.tunehog.local:3001"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Gifts Ruby Local"
  c.identifier = "12a2ce006248eb03cc82d32c05720218"
  c.secret = "f1fc776f6be46c99c423f60420e151e8"
end

define_client do |c|
  c.name = "Tunehog Gifts Local 3"
  c.redirect_uri = "http://gifts.loc:3001/application/authentication/callback"
  c.sign_out_uri = "http://gifts.loc:3001/application/authentication/logout"
  c.source_label = "gifts"
  c.website = "gifts.loc:3001"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Gifts"
  c.identifier = "c3cd90e9cef94b50515ad7e6498399e7"
  c.secret = "114f1fc255893ffee32857b33814b9c8"
end

define_client do |c|
  c.name = "Tunehog Gifts [ruby version]"
  c.redirect_uri = "https://gifts.qa.vocvox.com/auth/randr/callback"
  c.sign_out_uri = "https://gifts.qa.vocvox.com/sign_out"
  c.source_label = "gifts"
  c.website = "gifts.qa.vocvox.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Gifts [ruby version]"
  c.identifier = "f2964d69bd469f71049573059a2b57ef"
  c.secret = "410c55957897cd322dea98ea96520833"
end

define_client do |c|
  c.name = "Tunehog Gifts"
  c.redirect_uri = "https://gifts.tunehog.com/auth/randr/callback"
  c.sign_out_uri = "https://gifts.tunehog.com/sign_out"
  c.source_label = "gifts"
  c.website = "gifts.tunehog.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Gifts"
  c.identifier = "5eb588d509c6976e5365446388d139fc"
  c.secret = "d500016f89091c826e073e183c3b508c"
end

# Landingpages
define_client do |c|
  c.name = "Tunehog Discovery Landingpage"
  c.redirect_uri = "http://tunehogdiscovery.com/callback"
  c.sign_out_uri = "http://tunehogdiscovery.com/logout"
  c.source_label = "tunehogdiscovery"
  c.website = "tunehogdiscovery.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Discovery Landingpage"
  c.identifier = "10d18fb5ee637498deb22f1177c6cb1d"
  c.secret = "26548031beba4a8dff87cb1d09cb58a6"
end


define_client do |c|
  c.name = "Tunehog Radio Landingpage"
  c.redirect_uri = "http://tunehogradio.com/callback"
  c.sign_out_uri = "http://tunehogradio.com/logout"
  c.source_label = "tunehogradio"
  c.website = "tunehogradio.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Radio Landingpage"
  c.identifier = "fbd7d2a538b92799a10a36f5b6333176"
  c.secret = "7fec07727eb5935ce038086c75fb15e6"
end

define_client do |c|
  c.name = "Tunehog FMS Landingpage"
  c.redirect_uri = "http://tunehogfms.com/callback"
  c.sign_out_uri = "http://tunehogfms.com/logout"
  c.source_label = "tunehogfms"
  c.website = "tunehogfms.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog FMS Landingpage"
  c.identifier = "5a1c581a61fd3a3361ddff49274f49dc"
  c.secret = "6de33587655f98279b79641737fff72d"
end

define_client do |c|
  c.name = "Tunehog Scout Landingpage"
  c.redirect_uri = "http://tunehogscout.com/callback"
  c.sign_out_uri = "http://tunehogscout.com/logout"
  c.source_label = "tunehogscout"
  c.website = "tunehogscout.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Scout Landingpage"
  c.identifier = "87e707a49ea2c804890e3661fab02c7b"
  c.secret = "29d67da50c1fcd240acc960bc9abc11f"
end

define_client do |c|
  c.name = "Tunehog Hitmaker Landingpage"
  c.redirect_uri = "http://tunehoghitmaker.com/callback"
  c.sign_out_uri = "http://tunehoghitmaker.com/logout"
  c.source_label = "tunehoghitmaker"
  c.website = "tunehoghitmaker.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Hitmaker Landingpage"
  c.identifier = "0c38480099fbc16f449cddc764bbb7f4"
  c.secret = "0d6c9c7e7f32d0a0519ebde1e19e1da0"
end

define_client do |c|
  c.name = "Tunehog Sync Landingpage"
  c.redirect_uri = "http://tunehogsync.com/callback"
  c.sign_out_uri = "http://tunehogsync.com/logout"
  c.source_label = "tunehogsync"
  c.website = "tunehogsync.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Sync Landingpage"
  c.identifier = "2454880863f6826d5144371861a08f94"
  c.secret = "5f3c38434c3d5cc9aa563f406334199f"
end

define_client do |c|
  c.name = "Tunehog Gifts Landingpage"
  c.redirect_uri = "http://tunehoggifts.com/callback"
  c.sign_out_uri = "http://tunehoggifts.com/logout"
  c.source_label = "tunehoggifts"
  c.website = "tunehoggifts.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Gifts Landingpage"
  c.identifier = "048493f7751bd6444a24a68d0d752180"
  c.secret = "6818ef8fabdeedf80d7c51154b3ae8c3"
end


define_client do |c|
  c.name = "Tunehog Universe Landingpage"
  c.redirect_uri = "http://tunehoguniverse.com/callback"
  c.sign_out_uri = "http://tunehoguniverse.com/logout"
  c.source_label = "tunehoguniverse"
  c.website = "tunehoguniverse.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Gifts Landingpage"
  c.identifier = "f76526e4a9fa429f72760b43e99f8995"
  c.secret = "e3a5a4c24ca1429cc65f5f15d4ba9018"
end

# Universe

define_client do |c|
  c.name = "Universe Local"
  c.redirect_uri = "http://universe.tunehog.local:3999/auth/randr/callback"
  c.sign_out_uri = "http://universe.tunehog.local:3999/sign_out"
  c.source_label = "universe"
  c.website = "universe.tunehog.local:3999"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Universe Local"
  c.identifier = "0687fdcda0b3bb9c65af0a3e0067dc73"
  c.secret = "d46049256d931ba3c2cfec89cacf4bd2"
end

define_client do |c|
  c.name = "Universe QA"
  c.redirect_uri = "https://universe.qa.vocvox.com/auth/randr/callback"
  c.sign_out_uri = "https://universe.qa.vocvox.com/sign_out"
  c.source_label = "universe"
  c.website = "universe.qa.vocvox.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Universe QA"
  c.identifier = "1abd7cd270ca2c3b88770be72574a748"
  c.secret = "01cae4610b0c372fc6238f40ed6d08db"
end

define_client do |c|
  c.name = "Universe"
  c.redirect_uri = "https://universe.tunehog.com/auth/randr/callback"
  c.sign_out_uri = "https://universe.tunehog.com/sign_out"
  c.source_label = "universe"
  c.website = "universe.tunehog.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Universe"
  c.identifier = "b35cf05dcb4caa3fddc6212364070170"
  c.secret = "8fffbd4dd9f9d861c2e33a243e3a219a"
end

define_client do |c|
  c.name = "Video"
  c.redirect_uri = "https://video.tunehog.com/callback"
  c.sign_out_uri = "https://video.tunehog.com/logout"
  c.source_label = "video"
  c.website = "video.tunehog.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Video"
  c.identifier = "4da1bad7a009649b8be67581afe3ae2d"
  c.secret = "8a9da5d79a12170e48078883c00e3944"
end

define_client do |c|
  c.name = "Competitions Local"
  c.redirect_uri = "http://competitions.tunehog.local:3998/auth/randr/callback"
  c.sign_out_uri = "http://competitions.tunehog.local:3998/sign_out"
  c.source_label = "competitions"
  c.website = "competitions.tunehog.local:3998"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Competitions Local"
  c.identifier = "d755a484fe0c6ce4f6a41035aa1a30c1"
  c.secret = "2d1a2395f494bcc922d34b77a8bea46c"
end

define_client do |c|
  c.name = "Competitions QA"
  c.redirect_uri = "https://competitions.qa.vocvox.com/auth/randr/callback"
  c.sign_out_uri = "https://competitions.qa.vocvox.com/sign_out"
  c.source_label = "competitions"
  c.website = "competitions.qa.vocvox.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Competitions QA"
  c.identifier = "0f24880c1298325b8f2e68f33fc6ebb8"
  c.secret = "8665751bbb63085ef8529f3451726beb"
end

define_client do |c|
  c.name = "Competitions"
  c.redirect_uri = "https://competitions.tunehog.com/auth/randr/callback"
  c.sign_out_uri = "https://competitions.tunehog.com/sign_out"
  c.source_label = "competitions"
  c.website = "competitions.tunehog.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Competitions"
  c.identifier = "0bbd84acb42f377d1544c7f106b90e2c"
  c.secret = "3627a221abfcc2f252a666109b63b074"
end

define_client do |c|
  c.name = "Tunehog Competitions Landingpage"
  c.redirect_uri = "http://tunehogcompetitions.com/callback"
  c.sign_out_uri = "http://tunehogcompetitions.com/logout"
  c.source_label = "tunehogcompetitions"
  c.website = "tunehogcompetitions.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.app_description = "Tunehog Competitions Landingpage"
  c.identifier = "5fe6c32f70cd5c8b3955c0da1a6a24b3"
  c.secret = "db16c4f6f941d69081622c234f2914c2"
end

# Redbull Demo
define_client do |c|
  c.name = "Discovery Redbull"
  c.redirect_uri = "https://redbull.discovery.tunehog.com/auth/callback"
  c.sign_out_uri = "https://redbull.discovery.tunehog.com/auth/logout"
  c.source_label = "discovery"
  c.website = "redbull.discovery.tunehog.com"
  c.support_email = "nikolay.m@randrmusic.com"
  c.contact_email = "nikolay.m@randrmusic.com"
  c.identifier = "d0cbb2fa4cd38677c7b3ecb5547aa2e6"
  c.secret = "19daeadc6508c0c6f18e22622bf8949a"
end

define_client do |c|
  c.name = "Radio Redbull"
  c.redirect_uri = "https://redbull.radio.tunehog.com/auth/randr/callback"
  c.sign_out_uri = "https://redbull.radio.tunehog.com/sign_out"
  c.source_label = "radio"
  c.website = "redbull.radio.tunehog.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "89ddbb2c2f96f3e5f143251185fc8b0c"
  c.secret = "7880a43b6f7fadc1b7b5a338402ce696"
end

define_client do |c|
  c.name = "TH-Admin (local)"
  c.redirect_uri = "http://admin.tunehog.local/auth/randr/callback"
  c.sign_out_uri = "http://admin.tunehog.local/sign_out"
  c.source_label = "admin"
  c.website = "admin.tunehog.local"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "3e09efe7694c40db6ad990f9d7d8e786"
  c.secret = "3a09efe7694c40db6ad990f9d7d8e786"
end


define_client do |c|
  c.name = "TH-Admin (qa)"
  c.redirect_uri = "https://admin.qa.vocvox.com/auth/randr/callback"
  c.sign_out_uri = "https://admin.qa.vocvox.com/sign_out"
  c.source_label = "admin"
  c.website = "admin.qa.vocvox.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "3e09efe7694c40db6ad990f9d7d8e78a"
  c.secret = "3a09efe7694c40db6ad990f9d7d8e78b"
end

# DNA
define_client do |c|
  c.name = "Dna Local"
  c.redirect_uri = "http://dna.tunehog.local/auth/randr/callback"
  c.sign_out_uri = "http://dna.tunehog.local/sign_out"
  c.source_label = "dna"
  c.website = "dna.tunehog.local"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "01698f742ecc5d99634304f5842fc478"
  c.secret = "e3f3c6c4dce0b12936f51cc547f9a486"
end

define_client do |c|
  c.name = "Dna QA"
  c.redirect_uri = "https://dna.qa.vocvox.com/auth/randr/callback"
  c.sign_out_uri = "https://dna.qa.vocvox.com/sign_out"
  c.source_label = "dna"
  c.website = "dna.qa.vocvox.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "262312917aff702b445e6a2863be582c"
  c.secret = "24c60ec5f11e25a607907defe961623a"
end

define_client do |c|
  c.name = "Dna"
  c.redirect_uri = "https://dna.tunehog.com/auth/randr/callback"
  c.sign_out_uri = "https://dna.tunehog.com/sign_out"
  c.source_label = "dna"
  c.website = "dna.tunehog.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "3ae0984c26afbbcde0ee8d44d089645a"
  c.secret = "edd6aefb46f6fc73372195c8fd529638"
end

# TMM
define_client do |c|
  c.name = "tmm (local)"
  c.redirect_uri = "http://musicmanager.tunehog.local/auth/randr/callback"
  c.sign_out_uri = "http://musicmanager.tunehog.local/sign_out"
  c.source_label = "tmm"
  c.website = "tmm.tunehog.local"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "142f8a1f3aa9332ea40d1a17961eb7d3"
  c.secret = "4b5bba25cf27b69a86c6a02df883736c"
end

define_client do |c|
  c.name = "TH-Admin (qa)"
  c.redirect_uri = "https://tmm.qa.vocvox.com/auth/randr/callback"
  c.sign_out_uri = "https://tmm.qa.vocvox.com/sign_out"
  c.source_label = "tmm"
  c.website = "tmm.qa.vocvox.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "76f95907198c28d6741781e1c06ce128"
  c.secret = "c637a892a5964bdd4ed610f3a248e59e"
end

# Remote

define_client do |c|
  c.name = "Remote (development)"
  c.redirect_uri = "http://remote.tunehog.local/auth/randr/callback"
  c.sign_out_uri = "http://remote.tunehog.local/sign_out"
  c.source_label = "remote"
  c.website = "remote.tunehog.local"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "d755a484fe0c6ce4f6a41035bb1a30c1"
  c.secret = "2d1a2395f494bcc922d34b77a8beb56c"
end

define_client do |c|
  c.name = "Remote (qa)"
  c.redirect_uri = "https://remote.qa.vocvox.com/auth/randr/callback"
  c.sign_out_uri = "https://remote.qa.vocvox.com/sign_out"
  c.source_label = "remote"
  c.website = "remote.qa.vocvox.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "d755a484fe0c6ce4f6a41035bb1a30c2"
  c.secret = "2d1a2395f494bcc922d34b77a8beb56d"
end

define_client do |c|
  c.name = "Remote"
  c.redirect_uri = "https://remote.tunehog.com/auth/randr/callback"
  c.sign_out_uri = "https://remote.tunehog.com/sign_out"
  c.source_label = "remote"
  c.website = "remote.tunehog.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "d755a484fe0c6ce4f6a41035bb1a30c3"
  c.secret = "2d1a2395f494bcc922d34b77a8beb56e"
end

define_client do |c|
  c.name = "SnapTrack"
  c.redirect_uri = "http://tunehog.com"
  c.sign_out_uri = "http://tunehog.com"
  c.source_label = "snap_track"
  c.website = "tunehog.com"
  c.support_email = admin.email
  c.contact_email = admin.email
  c.identifier = "62aeb4d22ff76eba5ab10cb92fb35711"
  c.secret = "c7d1f09c309b5b4f6258fc6f573a7ae9"
end

puts ("="* 20)+"[End Load Seeds]"+("="* 20)
