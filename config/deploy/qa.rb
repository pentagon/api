ssh_options[:keys] = Dir[File.dirname(__FILE__) + "/../../config/deploy/keys/qa.pem"]

set :torquebox_home, "/opt/torquebox"
set :deploy_to, "#{torquebox_home}/stage/#{application}"

set :jboss_control_style, :initd
set :jboss_init_script, '/etc/init.d/torquebox'

server 'qa1.vocvox.com', :app, :web, :indexes, jboss_bind_address: '88.150.233.50'
server 'qa2.vocvox.com', :app, :web, jboss_bind_address: '88.150.213.250'
server 'qa1.vocvox.com', :db, :primary => true, :no_release => true

set :user, 'torquebox'
set :keep_releases, 3
set :deploy_via, :remote_cache

set :rails_env, 'qa'
set :app_host, 'restapi.qa.vocvox.com'
set :app_ruby_version, '1.9'
set :app_context, '/'
set :cluster, true
set(:branch, 'develop') unless exists?(:branch)
