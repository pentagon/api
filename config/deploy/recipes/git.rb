namespace :git do
  def assert_version
    unless version
      tags = `git tag`.split("\n").select{|a| a =~ /^\d+\.\d+\.\d+-qa\d+$/}.sort_by{|a| a.gsub('-qa','.').split('.').map{|a| a.to_i}}
      prompt_version = tags.last.gsub /\d+$/, tags.last.match(/\d+$/)[0].succ
      puts 'Last 10 tags'
      puts tags.last(10)
      _version = Capistrano::CLI.ui.ask "Please add version of deploy: default is (#{prompt_version}): "
      set :version, _version.empty? ? prompt_version : _version
    end

    raise Capistrano::Error, %Q{No version is set. Usage:
      cap deploy version=x.y.z
    } if version.nil?
  end

  desc "Move tag of current stage"
  task :move_stage_tag, roles: :app, except: {no_release: true} do
    logger.info "Moving #{stage} tag to #{real_revision}"
    system "#{source.command} tag --force --message='Moving stage tag for qa to #{real_revision}' #{stage} #{real_revision}"
  end

  desc "Bump verion"
  task :bump_version, roles: :app, except: {no_release: true} do
    assert_version
    logger.info "Tagging version #{version} on #{real_revision}"
    system "#{source.command} tag --force --message='Version bump: #{version} (#{real_revision})' #{version} #{real_revision}"
  end

  desc "Print CHANGELOG"
  task :print_changelog, roles: :app, except: {no_release: true}, on_error: :continue do
    assert_version
    from = current_revision
    to = source.next_revision(real_revision)
    puts
    puts "(*) Deploying #{application} to #{stage} (*)"
    puts "Version #{version} (#{release_name})"
    puts "-"*30
    system "#{source.command} --no-pager log --no-color --pretty=format:'* %s [%an]' --abbrev-commit --no-merges #{from}..#{to}"
    puts
    puts
  end

  desc "Push tags"
  task :push_tags, roles: :app, except: {no_release: true} do
    logger.info "Pushing tags to remotes"
    system "#{source.command} push --force --tags"
  end
end
