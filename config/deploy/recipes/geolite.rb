namespace :geolite do
  task :upload_data, :roles => :app do
    upload File.expand_path('data/GeoLiteCity.dat'), "#{shared_path}/GeoLiteCity.dat"
  end

  task :symlink, :roles => :app do
    run "ln -nfs #{shared_path}/GeoLiteCity.dat #{latest_release}/data/GeoLiteCity.dat"
  end
end
