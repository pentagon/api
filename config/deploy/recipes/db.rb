namespace :db do
  task :configure, :roles => :app do
    # # database.yml
    # template "database.yml.erb", "#{shared_path}/database.yml"
    # couchdb.yml
    template "mongoid.yml.erb", "#{shared_path}/mongoid.yml"
  end

  task :symlink, :roles => :app do
    # run "ln -nfs #{shared_path}/database.yml #{latest_release}/config/database.yml"
    run "ln -nfs #{shared_path}/mongoid.yml #{latest_release}/config/mongoid.yml"
  end

  task :create_indexes, :roles => :indexes do
    run "cd #{latest_release} && PATH=#{torquebox_home}/jruby/bin:$PATH RAILS_ENV=#{rails_env} #{rake} db:mongoid:create_indexes", pty: true
  end
end
