def git_fetch(type, from_base = 1)
  format_str = case type
               when 'commit' then '%H'
               when 'author.name' then '%aN'
               when 'author.email' then '%aE'
               when 'commit.subj' then '%s'
               when 'commit.date' then '%ad'
               end
  return `git --no-pager reflog -n #{from_base} --format='#{format_str}' #{branch}`.strip
end

namespace :deploy_info do
  task :upload, :roles => :app do
    put DateTime.now.to_s, "#{latest_release}/data/deploy.dat"
    put `git config --global user.name`, "#{latest_release}/data/deployer.dat"
    revision = {
      commit: git_fetch('commit'),
      author: {
        name: git_fetch('author.name'),
        email: git_fetch('author.email')
      },
      subject: git_fetch('commit.subj'),
      date: git_fetch('commit.date')
    }
    put revision.to_json, "#{latest_release}/data/revision.json"
  end
end
