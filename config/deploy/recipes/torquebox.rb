namespace :torquebox do
  task :configure, :roles => :app do
    template "torquebox.yml.erb", "#{shared_path}/torquebox.yml"
  end

  task :symlink, :roles => :app do
    run "ln -nfs #{shared_path}/torquebox.yml #{latest_release}/config/torquebox.yml"
  end

  task :stop_app, :roles => :app do
    run "rm -f #{jboss_home}/standalone/deployments/#{torquebox_app_name}-knob.yml.deployed"
  end

  task :start_app, :roles => :app do
    run "touch #{jboss_home}/standalone/deployments/#{torquebox_app_name}-knob.yml.dodeploy"
  end
end
