def template(from, to)
  erb = ERB.new(File.read("config/deploy/templates/#{from}")).result(binding)
  put erb, to
end

def set_default(name, *args, &block)
  set(name, *args, &block) unless exists?(name)
end
