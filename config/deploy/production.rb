ssh_options[:keys] = Dir[File.dirname(__FILE__) + "/../../config/deploy/keys/prod.pem"]

set :torquebox_home, "/opt/torquebox"
set :deploy_to, "#{torquebox_home}/stage/#{application}"

set :jboss_control_style, :initd
set :jboss_init_script, '/etc/init.d/torquebox'

server 'app2.vocvox.com', :app, :web, jboss_bind_address: '31.3.248.234'
server 'app3.vocvox.com', :app, :web, jboss_bind_address: '109.73.70.162'

set :user, 'torquebox'
set :keep_releases, 3
set :deploy_via, :remote_cache

set :rails_env, 'production'
set :app_host, 'restapi.tunehog.com'
set :app_ruby_version, '1.9'
set :app_context, '/'
set :cluster, true

set(:branch, 'master') unless exists?(:branch)
