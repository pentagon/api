# ssh_options[:keys] = Dir[File.dirname(__FILE__) + "/../../config/deploy/keys/qa.pem"]

set :torquebox_home, ENV['TORQUEBOX_HOME'] || "/opt/torquebox/torquebox"
set :deploy_to, "#{torquebox_home}/stage/#{application}"

set :jboss_control_style, :binscripts
set :jboss_bind_address, '0.0.0.0'

server 'localhost', :app, :web, :indexes
server 'qa.vocvox.com', :db, :primary => true, :no_release => true

set :user, ENV['USER'] || 'torquebox'
set :keep_releases, 3
set :deploy_via, :remote_cache

set :rails_env, 'qa'
set :app_host, 'restapi.tunehog.local'
set :app_ruby_version, '1.9'
set :app_context, '/'

set(:branch, 'develop') unless exists?(:branch)
