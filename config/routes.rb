Api::Application.routes.draw do
  scope '/api' do
    authenticable application: ''
    get 'referral/:referral_token' => 'users/registrations#new', :as => :new_user_referral_registration
    scope '/zendesk' do
      get 'sign_in' => 'zendesk#new', :as => :zendesk_sign_in
      get 'sign_out' => 'zendesk#destroy', :as => :zendesk_sign_out
    end

    namespace :debug do
      post :command
    end if Rails.env.qa? or Rails.env.development?

    namespace :clearplayer do
      authenticable application: 'clearplayer'
    end

    namespace :dwl do
      authenticable application: 'dwl'
    end

    namespace :slm do
      authenticable application: 'slm'
    end

    namespace :admin, constraints: {format: :json}  do
      authenticable application: "admin"
      resources :albums, only: [:index, :show]
      resources :artists, only: [:index, :show]
      resources :full_track_requests, only: [:index, :show] do
        collection do
          get :csv
          get :countries_list
        end
      end
      resources :payment_orders, only: [:index, :show] do
        member do
          get :additional_info
          put :refresh_status
        end
        collection do
          get :csv
        end
      end
      resources :payment_price_modifiers, except: [:new, :edit]
      resources :recommendations, only: [:show] do
        member do
          get :csv
        end
      end
      resources :subscriptions, except: [:new, :edit, :update] do
        collection do
          get :csv
        end
      end
      resources :stations, except: [:new, :edit] do
        member do
          get :next_track
          get :used_tracks
          get :track_feedbacks
        end
      end
      resources :test_stations, only: [:index, :show] do
        member do
          get :track_ratings
          post :rate_track
        end
      end
      resources :tracks, only: [:index, :show] do
        collection do
          get :csv
          get :page_csv
        end
      end
      resources :playlists do
        collection do
          get :csv
        end
      end
      resources :users, except: [:new, :edit] do
        collection do
          get :me
          get :registrations
        end
      end
      resources :videos
    end

    namespace :tmm do
      resources :share_links
      resources :users
      resources :artists
      resources :tracks
      resources :images, only: [:update, :create, :destroy]
      resources :videos
      resources :albums
      resources :image_albums
      resources :referrals do
        get :by_initiator, on: :member
      end
      resources :track_statistics, only: [:show]
      resources :conversations
      resources :profiles
      namespace :charts do
        resources :competitors
        resources :artists
        resources :tracks
      end
      resources :reports, only: [:index, :show]
    end

    namespace :player do
      namespace :v2 do
        resources :playlists
        resources :artworks, only: [:index]
        resources :video_galleries, only: [:index]
        resources :videos, only: [:create, :destroy]
        resources :tracks, only: [:show] do
          member {post :feedback}
        end
        resources :albums, only: [:show]
        resources :artists, only: [] do
          member {get :tracks}
        end
        resource :startup, only: [:show], controller: :startup
      end
    end

    namespace :discovery do
      authenticable application: 'discovery'
      resources :ads, only: [:show]
      resources :tracks, only: [:index, :show] do
        collection { get :search }
        member { get :download }
        member { get :get_price }
        member { post :send_mail }
      end
      resources :download_requests, only: [:show, :create] do
        member {get :download}
        member {put :deliver}
        member {get :set_downloaded}
      end
      resources :artists do
        collection { get :search }
      end
      resources :albums, only: [:show] do
        collection { get :search }
      end
      resources :recommendations, only: [:show] do
        collection do
          post :wheel_success_callback
          post :wheel_error_callback
        end
      end
      resource :start, only: [:show], controller: :start
    end

    namespace :statistic do
      resources :tracks, only: [:show, :create] do
        collection {get :genre_chart}
      end
      resources :artists, only: [] do
        collection do
          get :listen
          get :genre_chart
          get :aggregated_listen_stat
        end
      end
      resource :dashboard, only: [] do
        get :registrations
        get :activities
        get :purchases
        get :analytics
      end
    end

    # Hit Maker API version 2
    namespace :hit_maker do
      authenticable application: 'hit_maker'
      resources :users, only: [:show]
      resources :tracks, only: [:show, :create]
      resources :reports, only: [:show, :create, :update, :destroy]
      resources :past_reports, only: [:show]
    end

    namespace :hitlogic do
      authenticable application: "hitlogic"
      resources :track_rights, only: [:show]
      resources :tracks
      resources :data_sets do
        get :tags, on: :member
      end
      resources :data_module_prototypes
      resources :data_modules, only: [:show, :index]
      resources :users  do
        collection do
          get :me
          get :reports
        end
      end
    end

    namespace :radio do
      authenticable application: 'radio'
      resources :tracks, only: [:show] do
        collection do
          get :discovery
          get :simple_search
          get :advanced_search
          get :next_for_station
          get :latest_played_for_station
          get :multi_load
          get :radio_presets
        end
        member do
          get :more_by_artist
          get :radio_url
        end
      end
      resources :stations, except: [:new, :edit] do
        collection do
          get :playlist
          get :most_popular
          get :multi_load
        end
        member do
          post :send_request
          post :skip_track
          get  :tracks_uris
        end
      end
      resources :radio_playlists, only: [:index, :show, :update, :create] do
        collection {post :set}
      end
      resources :subscriptions, except: [:new, :edit]
      resources :users, only: :show
    end

    namespace :sync do
      resources :tracks, except: [:destroy, :new, :edit]
      resources :recommendations, only: [:show]
      resources :artists, except: [:new, :edit, :index] do
        member {get :by_user}
      end
    end

    resources :notifications, only: [:index]

    resources :users, only: [:show, :update] do
      member do
        get :validate_contact_info
      end
      collection do
        get :find_by_email
        post :bulk_load
        post :confirm_social
        get :subscribe
        get :unsubscribe
        get :with_roles
        post 'mobile/:provider', action: :mobile_authentication
      end
    end

    resources :genres, only: [:index]

    resources :tracks, only: [] {member {get :wavejson}}

    resources :recommendations, only: [:show, :destroy] do
      collection {post :bulk_destroy}
      member     {get  :precache}
    end

    resources :hit_maker_reports, only: [:show]

    namespace :locate do
      resources :tracks, only: [] do
        collection {post :load_by_uris}
        collection {get :search}
      end
      resources :artists, only: [] do
        collection {get :search}
      end
      resources :statistics do
        collection {get :listen_geo_by_period}
        collection {get :realtime_listen}
        collection {get :listen_by_artist}
        collection {get :listen_by_track}
        collection {get :listen_by_user}
      end
    end

    namespace :gifts do
      resources :pictures, except: [:index]
      resources :cassettes, only: [:index, :show]
      resources :presets, only: [:index, :show]
      resources :recommendations, only: [:show]
      resources :packs, only: [:index, :show]
      resources :categories, only: [:index, :show]
      resources :templates, only: [:index, :show]
      resources :ecards, only: [:create, :update, :show] do
        member {post :read}
        collection {get :get_price}
      end
      resources :mixtapes, only: [:create, :update, :show] do
        member {post :read}
        collection {get :get_prices}
      end
      resources :tracks, only: [:index, :show] do
        collection {get :search}
        member {get :radio}
        member {get :mixtape_full}
      end
    end

    namespace :dna do
      resources :tests, only: [:index, :random, :user_liked_tracks] do
        collection do
          put :update
          get :random
          get :user_liked_tracks
        end
      end
    end

    namespace :tiles do
      namespace :v2 do
        resources :searches
        resources :artists
        resources :tracks
        resources :albums
        resources :recommendations
      end

      resources :recommendations, only: [:show]
      resources :artists, only: [:show, :index]
      resources :albums, only: [:index]
      resources :tracks, only: [:index] do
        collection do
          get :feedback
          get :search
        end
      end
    end

    resources :subscriptions, only: [:index, :show, :destroy] do
      collection do
        get :latest_for_user
        get :result_cancel
        get :result_error
        get :result_success
        get :subscribe
        get :subscribe_confirm
        get :templates
        get :trial_template
        post :activate_trial
      end
      member do
        post :cancel
        post :reactivate
      end
    end

    resources :payments, only: [] do
      collection do
        post :process_ipn
        get :check_billing_address
        put :billing_address, action: :update_billing_address, as: :update_billing_address
        get :countries_list
        get 'result_:status', action: :result, as: :result
        get ':type/get_prices' => :get_prices
        get ':type/:id/get_price', action: :get_price, as: :get_price
        get ':type/:id/buy', action: :buy, as: :buy
      end
      member do
        get :confirm_purchase
        get :confirm_cancel
      end
    end

    namespace :mobile do
      resources :devices, only: [] {collection {post :register}}
      resources :recommendations, only: [:show] do
        collection {get :start}
      end

      resources :radio_stations, only: [:index, :create, :update, :destroy] do
        member {get  :next_track}
        member {post :feedback}
      end

      resources :users, only: [] do
        member {post :mail}
        collection do
          get :me
          put :update
        end
      end

      namespace :myfirstplayer do
        authenticable application: 'mobile/myfirstplayer'
      end

      namespace :kurio do
        resources :users, only: [:update]
        resources :subscriptions, only: [:index]
        resources :start, only: [:index]
        resources :recommendations, only: [:show]
      end

      resources :tracks, only: [] do
        member {post :ban}
        collection {get :search}
      end

      resources :albums, only: [:show] do
        collection {get :search}
      end

      resources :artists, only: [:show] do
        collection {get :search}
      end

      namespace :radio do
        authenticable application: 'mobile/radio'
        resources :subscriptions, only: [] do
          collection do
            get :current
            get :templates
          end
          member do
            get :activate
            get :cancel
          end
        end
      end

      namespace :astro do
        authenticable application: 'mobile/astro'
        post 'mobile/:provider', action: :mobile_authentication
        get 'latest' => 'versions#latest'
        get 'subscription' => 'subscriptions#current'
        resources :subscriptions, only: [:index] do
          collection do
            get :current
            get 'result_:status', action: :result, as: :result
            get :subscribe
            get :subscribe_confirm
            get :templates
            get :trial_template
            post :activate_trial
          end
          member do
            post :cancel
            post :activate
          end
        end
        resources :forecasts, only: [:index]
        resources :users, only: [] {collection {get :me}}
        resources :stations, only: [:create] {collection {get :next_track}}
        resources :statistics, only: [:create]
        resources :tracks, only: [] do
          collection do
            get  :bookmark, action: :get_bookmarks
            post :bookmark, action: :add_bookmark
            delete :bookmark, action: :remove_bookmark
            post :feedback
          end
        end
        get :geoip
      end

      namespace :slm do
        resources :profiles, only: [] do
          collection do
            get  :show
            post :create
          end
        end
        resource :portrait, only: [:create]
        resources :recommendations, only: [:index]
        resources :activities, only: [:index]
      end

      namespace :startune do
        authenticable application: 'mobile/startune'
        resources :subscriptions, only: [] do
          collection do
            get :current
            get :templates
            get :subscribe
            get :subscribe_confirm
            get :result_success
            get :result_cancel
            get :result_error
          end
          member do
            post :activate
            post :cancel
          end
        end
        resources :stations, only: [:create] {collection {get :next_track}}
        resources :forecasts, only: [:index]
        get 'shop/templates' => 'shop#templates'
        get 'shop/buy' => 'shop#buy'
        resources :reports, only: [:index, :create]
        resources :tracks, only: [] do
          collection do
            get  :bookmark, action: :get_bookmarks
            post :bookmark, action: :add_bookmark
            delete :bookmark, action: :remove_bookmark
          end
        end
      end

      namespace :contextual do
        authenticable application: 'mobile/contextual'
        resources :tracks, only: [:index]
      end

      namespace :snapjam do
        authenticable application: 'mobile/snapjam'
        resources :snaps, only: [:create, :delete] {collection {get :history}}
        resources :tracks, only: [:index]
      end
    end

    namespace :thunderbird do
      resources :snap_tracks, only: [:create, :destroy] do
        collection {get :list}
      end
      resources :tracks, only: [] do
        collection {get :search}
      end
      resources :users, only: [] do
        resources :snaps_web, only: [:index, :show]
      end
      resources :snaps_web, only: [:show, :destroy]
    end

    namespace :feature do
      namespace :magazine do
        namespace :blocks do
          get 'available-blocks', action: 'available_blocks'
        end
        resources :blocks
      end
    end

    resources :logger, only: [:index, :create]

    namespace :lml do
      resources :libraries, only: [:index, :create]
      resources :tracks, only: [:index, :create, :delete] do
        collection do
          post :delete, action: :destroy
          post :analysis, action: :send_analysis
          get :analysis, action: :get_analysis
        end
      end
    end

    namespace :recommend do
      resources :tracks, only: [:show] do
        collection do
          get :autocomplete
          get :search
        end
      end
      resources :proxies
      resources :users, only: [:show] do
        collection do
          post :search
        end
      end
    end

    namespace :blog do
      resources :articles, only: [:index] do
        collection {get :latest}
      end
      # V2
      resources :inits
      resources :categories
      resources :posts
      resources :comments
    end

    namespace :astroweb do
      authenticable application: 'astroweb'
      resources :tracks, only: [] do
        collection {post :load_by_uris}
      end
      resources :forecasts, only: [:index]
      resources :subscriptions, only: [] do
        collection do
          get :subscribe
          get :subscribe_confirm
          get :result_success
          get :result_cancel
          get :result_error
        end
        member do
          post :cancel
        end
      end
    end

    namespace :startune_web do
      get 'shop/templates' => 'shop#templates'
      get 'shop/buy' => 'shop#buy'
      get 'startune/reports' => 'startune/reports#index'
    end

    namespace :snap_jam_web do
      authenticable application: 'snap_jam_web'
      resources :users, only: [:show]
      resources :tracks, only: [:show]
      resources :snaps, only: [:index, :show]
    end

    root to: 'main#index'
    get 'geoip' => 'main#geoip'
    get 'mobile/startune/geoip' => 'main#geoip'
    get 'geosearch' => 'main#geosearch'
    get 'status' => 'main#status'
  end

  scope '/oauth' do
    get '/'           => 'authorizations#new', :module => ''
    match '/authorize' => 'authorizations#new', :module => ''
    post '/authorizations' => 'authorizations#create', :module => ''
    post '/token' => 'tokens#create', :module => ''
  end
end
