require 'capistrano/version'
require 'torquebox-capistrano-support'
require 'bundler/capistrano'
require 'json'
# require 'capistrano/slack'

set :stages, %w(development qa production)
set :default_stage, "development"
require 'capistrano/ext/multistage'

set :application, "api_tunehog"

set :scm, :git
set :repository,  "git@github.com:wetunein/#{application}.git"
set(:branch, ENV["BRANCH"]) if ENV["BRANCH"]

set :use_sudo, false
set :version, ENV['version']

# set :slack_token, 'ImN4Hi6chXfTPIC20RgMwNrP'
# set :slack_room, '#deploys'
# set :slack_subdomain, 'randrinnovation'
# set :slack_application, 'API Tunehog'
# set :slack_username, `echo $USER`
# set :slack_emoji, ":rocket:"

%w(base db geolite deploy_info torquebox git).each do |lib|
  load "config/deploy/recipes/#{lib}"
end

before 'deploy:update', 'git:print_changelog'
after 'deploy:setup', 'geolite:upload_data'
after 'deploy:update_code', 'db:configure', 'db:symlink', 'geolite:symlink', 'deploy_info:upload', 'torquebox:configure',
  'torquebox:symlink'
after 'deploy:stop', 'torquebox:stop_app'
after 'deploy:start', 'torquebox:start_app'
after 'deploy', 'git:move_stage_tag', 'git:bump_version', 'git:push_tags'
after 'deploy:deploy_zero_downtime', 'git:move_stage_tag', 'git:bump_version', 'git:push_tags'

namespace :deploy do
  task :default do
    deploy_zero_downtime
  end
  task :migrate do
  end
end
