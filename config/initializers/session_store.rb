# Configure the TorqueBox Servlet-based session store.
# Provides for server-based, in-memory, cluster-compatible sessions
Api::Application.config.session_store :disabled
# if ENV['TORQUEBOX_APP_NAME']
#   Api::Application.config.session_store :torquebox_store
# else
#   Api::Application.config.session_store :cookie_store, :key => '_api_tunehog_session'
# end
