MisService.configure do |srv|
  srv.callback_tubes = MisSettings.callback_tubes.values
  srv.beanstalk_host = MisSettings.beanstalk
  srv.logger = Logger.new File.join Settings.log_path, 'mis_service.log'
  srv.job_logger = MisLogger
end

MisCommand::Base.configure do |cmd|
  cmd.priority = 5
  cmd.processing_time_limit = 0.seconds
end

MisCommand::GetRecommendation.configure do |cmd|
  cmd.callback_tube = MisSettings.callback_tubes.recommendation
  cmd.tube = MisSettings.job_tubes.recommendation
  cmd.job_name = 'wheels2'
  cmd.callback_protocol = MisSettings.callback_protocols.recommendation
  cmd.processing_time_limit = 12.seconds
end

MisCommand::GetWheels3Recommendation.configure do |cmd|
  cmd.callback_tube = MisSettings.callback_tubes.recommendation_wheels3
  cmd.tube = MisSettings.job_tubes.recommendation
  cmd.job_name = 'wheels3'
  cmd.callback_protocol = MisSettings.callback_protocols.recommendation_wheels3
  cmd.processing_time_limit = 12.seconds
end

MisCommand::GetHssInfo.configure do |cmd|
  cmd.callback_tube = MisSettings.callback_tubes.hss
  cmd.tube = MisSettings.job_tubes.hss
  cmd.job_name = 'hss3'
  cmd.callback_protocol = MisSettings.callback_protocols.hss
  cmd.processing_time_limit = 300.seconds
end

MisCommand::GetRadioData.configure do |cmd|
  cmd.callback_tube = MisSettings.callback_tubes.radio
  cmd.tube = MisSettings.job_tubes.radio
  cmd.job_name = 'radio3'
  cmd.processing_time_limit = 8.seconds
end

MisCommand::GetTestRadioData.configure do |cmd|
  cmd.callback_tube = MisSettings.callback_tubes.test_radio
  cmd.tube = MisSettings.job_tubes.test_radio
  cmd.job_name = 'radio3'
  cmd.processing_time_limit = 8.seconds
end

MisCommand::GetAstroRadioData.configure do |cmd|
  cmd.callback_tube = MisSettings.callback_tubes.radio
  cmd.tube = MisSettings.job_tubes.radio_astro
  cmd.job_name = 'radio3'
  cmd.processing_time_limit = 8.seconds
end

MisCommand::GetFgpInfo.configure do |cmd|
  cmd.callback_tube = MisSettings.callback_tubes.fgp
  cmd.tube = MisSettings.job_tubes.fgp
  cmd.job_name = 'mfs_search'
  cmd.callback_protocol = MisSettings.callback_protocols.fgp
  cmd.processing_time_limit = 10.seconds
end

[MisCommand::GetTrackStatus, MisCommand::UnregisterTrack, MisCommand::RegisterTrack, MisCommand::UpdateTrack].each do |klass|
  klass.configure do |cmd|
    cmd.callback_tube = MisSettings.callback_tubes.track_status
    cmd.tube = MisSettings.job_tubes.track_status
    cmd.job_name = 'unsigned_sync'
    cmd.callback_protocol = MisSettings.callback_protocols.track_status
  end
end

MisCommand::LmlTrackAnalysis.configure do |cmd|
  cmd.callback_tube = MisSettings.callback_tubes.lml
  cmd.tube = MisSettings.job_tubes.lml
  cmd.job_name = 'lml_mad3'
  cmd.processing_time_limit = 1.minute
end

MisCommand::LmlTrackAdd.configure do |cmd|
  cmd.callback_tube = MisSettings.callback_tubes.lml
  cmd.tube = MisSettings.job_tubes.lml
  cmd.job_name = 'lml_add_track'
  cmd.processing_time_limit = 1.minute
end

MisCommand::LmlTrackDelete.configure do |cmd|
  cmd.callback_tube = MisSettings.callback_tubes.lml
  cmd.tube = MisSettings.job_tubes.lml
  cmd.job_name = 'lml_delete_track'
  cmd.processing_time_limit = 1.minute
end

JobMonitoringService.configure do |srv|
  srv.polling_interval = 1
  srv.logger = Logger.new File.join Settings.log_path, 'job_monitoring_service.log'
  srv.logger.formatter = proc do |severity, timestamp, progname, msg|
    "\n[#{severity.first}][#{timestamp}] #{String === msg ? msg : msg.inspect}\n"
  end
end

Radio::QueueManager.configure do |qmgr|
  qmgr.logger = Logger.new File.join Rails.root, 'log', 'radio_queue_mgr.log'
  qmgr.logger.formatter = proc do |severity, timestamp, progname, msg|
    "\n[#{severity.first}][#{timestamp}] #{String === msg ? msg : msg.inspect}"
  end
end
