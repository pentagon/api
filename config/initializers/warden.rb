=begin
require 'warden_proxy'

Warden::Manager.after_authentication do |resource, warden, options|
  proxy = WardenProxy.new(warden, options)
  proxy.log_sign_in(resource, remote_ip: proxy.request.remote_ip)
end

Warden::Manager.before_logout do |resource, warden, options|
  proxy = WardenProxy.new(warden, options)
  proxy.log_sign_out(resource, remote_ip: proxy.request.remote_ip)
  proxy.reset_authentication_token!(resource)
end

#TODO: remove this right after debugging
Warden::Strategies::Base.class_eval do
  def _run! # :nodoc:
    @performed = true
    Rails.logger.info self.class.inspect
    Rails.logger.info authentication_hash.inspect
    Rails.logger.info ActionController::HttpAuthentication::Token.token_and_options(request).inspect
    authenticate!
    self
  end
end
=end
