OmniAuth.config.full_host = lambda do |env|
  scheme         = env['rack.url_scheme']
  local_host     = env['HTTP_HOST']
  forwarded_host = env['HTTP_X_FORWARDED_HOST']
  forwarded_host.blank? ? "#{scheme}://#{local_host}" : "#{scheme}://#{forwarded_host}"
end

module OmniAuth::Strategies
  class FacebookAstro < Facebook
    def name
      :facebook_astro
    end
  end
  class GoogleOauth2Astro < GoogleOauth2
    def name
      :google_oauth2_astro
    end
  end
  class FacebookDwl < Facebook
    def name
      :facebook_dwl
    end
  end
  class GoogleOauth2Dwl < GoogleOauth2
    def name
      :google_oauth2_dwl
    end
  end
  class FacebookHitMaker < Facebook
    def name
      :facebook_hit_maker
    end
  end
  class GoogleOauth2HitMaker < GoogleOauth2
    def name
      :google_oauth2_hit_maker
    end
  end
  class FacebookHitlogic < Facebook
    def name
      :facebook_hitlogic
    end
  end
  class GoogleOauth2Hitlogic < GoogleOauth2
    def name
      :google_oauth2_hitlogic
    end
  end
  class FacebookMobileAstro < FacebookAccessToken
    def name
      :facebook_mobile_astro
    end
  end
  class FacebookTokenSlm < FacebookAccessToken
    def name
      :facebook_token_slm
    end
  end
  class FacebookSlm < Facebook
    def name
      :facebook_slm
    end
  end
  class GoogleOauth2Slm < GoogleOauth2
    def name
      :google_oauth2_slm
    end
  end
  class FacebookSnapJamWeb < Facebook
    def name
      :facebook_snap_jam_web
    end
  end
  class GoogleOauth2SnapJamWeb < GoogleOauth2
    def name
      :google_oauth2_snap_jam_web
    end
  end
  class FacebookMobileStartune < Facebook
    def name
      :facebook_mobile_startune
    end
  end
  class FacebookMobileStartuneToken < FacebookAccessToken
    def name
      :facebook_mobile_startune_token
    end
  end
  class GoogleOauth2MobileStartune < GoogleOauth2
    def name
      :google_oauth2_mobile_startune
    end
  end
  class FacebookMobileContextual < FacebookAccessToken
    def name
      :facebook_mobile_contextual
    end
  end
  class GoogleOauth2MobileContextual < GoogleOauth2
    def name
      :google_oauth2_mobile_contextual
    end
  end
  class FacebookMobileSnapjam < FacebookAccessToken
    def name
      :facebook_mobile_snapjam
    end
  end
  class GoogleOauth2MobileSnapjam < GoogleOauth2
    def name
      :google_oauth2_mobile_snapjam
    end
  end
  class FacebookClearplayer < FacebookAccessToken
    def name
      :facebook_clearplayer
    end
  end
  class GoogleOauth2Clearplayer < GoogleOauth2
    def name
      :google_oauth2_clearplayer
    end
  end
end

Rails.application.config.middleware.use OmniAuth::Builder do
  Settings.oauth.each do |omniprovider, opt|
    provider omniprovider.to_sym, opt['key'], opt['secret'], scope: opt['scope'], provider_ignores_state: opt['ignores_state'], callback_path: "/api/users/auth/#{omniprovider}/callback"
    provider "#{omniprovider}_astro".to_sym, opt['key'], opt['secret'], scope: opt['scope'], provider_ignores_state: opt['ignores_state'], callback_path: "/api/astroweb/users/auth/#{omniprovider}/callback"
    provider "#{omniprovider}_dwl".to_sym, opt['key'], opt['secret'], scope: opt['scope'], provider_ignores_state: opt['ignores_state'], callback_path: "/api/dwl/users/auth/#{omniprovider}/callback"
    provider "#{omniprovider}_slm".to_sym, opt['key'], opt['secret'], scope: opt['scope'], provider_ignores_state: opt['ignores_state'], callback_path: "/api/slm/users/auth/#{omniprovider}/callback"
    provider "#{omniprovider}_hit_maker".to_sym, opt['key'], opt['secret'], scope: opt['scope'], provider_ignores_state: opt['ignores_state'], callback_path: "/api/hit_maker/users/auth/#{omniprovider}/callback"
    provider "#{omniprovider}_hitlogic".to_sym, opt['key'], opt['secret'], scope: opt['scope'], provider_ignores_state: opt['ignores_state'], callback_path: "/api/hitlogic/users/auth/#{omniprovider}/callback"
    provider "#{omniprovider}_snap_jam_web".to_sym, opt['key'], opt['secret'], scope: opt['scope'], provider_ignores_state: opt['ignores_state'], callback_path: "/api/snap_jam_web/users/auth/#{omniprovider}/callback"
    #provider "#{omniprovider}_mobile_startune".to_sym, opt['key'], opt['secret'], scope: opt['scope'], provider_ignores_state: opt['ignores_state'], callback_path: "/api/mobile/startune/users/auth/#{omniprovider}/callback"
  end
  provider "facebook_mobile_snapjam".to_sym, Settings.oauth.facebook.key, Settings.oauth.facebook.secret, scope: Settings.oauth.facebook.scope, provider_ignores_state: Settings.oauth.facebook.ignores_state, callback_path: "/api/mobile/snapjam/users/auth/facebook/callback", client_options: {ssl: {version: 'TLSv1'}}
  provider "facebook_mobile_contextual".to_sym, Settings.oauth.facebook.key, Settings.oauth.facebook.secret, scope: Settings.oauth.facebook.scope, provider_ignores_state: Settings.oauth.facebook.ignores_state, callback_path: "/api/mobile/contextual/users/auth/facebook/callback", client_options: {ssl: {version: 'TLSv1'}}
  provider "facebook_mobile_astro".to_sym, Settings.oauth.facebook.key, Settings.oauth.facebook.secret, scope: Settings.oauth.facebook.scope, provider_ignores_state: Settings.oauth.facebook.ignores_state, callback_path: "/api/mobile/astro/users/auth/facebook/callback", client_options: {ssl: {version: 'TLSv1'}}
  provider "facebook_mobile_startune_token".to_sym, Settings.oauth.facebook.key, Settings.oauth.facebook.secret, scope: Settings.oauth.facebook.scope, provider_ignores_state: Settings.oauth.facebook.ignores_state, callback_path: "/api/mobile/startune/users/auth/facebook/callback", client_options: {ssl: {version: 'TLSv1'}}
  provider "facebook_token_slm".to_sym, Settings.oauth.facebook.key, Settings.oauth.facebook.secret, scope: Settings.oauth.facebook.scope, provider_ignores_state: Settings.oauth.facebook.ignores_state, callback_path: "/api/slm/users/auth/facebook_token/callback", client_options: {ssl: {version: 'TLSv1'}}
  provider "facebook_clearplayer".to_sym, Settings.oauth.facebook.key, Settings.oauth.facebook.secret, scope: Settings.oauth.facebook.scope, provider_ignores_state: Settings.oauth.facebook.ignores_state, callback_path: "/api/clearplayer/users/auth/facebook/callback", client_options: {ssl: {version: 'TLSv1'}}
end

OmniAuth.configure do |config|
  config.logger = Rails.logger
end
