module Mongoid
  class Criteria
    def each_by by = 100, &block
      idx = 0
      total = 0
      set_limit = options[:limit]
      while ((results = ordered_clone.limit(by).skip(idx)) && results.any?)
        results.each do |result|
          return self if set_limit and set_limit >= total
          total += 1
          yield result, total, idx
        end
        results = nil
        GC.start
        idx += by
      end
      self
    end

    def ordered_clone
      options[:sort] ? clone : clone.asc(:_id)
    end
    private :ordered_clone
  end

  module Slug
    # Getting back the slug attribute
    def slug
      return self['slug'] if attributes.include? 'slug'
      mongoid_slug
    end

    # Returned slugged id
    def mongoid_slug
      return _slugs.last if _slugs.try(:any?)
      return uri.to_s
    end

    module ClassMethods
      def find_by_slug (slug_or_uri)
        return nil if slug_or_uri.nil?
        return find_by uri: slug_or_uri if slug_or_uri =~ /^\w+:\w+:\d+$/
        return find slug_or_uri
      end
    end
  end

  # TODO remove this after switching to Mongoid 4
  module Contextual
    class Mongo
      def pluck *fields
        normalized_select = fields.inject({}) do |hash, f|
          hash[klass.database_field_name(f)] = 1
          hash
        end

        query.dup.select(normalized_select).map do |doc|
          if normalized_select.size == 1
            doc[normalized_select.keys.first]
          else
            normalized_select.keys.map {|n| doc[n]}.compact
          end
        end.compact
      end
    end
  end
end
