CarrierWave.configure do |config|
  config.storage = :file
  config.asset_host = Settings.storage_url
end
