if Settings.paypal.use_sandbox
  ActiveMerchant::Billing::Base.mode = :test
  OffsitePayments.mode = :test
end

module ActiveMerchant
  module Billing
    class PaypalDigitalGoodsPatchedGateway < PaypalDigitalGoodsGateway
      def add_payment_details(xml, money, currency_code, options = {})
        super
        options[:req_confirm_shipping] = options[:req_confirm_shipping] ? 1 : 0
        xml.tag!("n2:ReqConfirmShipping", options[:req_confirm_shipping])
      end
    end
  end
end
