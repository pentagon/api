Tire.configure do
  url Settings.elastic_search.url
  logger 'log/elasticsearch.log', level: 'debug'
end

module Tire
  module Search
    class Query
      def indices(indices = [], &block)
        @indices_query ||= IndicesQuery.new indices
        block.arity < 1 ? @indices_query.instance_eval(&block) : block.call(@indices_query) if block_given?
        @value[:indices] = @indices_query.to_hash
        @value
      end

      def raw_dsl(key, value)
        @value[key] = value.to_hash
        @value
      end

      class IndicesQuery
        def initialize(indices=[], &block)
          @indices_array = indices
          @value   = {}
          block.arity < 1 ? self.instance_eval(&block) : block.call(self) if block_given?
        end

        def query(&block)
          @value[:query] = Query.new(&block).to_hash
          @value
        end

        def no_match_query(string_value = nil, &block)
          @value[:no_match_query] = if string_value
            string_value
          else
            Query.new(&block).to_hash
          end
          @value
        end

        def to_hash
          @value.merge({indices: @indices_array})
        end
      end
    end
  end

  # Dirty patch to allow tire to correctly handle not_found error for ES > 0.9
  class Index
    def retrieve(type, id, options={})
      raise ArgumentError, "Please pass a document ID" unless id

      url       = "#{self.url}/#{Utils.escape(type)}/#{Utils.escape(id)}"

      params    = {}
      params[:routing]    = options[:routing] if options[:routing]
      params[:fields]     = options[:fields]  if options[:fields]
      params[:preference] = options[:preference] if options[:preference]
      params_encoded      = params.empty? ? '' : "?#{params.to_param}"

      @response = Configuration.client.get "#{url}#{params_encoded}"

      h = defined?(GsonEngine) ? GsonEngine.decode(@response.body) : MultiJson.decode(@response.body)
      wrapper = options[:wrapper] || Configuration.wrapper
      if wrapper == Hash then h
      else
        return nil if ((h.key?('found') and h['found'] == false) or (h.key?('exists') and h['exists'] == false))
        document = h['_source'] || h['fields'] || {}
        document.update('id' => h['_id'], '_type' => h['_type'], '_index' => h['_index'], '_version' => h['_version'])
        wrapper.new(document)
      end

    ensure
      curl = %Q|curl -X GET "#{url}"|
      logged("#{type}/#{id}", curl)
    end

    # allow uri as id for url for POSTing
    def get_id_from_document(document)
      old_verbose, $VERBOSE = $VERBOSE, nil # Silence Object#id deprecation warnings
      id = case
        when document.respond_to?(:uri)
          document.uri
        when document.is_a?(Hash)
          document[:_id] || document['_id'] || document[:id] || document['id']
        when document.respond_to?(:id) && document.id != document.object_id
          document.id.to_s
      end
      $VERBOSE = old_verbose
      id
    end
  end
end
