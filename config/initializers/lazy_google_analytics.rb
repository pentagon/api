LazyGoogleAnalytics::Config.setup do |config|
  config.pass_phrase = Settings.analytics.pass_phrase
  config.key_file    = File.join Rails.root, 'config', 'keys', Settings.analytics.key_file
  config.client_id   = Settings.analytics.client_id
  config.scope       = Settings.analytics.scope
  config.profile_id  = Settings.analytics.profile_id
  config.email       = Settings.analytics.email
end
