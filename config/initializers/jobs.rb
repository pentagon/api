if Rails.env.qa? or Rails.env.production?

  formatter = proc do |severity, timestamp, progname, msg|
    "\n[#{severity.first}][#{timestamp}] #{String === msg ? msg : msg.inspect}"
  end
  PNService.configure do |srv|
    srv.logger = Logger.new File.join Rails.root, 'log', 'pn_service.log'
    srv.logger.formatter = formatter
  end
  SubscriptionTrialManagerService.configure do |srv|
    srv.logger = Logger.new(File.join Rails.root, 'log', 'subscription_trial_manager_service.log')
    srv.logger.formatter = formatter
  end

  if (`hostname`.include?('2')) and (not defined?(Rails::Console))
    TorqueBox::ScheduledJob.remove_sync('pn_service') if TorqueBox::ScheduledJob.lookup('pn_service')
    TorqueBox::ScheduledJob.schedule(
      'PNService',
      '0 */30 * * * ?',
      name: 'pn_service',
      description: 'Push Notification Service',
      timeout: '1800s',
      config: {default_hour: 8, default_minute: 30},
      singleton: false
    )
    TorqueBox::ScheduledJob.remove_sync('subscription_trial_manager_service') if TorqueBox::ScheduledJob.lookup('subscription_trial_manager_service')
    TorqueBox::ScheduledJob.schedule(
      'SubscriptionTrialManagerService',
      '0 0 * * * ?',
      name: 'subscription_trial_manager_service',
      description: 'Subscription Trial Manager Service',
      timeout: '600s',
      singleton: false
    )
  end
end
