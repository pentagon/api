if defined?(TorqueBox::Logger) && !Rails.env.test?
  Rails.logger = TorqueBox::Logger.new
else
  RestClient.log = Logger.new(Rails.root.join 'log', 'restclient.log')
end

unless Rails.env.production?
  Mongoid.logger = Logger.new File.join Rails.root, 'log', 'mongoid.log'
  Moped.logger = Logger.new File.join Rails.root, 'log', 'moped.log'
end

# begin
#   require 'grizzled/rails/logger'
#   Grizzled::Rails::Logger.configure do |cfg|
#     cfg.colorize = !!(ENV['TERM'] =~ /xterm/i)
#   end
# rescue LoadError
#   # skip
# end
