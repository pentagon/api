protocol = Rails.env.in?(%w(development test)) ? 'http' : 'https'

Api::Application.configure do
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = Settings.mail.symbolize_keys
  config.action_mailer.asset_host = "#{protocol}://#{Settings.domain}"
end

ActionMailer::Base.default_url_options[:host] = Settings.domain
ActionMailer::Base.default_url_options[:protocol] = protocol
