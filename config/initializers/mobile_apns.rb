if Rails.env.production?
  Mobile::APNS.host = 'gateway.push.apple.com'
  Mobile::APNS.pem  = File.join Rails.root, 'config', 'keys', 'apns_prod.pem'
else
  Mobile::APNS.host = 'gateway.sandbox.push.apple.com'
  Mobile::APNS.pem  = File.join Rails.root, 'config', 'keys', 'apns_dev.pem'
end
