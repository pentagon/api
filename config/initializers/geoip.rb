MaxmindGeoip.configure do |geoip|
  geoip.url = 'http://geoip.maxmind.com/b'
  geoip.license_key = Settings.maxmind.license_key
  geoip.geoip_file = File.join Rails.root, 'data', 'GeoLiteCity.dat'
  geoip.logger = Logger.new File.join Settings.mis_api.log_path, 'geoip.log'
  geoip.cache_time = Rails.env.production? ? 1.hour : 1.day
end
