require 'action_dispatch'
require 'active_support/core_ext/module/aliasing'

module ActionDispatch::Routing::Mapper::Authenticable
  extend ActiveSupport::Concern

  WrongAuthenticationApplicationException = Class.new(StandardError)
  ALLOWED_AUTHENTICABLE_APPLICATIONS = [
    'admin',
    'mobile/astro',
    'mobile/radio',
    'radio',
    'astroweb',
    'slm',
    'mobile/startune',
    'mobile/contextual',
    'mobile/snapjam',
    'snap_jam_web',
    'dwl',
    'hit_maker',
    'clearplayer',
    'discovery',
    'mobile/myfirstplayer',
    'hitlogic',
    ''
  ]

  def authenticable *args, &block
    options = args.extract_options!
    options = options.dup
    application = options[:application].to_s || ''
    unless ALLOWED_AUTHENTICABLE_APPLICATIONS.include?(application)
      Rails.logger.error "#{application} is absent in allowed applications"
      raise WrongAuthenticationApplicationException
    end
    scope '/oauth', application: application do
      get '/'           => 'authorizations#new'
      match '/authorize' => 'authorizations#new'
      post '/authorizations' => 'authorizations#create'
      post '/token' => 'tokens#create'
    end
    scope '/users', application: application do
      get 'sign_in' => 'users/sessions#new', :as => :new_user_session
      post 'sign_in' => 'users/sessions#create', :as => :user_session
      match 'sign_out' => 'users/sessions#destroy', :as => :destroy_user_session, :via => [:delete, :get]
      get 'me' => 'users/sessions#me', :as => :current_user_session
      get 'current_user' => 'users/sessions#me'
      put 'me' => 'users/sessions#update_me'
      scope '/auth' do
        omniauth_providers = %w(facebook google_oauth2 facebook_token)
        providers_regex = Regexp.new(omniauth_providers.join("|"))
        match '/:provider',
          :to => 'users/omniauth_callbacks#passthru',
          :constraints => {:provider=>providers_regex},
          :as => :user_omniauth_authorize,
          :module => '',
          :via => [:post, :get]
        match '/:action/callback',
          :to => 'users/omniauth_callbacks',
          :constraints => {:action => providers_regex},
          :as => :user_omniauth_callback,
          :module => '',
          :via => [:post, :get]
      end
      scope '/password' do
        get 'new' => 'users/passwords#new', :as => :new_user_password
        post '' => 'users/passwords#create', :as => :user_password
        get 'edit' => 'users/passwords#edit', :as => :edit_user_password
        put '' => 'users/passwords#update'
      end
      get 'cancel' => 'users/registrations#cancel', :as => :cancel_user_registration
      post '' => 'users/registrations#create', :as => :user_registration
      get 'sign_up' => 'users/registrations#new', :as => :new_user_registration
      get 'edit' => 'users/registrations#edit', :as => :edit_user_registration
      put '' => 'users/registrations#update'
      delete '' => 'users/registrations#destroy'
    end
    scope '/verify', application: application do
      get '' => 'users/verifies#verify', :as => :verify
      post '' => 'users/verifies#send_verification_email'
      put '' => 'users/verifies#confirm_verification'
    end
  end
end

ActionDispatch::Routing::Mapper.send :include, ActionDispatch::Routing::Mapper::Authenticable
