class PNService
  DEFAULT_NOTIFY_HOUR = 8
  DEFAULT_NOTIFY_MINUTE = 30
  NOTIFICATION_TEMPLATE = {
    'alert' => {
      'body' => 'Bob wants to play poker',
      'action-loc-key' => 'OPEN'
    }
  }

  class << self
    attr_accessor :logger

    def configure
      yield self
    end
  end

  delegate :logger, to: 'self.class'

  def initialize(args)
    @default_hour = args[:default_hour] || DEFAULT_NOTIFY_HOUR
    @default_minute = args[:default_minute] || DEFAULT_NOTIFY_MINUTE
    @messages = (YAML.load_file File.join Rails.root, 'config', 'astro.yml')['messages']
  end

  def run
    logger.info "Running PN messaging service..."
    logger.info "Timezones to process: #{time_zones_to_process} for #{tokens.count} token(s)"
    notifications = tokens.map {|t| Mobile::APNS::Notification.new t.first, pick_message(t.last)}
    Mobile::APNS.send_notifications notifications
    logger.info "PN messaging complete."
  end

  def time_zones_to_process
    @time_zones_to_process ||= ActiveSupport::TimeZone::MAPPING.values.select do |x|
      time = Time.now.in_time_zone(x)
      time.hour.eql?(@default_hour) && (@default_minute - 5..@default_minute + 5).include?(time.min)
    end
  end

  def tokens
    @tokens ||= Persistence::PushSubscription.any_in(device_time_zone:
      time_zones_to_process).pluck(:device_token, :device_locale)
  end

  def pick_message locale
    locale = @messages.has_key?(locale) ? locale : @messages.keys.first
    res = NOTIFICATION_TEMPLATE.dup
    res['alert']['body'] = @messages[locale].shuffle.first
    res
  end

  def on_timeout
    @timeout = true
  end
end
