class JobMonitoringService
  include MisUtils
  delegate :polling_interval, :logger, to: 'self.class'

  class << self
    attr_accessor :polling_interval, :logger
    def configure; yield self end
  end

  def initialize args = {}
    @name = args['name']
  end

  def start
    @processing = true
    logger.info "Starting JobMonitoringService ..."
    @thread = Thread.new {do_monitor}
    logger.info "... started."
  end

  def do_monitor
    while @processing do
      RedisDb.client.keys("#{MIS_JOB_PREFIX}_*").each {|k| ensure_job_lifetime k}
      sleep polling_interval
    end
  end

  def ensure_job_lifetime job_id
    job_json = RedisDb.client.get job_id
    if job_json
      job = JSON.parse job_json
      created_at = DateTime.parse job['created_at']
      klass = job['handler_class'].constantize
      lifetime = DateTime.now - created_at
      if lifetime > klass.processing_time_limit
        logger.info "Job #{job_id} :: #{job_json} has exceeded its processing_time_limit! Excess is #{lifetime} seconds."
      end
    end
  rescue e
    logger.error "ensure_job_lifetime :: #{e}"
  end

  def stop
    logger.info "Stopping JobMonitoringService ..."
    @processing = false
    Thread.kill @thread
    logger.info "... stopped."
  end
end
