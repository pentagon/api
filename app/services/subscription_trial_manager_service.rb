class SubscriptionTrialManagerService

  include AstroNotificationUtils

  BEFORE_EXPIRY_MAIL_PERIOD = 2.days
  SUBSCRIPTION_TYPE = 'astro'

  class << self
    attr_accessor :logger

    def configure
      yield self
    end
  end

  delegate :logger, to: "self.class"

  def initialize(args = {})
    # noop
  end

  def run
    logger.info "Service run start."
    current_time = Time.now
    subscriptions = ::Persistence::Subscription.where({
      status: "active",
      is_internal_trial: true,
    })
    subscriptions.each do |subscription|
      trial_end_date = subscription.created_at + subscription.trial_length
      if current_time >= trial_end_date
        expire_subscription(subscription)
      elsif current_time >= trial_end_date - BEFORE_EXPIRY_MAIL_PERIOD
        send_before_expiry_mail(subscription)
      end
    end
    logger.info "Service run end."
  rescue Exception => e
    logger.error "Service crashed: #{e.message}"
  end

private

  def get_mailer(subscription)
    AstroMailer if subscription.type == SUBSCRIPTION_TYPE
  end

  def send_before_expiry_mail(subscription)
    if subscription.notification_status.nil?
      logger.info "[#{subscription.uri}] Send before expiry mail."
      send_in_advance_expiration_notification(subscription)
      mailer = get_mailer(subscription)
      mailer.trial_near_expiry(subscription).deliver if mailer
      subscription.update_attributes({notification_status: 'before_trial_expiry'})
    end
  end

  def send_expiry_mail(subscription)
    unless subscription.notification_status == 'trial_expiry'
      logger.debug "[#{subscription.uri}] Send expiry mail."
      send_expiration_notification(subscription)
      mailer = get_mailer(subscription)
      mailer.trial_expired(subscription).deliver if mailer
      subscription.update_attributes({notification_status: 'trial_expiry'})
    end
  end

  def expire_subscription(subscription)
    logger.info "[#{subscription.uri}] Expire."
    send_expiry_mail(subscription)
    subscription.update_attributes(status: "cancelled")
  end
end
