# require 'thread/pool'
require 'timeout'

class MisService
  include MisUtils
  delegate :beanstalk_host, :callback_tubes, :logger, :job_logger, :ts, :tag, to: 'self.class'

  class << self
    attr_accessor :beanstalk_host, :callback_tubes, :logger, :job_logger
    def configure; yield self end

    def enqueue command
      bt = Beaneater::Pool.new [beanstalk_host]
      tube = bt.tubes[command.tube]
      data = compile_and_register_command! command
      data['dest_job_tube_name'] = tube.name
      job_logger.info job: data[:job], tube: tube.name, body: data
      res = tube.put data.to_json, pri: command.priority
      logger.debug "[#{tag} :: #{ts}] Job '#{data}' enqueued to tube: #{tube.name}, response: '#{res}'"
      bt.close
      res['job_id']
    end

    def compile_and_register_command! command
      res = command.as_json
      res['job_id'] = "mis_job_#{SecureRandom.hex(8)}"
      RedisDb.client.set res['job_id'], command.callback_data.to_json
      res
    end

    def ts; DateTime.now.strftime "%Y-%m-%d %H:%M:%S:%L" end
    def tag; name end
  end

  def initialize args = {}
    init_connections
  end

  def start
    @processing = true
    @thread = Thread.new {listen}
    # @pool = Thread.pool 8
    logger.info "[#{tag} :: #{ts}] Started."
  end

  def stop
    logger.info "[#{tag} :: #{ts}] Stopping..."
    @processing = false
    @bt.close
    Thread.kill @thread
    # @pool.shutdown
    logger.info "[#{tag} :: #{ts}] Stopped."
  end

  def init_connections
    logger.info "[#{tag} :: #{ts}] Starting ..."
    @bt = Beaneater::Pool.new [beanstalk_host]
    @bt.tubes.watch! *callback_tubes
    tube_names = @bt.tubes.watched.collect(&:name).join ', '
    logger.info "[#{tag} :: #{ts}] Watching tubes: #{tube_names}"
  end

  def listen
    while @processing do
      begin
        job = @bt.tubes.reserve
        process_job job
        logger.info "[#{tag} :: #{ts}] Job ID: #{job.id} is sent to processing. Body: '#{job.body}'"
        job.delete
      rescue Beaneater::NotConnected
        logger.info "[#{tag} :: #{ts}] Listener lost connection to beanstalkd! Trying to reconnect..."
        init_connections
      rescue Exception => e
        logger.error "[#{tag} :: #{ts}] Listener got exception:\n#{e}"
      end
    end
  end

  def process_job job
    res = JSON.parse job.body
    job_logger.info job: res['job'], tube: job.tube, body: res
    if res['job_id']
      finalizer_data = extract_finalizer_for_job res['job_id']
      finalizer_class = finalizer_data['handler_class'].constantize
      finalizer_method = finalizer_data['method']
      finalizer_command = finalizer_data['type'].constantize
      begin
        Timeout.timeout(60) {finalizer_class.send finalizer_method, res}
      rescue Timeout::Error
        logger.error "[#{tag} :: #{ts}] process_job failed to complete a job '#{job}' within 60 seconds!"
      end
    else
      logger.info "[#{tag} :: #{ts}] process_job got invalid job from tube: '#{job.tube}' with no job_id: #{res}"
    end
  rescue Exception => e
    logger.error "[#{tag} :: #{ts}] process_job for job_id: '#{res['job_id']}', data: '#{res}"
    logger.error "[#{tag} :: #{ts}] process_job for job_id: '#{res['job_id']}', exception:\n#{e}\nat: #{e.backtrace.first(4)}"
  end

  def extract_finalizer_for_job id
    data = RedisDb.client.get id
    logger.error "Cannot fetch data from Redis for key '#{id}'" unless data
    res = JSON.parse data
    logger.debug "[#{tag} :: #{ts}] Got finalizer '#{res}' for job_id: '#{id}'"
    RedisDb.client.del id
    res
  end
end
