module MisUtils
  MIS_JOB_PREFIX = 'mis_job'

  def new_mis_job_id
    "#{MIS_JOB_PREFIX}_#{SecureRandom.hex(8)}"
  end
end
