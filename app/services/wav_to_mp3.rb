class WavToMp3
  def self.convert track_uri
    track = Persistence::Track.find_by uri: track_uri
    wav =  "#{track.track_path_without_ext}.wav"
    mp3 =  "#{track.track_path_without_ext}.mp3"
    meta = {
      title: track.title,
      artist: track.artist.display_name,
      album: track.album,
      genre: track.genre
    }
    opt = meta.map {|key, value| "-metadata #{key}='#{meta[key]}'" }.join(' ')
    result = system "avconv -y -i #{wav} -vn -ac 2 #{opt} #{mp3}"
    track.update_attribute(:track_status, 0) if result
  end
end

