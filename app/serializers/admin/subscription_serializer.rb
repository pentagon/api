class Admin::SubscriptionSerializer < BaseSerializer

private

  def json_hash(resource)
    {
      id: resource.uri,
      agent: resource.agent,
      country: resource.country,
      created_at: resource.created_at,
      is_dummy: resource.is_dummy,
      is_usable: resource.usable?,
      is_internal_trial: resource.is_internal_trial,
      last_pay_date: resource.last_pay_date,
      outstanding_balance: resource.outstanding_balance,
      payment_provider_subscription_id: resource.payment_prov_id,
      price_amount: resource.price_cents,
      price_currency: resource.currency,
      repeat_frequency: resource.repeat_frequency,
      repeat_period: resource.repeat_period,
      status: resource.status,
      trial_cycles: resource.trial_cycles,
      trial_frequency: resource.trial_frequency,
      trial_period: resource.trial_period,
      trial_price: resource.trial_price,
      type: resource.type,
      user_id: resource.user_uri
    }
  end

end
