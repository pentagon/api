class Admin::UserSerializer < BaseSerializer

  private

  def json_hash(resource)
    subscription_types = SubscriptionTemplate.all.map(&:type).uniq
    subscriptions = subscription_types.map do |type|
      resource.subscription(type)
    end
    subscriptions = subscriptions.compact.map do |subscription|
      {type: subscription.type, id: subscription.uri}
    end
    {
      id: resource.uri,
      address: resource.contact_info.address,
      available_credits: UserAccounting.credit_bundle_count(resource),
      billing_address: resource.billing_address_hash,
      biography: resource.personal_info.biography,
      city: resource.contact_info.city,
      contact_info: resource.contact_info,
      country: resource.contact_info.country,
      created_at: resource.created_at,
      date_of_birth: resource.personal_info.date_of_birth,
      display_name: resource.display_name,
      email: resource.email,
      first_name: resource.personal_info.first_name,
      gender: resource.personal_info.gender,
      image_url: resource.image_url,
      is_admin: resource.admin?,
      is_superuser: resource.superuser?,
      last_name: resource.personal_info.last_name,
      nick_name: resource.personal_info.nick_name,
      personal_info: resource.personal_info,
      phone: resource.contact_info.phone,
      postal_code: resource.contact_info.postal_code,
      roles: resource.roles,
      site_url: resource.contact_info.site_url,
      slug: resource.slug,
      stage_name: resource.stage_name,
      state: resource.contact_info.state,
      street: resource.contact_info.street,
      subscriptions: subscriptions
    }
  end


end
