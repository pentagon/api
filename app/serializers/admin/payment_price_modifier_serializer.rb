class Admin::PaymentPriceModifierSerializer < BaseSerializer

private

  def json_hash(resource)
    {
      id: resource.code,
      code: resource.code,
      created_at: resource.created_at,
      description: resource.description,
      item_type: resource.digital_goods_type,
      item_id: resource.digital_goods_id,
      max_use_count: resource.max_use_count,
      use_count: ::Persistence::PaymentOrder.processed.where(discount_code: resource.code).count,
      name: resource.name,
      operation: resource.operation,
      valid_date_from: resource.valid_date_from,
      valid_date_till: resource.valid_date_till,
      value: resource.value
    }
  end

end
