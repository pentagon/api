class Admin::PaymentOrderSerializer < BaseSerializer

private

  def json_hash(resource)
    {
      id: resource.uri,
      agent: resource.agent,
      created_at: resource.created_at,
      department_code: resource.department_code,
      increment_code: resource.increment_code,
      invoice_id: resource.invoice_id,
      ip_address: resource.ip_address,
      item_description: resource.item.try(:digital_goods_reporting_description),
      item_id: resource.item_uri,
      item_provider_item_id: resource.item_provider_item_id,
      item_provider_name: resource.item_provider_name,
      item_provider_response: resource.item_provider_response,
      item_title: resource.item_title,
      item_type: resource.item_type,
      nominal_code: resource.nominal_code,
      payment_price_modifier_id: resource.discount_code,
      payment_provider_name: resource.payment_provider_name,
      payment_provider_payment_id: resource.payment_provider_payment_id,
      payment_provider_payment_token: resource.payment_provider_payment_token,
      payment_provider_response: resource.payment_provider_response,
      price_amount: resource.price_amount,
      price_currency: resource.price_currency,
      purchase_country: resource.purchase_country,
      quantity: resource.quantity,
      refunded_payment_id: resource.refunded_payment_id,
      status: resource.status,
      user_id: resource.user_uri
    }
  end

end
