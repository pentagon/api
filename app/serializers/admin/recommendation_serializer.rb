class Admin::RecommendationSerializer < BaseSerializer

private

  def json_hash(resource)
    tracks = resource.tracks.sort { |a, b| a['d'] <=> b['d'] }
    {
      id: "#{resource.territory}-#{resource.track_uri}",
      created_at: resource.created_at,
      has_error: resource.has_error,
      is_processed: resource.is_processed,
      seed_id: resource.track_uri,
      updated_at: resource.updated_at,
      track_ids: tracks.map { |track| {id: track['uri'], distance: track['d']} }
    }
  end

end
