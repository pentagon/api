class Admin::FullTrackRequestSerializer < BaseSerializer

private

  def json_hash(resource)
    {
      id: resource.uri,
      agent: resource.agent,
      artist_id: resource.artist_uri,
      artist_name: resource.artist_name,
      country: resource.country,
      created_at: resource.created_at,
      environment: resource.environment,
      error_message: nil,
      ip_address: resource.ip_address,
      success: resource.success,
      track_id: resource.track_uri,
      track_name: resource.track_name,
      type: resource.type,
      user_id: resource.user_uri,
      user_email: resource.user_email
    }
  end

end
