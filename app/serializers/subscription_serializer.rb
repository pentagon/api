class SubscriptionSerializer < BaseSerializer

private

  def json_hash(resource)
    limit = resource.custom_options["labeled_tracks_limit"].to_i
    bonus = resource.custom_options["bonus_track_requests"].to_i
    trial_end_date = resource.created_at + resource.trial_length if resource.is_internal_trial
    hash = {
      id: resource.uri,
      country: resource.country,
      created_at: resource.created_at,
      currency: resource.currency,
      is_trial: resource.is_internal_trial,
      is_usable: resource.usable?,
      last_pay_date: resource.last_pay_date,
      next_payment_date: resource.next_payment_date,
      price: resource.price,
      price_cents: resource.price_cents,
      repeat_frequency: resource.repeat_frequency,
      repeat_period: resource.repeat_period,
      status: resource.status,
      track_requests: resource.custom_options["track_requests"],
      tracks_limit: limit,
      tracks_limit_bonus: bonus,
      tracks_limit_reached: resource.tracks_limit_reached,
      trial_cycles: resource.trial_cycles,
      trial_end_date: trial_end_date,
      trial_frequency: resource.trial_frequency,
      trial_period: resource.trial_period,
      trial_price: resource.trial_price,
      type: resource.type,
      user_id: resource.user_uri
    }
    # Compatibility for some android clients.
    # TODO: Remove this.
    hash[:subscription] = hash.clone
    hash
  end

  def default_options
    {
      json_root: false
    }
  end

end
