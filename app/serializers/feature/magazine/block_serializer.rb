class Feature::Magazine::BlockSerializer < ActiveModel::Serializer
  embed :ids, include: true
  attributes :id, :title, :description, :cover, :action_link_caption, :action_link_class, :button_url, :button_text, :feed, :position, :type, :randomized
  has_many :items, serializer: Feature::Magazine::BlockItemSerializer
end
