class Feature::Magazine::BlockItemSerializer < ActiveModel::Serializer
  attributes :id, :uri, :url, :cover, :name, :name_url, :subname, :subname_url, :user_score, :wishlist
  has_one :block, embed: :ids
end
