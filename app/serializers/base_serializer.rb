class BaseSerializer

  def initialize(resource, options = {})
    @resource = resource
    @options = default_options.merge(options)
  end

  def as_json(options = {})
    options.reverse_merge!(@options)
    result = resource_plural ? as_json_plural(options) : as_json_singular(options)
    json_root = fetch_json_root(options)
    result = {json_root => result} if json_root.present?
    result = add_meta(result, options)
    result
  end

private

  def fetch_json_root(options)
    if options[:json_root]
      json_root = options[:json_root] if options[:json_root].is_a?(String) || options[:json_root].is_a?(Symbol)
      if json_root.blank?
        klass = if resource_plural
          @resource.klass if @resource.respond_to?(:klass)
        else
          @resource.class
        end
        if klass
          json_root = klass.name.demodulize.underscore
          json_root = json_root.pluralize if options[:pluralize_json_root] && !resource_plural
        end
      end
      json_root
    end
  end

  def add_meta(result, options)
    if options[:meta].present? && result.is_a?(Hash)
      meta = options[:meta]
      if options[:meta_root]
        if options[:meta_root].is_a?(String) || options[:meta_root].is_a?(Symbol)
          meta_root = options[:meta_root]
        else
          meta_root = :meta
        end
        result[meta_root] = meta
      elsif meta.is_a?(Hash)
        result.merge!(meta)
      end
    end
    result
  end

  def as_json_singular(options, resource = nil)
    resource ||= @resource
    return nil unless resource
    hash = json_hash(resource)
    hash
  end

  def as_json_plural(options)
    result = @resource.map { |resource| json_hash(resource) if resource }
    result || []
  end

  def resource_plural
    @resource.kind_of?(Enumerable)
  end

  def json_hash(resource)
    # Override it
    {}
  end

  def default_options
    {
      json_root: true,
      meta_root: true,
      pluralize_json_root: false
    }
  end

end
