class Hitmaker::ReportSerializer < ActiveModel::Serializer
  attributes :id, :hit_score, :human_report_id, :created_at,
             :hss_category, :is_private, :energy_mood, :color,
             :color_tune, :show_hit_score, :catalogue,
             :genre_cloud, :similar_tracks, :hitscore_crown,
             :track_image_url, :track_title, :track_artist,
             :genre_probability, :track_created_at

  has_one :track, embed: :ids, embed_key: :uri
  has_one :user, embed: :ids, embed_key: :uri


  def id
    object.uri
  end

  def human_report_id
    /\d+/.match(object.uri)[0]
  end

  def hit_score
    object.hit_score.try(:round) || 0
  end

  def created_at
    object.created_at
  end

  def hss_category
    object.hss_category
  end

  def is_private
    object.is_private
  end

  def energy_mood
    (object.energy_mood / 180.0 * 100).round
  end

  def color
    if color_tune == 'black'
      "#444"
    else
      color_tune
    end
  end

  def color_tune
    object.color_tune
  end

  def show_hit_score
    object.hit_score > 150
  end

  def catalogue
    case
      when object.hss_version == 2 then 'US'
      when object.hss_version == 3 then 'UK'
    end
  end

  def genre_cloud
    object.genre_cloud
  end

  def similar_tracks
    object.similar_tracks
  end

  def hitscore_crown
    object.hitscore_crown
  end

  def track_image_url
    object.track.image_url
  end

  def track_title
    object.track.title
  end

  def track_created_at
    object.track.created_at
  end

  def track_artist
    object.track.artist_name
  end

  def genre_probability
    object.genre_probability
  end

  def similar_tracks_data
    object.similar_tracks_data
  end
end
