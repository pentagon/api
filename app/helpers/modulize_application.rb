module ModulizeApplication

  private

  def modulize_application
    params[:application].camelize.constantize
  end

  def find_interaction interaction
    application = params[:application]
    full_constant = "#{application.try(:camelize)}::Authorization::#{interaction.camelize}"
    constant = full_constant.safe_constantize
    return constant if constant.present?
    Rails.logger.warn "#{full_constant} not found, falling back to Authorization::#{interaction.camelize}"
    "Authorization::#{interaction.camelize}".constantize
  end
end
