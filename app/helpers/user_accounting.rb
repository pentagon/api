class UserAccounting
  attr_reader :user

  def self.credit_bundle_count(user)
    available_credit_bundles = user.hitlogic_credits_bundles.limited.to_a + user.hitlogic_credits_bundles.unlimited.to_a
    available_credit_bundles.sum(&:amount) - available_credit_bundles.map(&:written_off_credits).flatten.sum(&:amount)
  end

  def initialize(user)
    @user = user
  end

  def write_off_credits(credits)
    aranged_bundles ||= user.hitlogic_credits_bundles.limited.to_a + user.hitlogic_credits_bundles.unlimited.to_a
    aranged_bundles.each do |credit_bundle|
      next if credit_bundle.amount_left == 0
      break if (credits -= credit_bundle.write_of(credits)).zero?
    end
  end

  def calculate!(amount)
    credit_bundle_amount = amount.to_i - self.class.credit_bundle_count(user)
    if credit_bundle_amount > 0
      b=Persistence::HitlogicCreditsBundle.digital_goods_find_by_id('bundle_3')
      b.update_attributes({amount: credit_bundle_amount.to_i, user_uri: user.uri, type: "bundle_inventory"})
    elsif credit_bundle_amount < 0
      write_off_credits(credit_bundle_amount.abs)
    end
  end     
end
