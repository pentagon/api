module AddServiceName
  def add_service_name user, source
    service_name = source.to_sym
    unless user.visited_services.include? service_name
      visited_services = user.visited_services
      visited_services <<= service_name
      user.update_attribute(:visited_services, visited_services)
    end
  end
end
