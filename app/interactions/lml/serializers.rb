module Lml::Serializers
  extend ActiveSupport::Concern

  def serialize_library lib
    {
      library_uri: lib.uri,
      name: lib.name,
      port: lib.port,
      ip_address: lib.ip_address,
      created_at: lib.created_at,
      updated_at: lib.updated_at
    }
  end

  def serialize_track track
    {
      track_uri: track.uri,
      library_uri: track.library_uri,
      title: track.title,
      metadata: track.track_metadata,
      analysis: track.analysis,
      fingerprint: track.fingerprint,
      created_at: track.created_at,
      updated_at: track.updated_at
    }
  end
end
