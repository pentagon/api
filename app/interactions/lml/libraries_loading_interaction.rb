class Lml::LibrariesLoadingInteraction < Interaction
  include Lml::Serializers
  attr_accessor :libraries

  def initialize args
    super
    @libraries = Persistence::LmlLibrary.where user_uri: @user.uri
  end

  def as_json opts = {}
    {
      libraries: libraries.map {|l| serialize_library l}
    }
  end
end
