class Lml::TracksSendAnalysisInteraction < Interaction
  def initialize args
    super
    @tracks = args[:tracks] || []
    @tracks.map! { |t| process_track t }
    command = MisCommand::LmlTrackAnalysis.new self.class.name, :callback_track, tracks: @tracks.map { |t| t[:track_uri] }
    MisService.enqueue command
  end

  def callback_track results
    Rails.logger.info results.inspect
  end

  def process_track track
    db_track = Persistence::LmlTrack.find_by(uri: track["track_uri"])
    db_track.update_attributes(raw: track["an80"])
    {
      track_uri: db_track.uri,
      status_an80: {
        an31: Persistence::LmlTrack::STATUSES[:pending],
        fgp: Persistence::LmlTrack::STATUSES[:pending]
      }
    }
  rescue
    {
      track_uri: track["track_uri"],
      status_an80: {
        an31: Persistence::LmlTrack::STATUSES[:error],
        fgp: Persistence::LmlTrack::STATUSES[:error]
      }
    }
  end

  def as_json opts = {}
    {
      tracks: @tracks
    }
  end
end
