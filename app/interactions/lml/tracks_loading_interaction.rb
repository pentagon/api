class Lml::TracksLoadingInteraction < Interaction
  include Lml::Serializers
  attr_accessor :tracks

  def initialize args
    super
    @tracks = Persistence::LmlTrack.where library_uri: args[:library_uri], user_uri: @user.uri
  end

  def as_json opts = {}
    {
      tracks: tracks.map {|t| serialize_track t}
    }
  end
end
