class Lml::TracksDeletingInteraction < Interaction
  include Lml::Serializers
  attr_accessor :tracks

  def initialize args
    super
    @tracks = args[:tracks] || []
    @library_uri = args[:library_uri]
    @tracks_to_sync = []
    @tracks.map! { |t| process_track t }
    command = MisCommand::LmlTrackDelete.new self.class.name, :callback_track, tracks: @tracks_to_sync
    MisService.enqueue command
  end

  def process_track track
    t = if @library_uri.present?
      Persistence::LmlTrack.find_by(user_uri: user.uri, uri: track[:track_uri], library_uri: @library_uri)
    else
      Persistence::LmlTrack.find_by(user_uri: user.uri, uri: track[:track_uri])
    end
    hash = ({track_uri: t.uri, library_uri: t.library_uri})
    t.destroy
    @tracks_to_sync << hash
    {
      track_uri: track[:track_uri],
      status: 'ok'
    }
  rescue
    {
      track_uri: track[:track_uri],
      status: 'error'
    }
  end

  def as_json opts = {}
    {
      tracks: tracks
    }
  end
end
