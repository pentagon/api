class Lml::LibraryCreatingInteraction < Interaction
  include Lml::Serializers
  attr_accessor :library

  def initialize args
    super
    raise InteractionErrors::WrongArgument.new "Insufficient arguments!, Got: '#{args}'" if
      args[:library_hash].blank? or args[:port].blank? or args[:name].blank?
    @library = Persistence::LmlLibrary.find_or_create_by user_uri: @user.uri, library_hash: args[:library_hash]
    @library.update_attributes ip_address: user_ip, port: args[:port], name: args[:name]
  end

  def as_json opts = {}
    serialize_library library
  end
end
