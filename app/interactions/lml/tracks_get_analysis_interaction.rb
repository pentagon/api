class Lml::TracksGetAnalysisInteraction < Interaction
  def initialize args
    super
    @tracks = Persistence::LmlTrack.where user_uri: user.uri
    @tracks = @tracks.where(library_uri: args[:library_uri]) if args[:library_uri].present?
    @tracks = @tracks.map { |t| process_track t }
  end

  def process_track track
    types = []
    %w(an31 fgp).each { |t| types << t if track.analysis["an80"][t] == Persistence::LmlTrack::STATUSES[:not_analyzed] }
    {
      track_uri: track.uri,
      an80: types
    }
  end

  def as_json opts = {}
    {
      tracks: @tracks
    }
  end
end
