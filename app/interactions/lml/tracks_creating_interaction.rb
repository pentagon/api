# {"track_uri"=>"lml:track:ajbci124wbiuqsds",
#  "library_uri"=>"asdcajsbcb",
#  "title"=>"Test title",
#  "metadata"=>
#   {"title_md5"=>"asdqwfwq",
#    "genre"=>"Folk",
#    "duration"=>132,
#    "release_date"=>"2010-01-01",
#    "release_year"=>2010,
#    "album"=>{"id"=>"", "title"=>"ddsveces"},
#    "artist"=>{"id"=>"", "title"=>"Best Of Marching Brass Volume 2 - Die Schönsten Märsche Der Blasmusik - Teil 2"},
#    "file"=>{"description"=>"MP3 Stream 128 kbps cbr", "folder_id"=>1, "folder_name"=>"ascsdvsd", "name"=>"99343269_014.mp3", "extension"=>"mp3", "file_size"=>5976465, "bit_rate"=>320}},
#  "analysis"=>{"md5"=>"106f97a18d5b0019d2fe0c5d21127bd1", "energy"=>12312.9, "bpm"=>57.78, "an80"=>{"an31"=>0, "fgp"=>0}},
#  "fingerprint"=>{"song_uri"=>"", "track_uri"=>"medianet:track:1235455"},
#  "created_at"=>"2014-03-14",
#  "updated_at"=>"2014-03-22"}

class Lml::TracksCreatingInteraction < Interaction
  def initialize args
    super
    @tracks = args[:tracks] || []
    @tracks_to_sync = []
    @tracks.map! { |t| process_track t }
    command = MisCommand::LmlTrackAdd.new self.class.name, :callback_track, tracks: @tracks_to_sync
    MisService.enqueue command
  end

  def process_track track_hash
    track = Persistence::LmlTrack.find_by(:uri => track_hash[:track_uri], :'track_metadata.title_md5' => track_hash[:metadata][:title_md5], :'analysis.md5' => track_hash[:analysis][:md5], :library_uri => track_hash[:library_uri], :user_uri => user.uri)
    if track.present?
      {
        track_uri: track.uri,
        status: (track.analyzed? ? 'ok' : 'analysis')
      }
    else
      track = Persistence::LmlTrack.create(
        uri: track_hash[:track_uri],
        title: track_hash[:title],
        user_uri: user.uri,
        library_uri: track_hash[:library_uri],
        track_metadata: track_hash[:metadata],
        analysis: track_hash[:analysis],
        fingerprint: track_hash[:fingerprint]
      )
      @tracks_to_sync << ({track_uri: track.uri, library_uri: track.library_uri})
      {
        track_uri: track.uri,
        status: 'analysis'
      }
    end
  rescue
    {
      track_uri: track_hash[:track_uri],
      status: 'error'
    }
  end

  def as_json opts = {}
    @tracks
  end
end
