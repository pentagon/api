class RecommendationLoadingInteraction < Interaction
  RequestSent = Class.new(StandardError)
  HasError = Class.new(StandardError)

  include CatalogUtils

  attr_accessor :recommendation

  def initialize params
    super
    @track_id = params[:track_id]
    @priority = params[:priority]
    @region = (@country.eql?('us') ? @country : 'uk')
    @recommendation = Persistence::Recommendation.find_or_create_by track_uri: @track_id, territory: @region
    if recommendation.unprocessed? and recommendation.has_no_error?
      send_request_recommendation
      raise RequestSent.new
    elsif recommendation.has_error?
      raise HasError.new recommendation.mis_error
    end
  end

  def send_request_recommendation
    life_time = Time.now - recommendation.updated_at
    if life_time > 15.seconds or recommendation.new?
      Rails.logger.info "Requesting recommendation for track: #{@track_id}, territory: '#{recommendation.territory}'"
      MisService.enqueue build_command
    end
    recommendation.touch
  end

  def build_command
    query = {track_uri: @track_id, region: @region, num_tracks_per_subset: 20, app: 'purchase'}
    query.update(track_url: seed_track.music_url) if seed_track and seed_track.unsigned?
    MisCommand::GetRecommendation.new RecommendationUpdatingInteraction, :process_mis_response, query, @priority
  end

  def track_uris
    @track_uris ||= recommendation.tracks.collect {|t| t['uri']}.unshift @track_id
  end

  def track_pool
    @track_pool ||= begin
      res = fetch_items_by_ids track_uris, {preview: true}
      # filter out track that are unavailable for purchase for allowed countries only, i.e. us, gb, ca
      res.reject! {|t| t.id.include?('medianet') and not t.rights[:purchase].include?(catalog)} if
        catalog == country
      res
    end
  end

  def seed_track
    @seed_track ||= track_pool.detect {|t| t.id == @track_id}
  end

  def recommendation_tracks_hash
    @recommendation_tracks_hash ||= recommendation.tracks.group_by {|t| t['uri']}
  end

  def compiled_tracks
    @compiled_tracks ||= track_pool.map do |t|
      hash = recommendation_tracks_hash.fetch(t.id, []).first
      trk = serialize_track t
      trk.update(distance: hash['d'], x: hash['x'], y: hash['y'], d: hash['d'], h: 0) if hash
      trk.update(distance: 0, is_seed: true, x: 0, y: 0, d: 0, h: 0) if trk[:id] == @track_id
      trk
    end
  end
end
