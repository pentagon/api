class Featured::BlockItemsCreatingInteraction
  include Featured::Errors
  include Featured::BlockSettings

  def initialize block, type, items
    @block, @type, @items = block, type, items

    case block.method_type.to_i
    when ARTIST_METHOD
      handle_artst_method
    when TRACKS_METHOD
      handle_tracks_method
    when RECOMMEND_METHOD
      handle_recommend_method
    end
  end

  private

    def handle_artst_method
      Persistence::Artist.in(uri: @items).all.map do |artist|
        obj = {
          uri: artist.uri,
          cover: artist.image_url,
          name: artist.display_name,
          name_url: File.join(::Settings.musicmanager_url, 'users', artist.user_uri)
        }

        @block.send(@type).create obj
      end
    end

    # TODO re-design it with generic Interaction's properties
    def handle_tracks_method
      Track.fetch(@items).map do |track|
        obj = {
          uri: track.id,
          cover: track.image_url,
          name: track.title,
          name_url: (File.join(::Settings.musicmanager_url, 'users', track.artist.user_uri, 'music', track.id) if track.artist.user_uri),
          subname: track.artist.title,
          subname_url: (File.join(::Settings.musicmanager_url, 'users', track.artist.user_uri) if track.artist.user_uri),
          url: File.join(::Settings.storage_url, track.id.split(':')) + '.' + 'mp3'
        }

        @block.send(@type).create obj
      end
    end

    def handle_recommend_method
      @items.each do |item|
        @block.send(@type).create uri: item
      end
    end

end
