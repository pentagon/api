module Featured::BlockSettings
  ARTIST_METHOD = 1
  TRACKS_METHOD = 2
  RECOMMEND_METHOD = 3

  BLOCK_ITEMS_LIMIT = 24
end
