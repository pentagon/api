class Featured::BlockUpdatingInteraction
  include Featured::Errors

  def initialize params
    @block = Persistence::MagazineBlock.find params[:id]
    raise Featured::Errors::NotFound.new unless @block

    block_params = params[:block].slice!(:items_gb, :items_us, :items_ca)
    @block.update_attributes block_params
    populate_items(params[:block])
  end

  def populate_items(set_of_items)
    set_of_items.each do |name, items|
      @block.send(name).delete_all
      FeaturedBlockSeeder.new(@block, name).seed(items)
    end
  end

end
