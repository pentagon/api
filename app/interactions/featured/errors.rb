module Featured::Errors
  class NotFound < Exception; end
  class CountryCodeMissing < Exception; end
end
