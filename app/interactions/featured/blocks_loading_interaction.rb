class Featured::BlocksLoadingInteraction < Interaction
  include Featured::Serializers
  include Featured::Errors
  include Featured::BlockSettings

  def initialize params
    super
    raise Featured::Errors::CountryCodeMissing.new unless @country.present?

    @blocks   = Persistence::MagazineBlock.find_by(type: params[:type]).to_a

    @blocks.each do |block|
      case block.method_type.to_i
        when RECOMMEND_METHOD
          block.items = fetch_recommend_items block
        else
          block.items = block.send "items_#{@country.downcase}"
      end
    end
  end

  def as_json params = {}
    serialize_blocks_for_ember @blocks
  end

  private

    def fetch_recommend_items block
      if block.fetch_realtime
        get_trending_items(block).map do |item|
          Persistence::MagazineBlockItem.new map_recommend_item(item)
        end
      else
        p = Hash.new
        p[:ids] = block.send("items_#{@country.downcase}").map { |i| i.uri }
        p[:user_id] = @user.uri if @user
        Recommend::Product.get_by_ids(p).map do |item|
          Persistence::MagazineBlockItem.new map_recommend_item(item)
        end if p[:ids].present?
      end
    end

    def get_trending_items block
      date_from = [
        Date.today.ago(1.week).to_time.to_i,
        Date.today.ago(1.month).to_time.to_i,
        Date.today.ago(1.year).to_time.to_i
      ]

      date_to = Time.now.to_i

      items = []
      date_from.each do |date|
        unless items.length == BLOCK_ITEMS_LIMIT
          params = { media: block.type, from: date, to: date_to, limit: BLOCK_ITEMS_LIMIT }
          params[:user_id] = @user.uri if @user
          items = Recommend::Product.top_by_period params
        end
      end

      items
    end

    def map_recommend_item item
      {
        uri: "recommend:#{item["media"]}:#{item["id"]}",
        cover: item['image'],
        name: item['track'],
        subname: item['artist'],
        user_score: item["user_score"],
        wishlist: item["wishlist"]
      }
    end

end
