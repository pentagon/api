class Featured::BlockCreatingInteraction
  include Featured::Errors

  def initialize params
    block_params = params.slice!(:items_gb, :items_us, :items_ca)
    @block = Persistence::MagazineBlock.create block_params
    populate_items(params)
  end

  def populate_items(set_of_items)
    set_of_items.each do |name, items|
      FeaturedBlockSeeder.new(@block, name).seed(items)
    end
  end

end
