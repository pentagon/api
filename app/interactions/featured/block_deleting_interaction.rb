class Featured::BlockDeletingInteraction
  include Featured::Errors

  def initialize id
    @block = Persistence::MagazineBlock.find id
    raise Featured::Errors::NotFound.new unless @block

    @block.delete
  end

end
