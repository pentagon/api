module Featured::Serializers
  def serialize_item item, block_id
    {
      id: item.id,
      uri: item.uri,
      url: item.url,
      cover: item.cover,
      name: item.name,
      name_url: item.name_url,
      subname: item.subname,
      subname_url: item.subname_url,
      user_score: item.user_score,
      wishlist: item.wishlist,
      block_id: block_id
    }
  end

  def serialize_block block
    {
      id: block.id,
      title: block.title,
      description: block.description,
      cover: block.cover,
      action_link_caption: block.action_link_caption,
      action_link_class: block.action_link_class,
      button_url: block.button_url,
      button_text: block.button_text,
      feed: block.feed,
      position: block.position,
      type: block.type,
      randomized: block.randomized,
      method_type: block.method_type,
      fetch_realtime: block.fetch_realtime,
      item_ids: block.items.collect(&:id)
    }
  end

  def serialize_blocks_for_ember blocks
    result = { items: [] }

    result[:blocks] = blocks.map do |block|
      result[:items] += block.items.map { |i| serialize_item(i, block.id) }
      serialize_block block
    end
    result
  end
end