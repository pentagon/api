class RecommendationUpdatingInteraction
  attr_accessor :recommendation

  class << self
    def process_mis_response response
      new response
    end
  end

  def initialize response
    if (track_uri = response['track_uri']).present?
      territory = response['region']
      @recommendation = Persistence::Recommendation.find_or_create_by track_uri: track_uri, territory: territory
      if response['status'].eql?('OK')
        Rails.logger.info "[#{self.class.name} :: #{ts}] Updating recommendation #{recommendation.uri} with mis data"
        response['results']['tracks'].present? ? update_from_response(response) : update_from_old_response(response)
      else
        Rails.logger.error "[#{self.class.name} :: #{ts}] Updating recommendation #{recommendation.uri} with mis error"
        recommendation.has_error = true
        recommendation.mis_error = response['message']
      end
      recommendation.is_processed = true
      recommendation.save
    end
  end

  def update_from_response response
    recommendation.tracks = response['results']['tracks']
    recommendation.version = 1
  end

  def update_from_old_response response
    num_tracks_per_subset = (response['num_tracks_per_subset'] or 20).to_i
    recommendation.tracks = %w(future present present_discovery present_hits past past_discovery past_hits).collect do |v|
      response['results'][v].first(num_tracks_per_subset).collect do |t|
        t.slice 'uri', 'suri', 'd', 'x', 'y'
      end
    end.flatten.sort {|a, b| a['d'] <=> b['d']}
    recommendation.version = 0
  end

  def ts; DateTime.now.strftime "%Y-%m-%d %H:%M:%S:%L" end
end
