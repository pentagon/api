module SearchFiltering
  module Artist
    extend ActiveSupport::Concern

    def artist_filter_params
      hash = {}
      must_array = artist_explicit_must_filter_array
      hash[:must] = must_array unless must_array.blank?
      hash[:must] ||= [{match_all: {}}]
      hash
    end

    def artist_explicit_must_filter_array
      res = []
      res.append({term: {'rights.explicit' => false}}) if self.explicit_level > 0
      res
    end
  end

  module Album
    extend ActiveSupport::Concern

    def album_filter_params
      hash = {}
      must_array = album_default_must_filters_array + album_explicit_must_filter_array
      hash[:must] = must_array unless must_array.blank?
      must_not_array = album_default_must_not_filters_array
      hash[:must_not] = must_not_array unless must_not_array.blank?
      hash[:must] ||= [{match_all: {}}]
      hash
    end

    def album_default_must_filters_array
      [
        {term: {"rights.purchase" => self.catalog}},
        {term: {"rights.available" => self.catalog}}
      ]
    end

    def album_explicit_must_filter_array
      res = []
      res.append({term: {'rights.explicit' => false}}) if self.explicit_level > 0
      res
    end

    def album_default_must_not_filters_array
      [
        {term: {"title" => "karaoke"}}
      ]
    end
  end

  module Track
    extend ActiveSupport::Concern

    def track_filter_params(must_filters_array = [], must_not_filters_array = [])
      hash = {}
      must_array = must_filters_array + track_default_must_filters_array + track_explicit_must_filter_array
      must_not_array = must_not_filters_array + track_default_must_not_filters_array
      hash[:must] = must_array unless must_array.blank?
      hash[:must_not] = must_not_array unless must_not_array.blank?
      hash[:must] ||= [{match_all: {}}]
      hash
    end

    def track_default_must_filters_array
      [
        {term: {"rights.mis_online" => true}},
        {term: {"rights.purchase" => self.catalog}}
      ]
    end

    def track_explicit_must_filter_array
      res = []
      if self.explicit_level > 0
        res.append({term: {'rights.explicit' => false}})
        res.append({term: {'genre' => "Children's"}}) if self.explicit_level == 2
      end
      res
    end

    def track_default_must_not_filters_array
      [
        {term: {"title" => "karaoke"}}
      ]
    end
  end
end
