module Admin::Serializers
  extend ActiveSupport::Concern

  def serialize_album(album)
    {
      id: album.id,
      artist_id: album.artist.id,
      contributors: album.contributors,
      duration: album.duration,
      explicit: album.rights.explicit,
      image_url: album.image_url,
      label: album.label,
      popularity_rank: album.popularity_rank,
      release_date: album.release_date,
      release_year: album.release_year,
      rights: serialize_rights(album.rights, %w(available purchase)),
      title: album.title,
    }
  end

  def serialize_artist(artist)
    {
      id: artist.id,
      explicit: artist.rights.explicit,
      image_url: artist.image_url,
      popularity_rank: artist.popularity_rank,
      rights: serialize_rights(artist.rights, %w(available)),
      title: artist.title,
    }
  end

  def serialize_station(station)
    {
      id: station.uri,
      is_free: station.is_free,
      country: station.country,
      created_at: station.created_at,
      seed_track_ids: station.seed_track_uris,
      title: station.title,
      user_id: station.user_uri,
    }
  end

  def serialize_track(track)
    genres = track.genre_cloud.map do |hash|
      [hash["genre_name"], hash["probability"]]
    end
    genre_cloud = Hash[genres]

    {
      id: track.id,
      album_id: track.album_uri,
      album_name: track.album_name,
      artist_id: track.artist_uri,
      artist_name: track.artist_name,
      bpm: track.bpm,
      colors: track.color_tune,
      duration: track.duration,
      energy: track.energy,
      explicit: track.rights.explicit,
      genre: track.genre,
      genre_cloud: genre_cloud,
      hit: (track.rights.hit == 1),
      hss_uk: track.hss_uk,
      item_number: track.item_number,
      label: track.label,
      mis_online: track.rights.mis_online,
      music_url: track.music_url,
      popularity: track.popularity,
      public: track.rights.public,
      release_date: track.release_date,
      release_year: track.release_year,
      rights: serialize_rights(track.rights, %w(available preview purchase radio)),
      title: track.title
    }
  end

  def serialize_playlist playlist
    {
      id: playlist.uri,
      title: playlist.title,
      track_uris: playlist.track_uris,
      items: (playlist.items || []),
      position: playlist.position,
      created_at: playlist.created_at
    }
  end

  def serialize_test_station_track(track)
    serialize_track(track).merge({
      average_rating: 2,
      current_user_rating: 3,
    })
  end

  def serialize_test_station(station)
    {
      id: station.uri,
      algoritm_version: station.algoritm_version,
      environment: station.environment,
      seed_track_id: station.seed_track_uri,
      track_ids: station.radio_queue,
    }
  end

  def serialize_rights(rights_hash, names = [])
    result = {}
    names.each do |permission|
      result[permission] ||= {}
      %w(us gb ca).each do |country|
        countries = rights_hash[permission]
        allowed = countries.present? && countries.include?(country)
        result[permission][country] = allowed
      end
    end
    result
  end

  def csv_track_columns
    [
      {title: "ID", lambda: lambda(&:id)},
      {title: "Artist", lambda: lambda(&:artist_name)},
      {title: "Title", lambda: lambda(&:title)},
      {title: "Album", lambda: lambda(&:album_name)},
      {title: "Genre", lambda: lambda(&:genre)},
      {title: "Year", lambda: lambda(&:release_year)},
      {title: "Label", lambda: lambda(&:label)},
      {title: "Energy", lambda: lambda(&:energy)},
      {title: "HSS UK", lambda: lambda(&:hss_uk)},
      {title: "Hit", lambda: ->(t) { t.rights["hit"] }},
      {title: "MN Popularity", lambda: ->(t) { t.popularity["mn_popularity"] }},
      {title: "Popularity", lambda: ->(t) { t.popularity["popularity"] }},
      {title: "Hits appearance", lambda: ->(t) { t.popularity["hits_appearance"] }},
      {title: "UK popularity", lambda: ->(t) { t.popularity["uk_popularity"] }},
      {title: "UK hits appearance", lambda: ->(t) { t.popularity["uk_hits_appearance"] }},
      {title: "US popularity", lambda: ->(t) { t.popularity["us_popularity"] }},
      {title: "US hits appearance", lambda: ->(t) { t.popularity["us_hits_appearance"] }},
      {title: "Public", lambda: ->(t) { t.rights["public"].to_s }},
      {title: "CA Avaiable", lambda: ->(t) { t.rights["available"].include?("ca").to_s }},
      {title: "CA Preview", lambda: ->(t) { t.rights["preview"].include?("ca").to_s }},
      {title: "CA Purchase", lambda: ->(t) { t.rights["purchase"].include?("ca").to_s }},
      {title: "CA Radio", lambda: ->(t) { t.rights["radio"].include?("ca").to_s }},
      {title: "GB Avaiable", lambda: ->(t) { t.rights["available"].include?("gb").to_s }},
      {title: "GB Preview", lambda: ->(t) { t.rights["preview"].include?("gb").to_s }},
      {title: "GB Purchase", lambda: ->(t) { t.rights["purchase"].include?("gb").to_s }},
      {title: "GB Radio", lambda: ->(t) { t.rights["radio"].include?("gb").to_s }},
      {title: "US Avaiable", lambda: ->(t) { t.rights["available"].include?("us").to_s }},
      {title: "US Preview", lambda: ->(t) { t.rights["preview"].include?("us").to_s }},
      {title: "US Purchase", lambda: ->(t) { t.rights["purchase"].include?("us").to_s }},
      {title: "US Radio", lambda: ->(t) { t.rights["radio"].include?("us").to_s }},
      {title: "Explicit", lambda: ->(t) { t.rights["explicit"].to_s }},
      {title: "MIS Online", lambda: ->(t) { t.rights["mis_online"].to_s }}
    ]
  end

end
