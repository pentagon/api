class Admin::AlbumLoadingInteraction < Interaction
  include Admin::Serializers

  def initialize(args)
    super
    @is_array = args[:id].nil?
    ids = args[:id].presence || args[:ids].presence
    if ids
      @resource = Album.fetch(ids)
      if @resource.blank? && !@is_array
        raise InteractionErrors::NotFound.new(ids)
      end
    elsif args[:artist_id]
      @resource = Album.search per_page: 200, index: "#{labeled_index},unsigned" do |s|
        s.filter(:term, {"artist.id" => args[:artist_id]})
        s.sort { |sort| sort.by('release_date', 'asc') }
      end
    end
  end

  def as_json(opts = {})
    if @is_array
      {albums: @resource.map { |album| serialize_album(album) }}
    else
      {album: serialize_album(@resource)}
    end
  end
end
