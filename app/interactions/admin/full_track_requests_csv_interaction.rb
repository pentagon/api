require 'csv'

class Admin::FullTrackRequestsCSVInteraction

  RECORDS_LIMIT = 10000
  VALID_TYPES = [
    :no_aggregation, :requests_per_track, :duration_per_user,
    :duration_per_user_agent
  ]

  def initialize(params = {})
    @criteria = Persistence::FullTrackRequest.load_from_params(params).limit(RECORDS_LIMIT)
  end

  def to_csv(type)
    raise ArgumentError unless VALID_TYPES.include?(type.to_sym)
    csv_data = self.send(type)
    CSV.generate force_quotes: true do |csv|
      indexes_hash = {}
      csv << csv_data[:header]
      csv_data[:rows].each { |row| csv << row }
    end
  end

private

  def no_aggregation
    header = [
      "Track ID", "Artist name", "Track title", "User email", "Success",
      "Source", "IP Address", "Country", "Date"
    ]
    @requests_no_aggregation ||= if Rails.env != "development"
      @criteria.map do |request|
        [
          request.track_uri, request.artist_name, request.track_name,
          request.user_email, request.success, request.agent, request.ip_address,
          request.country, request.created_at
        ]
      end
    else
      []
    end
    {header: header, rows: @requests_no_aggregation}
  end

  def requests_per_track
    map = %Q{
      function() {
        var track_uri = this.track_uri || "";
        var track_name = this.track_name || "";
        var artist_name = this.artist_name || "";
        var key = [track_uri, track_name, artist_name];
        emit(key, 1);
      }
    }
    reduce = %Q{
      function(key, values) {
        return Array.sum(values);
      }
    }
    header = ["Track ID", "Track title", "Artist name", "Number of streams"]
    @requests_per_track ||= if Rails.env != "development"
      @criteria.map_reduce(map, reduce).out(inline: true).map do |row|
        row["_id"] << row["value"].to_i
      end
    else
      []
    end
    {header: header, rows: @requests_per_track}
  end

  def duration_per_user
    map = %Q{
      function() {
        var user_uri = this.user_uri || "";
        var user_email = this.user_email || "";
        var key = [user_uri, user_email];
        var value = this.track_duration || #{::Persistence::FullTrackRequest::FALLBACK_TRACK_DURATION}
        emit(key, value);
      }
    }
    reduce = %Q{
      function(key, values) {
        return Array.sum(values);
      }
    }
    header = ["User ID", "User source", "User email", "Duration days", "Hours", "Minutes", "Seconds"]
    @duration_per_user ||= if Rails.env != "development"
      @criteria.map_reduce(map, reduce).out(inline: true).map do |row|
        id = row["_id"][0]
        source = id.present? ? id.split(":").first : "Anonymous"
        email = row["_id"][1]
        duration_array = precise_distance_of_time_in_words_array(row["value"].to_i)
        [id, source, email] + duration_array
      end
    else
      []
    end
    {header: header, rows: @duration_per_user}
  end

  def duration_per_user_agent
    map = %Q{
      function() {
        var user_uri = this.user_uri || "";
        var user_email = this.user_email || "";
        var agent = this.agent || "";
        var key = [user_uri, user_email, agent];
        var value = this.track_duration || #{::Persistence::FullTrackRequest::FALLBACK_TRACK_DURATION}
        emit(key, value);
      }
    }
    reduce = %Q{
      function(key, values) {
        return Array.sum(values);
      }
    }
    header = ["User ID", "User email", "Request source", "Duration days", "Hours", "Minutes", "Seconds"]
    @duration_per_user ||= if Rails.env != "development"
      @criteria.map_reduce(map, reduce).out(inline: true).map do |row|
        id = row["_id"][0]
        agent = row["_id"][2].presence || "UNKNOWN"
        email = row["_id"][1]
        duration_array = precise_distance_of_time_in_words_array(row["value"].to_i)
        [id, email, agent] + duration_array
      end
    else
      []
    end
    {header: header, rows: @duration_per_user}
  end

  def precise_distance_of_time_in_words_array(seconds)
    [[60, :seconds], [60, :minutes], [24, :hours], [1000, :days]].map{ |count, name|
      if seconds > 0
        seconds, n = seconds.divmod(count)
        "#{n.to_i} #{name}"
      end
    }.reverse
  end

end
