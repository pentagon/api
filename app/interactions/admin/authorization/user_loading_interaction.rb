class Admin::Authorization::UserLoadingInteraction < ::Authorization::UserLoadingInteraction

  def as_json(options = {})
    Admin::UserSerializer.new(@current_user).as_json(options).merge({
      authentication_token: @current_user.authentication_token
    })
  end
end
