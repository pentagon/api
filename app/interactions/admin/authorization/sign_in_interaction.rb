class Admin::Authorization::SignInInteraction < ::Authorization::SignInInteraction

  def as_json(options = {})
    Admin::UserSerializer.new(@user).as_json(options).merge({
      authentication_token: @user.authentication_token
    })
  end
end
