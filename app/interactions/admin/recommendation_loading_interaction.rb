require 'csv'

class Admin::RecommendationLoadingInteraction < RecommendationLoadingInteraction

  include Admin::Serializers

  def initialize params
    super
  rescue RequestSent, HasError
  end

  def to_csv
    return if recommendation.has_error || !recommendation.is_processed
    tracks_data = recommendation.tracks.sort { |a, b| a["d"] <=> b["d"] }
    tracks = Track.fetch(tracks_data.map { |t| t["uri"] } )
    CSV.generate force_quotes: true do |csv|
      headers = csv_track_columns.map { |column| column[:title] }
      headers = ["Distance"] + headers
      csv << headers
      tracks.each_with_index do |track, index|
        fields = csv_track_columns.map do |column|
          column[:lambda].call(track)
        end
        fields = [tracks_data[index]["d"]] + fields
        csv << fields
      end
    end

  end
end
