class Admin::PlaylistCreatingInteraction < Interaction
  include Admin::Serializers

  def initialize params
    super
    raise WrongArgument.new "Unable to create playlist for non existing user" if @user.blank?
    raise WrongArgument.new "Unable to create playlist with no params" if params[:user_playlist].blank?
    position = @user.user_playlists.count
    @playlist = @user.user_playlists.create params[:user_playlist].except(:items)
      .merge(position: position, user_uri: @user.uri)
    if params[:user_playlist][:items].present?
      @playlist.items = params[:user_playlist][:items]
      @playlist.save
    end

  end

  def as_json(opts = {})
    { playlist: serialize_playlist(@playlist) }
  end
end
