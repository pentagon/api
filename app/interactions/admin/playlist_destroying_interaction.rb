class Admin::PlaylistDestroyingInteraction < Interaction
  def initialize params
    super
    raise WrongArgument.new "Unable to destroy a playlist for non existing user" if @user.blank?
    @user.user_playlists.find_by(uri: params[:playlist_id]).try :destroy
  end
end
