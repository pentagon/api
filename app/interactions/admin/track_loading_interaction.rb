class Admin::TrackLoadingInteraction < Interaction
  include Admin::Serializers

  def initialize(args)
    super
    @is_array = args[:id].nil?
    ids = args[:id].presence || args[:ids].presence
    if ids
      @resource = Track.fetch(ids)
      if @resource.blank? && !@is_array
        raise InteractionErrors::NotFound.new(ids)
      end
    elsif args[:album_id]
      @resource = Track.search per_page: 200, index: "#{labeled_index},unsigned" do |s|
        s.filter(:term, {"album.id" => args[:album_id]})
        s.sort { |sort| sort.by('item_number', 'asc') }
      end
    end
  end

  def to_csv
    CSV.generate force_quotes: true do |csv|
      csv << csv_track_columns.map { |column| column[:title] }
      @resource.each do |track|
        fields = csv_track_columns.map do |column|
          column[:lambda].call(track)
        end
        csv << fields
      end
    end
  end

  def as_json(opts = {})
    if @is_array
      {tracks: @resource.map { |track| serialize_track(track) }}
    else
      {track: serialize_track(@resource)}
    end
  end
end
