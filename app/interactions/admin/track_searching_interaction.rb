require 'csv'

class Admin::TrackSearchingInteraction < TrackSearchingInteraction
  include Admin::Serializers

  CSV_RECORDS_LIMIT = 25000
  SEARCH_INDICES = %w[labeled unsigned]

  def initialize(args)
    super
    @sort_field = args[:order_by].presence || "_score"
    @sort_direction = args[:sort].to_s.downcase.presence || "desc"
    @rights = args[:rights].presence || []
    @rights_not = args[:rights_not].presence || []
    @catalogs = Array.wrap(args[:catalogs]) & SEARCH_INDICES
    @catalogs = SEARCH_INDICES if @catalogs.blank?
    @tracks = if args[:q].present?
      track_simple_search(args[:q], args[:page], args[:per_page])
    else
      track_advanced_search(args, args[:page], args[:per_page])
    end
  end

  def track_simple_search(query_string, page = 1, per_page = 20)
    results = Track.search page: page, per_page: per_page, index: indices_to_search_in.compact.join(",") do |s|
      s.query do |q|
        multi_match_hash = {
          query: query_string,
          fields: ["artist.title", "title"],
          type: "cross_fields",
          minimum_should_match: "33%"
        }
        if @sort_field == "_score"
          dsl = {
            query: {
              multi_match: multi_match_hash
            },
            functions: [
              {
                filter: {
                  term: {
                    "rights.hit" => 1
                  }
                },
                boost_factor: 2
              },
              {
                filter: {
                  not: {
                    term: {
                      "popularity.popularity" => 0
                    }
                  }
                },
                field_value_factor: {
                  field: "popularity.popularity",
                  modifier: "log1p",
                  factor: 1.5
                }
              }
            ]
          }
          q.raw_dsl(:function_score, dsl)
        else
          q.raw_dsl(:multi_match, multi_match_hash)
        end
      end
      s.filter(:bool, track_filter_params)
      s.sort do |sort|
        sort.by(@sort_field, @sort_direction)
      end
    end
  end

  def as_json opts = {}
    {
      tracks: @tracks.map {|t| serialize_track t},
      meta: {
        total_count: @tracks.total_count
      }
    }
  end

  def to_csv

    CSV.generate force_quotes: true do |csv|
      csv << csv_track_columns.map { |column| column[:title] }
      @tracks.first(CSV_RECORDS_LIMIT).each do |track|
        fields = csv_track_columns.map do |column|
          column[:lambda].call(track)
        end
        csv << fields
      end
    end
  end

private

  def track_default_must_filters_array
    arr = []
    @rights.each do |key, value|
      if value.is_a?(Array)
        value.each { |catalog| arr << {terms: {"rights.#{key}" => [catalog]}} }
      else
        arr << {term: {"rights.#{key}" => value}}
      end
    end
    arr
  end

  def track_default_must_not_filters_array
    arr = [{term: {"title" => "karaoke"}}]
    @rights_not.each do |key, catalogs|
      catalogs.each { |catalog| arr << {terms: {"rights.#{key}" => [catalog]}} }
    end
    arr
  end

  def explicit_must_filter_array
    []
  end

  def indices_to_search_in
    @catalogs
  end

  def sort_options
    {field: @sort_field, direction: @sort_direction}
  end

end
