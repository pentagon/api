class Admin::PlaylistsLoadingInteraction < Interaction
  include Admin::Serializers

  def initialize params
    super
    raise WrongArgument.new "Unable to load playlists for non existing user" if @user.blank?
    @playlists = @user.user_playlists.asc(:position)
  end

  def as_json params = {}
    { playlists: @playlists.map {|p| serialize_playlist p} }
  end
end
