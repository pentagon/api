class Admin::VideosLoadingInteraction < Interaction
  attr_accessor :videos

  def initialize(args)
    super
    @videos = Persistence::Video.any_in uri: args[:ids]
  end

  def serialize_video video
    {
      id: video.id,
      url: video.uri,
      video_id: video.video_id,
      title: video.title,
      description: video.description
    }
 end

  def as_json opts = {}
    {
      videos: videos.map {|v| serialize_video v}
    }
  end
end
