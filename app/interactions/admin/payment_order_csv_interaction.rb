require 'csv'

class Admin::PaymentOrderCSVInteraction

  RECORDS_LIMIT = 10000

  def initialize(params = {})
    @criteria = ::Persistence::PaymentOrder.load_from_params(params).limit(RECORDS_LIMIT)
  end

  def to_csv
    CSV.generate force_quotes: true do |csv|
      indexes_hash = {}
      csv << csv_columns.map.with_index do |column, index|
        indexes_hash[column[:key]] = index if column[:key]
        column[:title]
      end
      @criteria.each do |payment|
        fields = csv_columns.map do |column|
          column[:lambda].call(payment) if column[:lambda]
        end
        csv_columns.each_with_index do |column, index|
          fields[index] = column[:remap_lambda].call(indexes_hash, fields) if column[:remap_lambda]
        end
        csv << fields
      end
    end
  end

private

  def csv_columns
    @csv_columns ||= [
      {title: "API Payment Order ID", lambda: lambda(&:uri)},
      {title: "Payment Provider Name", lambda: lambda(&:payment_provider_name)},
      {title: "Date", lambda: ->(p) { p.created_at.in_time_zone('London').to_date.to_s }},
      {title: "Time", lambda: ->(p) { p.created_at.in_time_zone('London').to_s(:time) }},
      {title: "Country", lambda: lambda(&:purchase_country)},
      {title: "Status", lambda: lambda(&:status)},
      {title: "Agent", lambda: lambda(&:agent)},
      {title: "Tunehog customer number", lambda: ->(p) { "#{p.user_uri} #{p.ip_address}" }},
      {title: "Subject", lambda: ->(p) { "#{p.item_type.split('/').last.camelize}: #{p.item_title}" }},
      {title: "Discount code", lambda: lambda(&:discount_code)},
      {title: "Sales", lambda: ->(p) { p.price_amount.to_f / 100 }, key: :sales_amount},
      {title: "VAT", lambda: ->(p) { vat_for_payment(p).to_f / 100 }, key: :vat_amount},
      {title: "Net sales", remap_lambda: lambda do |keys, rows|
        sales = rows[keys[:sales_amount]]
        vat = rows[keys[:vat_amount]]
        sales - vat if sales && vat
      end},
      {title: "Currency", lambda: ->(p) { p.price_currency }},
      {title: "Transaction ID", lambda: lambda(&:payment_provider_payment_id)},
      {title: "Transaction refund ID", lambda: ->(p) { p.payment_provider_order_info["parent_transaction_id"] if p.refund? }},
      {title: "Department code", lambda: lambda(&:department_code)},
      {title: "Nominal code", lambda: lambda(&:nominal_code)},
      {title: "Increment code", lambda: lambda(&:increment_code)},
      {title: "Customer name", lambda: lambda do |p|
        fields = ["Salutation", "FirstName", "MiddleName", "LastName", "Suffix"]
        hash = p.payment_provider_order_info["PaymentTransactionDetails"]
        if hash && hash["PayerInfo"] && hash["PayerInfo"]["PayerName"]
          fields.map { |f| hash["PayerInfo"]["PayerName"][f] }.reject(&:blank?).join(" ")
        end
      end},
      {title: "Customer email", lambda: lambda do |p|
        hash = p.payment_provider_order_info["PaymentTransactionDetails"]
        if hash && hash["PayerInfo"]
          hash["PayerInfo"]["Payer"]
        end
      end},
      {title: "Customer address", lambda: lambda do |p|
        string = ""
        fields = ["Street1", "Street2", "CityName", "StateOrProvince", "PostalCode", "Country"]
        hash = p.payment_provider_order_info["PaymentTransactionDetails"]
        if hash && hash["PayerInfo"] && hash["PayerInfo"]["Address"]
          string << fields.map { |f| hash["PayerInfo"]["Address"][f] }.reject(&:blank?).join(" ")
        end
        string.presence || "N/A"
      end},
      {title: "Gross amount", lambda: lambda do |p|
        hash = p.payment_provider_order_info["PaymentTransactionDetails"]
        if hash && hash["PaymentInfo"]
          hash["PaymentInfo"]["GrossAmount"].to_f
        end
      end, key: :gross_amount},
      {title: "Fee amount", lambda: lambda do |p|
        hash = p.payment_provider_order_info["PaymentTransactionDetails"]
        if hash && hash["PaymentInfo"]
          hash["PaymentInfo"]["FeeAmount"].to_f
        end
      end, key: :fee_amount},
      {title: "Net amount", remap_lambda: lambda do |keys, row|
        gross = row[keys[:gross_amount]]
        fee = row[keys[:fee_amount]]
        gross - fee if gross && fee
      end},
      {title: "Medianet order ID", lambda: lambda do |p|
        hash = p.item_provider_response["Order"]
        hash["ID"] if hash
      end},
      {title: "Item provider track info", lambda: lambda do |p|
        hash = p.item_provider_item_info["Track"]
        if hash
          "#{hash["MnetId"]} - #{hash["Title"]}"
        end
      end},
      {title: "Item provider artist info", lambda: lambda do |p|
        hash = p.item_provider_item_info["Track"]
        if hash && hash['Artist']
          "#{hash["Artist"]["MnetId"]} - #{hash["Artist"]["Name"]}"
        end
      end},
      {title: "Item provider album info", lambda: lambda do |p|
        hash = p.item_provider_item_info["Track"]
        if hash && hash['Album']
          "#{hash["Album"]["MnetId"]} - #{hash["Album"]["Title"]} (#{hash["Album"]["Label"]})"
        end
      end},
      {title: "Item provider set price", lambda: lambda do |p|
        hash = p.item_provider_item_info["Track"]
        if hash && hash["PriceTag"]
          hash["PriceTag"]["IsSetPrice"]
        end
      end},
      {title: "Item provider price amount", lambda: lambda do |p|
        hash = p.item_provider_item_info["Track"]
        if hash && hash["PriceTag"]
          hash["PriceTag"]["Amount"]
        end
      end},
      {title: "Item provider wholesale price", lambda: lambda do |p|
        hash = p.item_provider_item_info["Track"]
        if hash && hash["PriceTag"]
          hash["PriceTag"]["WholesalePrice"] unless p.refund?
        end
      end, key: :wholesale_price},
      {title: "Item provider publishing cost", lambda: lambda do |p|
        hash = p.item_provider_item_info["Track"]
        if hash && hash["PriceTag"]
          hash["PriceTag"]["PublishingCost"] unless p.refund?
        end
      end, key: :publishing_cost},
      {title: "Total cost", remap_lambda: lambda do |keys, row|
        wholesale_price = row[keys[:wholesale_price]].to_f
        publishing_cost = row[keys[:publishing_cost]].to_f
        wholesale_price + publishing_cost if wholesale_price || publishing_cost
      end},
    ]
  end

  def vat_for_payment(payment)
    return 0 unless payment.purchase_country
    eu_countries = %W[AT BE BG HR CY CZ DK EE FI FR DE GR HU IE IT LV LT LU MT
      NL PL PT RO SK SI ES SE GB]
    vat_percentage = eu_countries.include?(payment.purchase_country.upcase) ? 0.2 : 0
    (vat_percentage * payment.price_amount / (1 + vat_percentage)).ceil
  end

end
