require 'csv'

class Admin::SubscriptionCSVInteraction

  RECORDS_LIMIT = 10000

  def initialize(params = {})
    @criteria = ::Persistence::Subscription.load_from_params(params).limit(RECORDS_LIMIT)
  end

  def to_csv
    CSV.generate force_quotes: true do |csv|
      indexes_hash = {}
      csv << csv_columns.map.with_index do |column, index|
        indexes_hash[column[:key]] = index if column[:key]
        column[:title]
      end
      @criteria.each do |payment|
        fields = csv_columns.map do |column|
          column[:lambda].call(payment) if column[:lambda]
        end
        csv_columns.each_with_index do |column, index|
          fields[index] = column[:remap_lambda].call(indexes_hash, fields) if column[:remap_lambda]
        end
        csv << fields
      end
    end
  end

private

  def csv_columns
    @csv_columns ||= [
      {title: "ID", lambda: lambda(&:uri)},
      {title: "Paypal ID", lambda: lambda(&:payment_prov_id)},
      {title: "Type", lambda: lambda(&:type)},
      {title: "User ID", lambda: lambda(&:user_uri)},
      {title: "User email", lambda: ->(s) { s.user.present? ? s.user.email : "" }},
      {title: "Price amount", lambda: lambda(&:price)},
      {title: "Price currency", lambda: lambda(&:currency)},
      {title: "Repeat frequency", lambda: lambda(&:repeat_frequency)},
      {title: "Repeat period", lambda: lambda(&:repeat_period)},
      {title: "Trial", lambda: lambda(&:is_internal_trial)},
      {title: "Trial frequency", lambda: lambda(&:trial_frequency)},
      {title: "Trial period", lambda: lambda(&:trial_period)},
      {title: "Status", lambda: lambda(&:status)},
      {title: "Is Dummy", lambda: lambda(&:is_dummy)},
      {title: "Last Payment Date", lambda: lambda(&:last_pay_date)},
      {title: "Created at", lambda: lambda(&:created_at)},
    ]
  end

end
