class Admin::ArtistLoadingInteraction < Interaction
  include Admin::Serializers

  def initialize(args)
    super
    @is_array = args[:id].nil?
    ids = args[:id].presence || args[:ids].presence
    @resource = Artist.fetch(ids)
    if @resource.blank? && !@is_array
      raise InteractionErrors::NotFound.new(ids)
    end
  end

  def as_json(opts = {})
    if @is_array
      {artists: @resource.map { |artist| serialize_artist(artist) }}
    else
      {artist: serialize_artist(@resource)}
    end
  end
end
