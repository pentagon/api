class Admin::TestDataTrackSearchingInteraction < Interaction
  include Admin::Serializers

  INDEX_NAME = "test_data"
  FULL_TRACK_PATH = "#{Settings.storage_url}/full_mp3"

  def initialize(args)
    super
    @args = args
    @page = args[:page].to_i || 1
    @per_page = args[:per_page].to_i || 20
  end

  def as_json opts = {}
    {
      tracks: tracks_fetched.map { |t| serialize_track(t) },
      meta: {
        total_count: tracks.total_count,
      }
    }
  end

private

  def serialize_track(track)
    url = "#{FULL_TRACK_PATH}/#{Track.get_id_from_uri(track.id)}.mp3"
    super.merge(music_url: url)
  end

  def tracks
    @tracks ||= search_tracks
  end

  def tracks_fetched
    @tracks_fetched ||= Track.fetch(tracks.map(&:id))
  end

  def search_tracks
    Track.search page: @page, per_page: @per_page, index: INDEX_NAME do |s|
      s.query do |q|
        if [@args[:artist], @args[:title]].any?(&:present?)
          q.boolean do |b|
            b.must { |m| m.match "title", @args[:title] } if @args[:title].present?
            b.must { |m| m.match "artist", @args[:artist] } if @args[:artist].present?
          end
        else
          q.all
        end
      end
    end
  end
end
