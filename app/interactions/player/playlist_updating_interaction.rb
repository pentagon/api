class Player::PlaylistUpdatingInteraction < Interaction
  def initialize params
    super
    raise WrongArgument.new 'No playlist_id specified' if params[:playlist_id].blank?
    raise WrongArgument.new "Unable to update a playlist for non existing user" if @user.blank?
    @playlist = @user.user_playlists.find_by uri: params[:playlist_id]
    update_from_params params[:user_playlist]
    if params[:user_playlist][:update_position].present?
      swap_position(@playlist.position, params[:user_playlist][:update_position])
    end
  end

  def update_from_params params
    # Remove this 2 lines after migrating to new player version
    @playlist.track_uris = params[:track_uris] if params.has_key? :track_uris
    @playlist.track_uris = [] if params[:empty_uris]
    @playlist.items = params[:items] if params.has_key? :items
    @playlist.items = [] if params[:empty_uris]
    @playlist.title = params[:title] if params[:title].present?
    @playlist.save
  end

  def swap_position old_position, new_position
    ar = @playlist.class.where(user_uri: @playlist.user_uri).asc(:position).to_a
    unless ar.find{|playlist| playlist[:position] == new_position}.present?
      raise WrongArgument.new 'New position does not exist'
    end
    ar.insert(new_position, ar.delete_at(old_position))
    ar.each_with_index {|playlist, index| playlist.update_attribute :position, index}
  end
end
