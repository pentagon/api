class Player::VideoCreatingInteraction < Interaction
  include Player::Serializers

  def initialize params
    super
    raise WrongArgument.new "Unable to create video for non existing user" if @user.blank?
    @video = @user.videos.create params[:video]
  end

  def as_json params = {}
    {video: serialize_video(@video)}
  end
end
