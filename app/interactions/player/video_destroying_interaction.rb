class Player::VideoDestroyingInteraction < Interaction
  def initialize params
    super
    raise WrongArgument.new "Unable to destroy a video for non existing user" if @user.blank?
    @user.videos.find_by(uri: params[:uri]).try :destroy
  end
end
