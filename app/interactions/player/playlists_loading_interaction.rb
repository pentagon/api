class Player::PlaylistsLoadingInteraction < Interaction
  include Player::Serializers

  def initialize params
    super
    raise WrongArgument.new "Unable to create playlist for non existing user" if @user.blank?
    @playlists = @user.user_playlists.asc(:position)
  end

  def as_json params = {}
    @playlists.map {|p| serialize_playlist p}
  end
end
