module Player::Serializers
  extend ActiveSupport::Concern

  def serialize_track track
    {
      id: track.id,
      album_uri: track.album.try(:id).to_s,
      album_name: track.album.try(:title) || '',
      allowed: allowed?(track),
      artist_name: track.artist_name,
      artist_uri: track.artist_uri,
      cover_uri: track.image_url,
      rights: transform_rights(track.rights),
      status: user_feedback_for_track(track),
      track_name: track.title.to_s,
      track_url: track.music_url.to_s,
      user_uri: track.artist.try(:user_uri).to_s
    }
  end

  def serialize_video video
    {
      id: video.id,
      title: video.title,
      video: video.video_id,
      release_date: video.created_at.to_i,
      image_preview: video.image_preview('hqdefault')
    }
  end

  def serialize_playlist playlist
    {
      id: playlist.uri,
      title: playlist.title,
      track_uris: playlist.track_uris,
      items: playlist.items,
      position: playlist.position
    }
  end
end
