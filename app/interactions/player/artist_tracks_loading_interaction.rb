class Player::ArtistTracksLoadingInteraction < Interaction
  include Player::Serializers
  include Player::Utils

  def initialize params
    super
    raise WrongArgument.new 'No artist_id specified' if params[:artist_id].blank?
    @artist = fetch_items_by_ids params[:artist_id]
    raise NotFound.new params[:artist_uri], @country if @artist.blank?
  end

  def track_uris
    @track_uris ||= tracks.collect &:id
  end

  def user_feedbacks
    @user_feedbacks ||= fetch_user_feedbacks_map_for_tracks track_uris
  end

  def tracks
    @tracks ||= begin
      trks = fetch_related_for_items_by_type(@artist, 'track').to_a
      if @artist.unsigned? and trks.any?
        pers_album = Persistence::Artist.find_by uri: @artist.id
        accessible_track_uris = pers_album.tracks.accessible_for(@user).pluck(:uri)
        trks.delete_if {|t| not accessible_track_uris.include? t.id}
      end
      trks
    end
  end

  def as_json params = {}
    {
      artist_id: @artist.id,
      artist_name: @artist.title,
      tracks: tracks.map {|t| serialize_track t}
    }
  end
end
