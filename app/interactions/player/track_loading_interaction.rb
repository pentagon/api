class Player::TrackLoadingInteraction < Interaction
  include Player::Serializers
  include Player::Utils

  def initialize args
    super
    raise WrongArgument.new 'No track_id specified' if args[:track_id].blank?
    @track = fetch_items_by_ids args[:track_id]
    raise NotFound.new args[:track_id] if @track.blank?
  end

  def user_feedbacks
    @user_feedbacks ||= fetch_user_feedbacks_map_for_tracks @track.id
  end

  def as_json opts = {}
    serialize_track @track
  end
end

