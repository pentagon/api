class Player::PlaylistLoadingInteraction < Interaction
  include Player::Serializers

  def initialize params
    super
    raise WrongArgument.new "Unable to load playlist for non existing user" if @user.blank?
    raise WrongArgument.new "Unable to create playlist with no id" if params[:playlist_id].blank?
    @playlist = @user.user_playlists.find_by(uri: params[:playlist_id]) or @user.user_playlists.asc(:position).first
    raise NotFound.new params[:playlist_id], 'unsigned' if @playlist.blank?
  end

  def as_json params = {}
    serialize_playlist @playlist
  end
end
