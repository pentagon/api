class Player::ArtworkLoadingInteraction < Interaction
  def initialize params = {}
    super
    @artist = fetch_items_by_ids params[:artist_id]
    raise NotFound.new params[:artist_id], @country if @artist.blank?
  end

  def galleries
    if @artist.labeled?
      @artist.albums.collect {|a| {title: '', description: '', image_url: @artist.image_url_from_uri(a['id'])}}
    else
      if (a = ::Persistence::Artist.find_by uri: @artist.id).present?
        a.public_galleries.flatten.collect do |a|
          {title: a.title, description: a.description, image_url: @artist.image_url_from_uri(a['uri'])}
        end
      end
    end
  end

  def as_json params = {}
    {
      artist_id: @artist.id,
      galleries: galleries
    }
  end
end
