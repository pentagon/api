class Player::VideoGalleryLoadingInteraction < Interaction
  def initialize params
    track_uri = params[:track_uri]
    track = Persistence::Track.find_by(uri: track_uri)
    raise NotFound.new "Unable to fetch videos for non existing Track: '#{track_uri}'" if track.blank?
    @linked_videos = track.videos
    @user_videos = (track.user.videos + @linked_videos).uniq
  end

  def as_json opts = {}
    {
      linked_videos: @linked_videos,
      artist_videos: @user_videos
    }
  end
end
