class Player::AlbumLoadingInteraction < Interaction
  include Player::Serializers
  include Player::Utils

  def initialize params
    super
    @album = fetch_items_by_ids params[:uri]
    raise NotFound.new params[:uri], @country if @album.blank?
  end

  def track_uris
    @track_uris ||= tracks.collect &:id
  end

  def user_feedbacks
    @user_feedbacks ||= fetch_user_feedbacks_map_for_tracks track_uris
  end

  def tracks
    @tracks ||= begin
      trks = fetch_related_for_items_by_type(@album, 'track').to_a
      if @album.unsigned?
        pers_album = Persistence::Album.find_by uri: @album.id
        accessible_track_uris = pers_album.tracks.accessible_for(@user).pluck(:uri)
        trks.delete_if {|t| not accessible_track_uris.include? t.id}
      end
      trks
    end
  end

  def as_json params = {}
    {
      id: @album.id,
      title: @album.title,
      image_url: @album.image_url,
      artist: @album.artist,
      tracks: tracks.map {|t| serialize_track t}
    }
  end
end
