module Player::Utils
  extend ActiveSupport::Concern

  def user_feedbacks
    raise StandardError.new 'Method user_feedbacks has to be overriden!'
  end

  def user_feedback_for_track track
    user_feedbacks[track.id] if user_feedbacks
  end

  def fetch_user_feedbacks_map_for_tracks uris
    if uris.blank? or @user.blank?
      {}
    else
      Hash[*Persistence::UserTrackFeedback.where(user_uri: @user.uri)
        .any_in(track_uri: uris).pluck(:track_uri, :status).flatten]
    end
  end
end
