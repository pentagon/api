class Player::StartupLoadingInteraction < Interaction
  include Player::Serializers
  include Player::Utils

  def initialize params
    super
    raise WrongArgument.new "Unable to create startup for non existing user" if @user.blank?
  end

  def user_feedbacks
    @user_feedbacks ||= fetch_user_feedbacks_map_for_tracks merged_track_uris
  end

  def track_pool
    @track_pool ||= fetch_items_by_ids merged_track_uris.compact
  end

  def serialize_track_pool
    track_pool.map {|t| serialize_track t}
  end

  def video_pool
    user_playlists.map {|pl| pl.items.select {|x| x['type'] == 'video'}}.flatten.uniq
  end

  def merged_track_uris
    @merged_track_uris ||= (track_uris + history_uris + favourites_uris + items_uris).flatten.uniq
  end

  def serialize_playlists
    user_playlists.map {|playlist| serialize_playlist playlist}
  end

  def serialize_videos
    videos.map {|video| serialize_video video}
  end

  def user_playlists
    @user_playlists ||= @user.user_playlists.asc(:position)
  end

  def track_uris
    @track_uris ||= user_playlists.pluck(:track_uris).compact.flatten.uniq
  end

  def items_uris
    @items_uris ||= user_playlists.inject([]) do |res, pl|
      pl.items.each {|x| res.append(x['track_id']) if x['type'] == 'track'}
      res
    end
  end

  def history_uris
    @history_uris ||= Persistence::TrackStatistic.by_user(@user.uri).desc(:created_at).limit(100).pluck(:track_uri)
  end

  def serialize_history
    @history_uris.chunk { |e| e }.map { |x| { track_uri: x.to_a.first, count: x.to_a.last.length } }
  end

  def favourites_uris
    @favotites_uris ||= Persistence::UserTrackFeedback.where(user_uri: @user.uri, status: 'like')
      .desc(:created_at).pluck(:track_uri)
  end

  def videos
    @videos ||= @user.videos.desc(:created_at)
  end

  def stations
    @stations_playlist ||= ::Persistence::RadioPlaylist.find_by user_uri: @user.uri
    @stations_playlist ? @stations_playlist.stations : []
  end

  def as_json params = {}
    {
      user_playlists: serialize_playlists,
      track_pool: serialize_track_pool,
      video_pool: video_pool,
      favourite_playlist: favourites_uris,
      history_playlist: serialize_history,
      video_playlist: serialize_videos,
      stations: stations
    }
  end
end
