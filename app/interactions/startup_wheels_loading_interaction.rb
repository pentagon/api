class StartupWheelsLoadingInteraction < Interaction
  def initialize params
    super
    @result = wheels.inject({}) do |res, (name, ids)|
      res[name] = tracks_pool.select {|t| ids.include? t.id}.map {|t| serialize_track t}
      res[name].shuffle! if params[:random]
      res
    end
  end

  def wheels
    @@wheels ||= YAML.load_file File.join Rails.root, 'config', 'start_wheels.yml'
  end

  def tracks_pool
    @tracks_pool ||= fetch_items_by_ids wheels.values.flatten
  end

  def as_json params = {}
    {
      success: true,
      wheels: @result
    }
  end

  def serialize_track track
    # Shouls be overriden in descendants
    raise StandardError.new.new "Method 'serialize_track' should be everriden!"
  end
end
