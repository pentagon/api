module InteractionErrors
  WrongArgument = Class.new(StandardError)
  TrackLimitReached = Class.new(StandardError)
  TrackSkipDisallowed = Class.new(StandardError)
  StationCreatingDisallowed = Class.new(StandardError)
  SyncMisRequestTimeout = Class.new(StandardError)
  TooManyConnections = Class.new(StandardError)
  Forbidden = Class.new(StandardError)
  InsufficientReportsAvailableError = Class.new(StandardError)
  RadioQueueEmpty = Class.new(StandardError)
  InvalidHeaders = Class.new(StandardError)
  ThirdPartyFailure = Class.new(StandardError)

  class CodedErrors
    attr_reader :error_codes
    delegate :any?, :blank?, :empty?, to: :full_messages

    def initialize
      @error_codes = Set.new
    end

    def add error_code
      error_codes.add error_code
    end

    def full_messages
      error_codes.to_a
    end
  end

  class NotFound < StandardError
    def initialize uri = '', country = ''
      super "Unable to fetch item with uri: '#{uri}', passed country: '#{country}'"
    end
  end

  class UnprocessableEntity < StandardError
    def initialize active_model_errors = nil
      @active_model_errors = active_model_errors
    end

    def errors
      @active_model_errors
    end

    def message
      @active_model_errors.full_messages.join("\n") if @active_model_errors
    end
  end

  # it's not actually an error
  # TODO: Refactor this.
  class RedirectingError < StandardError
    attr_accessor :redirect_url, :message

    def initialize(redirect_url, message = '')
      @redirect_url = redirect_url
      @message = message
    end
  end
end
