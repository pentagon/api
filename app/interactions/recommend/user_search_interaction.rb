class Recommend::UserSearchInteraction < Interaction
  include Recommend::Serializers
  USER_WHITELIST = %w(email)

  def initialize(options)
    super
    unless USER_WHITELIST.include? options[:field]
      raise InteractionErrors::WrongArgument.new 'Only allowed fields'
    end
    @user = Persistence::User.find_by({options[:field].to_sym => options[:value]})
    raise InteractionErrors::NotFound.new options[:value] if @user.blank?
  end

  def as_json options = {}
    serialize_user(@user)
  end
end

