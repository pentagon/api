class Recommend::UserLoadingInteraction < Interaction
  include Recommend::Serializers

  def initialize options = {}
    @user = if options[:id].include? 'artist'
              Persistence::Artist.find_by(uri: options[:id]).try(:user)
            else
              Persistence::User.find_by_slug(options[:id])
            end
    raise InteractionErrors::NotFound.new options[:id] if @user.blank?
  end

  def as_json options = {}
    serialize_user(@user)
  end
end
