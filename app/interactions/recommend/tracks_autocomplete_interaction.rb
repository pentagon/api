class Recommend::TracksAutocompleteInteraction < Interaction
  include Recommend::Serializers
  include Searchers::Track

  def initialize(options)
    super
    raise InteractionErrors::WrongArgument.new 'Query should be filled' if options[:q].blank?
    @results = track_simple_search options[:q], options[:page], options[:per_page]
  end

  def as_json options = {}
    {
      current_page: @results.current_page,
      per_page: @results.per_page,
      total_count: @results.total_count,
      data: @results.map {|t| serialize_autocomplete_track t}
    }
  end
end
