class Recommend::TracksSearchInteraction < Interaction
  include Recommend::Serializers
  include Searchers::Track

  def initialize(options)
    super
    raise InteractionErrors::WrongArgument.new 'Track and artist name should be filled' if options[:title].blank? || options[:artist].blank?
    @results = track_advanced_search(options, options[:page], options[:per_page])
  end

  def track_default_must_filters_array
    []
  end

  def as_json options = {}
    {
      current_page: @results.current_page,
      per_page: @results.per_page,
      total_count: @results.total_count,
      data: @results.map {|t| serialize_search_track t}
    }
  end
end
