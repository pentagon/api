class Recommend::TracksLoadingInteraction < Interaction
  def initialize options = {}
    super
    raise InteractionErrors::WrongArgument.new "Track id is not specified" if options[:id].blank?
    @tracks = Track.fetch options[:id]
    raise InteractionErrors::NotFound.new options[:id] if @tracks.blank?
  end

  def as_json options = {}
    @tracks
  end
end
