module Recommend::Serializers
  def serialize_track track
    serialize_common_attributes(track).merge({
      genre: track.genre,
      genre_cloud: track.genre_cloud
    })
  end

  def serialize_common_attributes obj
    {
      id: obj.id,
      title: obj.title,
      image_url: obj.image_url
    }
  end

  def serialize_user user
    {
      uri: user.uri,
      display_name: user.display_name,
      image_url: user.image_url
    }
  end

  def serialize_autocomplete_track track
    {
      id: track.id,
      title: track.title,
      image_url: track.image_url,
      genre: track.genre,
      artist: {
        id: track.artist.try(:id),
        title: track.artist.try(:title)
      }
    }
  end

  def serialize_search_track track
    {
      id: track.id,
      title: track.title,
      image_url: track.image_url,
      music_url: track.music_url,
      genre_cloud: track.genre_cloud || [],
      genre: track.genre,
      release_date: track.release_date,
      album: {
        id: track.album.try(:id),
        title: track.album.try(:title)
      },
      artist: {
        id: track.artist.try(:id),
        title: track.artist.try(:title)
      },
      allowed: allowed?(track),
      rights: transform_rights(track.rights)
    }
  end
end
