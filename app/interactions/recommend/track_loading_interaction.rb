class Recommend::TrackLoadingInteraction < Interaction
  include Recommend::Serializers

  def initialize options = {}
    super
    raise InteractionErrors::WrongArgument.new "Track id is not specified" if options[:id].blank?
    @track = fetch_items_by_ids options[:id]
    raise InteractionErrors::NotFound.new options[:id] if @track.blank?
  end

  def artist
    @artist ||= fetch_items_by_ids @track.artist['id']
  end

  def album
    @album ||= fetch_items_by_ids @track.album['id']
  end

  def as_json options = {}
    res = serialize_track @track
    res[:artist] = serialize_common_attributes artist
    res[:album] = serialize_common_attributes album
    res
  end
end
