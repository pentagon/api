class SnapJamWeb::UserLoadingInteraction < Interaction
  include SnapJamWeb::Serializers

  def initialize params
    super
    raise WrongArgument.new 'No user_id specified' if params[:user_id].blank?
    @usr = Persistence::User.find_by(uri: params[:user_id])
    raise NotFound.new params[:user_id] if @usr.blank?
  end

  def as_json params = {}
    {
      user: serialize_user(@usr)
    }
  end
end
