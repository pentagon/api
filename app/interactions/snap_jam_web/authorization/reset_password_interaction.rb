class SnapJamWeb::Authorization::ResetPasswordInteraction < ::Authorization::ResetPasswordInteraction
  def send_instructions
    Persistence::User.send_reset_password_instructions(@user_params) do |user, token|
      raise InteractionErrors::UnprocessableEntity.new user.errors unless user.errors.empty?
      AccountsMailer.snapjam_reset_password_instructions(user, token).deliver
    end
  end
end
