class SnapJamWeb::Authorization::UserCreatingInteraction < ::Authorization::UserCreatingInteraction
  def send_verification_mail user
    AccountsMailer.snapjam_verification_instructions(user).deliver
  end
end
