class SnapJamWeb::Authorization::RequestVerificationInteraction < ::Authorization::RequestVerificationInteraction
  def send_verification_mail user
    AccountsMailer.snapjam_verification_instructions(user).deliver
  end
end
