class SnapJamWeb::TrackLoadingInteraction < Interaction
  include SnapJamWeb::Serializers

  def initialize params
    super
    raise WrongArgument.new 'No user_id specified' if params[:track_id].blank?
    @track = fetch_items_by_ids params[:track_id]
    raise NotFound.new params[:user_id] if @track.blank?
  end

  def as_json params = {}
    {
      track: serialize_track(@track)
    }
  end
end
