class SnapJamWeb::SnapLoadingInteraction < Interaction
  include SnapJamWeb::Serializers

  def initialize params
    super
    @snap = Persistence::SnapTrack.find(params[:snap_id])
    raise NotFound.new params[:snap_id] if @snap.blank?
  end

  def as_json params = {}
    {
      snap: serialize_snap(@snap)
    }
  end
end
