class SnapJamWeb::SnapsLoadingInteraction < Interaction
  include SnapJamWeb::Serializers

  def initialize params
    super
    @snaps = Persistence::SnapTrack.find(params[:snap_ids])
  end

  def as_json params = {}
    {
      snaps: @snaps.map {|s| serialize_snap(s)}
    }
  end
end
