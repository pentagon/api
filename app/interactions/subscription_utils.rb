module SubscriptionUtils
  module Initiating
    extend ActiveSupport::Concern

    module ClassMethods
      # make sure we have everything on place to work with
      %w(agent return_url success_url cancel_url error_url department_code subscription_template country).each do |m|
        define_method(m) do
          raise StandardError.new "Method '#{m}' should be overriden!"
        end
      end
    end

    def subscription_params
      {
        id: subscription_template.id, th_token: user.authentication_token, user_country: country,
        agent: agent, department_code: department_code, success_url: success_url,
        error_url: error_url, cancel_url: cancel_url
      }
    end

    def build_auth_redirect_url(return_url, cancel_url)
      params = {
        billing_agreement: {
          type: 'RecurringPayments',
          description: Persistence::Subscription.recurring_profile_description(subscription_template)
        },
        return_url: return_url,
        cancel_return_url: cancel_url
      }
      Rails.logger.warn "Subscription:: initiating param: #{params}"
      response = Persistence::Subscription.paypal_gateway.setup_authorization(0, params)
      Rails.logger.warn "Subscription:: paypal response: #{response.inspect}"
      Persistence::Subscription.paypal_gateway.redirect_url_for(response.token, mobile: mobile?)
    end
  end

  module Confirming
    extend ActiveSupport::Concern

    module ClassMethods
      # make sure we have everything on place to work with
      %w(success_url error_url agent department_code user_country user subscription_template paypal_token).each do |m|
        define_method(m) do
          raise StandardError.new "Method '#{m}' should be overriden!"
        end
      end
    end

    def validate_args_and_set_attrs! args
      @success_url = args[:success_url]
      @error_url = args[:error_url]
      @agent = args[:agent]
      @department_code = args[:department_code]
      @user_country = args[:user_country]
      @user = Persistence::User.find_by authentication_token: args[:token]
      @subscription_template = ::SubscriptionTemplate.find args[:id]
      @paypal_token = args[:paypal_token]
    end

    def setup_profile
      Persistence::Subscription.setup_profile subscription_template, paypal_token
    end

    def init_subscription profile_id
      subscription = Persistence::Subscription.new(subscription_template.to_hash.except(:valid_from, :valid_to))
      subscription.user_uri = user.uri
      subscription.payment_prov_id = profile_id
      subscription.agent = agent
      subscription.department_code = department_code
      subscription.country = country
      subscription
    end
  end

  module Activating
    extend ActiveSupport::Concern

    def activate_subscription(subscription)
      unless subscription.status == 'cancelled'
        Persistence::Subscription.paypal_gateway.bill_outstanding_amount(
          subscription.payment_prov_id).success?
      end
    end
  end

  module Cancelling
    extend ActiveSupport::Concern

    def cancel_subscription(subscription, message = nil)
      success = if subscription || subscription.internal?
        true
      else
        Persistence::Subscription.paypal_gateway.cancel_recurring(
          subscription.payment_prov_id, note: message).success?
      end
      subscription.update_attribute :status, 'cancelled' if success
      success
    end
  end
end
