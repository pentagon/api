class HoroscopeLoadingInteraction < Interaction
  include SearchFiltering::Track
  include CatalogUtils

  attr_accessor :horoscope, :date, :sign
  ALLOWED_SIGNS = %w(general aries taurus gemini cancer leo virgo libra scorpio sagittarius capricorn aquarius pisces)

  def initialize args
    super
    @sign = args[:id]
    @date = DateTime.now.strftime '%Y-%m-%d'
    raise InteractionErrors::WrongArgument.new "No sign is specified or invalid sign!" unless ALLOWED_SIGNS.include? sign
    @@preset_track_ids ||= YAML.load_file(File.join Rails.root, 'config', 'astro.yml')['tracks']
    request_recommendations
  end

  def consumer
    raise InteractionErrors::WrongArgument.new 'Method consumer have to be everriden!'
  end

  def fetch_data_from_service
    raise InteractionErrors::WrongArgument.new 'Method fetch_data_from_service have to be everriden!'
  end

  def related_track_ids
    recommendations.collect(&:tracks).flatten.collect {|t| t['uri']}.select {|u| u.include? 'medianet'}.uniq
  end

  def request_recommendations
    (preset_track_ids - recommendations.map(&:track_uri)).each do |uri|
      query = {track_uri: uri, region: country_override, num_tracks_per_subset: 20, app: 'purchase'}
      MisService.enqueue MisCommand::GetRecommendation.new RecommendationUpdatingInteraction, :process_mis_response, query
    end
  end

  def preset_track_ids
    @@preset_track_ids[sign] or []
  end

  def country_override
    @country.eql?('us') ? 'us' : 'uk'
  end

  def track_pool_country
    @country.eql?('us') ? 'us' : @country.eql?('ca') ? 'ca' : 'uk'
  end

  def recommendations
    @recommendations ||= Persistence::Recommendation.any_in(track_uri: preset_track_ids).and(territory: country_override)
  end

  def tracks
    @tracks ||= fetch_items_by_ids horoscope_track_ids, {size: 20}
  end

  def horoscope_track_ids
    @horoscope_track_ids ||= begin
      unless horoscope.tracks_hash[sign].present? and horoscope.tracks_hash[sign][track_pool_country].present?
        horoscope.tracks_hash[sign] ||= {}
        horoscope.tracks_hash[sign][track_pool_country] = related_track_ids || preset_track_ids
        horoscope.save
      end
      horoscope.tracks_hash[sign][track_pool_country]
    end
  end

  def horoscope
    @horoscope ||= begin
      Persistence::Horoscope.find_or_create_by(date_from: @date, date_to: @date, type: 'daily',
        consumer: consumer) do |h|
        h.response = fetch_data_from_service
        h.data = JSON.parse h.response
      end
    end
  end
end
