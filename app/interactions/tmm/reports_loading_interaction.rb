class Tmm::ReportsLoadingInteraction
  include Tmm::Serializers

  def initialize options = {}
    @ids = options[:ids]
    @current_user = options[:user]
    @reports = Persistence::HitMakerReport.accessible_for(@current_user).in(uri: @ids)
  end

  def as_json options = {}
    {
      reports: @reports.map { |r| serialize_report r }
    }
  end

end
