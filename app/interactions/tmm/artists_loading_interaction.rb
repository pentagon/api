class Tmm::ArtistsLoadingInteraction
  include Tmm::Serializers

  def initialize options = {}
    @page = options[:params][:page]
    @per_page = @page.eql?(1) ? 25 : 15
    @artists = Persistence::Artist.search(options[:params])
    @total = @artists.total
  end

  def as_json options = {}
    {
      artists: @artists.results.map { |a| serialize_elastic_artist a },
      meta: {
        count: @total,
        pages: (@total/@per_page.to_f).ceil
      }
    }
  end
end
