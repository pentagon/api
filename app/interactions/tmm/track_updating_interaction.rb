class Tmm::TrackUpdatingInteraction < Interaction
  include Tmm::Serializers

  attr_reader :track

  def initialize params
    super
    track_params = params[:track_params]
    @track = @user.tracks.find_by_slug track_params[:id]
    return false unless @track

    album = @track.album
    if @track.update_attributes params[:track_params][:track]
      @track.is_explicit = track_params[:track][:is_explicit]
      @track.is_private = track_params[:track][:is_private]
      @track.is_merchantable = track_params[:track][:is_merchantable]

      if track_params[:track][:album_id].blank?
        @track.album_uri = nil
      else
        album = @user.default_artist.albums.find_by_slug track_params[:track][:album_id]
        @track.album = album
      end
      if @track.changed?
        @track.save
        album.update_tracks_order( @track.album.nil? && track_params[:track][:album_id].blank? ? nil : @track.uri ) if album.present?
      end
    end
  end

  def valid?
    @track.valid?
  end

  def as_json options = {}
    if valid?
      { track: serialize_track(@track) }
    else
      { errors: @track.errors.full_messages }
    end
  end
end

