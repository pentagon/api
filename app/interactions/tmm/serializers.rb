module Tmm::Serializers

  def serialize_user user
    hash = {
      id: user.uri,
      email: user.email,
      slug: user.mongoid_slug,
      full_name: user.full_name,
      artist_uri: user.default_artist.uri,
      image_url: user.image_url,
      genres: user.default_artist.sorted_genres,
      biography: user.biography,
      referral_token: user.referral_token,
      tags_array: user.tags_array,
      referral_url: user.referral_url
    }
    hash.merge!({
      accepted_referrals: user.accepted_referrals.map { |a| serialize_referal a }
    }) if @current_user.try(:uri).eql? user.uri
    hash.merge!({
      uri: user.uri,
      country: user.contact_info.country,
      display_name: user.display_name,
      competitors: user.default_artist.all_competitors,
      facebook_slug: user.contact_info.facebook_slug,
      twitter_slug: user.contact_info.twitter_slug,
      soundcloud_slug: user.contact_info.soundcloud_slug,
      site_url: user.contact_info.site_url,
      video_ids: videos(user).pluck(:uri),
      track_ids: tracks(user).pluck(:uri),
      album_ids: albums(user).pluck(:uri),
      image_ids: images(user).map(&:uri),
      image_album_ids: image_albums(user).pluck(:uri)
    })
    hash.merge!({
      share_link_ids: user.share_links.pluck(:uri),
      personal_info_id: user.personal_info.id,
      contact_info_id: user.contact_info.id,
      artist_id: user.default_artist.uri
    }) if @current_user.try(:uri).eql? user.uri


    hash
  end

  def serialize_referal referal
    {
      full_name: referal.display_name,
      registered: referal.created_at.to_i * 1000,
      uri: referal.uri
    }
  end

  def serialize_video video
    {
      id: video.uri,
      title: video.title,
      video: video.video_id,
      url: video.url,
      description: video.description,
      preview_img: video.preview_img,
      release_date: video.created_at.to_i,
      track_ids: video.tracks.pluck(:uri)
    }
  end

  def serialize_track track
    {
      id: track.uri,
      title: track.title,
      genre: track.genre,
      duration: track.duration,
      music_url: track.music_url,
      waveform_url: track.waveform_url,
      release_date: track.release_date.to_i,
      lyrics: begin track.lyrics.gsub("\r\n", "<br />") rescue nil end,
      description: track.description,
      is_shared: track.share_links.any?,
      image_url: track.image_url,
      artist_name: track.artist_name,
      has_album: track.album_uri.present?,
      user_uri: track.user_uri,
      album_uri: track.album_uri,
      is_private: track.is_private,
      is_explicit: track.is_explicit,
      slug: track.slug,
      uri: track.uri,
      user_slug: track.user.mongoid_slug,
      is_merchantable: track.is_merchantable,
      is_copyrighted: track.infringement?,
      number_of_plays: track.number_of_plays,
      track_status: track.track_status,
      source: track.source,
      share_link_ids: track.share_links.pluck(:uri),
      report_ids: track.hit_maker_reports.pluck(:uri),
      album_id: track.album.try(:uri),
      user_id: track.user.uri

    }
  end

  def serialize_charts_track track
    {
      id: track.uri,
      title: track.title,
      genre: track.genre,
      duration: track.duration,
      artist_name: track.artist_name,
      user_slug: track.user.mongoid_slug,
      release_date: track.release_date.to_i,
      image_url: track.image_url,
      music_url: track.music_url,
      number_of_plays: track.number_of_plays,
      user_id: track.user.uri
    }
  end

  def serialize_album album
    album_tracks = album.tracks.accessible_for(@current_user).desc :created_at
    album_tracks = album_tracks.reject(&:infringement?) unless @current_user.try(:uri).eql? album.artist.user_uri
    {
      id: album.uri,
      title: album.title,
      image_url: album.image_url,
      genre: album.genre,
      release_date: album.release_date.to_time.to_i,
      duration: album.duration,
      tracks_count: album.tracks.count,
      tracks_order: album.tracks_order && album.tracks.pluck(:uri),
      slug: album.slug,
      uri: album.uri,
      track_ids: album_tracks.map { |t| t.uri },
      user_id: album.artist.user_uri
    }
  end

  def serialize_image image
    {
      id: image.uri,
      title: image.title,
      description: image.description,
      media_url: image.image_url,
      image_album_id: image.gallery.uri,
      created_at: image.created_at
    }
  end

  def serialize_image_album image_album
    { id: image_album.uri,
      title: image_album.title,
      created_at: image_album.created_at,
      cover_uri: image_album.cover_uri,
      image_ids: image_album.images.map(&:uri)
    }
  end

  def serialize_sharelink sharelink
    {
      id: sharelink.uri,
      uri: sharelink.uri,
      name: sharelink.name,
      track_uri: sharelink.track_uri,
      user_uri: sharelink.user.uri,
      is_downloadable: sharelink.is_downloadable,
      user_id: sharelink.user.try(:id),
      track_id: sharelink.track.try(:id)
    }
  end

  def serialize_personalinfo personal_info
    {
      id: personal_info.id,
      biography: personal_info.biography,
      date_of_birth: personal_info.date_of_birth,
      first_name: personal_info.first_name,
      gender: (personal_info.gender.presence or 1),
      last_name: personal_info.last_name,
      nick_name: personal_info.nick_name
    }
  end

  def serialize_contactinfo contact_info
    {
      id: contact_info.id,
      phone: contact_info.phone,
      site_url: contact_info.site_url,
      country: contact_info.country,
      state: contact_info.state,
      city: contact_info.city,
      street: contact_info.street,
      address: contact_info.address,
      postal_code: contact_info.postal_code,
      facebook_slug: contact_info.facebook_slug,
      soundcloud_slug: contact_info.soundcloud_slug,
      twitter_slug: contact_info.twitter_slug,
      messenger_info_ids: contact_info.messenger_infos.map { |m| m.id }
    }
  end

  def serialize_messengerinfo messenger_info
    {
      id: messenger_info.id,
      messenger_type: messenger_info.messenger_type,
      number: messenger_info.number
    }
  end

  def serialize_artist artist
    {
      id: artist.uri,
      stage_name: artist.stage_name.presence || artist.user.display_name,
      artist_roles: artist.artist_roles,
      user_id: artist.user.try(:uri),
      all_charts_positions: artist.all_charts_positions,
      image_url: artist.user.image_url
    }
  end

  def serialize_elastic_artist artist
    {
      id: artist.id,
      stage_name: artist.additional_fields.stage_name,
      artist_roles: artist.additional_fields.artist_roles,
      user_id: artist.user_uri,
      image_url: artist.image_url
    }
  end

  def serialize_report report
    {
      id: report.uri,
      url: "#{Settings.report_show_page_url}/#{report.uri}",
      territory: report.territory,
      hitscore: report.hss_score.to_i,
      is_processed: report.is_processed,
      is_private: report.is_private
    }
  end

  def videos user
    user.videos.desc :created_at
  end

  def tracks user
    skope = user.tracks.accessible_for(@current_user).desc :created_at
    skope.reject(&:infringement?) unless is_current_user?
    skope
  end

  def albums user
    user.default_artist.albums.albums
  end

  def image_albums user
    user.image_albums
  end

  def images user
    images = user.galleries.try(:first).try(:images) || []
    all_img = images + user.image_albums.map(&:images).flatten
    if all_img.present?
      all_img.sort_by(&:created_at).reverse
    else
      []
    end
  end

  def share_links user
    user.share_links
  end

  def is_current_user?
    @user.uri == @current_user.try(:uri)
  end

end
