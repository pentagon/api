class Tmm::UserUpdatingInteraction < Interaction
  include Tmm::Serializers

  def initialize params
    super
    @user_params  = params[:user_params]
    cleanup
    if @user.update_attributes @user_params
      @user.default_artist.update_attributes(@user_params.delete(:artist_attributes)) 
      assign_slug
      @user.remove_image! if @user_params[:remove_image].eql?(true)
    end
  end

  def valid?
    @user.valid?
  end

  def as_json options = {}
    if valid?
      {
        videos: videos(@user).map { |v| serialize_video v },
        tracks: tracks(@user).map { |t| serialize_track t },
        albums: albums(@user).map { |a| serialize_album a },
        images: images(@user).map { |i| serialize_image i },
        share_links: share_links(@user).map { |s| serialize_sharelink s},
        personal_infos: [@user.personal_info].map { |p| serialize_personalinfo p },
        contact_infos: [@user.contact_info].map { |c| serialize_contactinfo c },
        messenger_infos: @user.contact_info.messenger_infos.map { |m| serialize_messengerinfo m },
        artists: [@user.default_artist].map { |a| serialize_artist a },
        user: serialize_user(@user)
      }
    else
      {
        errors: @user.errors.full_messages
      }
    end
  end

  private
  def cleanup
    @user_params[:artist_attributes].delete(:user_uri) if @user_params[:artist_attributes]
  end

  def assign_slug
    new_slug = @user_params.delete(:slug)
    unless new_slug.eql? @user.slug
      @user.slug = new_slug
      @user.build_slug
      @user_params[:slug] = @user.mongoid_slug
    end
  end
end
