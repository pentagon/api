class Tmm::TrackLoadingInteraction
  include Tmm::Serializers

  def initialize options = {}
    @id = options[:id]
    @current_user = options[:user]
    @track = Persistence::Track.accessible_for(@current_user).find_by_slug(@id)
    @track = nil if @track && @track.infringement? && !@track.user.eql?(@current_user)
  end

  def status
    @track.present? ? :ok : :not_found
  end

  def as_json options = {}
    if @track.present?
      {
        track: serialize_track(@track)
      }
    else
      {}
    end
  end
end
