class Tmm::TracksLoadingInteraction
  include Tmm::Serializers

  def initialize options = {}
    @ids = options[:ids]
    @current_user = options[:user]
    @tracks = Persistence::Track.accessible_for(@current_user).in(uri: @ids).desc(:created_at)
  end

  def as_json options = {}
    {
      tracks: @tracks.map { |t| serialize_track t }
    }
  end
end
