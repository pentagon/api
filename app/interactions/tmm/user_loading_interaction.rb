class Tmm::UserLoadingInteraction < Interaction
  include Tmm::Serializers

  def initialize options = {}
    @id = options[:id]
    @current_user = options[:user]
    @user = is_artist? ? load_from_artist : load_from_user
  end

  def as_json options = {}
    hash = {
      videos: videos(@user).map { |v| serialize_video v },
      tracks: tracks(@user).map { |t| serialize_track t },
      albums: albums(@user).map { |a| serialize_album a },
      images: images(@user).map { |i| serialize_image i },
      image_albums: image_albums(@user).map { |i| serialize_image_album i }
    }
    if is_current_user?
      hash.merge!({
        share_links: share_links(@user).map { |s| serialize_sharelink s},
        personal_infos: [@user.personal_info].map { |p| serialize_personalinfo p },
        contact_infos: [@user.contact_info].map { |c| serialize_contactinfo c },
        messenger_infos: @user.contact_info.messenger_infos.map { |m| serialize_messengerinfo m },
        artists: [@user.default_artist].map { |a| serialize_artist a }
      })
    end
    hash.merge!({
      user: serialize_user(@user)
    })
    hash
  end

  private

  def is_artist?
    !!(@id =~ /artist/)
  end

  def load_from_artist
    Persistence::Artist.find_by(uri: @id).try(:user)
  end

  def load_from_user
    Persistence::User.find_by_slug(@id)
  end

end
