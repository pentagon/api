class Tmm::ImageAlbumCreatingInteraction
  include Tmm::Serializers

  def initialize params = {}, user
    @img_album = user.image_albums.create(params[:image_album])
  end

  def as_json options={}
    {image_album: serialize_image_album(@img_album)}
  end
end
