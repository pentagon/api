class Tmm::TrackCreatingInteraction < Interaction
  attr_reader :track

  include Tmm::Serializers

  def initialize params
    super
    track_params = params[:track_params]

    track_params[:track].merge!(JSON.parse track_params.delete(:track_data)) if track_params[:track_data].present?
    track_params[:track].update source: "fms"
    @track = @user.tracks.build track_params[:track].merge(artist_uri: @user.default_artist.uri)
    if @track
      @track.track_source_file = track_params[:track][:track_file].tempfile.path
      @track.track_source_file_ext = File.extname(track_params[:track][:track_file].original_filename).downcase
      @track.is_explicit = track_params[:track][:is_explicit]
      @track.is_private = track_params[:track][:is_private]
      @track.is_merchantable = track_params[:track][:is_merchantable]

      if @track.save
        if track_params[:track][:track_file].content_type.include? "wav"
          @track.update_attribute(:track_status, 1)
          Thread.new { WavToMp3.convert @track.uri }
        end
        track_params[:track][:track_file].tempfile.close(true)
        Tmm::FgpProcessCreatingInteraction.new track: @track
      end
    end
  end

  def as_json opts = {}
    {
      track: serialize_track(@track)
    }
  end
end
