class Tmm::ReportLoadingInteraction
  include Tmm::Serializers

  def initialize options = {}
    @id = options[:id]
    @current_user = options[:user]
    @report = Persistence::HitMakerReport.accessible_for(@current_user).find_by(uri: @id)
  end

  def as_json options = {}
    {
      report: serialize_report(@report)
    }
  end

end
