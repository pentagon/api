class Tmm::ConversationsLoadingInteraction
  include Tmm::Serializers

  def initialize options = {}
    @current_user = options[:user]
    @conversations = ::Message::Conversation.by_owner(@current_user.uri)
  end

  def as_json options = {}
    {
      conversations: @conversations.map { |c| serialize_conversation c }
    }
  end

end
