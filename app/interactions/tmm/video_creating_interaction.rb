class Tmm::VideoCreatingInteraction
  include Tmm::Serializers

  def initialize options = {}
    @video_params = options[:video_params][:video]
    @current_user = options[:user]
    @user = @current_user
    @video = @current_user.videos.build(@video_params)
    @video.save
  end

  def valid?
    @video.valid?
  end

  def as_json options = {}
    if valid?
      {
        video: serialize_video(@video)
      }
    else
      {
        errors: @video.errors.full_messages
      }
    end
  end
end
