class Tmm::AlbumUpdatingInteraction
  include Tmm::Serializers

  def initialize options = {}
    @album_params = options[:album_params]
    @current_user = options[:user]
    @album = @current_user.default_artist.albums.find_by_slug(@album_params[:id])
    @tracks = @current_user.tracks.find_by_slug(@album_params[:album][:track_ids])
    @album.assign_attributes(@album_params[:album])
    @album.save
  end

  def valid?
    @album.valid?
  end

  def as_json options = {}
    if valid?
      {
        album: serialize_album(@album)
      }
    else
      {
        errors: @album.errors.full_messages
      }
    end
  end
end
