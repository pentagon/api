class Tmm::SharelinkCreatingInteraction
  include Tmm::Serializers

  def initialize options = {}
    @sharelink_params = options[:sharelink_params]
    @current_user = options[:user]
    @sharelink = @current_user.share_links.build(@sharelink_params[:share_link])
    @sharelink.save
  end

  def valid?
    @sharelink.valid?
  end

  def as_json options = {}
    if valid?
      {
        share_link: serialize_sharelink(@sharelink)
      }
    else
      {
        errors: @sharelink.errors.full_messages
      }
    end
  end

end
