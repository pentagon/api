class Tmm::ReferralLoadingInteraction
  def initialize
    pipeline = [
      {"$match"=>{initiator_uri:{"$exists" => true},responder_uri:{"$exists" => true}}},
      {"$group"=>{"_id"=>"$initiator_uri", "count"=>{"$sum"=>1}}},
      {"$sort"=>{"count"=>-1}},
      {"$limit" => 10}
    ]
    @aggregated_referrals = Persistence::Referral.collection.aggregate(pipeline)
    @ordering = Hash[@aggregated_referrals.map {|a| [a['_id'], a['count']]}]
  end

  def referral_to_json u, index
    {
      id: index,
      position: index+1,
      display_name: u.display_name,
      uri: u.uri,
      image_url: u.image_url,
      accepted_referrals_count: @ordering[u.uri]
    }
  end

  def as_json opts = {}
    hash = {}
    hash[:referrals] = Persistence::User.any_in(uri: @ordering.keys).sort_by {|a| @ordering[a.uri]*-1}.map.with_index { |u, index| referral_to_json(u, index) }
    hash[:meta] = { max: hash[:referrals].first[:accepted_referrals_count] }
    hash
  end

end
