class Tmm::ImageAlbumLoadingInteraction
  include Tmm::Serializers

  def initialize params
    @img_album = Persistence::Gallery.find_by uri: params[:id]
  end

  def as_json options={}
    {image_album: serialize_image_album(@img_album)}
  end


end
