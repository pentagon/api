class Tmm::SharelinkLoadingInteraction
  include Tmm::Serializers

  def initialize options = {}
    @id = options[:id]
    @current_user = options[:user]
    @share_link = Persistence::ShareLink.find_by(uri: @id)
  end

  def as_json options = {}
    {
      share_link: serialize_sharelink(@share_link)
    }
  end
end
