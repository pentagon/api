class Tmm::VideosLoadingInteraction
  include Tmm::Serializers

  def initialize options = {}
    @ids = options[:ids]
    @current_user = options[:user]
    @videos = Persistence::Video.where(:uri.in => @ids).order_by([:created_at, :desc])
  end

  def as_json options = {}
    {
      videos: @videos.map { |v| serialize_video v }
    }
  end
end
