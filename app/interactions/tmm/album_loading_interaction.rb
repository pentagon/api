class Tmm::AlbumLoadingInteraction
  include Tmm::Serializers

  def initialize options = {}
    @id = options[:id]
    @current_user = options[:user]
    @album = Persistence::Album.find_by_slug(@id)
  end

  def as_json options = {}
    {
      album: serialize_album(@album)
    }
  end
end
