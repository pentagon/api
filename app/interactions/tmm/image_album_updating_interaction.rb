class Tmm::ImageAlbumUpdatingInteraction
  include Tmm::Serializers

  def initialize params, user
    @img_album = user.image_albums.find_by uri: params[:id]
    @valid = @img_album.update_attributes(params[:image_album])
  end

  def as_json options={}
    if @valid
      {image_album: serialize_image_album(@img_album)}
    else
      { errors: img_album.errors.full_messages, status: :unprocessable_entity}
    end
  end
end
