class Tmm::ChartsArtistsLoadingInteraction
  include Tmm::Serializers

  def initialize params = {}
    page = (params[:page] || 1).to_i
    per_page = 25
    cacheName = "v13-#{self.class.name.parameterize}IndexPage#{page}PerPage#{per_page}Genre#{params[:genre].presence || 'All'}"

    @data = Rails.cache.fetch(cacheName, expires_in: ChartPosition::UPDATE_PERIOD) do
      Rails.logger.debug "************ Update Cache for #{cacheName}"
      results = Persistence::Artist.charts(params[:genre]).page(page).per(per_page)
      artists = Persistence::Artist.any_in(uri: results).includes(:user).sort_by { |a| results.index(a.uri) }
      meta = { count: results.total_count, pages: (results.total_count / per_page.to_f).ceil }
      response = {
        'charts/artists' => artists.map {|a| serialize_artist(a)},
        'meta' => meta
      }
    end
  end

  def as_json options = {}
   @data
  end
end
