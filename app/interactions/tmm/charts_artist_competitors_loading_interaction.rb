class Tmm::ChartsArtistCompetitorsLoadingInteraction
  include Tmm::Serializers

  def initialize params = {}
    @artists = Persistence::Artist.any_in(uri: params[:ids]).includes(:user).sort_by { |a| params[:ids].index(a.uri) }
  end

  def as_json options = {}
    {
      'charts/competitors' => @artists.map {|a| serialize_artist(a)}
    }
  end

end
