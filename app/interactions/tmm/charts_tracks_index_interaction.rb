class Tmm::ChartsTracksIndexInteraction
  include Tmm::Serializers

  def initialize params ={}
    params = params
    page = (params[:page] || 1).to_i
    per_page = 25

    cacheName = "v08-#{self.class.name.parameterize}IndexPage#{page}PerPage#{per_page}Genre#{params[:genre].presence || 'All'}"

    @data = Rails.cache.fetch(cacheName, expires_in: ChartPosition::UPDATE_PERIOD) do
      Rails.logger.debug "************ Update Cache for #{cacheName}"
      results = Persistence::Track.chart(params[:genre]).page(page).per(per_page)
      Rails.logger.info results.inspect
      tracks = Persistence::Track.any_in(uri: results).includes(:user, :album).sort_by { |t| results.index(t.uri) }
      meta = { count: results.total_count, pages: (results.total_count / per_page.to_f).ceil }

      response = {
        'charts/tracks' => tracks.map { |t| serialize_charts_track(t) },
        'meta' => meta
      }
    end
  end

  def as_json options={}
    @data
  end


end
