class Tmm::ArtistLoadingInteraction
  include Tmm::Serializers

  def initialize options = {}
    @id = options[:id]
    @current_user = options[:user]
    @artist = Persistence::Artist.find_by(uri: @id)
  end

  def as_json options = {}
    {
      artist: serialize_artist(@artist)
    }
  end

end
