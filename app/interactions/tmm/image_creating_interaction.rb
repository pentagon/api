class Tmm::ImageCreatingInteraction
  include Tmm::Serializers

  EMBER_UNNEEDED_FIELDS = %w|user_id media_url|
  DEFAULT_OPTIONS = {
    source: 'fms'
  }

  def initialize params, user
    @image_params = params[:image].reject { |k,v| EMBER_UNNEEDED_FIELDS.include? k }
    @image_params.merge!(DEFAULT_OPTIONS).merge!({gallery_id: params[:parent_id]})
    @image = user.create_image_from_params(@image_params)
    raise InteractionErrors::UnprocessableEntity.new @image.errors unless @image.valid?
  end

  def as_json options = {}
    {image: serialize_image(@image)}
  end
end
