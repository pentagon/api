class Tmm::TrackStatisticLoadingInteraction
  include Tmm::Serializers

  def initialize options = {}
    @id = options[:id]
    @current_user = options[:user]
    @track = Persistence::Track.find_by_slug(@id)
  end

  def as_json options = {}
    {track_statistic: {
            id: @track.uri,
            markers: @track.stats_map,
            charts: charts,
            genre_charts: genre_charts,
            chart_position: chart_position,
            genre_chart_position: genre_chart_position,
            graph: @track.listen_graph
          }
    }
  end

  def charts
    chart = @track.class.chart(nil, false)
    case @track.chart_position
    when 1 then [@track.uri, chart[2], chart[3]]
    when nil then []
    else [chart[@track.chart_position - 1], @track.uri, chart[@track.chart_position + 1]]
    end
  end

  def genre_charts
    chart = @track.class.chart(@track.genre, false)
    case @track.genre_chart_position
    when 1 then [@track.uri, chart[2], chart[3]]
    when nil then []
    else [chart[@track.genre_chart_position - 1], @track.uri, chart[@track.genre_chart_position + 1]]
    end
  end

  def chart_position
    case @track.chart_position
    when 1 then 1
    when nil then nil
    else @track.chart_position - 1
    end
  end

  def genre_chart_position
    case @track.genre_chart_position
    when 1 then 1
    when nil then nil
    else @track.genre_chart_position - 1
    end
  end

end
