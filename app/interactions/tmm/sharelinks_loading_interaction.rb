class Tmm::SharelinksLoadingInteraction
  include Tmm::Serializers

  def initialize options = {}
    @current_user = options[:user]
    @share_links  = @current_user.share_links
  end

  def as_json options = {}
    {
      share_links: @share_links.map { |s| serialize_sharelink s }
    }
  end
end
