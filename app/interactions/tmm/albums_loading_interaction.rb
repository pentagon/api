class Tmm::AlbumsLoadingInteraction
  include Tmm::Serializers

  def initialize options = {}
    @ids = options[:ids]
    @current_user = options[:user]
    @albums = Persistence::Album.find_by_slug(@ids)
  end

  def as_json options = {}
    {
      albums: @albums.map { |a| serialize_album a }
    }
  end
end
