class Tmm::VideoLoadingInteraction
  include Tmm::Serializers

  def initialize options = {}
    @id = options[:id]
    @current_user = options[:user]
    @video = Persistence::Video.find_by(uri: @id)
  end

  def as_json options = {}
    {
      video: serialize_video(@video)
    }
  end
end
