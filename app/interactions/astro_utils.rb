module AstroUtils
  def explicit_level
    @explicit_level = 0
  end

  def consumer
    'astro'
  end

  def get_data_for_sign
    horoscope.data['result']['item'].detect {|i| i['lc_sign'].eql? sign}
  end

  def fetch_data_from_service
    RestClient.get Settings.astro.horoscope_url.sub '<date>', date
  rescue Exception => e
    Rails.logger.error "Failed to fetch horoscope! Error: #{e.message}"
    raise e
  end

  def related_track_ids
    if sign.eql? 'general'
      (@tracks ||= fetch_tracks_by_energy).collect &:id
    else
      super
    end.shuffle.first(40)
  end

  def fetch_tracks_by_energy
    rating = get_data_for_sign['rating'].to_i
    Track.search page: 1, per_page: 40, index: labeled_index do |s|
      must_filters_array = [{range: {energy: {include_lower: true, include_upper: true,
        from: rating * 30, to: rating.next * 30}}}]
      s.query {all}
      s.filter :bool, track_filter_params(must_filters_array)
    end
  end
end
