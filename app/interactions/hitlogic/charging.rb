module Hitlogic::Charging
  extend ActiveSupport::Concern

  def not_enough_credits
    available_credit_bundles_sum <= 0 || available_credit_bundles_sum < dataset_costs
  end
  private :not_enough_credits

  def available_credit_bundles_sum
    available_credit_bundles.sum(&:amount) - available_credit_bundles.map(&:written_off_credits).flatten.sum(&:amount)
  end
  private :available_credit_bundles_sum

  def dataset_costs
    @dataset_cost ||= begin 
      proto = Hash[Persistence::Hitlogic::DataModule.prototypes.map {|a| [a.module_type, a.cost_of_credit]}]
      @data_set.data_modules.reject(&:persisted?).inject(0) {|sum, a| sum + proto[a.module_type]}
    end
  end
  private :dataset_costs

  def charge_user
    total_credits = dataset_costs
    available_credit_bundles.each do |credit_bundle|
      next if credit_bundle.amount_left == 0
      break if (total_credits -= credit_bundle.write_of(total_credits)).zero?
    end
  end
  private :charge_user

  def available_credit_bundles
    @aranged_bundles ||= @user.hitlogic_credits_bundles.limited.to_a + @user.hitlogic_credits_bundles.unlimited.to_a
  end
  private :available_credit_bundles
end
