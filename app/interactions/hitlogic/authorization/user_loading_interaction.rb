class Hitlogic::Authorization::UserLoadingInteraction < ::Authorization::UserLoadingInteraction
  include Hitlogic::Serializers

  def as_json options={}
    { user: serialize_user(@current_user) }
  end
end
