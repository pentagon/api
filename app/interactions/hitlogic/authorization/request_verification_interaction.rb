class Hitlogic::Authorization::RequestVerificationInteraction < ::Authorization::RequestVerificationInteraction
  def send_verification_mail user
    AccountsMailer.hitlogic_verification_instructions(user).deliver
  end
end
