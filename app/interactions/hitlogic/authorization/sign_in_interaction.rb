class Hitlogic::Authorization::SignInInteraction < ::Authorization::SignInInteraction
  include Hitlogic::Serializers

  def as_json options={}
    { user: serialize_user(@user) }
  end
end
