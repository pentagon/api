class Hitlogic::TrackRightsLoadingInteraction < Interaction
  include Hitlogic::Serializers

  def initialize params
    super
    @track = fetch_items_by_ids params[:id]
    raise NotFound.new params[:id] unless @track
  end

  def as_json options={}
    {
      rights: @track.rights,
      transformed_rights: transform_rights(@track.rights)
    }
  end
end

