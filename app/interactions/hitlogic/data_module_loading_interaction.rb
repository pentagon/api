class Hitlogic::DataModuleLoadingInteraction < Interaction
  include Hitlogic::Serializers

  def initialize params
    @data_module = Persistence::Hitlogic::DataModule.find(params[:id])
  end

  def as_json options={}
    obj = {
      data_module: serialize_module(@data_module)
    }
    obj[:catalogs] = [serialize_catalog(@data_module.catalog)] if @data_module.catalog?
    obj
  end
end
