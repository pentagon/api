class Hitlogic::DataSetLoadingInteraction < Interaction
  include Hitlogic::Serializers

  def initialize params
    @data_set = Persistence::Hitlogic::DataSet.find(params[:id])
  end

  def as_json options = {}
    {data_set: serialize_data_set(@data_set) }
  end
end
