class Hitlogic::DataModulePrototypesLoadingInteraction < Interaction
  include Hitlogic::Serializers

  def initialize params
    @proto = Persistence::Hitlogic::DataModule.prototypes.asc(:priority)
    @catalogs = Persistence::Hitlogic::Catalog.all
  end

  def as_json opt={}
    {
      data_module_prototypes: @proto.map {|m| serialize_module(m)},
      catalogs: @catalogs.map {|c| serialize_catalog(c)}
    }
  end
end
