class Hitlogic::TrackLoadingInteraction < Interaction
  include Hitlogic::Serializers

  def initialize params
    super
    @track = Persistence::Track.find_by uri: params[:id]
  end

  def as_json options={}
    {track: serialize_track(@track)}
  end
end

