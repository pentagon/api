class Hitlogic::RecommendationSerializer
  attr_reader :value_attribute, :recomendation, :mod

  def self.get_data mod
    x = new(mod)
    x.send(x.value_attribute) if x.respond_to?(x.value_attribute) && !mod.is_prototype && !x.recomendation.nil?
  end

  private_class_method :new

  def initialize mod
    @mod = mod
    @recomendation = mod.get_data_for_module
    @value_attribute = mod.prototype.value_attribute
  end

  def genre_cloud
    @recomendation.sort { |x, y| y["probability"] <=> x["probability"] }.first(3).map do |i|
      i["probability"] *= 100
      i["probability"] = i["probability"].round
      i
    end
  end

  def color
    @recomendation.map {|k,v| {color: k, value: (v * 100).round} }.sort { |x,y| y[:value] <=> x[:value]}.first(5)
  end

  def similar_hits
    {
      score: (@mod.data_set.reports.find_by(territory: @mod.catalog.name).hss_score.to_f/100).round(1),
      tracks: @recomendation.sort { |x, y| x["dist"] <=> y["dist"] }.first(5)
    }
  end

end
