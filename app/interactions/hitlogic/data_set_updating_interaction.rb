class Hitlogic::DataSetUpdatingInteraction < Hitlogic::DataSetCreatingInteraction
  def initialize_data_set
    @data_set = Persistence::Hitlogic::DataSet.find @params[:id]
    @data_set.assign_attributes @params[:data_set]
  end

  def create_reports
    requested_reports = @data_set.catalogs - @data_set.available_catalogs
    requested_reports.inject(@data_set.reports) do |reports, catalog|
      reports << Persistence::HitMakerReport.create(user_uri: @data_set.user.uri, track_uri: @data_set.track.uri, territory: catalog)
    end
  end
end
