class Hitlogic::TrackUpdatingInteraction < Interaction
  attr_reader :track
  include Hitlogic::Serializers

  def initialize params
    super
    @track = Persistence::Track.find_by uri: params[:id]
    raise NotFound.new params[:id] if track.blank?
    track.assign_attributes params[:track]
    track.user = @user
    track.image = params[:file] if params[:file] && params[:file].content_type =~ /image/
    track.save
  end

  def as_json options={}
    { track: serialize_track(track) }
  end
end
