class Hitlogic::DataSetCreatingInteraction < Interaction
  include Hitlogic::Serializers
  include Hitlogic::Charging

  attr_reader :data_set, :reports, :params

  def initialize params
    super
    @params = params
    initialize_data_set
    raise InteractionErrors::InsufficientReportsAvailableError.new if !params[:dont_charge_user] && not_enough_credits
    @data_set.save
    create_reports
    enqueue_jobs
    charge_user unless params[:dont_charge_user]
  end

  def initialize_data_set
    @data_set = Persistence::Hitlogic::DataSet.new @params[:data_set]
    @data_set.user = @user
    @data_set.track = Persistence::Track.find_by(user_uri: @user.uri, uri: @params[:data_set][:track_id])
  end

  def create_reports
    @data_set.catalogs.inject(@data_set.reports) do |reports, catalog|
      reports << Persistence::HitMakerReport.create(user_uri: @data_set.user.uri, track_uri: @data_set.track.uri, territory: catalog)
    end
  end

  def enqueue_jobs
    @data_set.reports.each do |r|
      MisService.enqueue MisCommand::GetHssInfo.new HitMaker::ReportUpdatingInteraction.name, :process_mis_response, rep_id: r.uri, track_uri: r.track_uri, track_url: r.track.music_url, version: '3.2', region: r.territory, app: 'stream', report_uri: r.uri
    end
  end

  def as_json params = {}
    { data_set: serialize_data_set(@data_set) }
  end
end
