module Hitlogic::Serializers
  extend ActiveSupport::Concern

  def serialize_user user, attributes = nil
    data_sets = Persistence::Hitlogic::DataSet.where(user_uri: user.uri)
    {
      id: user.uri,
      email: user.email,
      display_name: user.display_name,
      token: user.authentication_token,
      image: user.image_url,
      track_ids: user.tracks.pluck(:uri),
      credits: credits_for(user),
      bundles_period: available_period(user),
      data_set_ids: data_sets.pluck(:id),
      created_at: user.created_at
    }
  end

  def serialize_track track
    {
      id: track.uri,
      title: track.title,
      image_url: track.image_url,
      is_private: track.is_private,
      user_id: track.user_uri,
      created_at: track.created_at,
      release_date: track.release_date.to_time.to_i,
      music_url: track.music_url,
      artist_name: track.user.display_name,
      album_name: track.try(:album).try(:title),
      album_uri: track.try(:album).try(:id),
      duration: track.duration.to_i,
      track_status: track.track_status,
      genre: track.genre,
      is_merchantable: track.is_merchantable,
      is_explicit: track.is_explicit,
      description: track.description,
      lyrics: track.lyrics,
      data_set_id: track.data_sets.try(:first).try(:id),
      id3: track.id3
    }
  end

  def serialize_module data_module
    priority = Persistence::Hitlogic::DataModule.prototypes.find_by(module_type: data_module.module_type).try(:priority) || 1000
    {
      id: data_module.id,
      module_type: data_module.module_type,
      title: data_module.title,
      description: data_module.description,
      sub_description: data_module.sub_description,
      visible: data_module.visible,
      data_set_id: data_module.data_set_id,
      cost_of_credit: data_module.cost_of_credit,
      is_multiple_charts: data_module.is_multiple_charts,
      catalog_id: data_module.catalog_id,
      value:  Hitlogic::RecommendationSerializer.get_data(data_module),
      priority: priority
    }
  end

  def serialize_data_set data_set
    {
      id: data_set.id,
      created_at: data_set.created_at,
      is_private: data_set.is_private,
      is_processing: !data_set.is_processed,
      user_id: data_set.user_uri,
      track_id: data_set.track_uri,
      is_newest: data_set.is_new,
      data_module_ids: data_set.data_module_ids
    }
  end

  def serialize_catalog catalog
    {
      id: catalog.id,
      title: catalog.title,
      name: catalog.name,
      priority: catalog.priority,
      description: catalog.description,
      tooltip: catalog.tooltip,
      sub_description: catalog.sub_description,
      year: catalog.year
    }
  end

  private
  def credits_for user
    available_credit_bundles = user.hitlogic_credits_bundles.limited.to_a + user.hitlogic_credits_bundles.unlimited.to_a
    available_credit_bundles.sum(&:amount) - available_credit_bundles.map(&:written_off_credits).flatten.sum(&:amount)
  end

  def available_period user
    bundle = user.hitlogic_credits_bundles.limited.last
    if bundle && bundle.amount_left > 0
      (bundle.valid_to - Date.today).to_i
    else
      nil
    end
  end
end
