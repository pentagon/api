class Hitlogic::UserLoadingInteraction < Interaction
  include Hitlogic::Serializers

  def initialize args
    super
    @user = Persistence::User.find_by uri: args[:id]
  end

  def as_json opts = {}
    { user: serialize_user(@user) }
  end
end
