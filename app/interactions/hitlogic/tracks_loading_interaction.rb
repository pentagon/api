class Hitlogic::TracksLoadingInteraction < Interaction
  include Hitlogic::Serializers

  def initialize params
    super
    @tracks = @user.tracks
  end

  def as_json options={}
    {
      tracks: @tracks.map {|a| serialize_track(a)},
      meta: {
        count: @tracks.count
      }
    }
  end
end
