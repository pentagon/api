class Hitlogic::TrackDestroyingInteraction < Interaction
  attr_reader :track

  def initialize params
    super
    @track = Persistence::Track.where(user_uri: @user.uri).find_by uri: params[:id]
    raise InteractionErrors::NotFound.new unless @track
    @track.destroy
  end

  def as_json options = {}
    {}
  end
end
