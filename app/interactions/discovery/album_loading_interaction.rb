class Discovery::AlbumLoadingInteraction < Interaction
  include Discovery::Serializers

  def initialize params
    super
    raise InteractionErrors::WrongArgument.new "'album_id' is not specified!" if params[:album_id].blank?
    @album = fetch_items_by_ids params[:album_id]
    raise InteractionErrors::NotFound.new params[:album_id] unless @album
  end

  def tracks
    @tracks ||= fetch_related_for_items_by_type @album, 'track'
  end

  def as_json params = {}
    res = serialize_album @album
    res[:tracks] = tracks.map {|t| serialize_track t}
    res
  end
end
