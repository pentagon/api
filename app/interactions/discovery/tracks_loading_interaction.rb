class Discovery::TracksLoadingInteraction < Interaction
  include Discovery::Serializers
  attr_accessor :tracks

  def initialize args
    super
    raise InteractionErrors::WrongArgument.new "Track ids are not specified" if args[:ids].blank?
    @tracks = fetch_items_by_ids args[:ids]
    raise InteractionErrors::NotFound.new args[:ids] if tracks.blank?
  end

  def as_json opts = {}
    tracks.map {|t| serialize_track t}
  end
end
