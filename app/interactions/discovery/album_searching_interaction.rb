class Discovery::AlbumSearchingInteraction < AlbumSearchingInteraction
  include Discovery::Serializers

  def initialize args
    super
    raise InteractionErrors::WrongArgument.new "Query is empty for searching!" if args[:q].blank?
    @results = if args[:autocomplete]
      limit = args[:per_page].presence || 10
      album_autocomplete_search(args[:q], limit)
    else
      discovery_search(args[:q], args[:page], args[:per_page])
    end
  end

  def discovery_search query_string, page = 1, per_page = 20
    Album.search page: page, per_page: per_page, index: indices_to_search_in.compact.join(",") do |s|
      s.query {match ["artist.title^2", "title"], query_string, type: "cross_fields", minimum_should_match: "33%"}
      s.filter :bool, album_filter_params
    end
  end

  def indices_to_search_in
    [labeled_index]
  end

  def as_json params = {}
    pages = @results.total_count / @results.per_page
    pages = 1 if pages.zero?
    {
      current_page: @results.current_page,
      per_page: @results.per_page,
      country: @country,
      total: {
        count: @results.total_count,
        pages: pages
      },
      data: @results.map {|a| serialize_album a}
    }
  end
end
