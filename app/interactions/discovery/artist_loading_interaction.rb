class Discovery::ArtistLoadingInteraction < Interaction
  include Discovery::Serializers
  attr_accessor :artist, :albums

  def initialize params
    super
    raise InteractionErrors::WrongArgument.new "'artist_id' is not specified!" if params[:artist_id].blank?
    @artist = fetch_items_by_ids params[:artist_id]
    raise InteractionErrors::NotFound.new params[:artist_id] unless @artist
    @albums = fetch_related_for_items_by_type artist, 'album'
  end

  def as_json params = {}
    res = serialize_artist artist
    res[:albums] = albums.map {|a| serialize_album a}
    res
  end
end
