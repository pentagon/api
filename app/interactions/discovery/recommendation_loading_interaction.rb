class Discovery::RecommendationLoadingInteraction < RecommendationLoadingInteraction
  include Discovery::Serializers

  def as_json params = {}
    {
      success: true,
      wheels: {
        future: compiled_tracks.select {|t| t[:id].split(':').first != 'medianet'},
        present: compiled_tracks.select {|t| t[:release_year] >= 3.years.ago.year and t[:id].split(':').first == 'medianet'},
        past: compiled_tracks.select {|t| t[:release_year] < 3.years.ago.year and t[:id].split(':').first == 'medianet'}
      }
    }
  end
end
