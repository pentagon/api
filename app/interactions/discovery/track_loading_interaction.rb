class Discovery::TrackLoadingInteraction < Interaction
  include Discovery::Serializers
  attr_accessor :track

  def initialize params
    super
    raise InteractionErrors::WrongArgument.new "Track id is not specified" if params[:id].blank?
    @track = fetch_items_by_ids params[:id]
    raise InteractionErrors::NotFound.new params[:id] if track.blank?
  end

  def as_json opts = {}
    serialize_track track
  end
end
