class Discovery::TrackSearchingInteraction < TrackSearchingInteraction
  include Discovery::Serializers

  def initialize args
    super
    raise InteractionErrors::WrongArgument.new "Query is empty for track searching!" if args[:q].blank?
    unless TorqueBox::ServiceRegistry.lookup("jboss.messaging.default").nil?
      backwriter_queue.publish({key: 'search', data: 
        {
          search_term: args[:q].presence,
          search_type: (args[:autocomplete] ? 'autocomplete' : 'standard'),
          user_ip: args[:user_ip],
          user_uri: args[:user].try(:uri)
        }
      })
    end
    @results = if args[:autocomplete]
      limit = args[:per_page].presence || 10
      track_autocomplete_search(args[:q], limit)
    else
      track_simple_search(args[:q], args[:page], args[:per_page])
    end
  end

  def indices_to_search_in
    [labeled_index]
  end

  def default_must_filters_array
    [
      {term: {"rights.mis_online" => true}},
      {term: {"rights.purchase" => true}},
      {term: {"rights.preview" => true}}
    ]
  end

  def backwriter_queue
    @backwriter_queue ||= TorqueBox::Messaging::Queue.new('/queues/backwriter')
  end

  def as_json params = {}
    pages = @results.total_count / @results.per_page
    pages = 1 if pages.zero?
    {
      current_page: @results.current_page,
      per_page: @results.per_page,
      country: @country,
      total: {
        count: @results.total_count,
        pages: pages
      },
      data: @results.map {|t| serialize_track t}
    }
  end
end
