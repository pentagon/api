class Discovery::ArtistSearchingInteraction < ArtistSearchingInteraction
  include Discovery::Serializers

  def initialize args
    super
    raise InteractionErrors::WrongArgument.new "Query is empty for searching!" if args[:q].blank?
    @results = if args[:autocomplete]
      limit = args[:per_page].presence || 10
      artist_autocomplete_search(args[:q], limit)
    else
      artist_simple_search(args[:q], args[:page], args[:per_page])
    end
  end

  def indices_to_search_in
    [labeled_index]
  end

  def as_json params = {}
    pages = @results.total_count / @results.per_page
    pages = 1 if pages.zero?
    {
      current_page: @results.current_page,
      per_page: @results.per_page,
      country: @country,
      total: {
        count: @results.total_count,
        pages: pages
      },
      data: @results.map {|a| serialize_artist a}
    }
  end
end
