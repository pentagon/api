class Discovery::Authorization::UserCreatingInteraction < ::Authorization::UserCreatingInteraction
  def send_verification_mail user
    AccountsMailer.discovery_verification_instructions(user).deliver
  end
end
