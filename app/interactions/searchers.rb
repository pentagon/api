module Searchers
  module Artist
    include SearchFiltering::Artist

    def artist_simple_search(query_string, page = 1, per_page = 20)
      ::Artist.search page: page, per_page: per_page, index: indices_to_search_in.compact.join(",") do |s|
        s.query {|q| q.match "title", query_string, minimum_should_match: "33%" }
        s.filter(:bool, artist_filter_params)
      end
    end

    def artist_autocomplete_search(query_string, limit = 20)
      ::Artist.search index: indices_to_search_in.compact.join(",") do |s|
        s.query {|q| q.match "title.autocomplete", query_string, minimum_should_match: "33%" }
        s.filter(:bool, artist_filter_params)
        s.size(limit)
      end
    end

    def indices_to_search_in
      [labeled_index, 'unsigned']
    end
  end

  module Album
    include SearchFiltering::Album

    def album_simple_search(query_string, page = 1, per_page = 20)
      ::Album.search page: page, per_page: per_page, index: indices_to_search_in.compact.join(",") do |s|
        s.query {|q| q.match "title", query_string, minimum_should_match: "33%" }
        s.filter(:bool, album_filter_params)
      end
    end

    def album_advanced_search(query_string, page = 1, per_page = 20)
      ::Album.search page: page, per_page: per_page, index: indices_to_search_in.compact.join(",") do |s|
        s.query {|q| q.match ["title", "artist.title"], query_string, type: "cross_fields",
          minimum_should_match: "33%" }
        s.filter(:bool, artist_filter_params)
      end
    end

    def album_autocomplete_search(query_string, limit = 20)
      ::Album.search index: indices_to_search_in.compact.join(",") do |s|
        s.query {|q| q.match "title.autocomplete", query_string, minimum_should_match: "33%" }
        s.filter(:bool, album_filter_params)
        s.size(limit)
      end
    end

    def indices_to_search_in
      [labeled_index, 'unsigned']
    end
  end

  module Track
    include SearchFiltering::Track

    def track_simple_search(query_string, page = 1, per_page = 20)
      dsl = if query_string.present?
        {
          query: {
            multi_match: {
              query: query_string,
              fields: ["artist.title", "title"],
              type: "cross_fields",
              minimum_should_match: "33%"
            }
          }
        }
      else
        {query: {match_all: {}}}
      end
      functions = {
        functions: [
          {
            filter: {term: {'rights.hit' => 1}},
            boost_factor: 2
          },
          {
            filter: {not: {term: {'popularity.popularity' => 0}}},
            field_value_factor: {field: 'popularity.popularity', modifier: 'log1p', factor: 1.5}
          }
        ]
      }
      dsl.update functions
      ::Track.search page: page, per_page: per_page, index: indices_to_search_in.compact.join(",") do |s|
        s.query {|q| q.raw_dsl(:function_score, dsl)}
        s.filter(:bool, track_filter_params)
        s.sort do |sort|
          sort.by(sort_options[:field], sort_options[:direction])
        end
      end
    end

    def track_autocomplete_search(query_string, limit = 20)
      ::Track.search index: indices_to_search_in.compact.join(",") do |s|
        s.query do |q|
          q.match(["artist.title.autocomplete^1.5", "title.autocomplete"], query_string,
            type: "cross_fields", minimum_should_match: "33%")
        end
        s.filter(:bool, track_filter_params)
        s.size(limit)
        s.sort do |sort|
          sort.by(sort_options[:field], sort_options[:direction])
        end
      end
    end

    def track_advanced_search(options, page = 1, per_page = 20)
      must_filters_array = []
      if options[:energy_from].present? || options[:energy_to].present?
        energy_filter = {include_lower: true, include_upper: true}
        energy_filter[:from] = options[:energy_from] if options[:energy_from].present?
        energy_filter[:to] = options[:energy_to] if options[:energy_to].present?
        must_filters_array << {range: {"energy" => energy_filter}}
      end
      if options[:decade_from].present? || options[:decade_to].present?
        decade_filter = {include_lower: true, include_upper: true}
        decade_filter[:from] = options[:decade_from] if options[:decade_from].present?
        decade_filter[:to] = options[:decade_to] if options[:decade_to].present?
        must_filters_array << {range: {"release_date" => decade_filter}}
      end
      if options[:genre_cloud].present?
        nested_hash = {}
        nested_hash[:path] = "genre_cloud"
        nested_hash[:filter] = {}
        nested_hash[:filter][:or] = []
        options[:genre_cloud].each_pair do |name, range|
          genre_cloud_must_filters_array = []
          genre_cloud_must_filters_array << { term: { genre_name: name } }
          genre_filter = {}
          genre_filter[:from] = range[:from]
          genre_filter[:to]   = range[:to]
          genre_cloud_must_filters_array << { range: { probability: genre_filter } }
          nested_hash[:filter][:or] << { bool: { must: genre_cloud_must_filters_array } }
        end
        nested_hash[:_cache] = true
        must_filters_array << { nested: nested_hash }
      end
      if options[:color_tune].present?
        or_array = []
        options[:color_tune].each_pair do |name, range|
          color_filter = {}
          color_filter[:from] = range[:from]
          color_filter[:to] = range[:to]
          or_array << { range: { "color_tune.#{name.downcase}" => color_filter } }
        end
        must_filters_array << { "or" => or_array }
      end
      if options[:color_name].present?
        or_array = []
        options[:color_name].each do |color|
          or_array << { term: { "color.name" => color.downcase } }
        end
        must_filters_array << { "or" => or_array }
      end
      if options[:genre].present?
        or_array = []
        options[:genre].each do |genre|
          or_array << { term: { "genre" => genre } }
        end
        must_filters_array << { "or" => or_array }
      end
      ::Track.search page: page, per_page: per_page, index: indices_to_search_in.compact.join(",") do |s|
        s.query do |q|
          if [options[:title], options[:artist], options[:album]].any?(&:present?)
            q.boolean do |b|
              b.must { |m| m.match "title", options[:title] } if options[:title].present?
              b.must { |m| m.match "album.title", options[:album] } if options[:album].present?
              b.must { |m| m.match "artist.title", options[:artist] } if options[:artist].present?
            end
          else
            q.all
          end
        end
        s.filter(:bool, track_filter_params(must_filters_array))
        s.sort do |sort|
          sort.by(sort_options[:field], sort_options[:direction])
        end
      end
    end

    def indices_to_search_in
      [labeled_index, 'unsigned']
    end

    def sort_options
      {field: "_score", direction: "desc"}
    end
  end
end
