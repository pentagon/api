class Dwl::Authorization::VerifyInteraction < ::Authorization::VerifyInteraction
  def url
    @success ? "#{Settings.dwl_site}/users/confirmation?token=#{@user.authentication_token}" : "#{Settings.dwl_site}/users/confirmation"
  end
end