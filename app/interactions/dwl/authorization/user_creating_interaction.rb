class Dwl::Authorization::UserCreatingInteraction < ::Authorization::UserCreatingInteraction
  def send_verification_mail user
    AccountsMailer.dwl_verification_instructions(user).deliver
  end
end