class Dwl::Authorization::RequestVerificationInteraction < ::Authorization::RequestVerificationInteraction
  include InteractionErrors

  def initialize options = {}
    @user = Persistence::User.find_for_authentication email: options[:email]
    raise InteractionErrors::NotFound.new options[:email] unless @user
    @user.generate_verification_token!
    send_verification_mail(@user)
  end

  def as_json options = {}
    {}
  end

  def send_verification_mail user
    AccountsMailer.dwl_verification_instructions(user).deliver
  end
end
