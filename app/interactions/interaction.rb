# @@@Yehuda Katz ask@@@
# Do you really guess what it's interaction

class Interaction
  include InteractionErrors
  include CatalogUtils

  attr_accessor :explicit_level, :country, :user, :user_ip, :request_protocol

  def initialize params
    @user = params[:user]
    self.country = params[:country]
    @user_ip = params[:user_ip]
    @request_protocol = params[:request_protocol]
  end

  def explicit_level
    @explicit_level ||= (user ? user.explicit_filter.to_i : 0)
  end

  def country= value
    @country = value.to_s.downcase.sub('uk', 'gb')
  end

  # Override this method in your interaction if you need special behaviour regarding default catalog
  def catalog
    catalog_for_country country
  end

  def allowed? item
    return true if @user.try(:superuser)
    return true if item.source_index.starts_with?('unsigned')
    item.rights['available'].include?(country)
  end

  def to_json opts = {}
    GsonEngine.encode self.as_json opts
  end
end
