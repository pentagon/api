class Locate::StatisticInteraction < Interaction
  include Locate::MapReduceFunctions
  include Locate::TrackLoading
  include Locate::UserLoading
  include Locate::Serializers

  def initialize params
    super
    @startkey = params[:start]
    @endkey = params[:end]
    @type = params[:type]
    @counter = params[:counter]
    @id = params[:id]
  end

  def load_data scope
    # collection = "geo_by_period"
    scope.map_reduce(geo_by_period_functions[:map], geo_by_period_functions[:reduce]).
      finalize(geo_by_period_functions[:finalize]).out(inline: true).js_mode.send(:documents).to_a
  end

  def load_data_for_track scope
    # collection = 'geo_for_track'
    scope.map_reduce(geo_for_track_functions[:map], geo_for_track_functions[:reduce]).
      finalize(geo_for_track_functions[:finalize]).out(inline: true).js_mode.send(:documents).to_a
  end

  def get_scope_for_artist
    Persistence::TrackStatistic.where :created_at.gte => @startkey, :created_at.lte => @endkey, :artist_uri => @id
  end

  def get_scope_for_period
    Persistence::TrackStatistic.where :created_at.gte => @startkey, :created_at.lte => @endkey
  end

  def get_scope_for_track
    Persistence::TrackStatistic.where :track_uri => @id, :created_at.gte => @startkey, :created_at.lte => @endkey
  end

  def get_scope_for_user
    Persistence::TrackStatistic.where :user_uri => @id, :created_at.gte => @startkey, :created_at.lte => @endkey
  end

  def geo_by_period
    load_data get_scope_for_period
  end

  def artist
    @artist ||= fetch_items_by_ids @id
  end

  def realtime_listen
    prepare_date
    if @startkey.present?
      boost = @counter.to_i * 300
      @startkey = Time.at(@startdate + boost)
      @endkey = @startkey + 2.hours
    else
      @startkey = 5.minutes.ago
      @endkey = Time.now
    end
    {date_range: @startkey.strftime("%d-%m-%Y %H:%M:%S"), list: geo_by_period}
  end

  def prepare_date
    current_month = Date.today.at_beginning_of_month.to_time.to_i
    six_month_ago = current_month - 6.month
    @startdate = @startkey.present? ? DateTime.strptime(@startkey,'%Y-%m-%d').to_i : six_month_ago
  end

  def artist_listen
    {
      artist_name: artist.title,
      list: load_data(get_scope_for_artist)
    }
  end

  def track_listen
    data = load_data_for_track(get_scope_for_track)
    uris = get_user_uris(data)
    track = load_track_by_uri @id
    {
      track: (serialize_track(track) if track),
      list: data,
      users: load_users_by_uris(uris).map {|user| serialize_user(user)}.each_with_object(Hash.new) {|o, h| h[o[:id]] = o}
    }
  end

  def user_listen
    {
      user: serialize_user(load_user_by_uri @id),
      list: load_data(get_scope_for_user)
    }
  end

  def get_user_uris data
    data.map{|pin| pin['value']['users'].keys}.flatten.reject &:blank?
  end

  def as_json params = {}
    send @type
  end
end
