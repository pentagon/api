module Locate::TrackLoading
  extend ActiveSupport::Concern

  def load_track_by_uri uri
     fetch_items_by_ids uri
  end

  def load_tracks_by_uris
    fetch_items_by_ids Kaminari.paginate_array(@uris).page(@page).per(@per_page)
  end
end
