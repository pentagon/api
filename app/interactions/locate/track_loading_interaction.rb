class Locate::TrackLoadingInteraction < Interaction
  include Locate::TrackLoading
  include Locate::Serializers

  def initialize params
    super
    @uris = params[:uris]
    @page = params[:page] || 1
    @per_page = params[:per_page] || 20
  end

  def as_json params = {}
    load_tracks_by_uris.map {|t| serialize_track t} if @uris.present?
  end
end
