module Locate::UserLoading
  extend ActiveSupport::Concern

  def load_user_by_uri uri
    Persistence::User.find_by(uri: uri)
  end

  def load_users_by_uris uris
    Persistence::User.any_in(uri: uris)
  end

  def serialize_user user
    {
      id: user.uri,
      full_name: user.full_name
    }
  end
end
