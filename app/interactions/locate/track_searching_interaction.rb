class Locate::TrackSearchingInteraction < TrackSearchingInteraction
  include Locate::Serializers

  def initialize args
    super
    raise InteractionErrors::WrongArgument.new "Query is empty for searching!" if args[:q].blank?
    @results = track_simple_search args[:q], args[:page], args[:per_page]
  end

  def as_json params = {}
    @results.map {|t| serialize_track t}
  end
end
