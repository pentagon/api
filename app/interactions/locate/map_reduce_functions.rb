module Locate::MapReduceFunctions
  extend ActiveSupport::Concern

  def geo_by_period_functions
    {
      map: %Q[
        function() {
          if (this.location.length) {
            var key = parseInt(this.location[0]) + ':' + parseInt(this.location[1]);
            if ((key.indexOf('NaN') == -1) && (this.track_uri != '')) {
              emit(key, {count: 1,  city: this.city, country_code: this.country_code, track_uri: this.track_uri});
            }
          }
        }],
      reduce: %Q[
        function(key, values) {
          var res = {count: 0, track_uris: []};
          values.forEach(function(v) {
            if (v.track_uris) {
              res.track_uris = res.track_uris.concat(v.track_uris);
            }
            else {
              res.track_uris.push(v.track_uri);
            }
            res.count += v.count;
          });
          res['city'] = values[0].city;
          res['country_code'] = values[0].country_code;
          return res;
        }],
      finalize: %Q[
        function(key, val) {
          var tracks = {};
          if (val.track_uri) {
            tracks[val.track_uri] = val['count'];
          }
          else{
            if (val.track_uris != undefined) {
              val.track_uris.forEach(function(v) {
                if (tracks[v] != undefined) {
                  tracks[v] += 1;
                }
                else {
                  tracks[v] = 1;
                }
              });
            }
          }
          return {count: val['count'], city: val['city'], country_code: val['country_code'], tracks: tracks};
        }]
    }
  end

  def geo_for_track_functions
    {
      map: %Q[
        function() {
          if (this.location.length) {
            var key = parseInt(this.location[0]) + ':' + parseInt(this.location[1]);
            if ((key.indexOf('NaN') == -1) && (this.track_uri != '')) {
              emit(key, {count: 1,  city: this.city, country_code: this.country_code, track_uri: this.track_uri, user_uri: this.user_uri});
            }
          }
        }],
      reduce: %Q[
        function(key, values) {
          var res = {count: 0, track_uris: [], user_uris: []};
          values.forEach(function(v) {
            if (v.track_uris) {
              res.track_uris = res.track_uris.concat(v.track_uris);
            }
            else {
              res.track_uris.push(v.track_uri);
            }
            if (v.user_uris) {
              res.user_uris = res.user_uris.concat(v.user_uris);
            }
            else{
              res.user_uris.push(v.user_uri);
            }
            res.count += v.count;
          });
          res['city'] = values[0].city;
          res['country_code'] = values[0].country_code;
          return res;
        }],
      finalize: %Q[
        function(key, val) {
          var tracks = {};
          var users = {};
          if (val.user_uri != undefined) {
            if (users[val.user_uri]) {
              if (val.user_uri.indexOf('temp') >= 0) {
                users[''] += 1;
              }
              else{
                users[val.user_uri] += 1;
              }
            }
            else {
              if (val.user_uri.indexOf('temp') >= 0) {
                users[''] = 1;
              }
              else {
                users[val.user_uri] = 1;
              }
            }
          }
          else{
            if (val.user_uris != undefined) {
              val.user_uris.forEach(function(v) {
                if (users[v] != undefined) {
                  if (v.indexOf('temp') >= 0) {
                    users[''] += 1;
                  }
                  else {
                    users[v] += 1;
                  }
                }
                else {
                  if (v.indexOf('temp') >= 0) {
                    users[''] = 1;
                  }
                  else{
                    users[v] = 1;
                  }
                }
              });
            }
          }
          if (val.track_uri) {
            tracks[val.track_uri] = val['count'];
          }
          else{
            if (val.track_uris != undefined) {
              val.track_uris.forEach(function(v) {
                if (tracks[v] != undefined) {
                  tracks[v] += 1;
                }
                else {
                  tracks[v] = 1;
                }
              });
            }
          }
          return {count: val['count'], city: val['city'], country_code: val['country_code'], tracks: tracks, users: users};
        }]
    }
  end
end
