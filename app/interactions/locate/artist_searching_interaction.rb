class Locate::ArtistSearchingInteraction < ArtistSearchingInteraction
  include Locate::Serializers

  def initialize args
    super
    raise InteractionErrors::WrongArgument.new "Query is empty for artist searching!" if args[:q].blank?
    @results = artist_simple_search args[:q], args[:page], args[:per_page]
  end

  def as_json params = {}
    @results.map {|a| serialize_artist a}
  end
end
