class Blog::InitsLoadingInteraction < Interaction
  attr_reader :responder

  def initialize params
    master_post = Persistence::Blog::Post.enabled.find_by(master: true)
    master_post = Persistence::Blog::Post.enabled.first unless master_post
    @responder = {
      init: {
        id: 'v1',
        main_image: master_post.try{ |a| a.image_url }
      }
    }
  end

  def as_json params = {}
    @responder
  end
end
