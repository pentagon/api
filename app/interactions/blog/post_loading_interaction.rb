class Blog::PostLoadingInteraction < Interaction
  include Blog::Serializers

  attr_reader :responder

  def initialize params
    super
    @responder = Persistence::Blog::Post.enabled.find(params[:id])
  end

  def as_json params = {}
    {
      post: serialize_post(@responder)
    }
  end
end
