class Blog::CategoriesLoadingInteraction < Interaction
  include Blog::Serializers

  attr_reader :responder

  def initialize params
    super
    @responder = Persistence::Blog::Category.enabled.all
  end

  def as_json params = {}
    {
      categories: @responder.map { |a| serialize_category a }
    }
  end
end
