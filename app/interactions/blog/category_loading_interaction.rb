class Blog::CategoryLoadingInteraction < Interaction
  include Blog::Serializers

  attr_reader :responder

  def initialize params
    super
    @responder = Persistence::Blog::Category.enabled.find params[:id]
  end

  def as_json params = {}
    {
      category: serialize_category(@responder)
    }
  end
end
