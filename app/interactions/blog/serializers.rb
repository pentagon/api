module Blog::Serializers
  extend ActiveSupport::Concern

  def serialize_category o
    {
      id: o.id,
      name: o.name,
      color: o.color,
      permalink: o.slug,
      post_ids: (o.posts.present? ? o.posts.enabled.pluck(:id) : [])
    }
  end

  def serialize_comment o
    {
      id: o.id,
      created_at: o.created_at,
      text: o.text,
      user_uri: o.user.uri,
      user_image: o.user.image_url,
      user_name: o.user.display_name,
      post_id: o.post_id
    }
  end

  def serialize_post o
    {
      id: o.id,
      title: o.title,
      permalink: o.slug,
      published_at: o.published_at,
      text: o.text,
      master: o.master,
      category_id: o.category_id,
      comment_ids: (o.comments.present? ? o.comments.desc(:created_at).pluck(:id) : []),
      image_url: o.image_url,
      tags: o.tags_array || o.tags,
      created_by_name: o.created_by_name,
      brief_text: o.brief_text
    }
  end
end
