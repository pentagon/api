class Blog::PostsLoadingInteraction < Interaction
  include Blog::Serializers

  attr_reader :responder

  def initialize params
    super
    posts_chain = Persistence::Blog::Post.enabled.includes(:category)
    @responder = if params[:ids]
      posts_chain.find(params[:ids])
    elsif params[:year]
      d1 = Date.new(params[:year].to_i)
      d2 = Date.new(params[:year].to_i,12,31)
      posts_chain.where(published_at: d1..d2).desc(:published_at)
    elsif params[:q]
      _search = params[:q]
      _tags = _search.scan(/tag:(\w+)/).flatten.map &:downcase
      s = Tire.search('blog')
      if _tags.any?
        s.filter :terms, tags: _tags
      else
        s.query { flt _search ,  min_similarity: 0.5 }
        # s.query { string _search }
      end.results
    elsif params[:relatedTo]
      if relatedPost = posts_chain.find_by_slug(params[:relatedTo])
        Tire.search('blog', size: 3).filter(:terms, tags: relatedPost.tags_array.map(&:downcase)).filter(:bool, must_not: {term: {_id: relatedPost.uri}}).results
      end || []
    else
      posts_chain.desc(:master).desc(:published_at).limit(26)
    end.reject {|a| a.disabled }
  end

  def as_json params = {}
    {
      posts: @responder.map { |a| serialize_post a }
    }
  end
end
