class Blog::CommentsLoadingInteraction < Interaction
  include Blog::Serializers

  attr_reader :responder

  def initialize params
    super
    @responder = if params[:post_id]
      Persistence::Blog::Post.enabled.find(params[:post_id]).try(:comments)
    else
      Persistence::Blog::Comment.find(params[:ids])
    end
  end

  def as_json params = {}
    {
      comments: @responder.map { |a| serialize_comment a }
    }
  end
end
