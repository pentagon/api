class Blog::CommentCreatingInteraction < Interaction
  include Blog::Serializers

  attr_reader :responder

  def initialize params
    super
    post = Persistence::Blog::Post.enabled.find params[:comment_params][:post_id]
    @responder = post.comments.new params[:comment_params]
    @responder.user = @user
    @responder.save
  end

  def as_json params = {}
    {
      comment: serialize_comment(@responder)
    }
  end
end
