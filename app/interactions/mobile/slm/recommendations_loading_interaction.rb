class Mobile::Slm::RecommendationsLoadingInteraction < TrackSearchingInteraction

  COLOR_TUNE_ACTIVITY_MAP = {'1' => :red, '2' => :orange, '3' => :yellow, '4' => :green, '5' => :blue,
    '6' => :purple, '7' => :black, '8' => :grey, '9' => :white}

  def initialize args
    super
    raise InteractionErrors::WrongArgument.new "Wrong value for activity: #{args[:activity]}" unless
      COLOR_TUNE_ACTIVITY_MAP.keys.include? args[:activity]
    @results = search COLOR_TUNE_ACTIVITY_MAP[args[:activity]], args[:page], args[:per_page]
  end

  def search color, page, per_page
    Track.search page: page, per_page: per_page, index: indices_to_search_in.compact.join(",") do
      query { all }
      filter :bool, track_filter_params([{range: {"color_tune.#{color}" => {gte: 100, lte: 255}}}])
      sort {by :popularity, 'desc'}
    end
  end

  def default_must_filters_array
    super << {term: {'rights.hit' => 1}}
  end

  def indices_to_search_in
    [labeled_index]
  end

  def serialize_track track
    {
      id: track.id,
      title: track.title,
      album_name: track.album.try(:title),
      artist_name: track.artist.try(:title),
      release_year: track.release_date.try {|d| d[0..3]},
      duration: track.duration,
      image_url: track.image_url,
      music_url: track.music_url
    }
  end

  def as_json opts = {}
    {tracks: @results.map {|t| serialize_track t}}
  end
end
