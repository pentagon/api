# {
#   "birthdate"        : "1928-3-7",
#   "birthplace"       : "53 Quarry Knowe, Rutherglen, Glasgow, South Lanarkshire G73 2RN, UK",
#   "gender"           : "M" or "F" or "N/A",
#   "school_list"  : ["Male', Maldives"]
# }

class Mobile::Slm::ProfileCreatingInteraction < Mobile::Interaction
  include Mobile::Slm::Serializers

  def initialize args
    super
    @user.personal_info.date_of_birth = args[:birthdate]
    @user.personal_info.place_of_birth = args[:birthplace]
    @user.personal_info.gender = args[:gender]
    @user.personal_info.school_list = args[:school_list]
    unless @user.save
      raise InteractionErrors::WrongArgument.new @user.errors.full_messages.join('; ')
    end
  end

  def as_json opts = {}
    serialize_user @user
  end
end
