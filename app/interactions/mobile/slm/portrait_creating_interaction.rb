# REQUEST:
# {
#   "track_feedback" : [
#     {
#       "track_uri": "uplaya:track:58281",
#       "type": "like" or "dislike" or "none",
#     },
#     ...
#   ]
# }
#
# RESPONSE:
# {
#   "portrait_text" : "You are an awesome person, with O.C.E.A.N. parameters and ЧСВ all rated over 9000!!!!",
#   "portrait_color" : "blue"/"red"
# }

class Mobile::Slm::PortraitCreatingInteraction < Mobile::Interaction
  def initialize args
    super
    args[:track_feedback].each do |f|
      Persistence::SlmUserTrackFeedback.create user_uri: @user.uri, track_uri: f[:track_uri], status: f[:type]
    end
  end

  def as_json opts = {}
    {
      portrait_text: "You are an awesome person ...",
      portrait_color: "blue"
    }
  end
end
