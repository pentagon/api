module Mobile::Slm::Serializers
  extend ActiveSupport::Concern

  def serialize_user user
    {
      email: user.email,
      birthdate: user.personal_info.date_of_birth,
      birthplace: user.personal_info.place_of_birth,
      gender: user.personal_info.gender,
      school_list: user.personal_info.school_list
    }
  end
end
