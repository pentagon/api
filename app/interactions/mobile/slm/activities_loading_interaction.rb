class Mobile::Slm::ActivitiesLoadingInteraction < Interaction
  def initialize args
    super
    @@activities ||= YAML.load_file File.join Rails.root, 'config', 'slm_activities.yml'
  end

  def as_json opts = {}
    @@activities
  end
end
