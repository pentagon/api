class Mobile::Slm::ProfileLoadingInteraction < Mobile::Interaction
  include Mobile::Slm::Serializers

  def as_json opts = {}
    serialize_user @user
  end
end
