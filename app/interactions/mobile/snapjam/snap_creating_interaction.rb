class Mobile::Snapjam::SnapCreatingInteraction < Mobile::Interaction
  attr_accessor :snap

  def initialize(args)
    super
    @snap = create_from_params args
  end

  def create_from_params params
    file = extract_image_to_file params[:image]
    preview = extract_image_to_file params[:preview], 'preview_'
    md5 = Digest::MD5.file(file).hexdigest
    res = Persistence::SnapTrack.find_or_create_by md5sum: md5, user_uri: params[:user_uri] do |r|
      r.image = file
      r.preview = preview
      r.track_uri = params[:track_uri]
      r.track = Track.fetch params[:track_uri]
      r.geo_location = params[:geo_location]
      r.created_at = DateTime.parse(params[:date]) rescue DateTime.now
      r.updated_at = DateTime.parse(params[:date]) rescue DateTime.now
    end
    res.touch
    res
  end

  def extract_image_to_file buffer, prefix = ''
    if buffer
      file = Tempfile.new ["#{prefix}snap_track_#{DateTime.now.to_i}", '.png']
      file.write Base64.decode64 buffer
      file.close
      file
    end
  end

  def share_url
    File.join Settings.mobile.snap_track_base_url, snap.id
  end

  def short_url
    snap['short_url'].presence or get_and_set_short_url
  end

  def get_and_set_short_url
    res = Googl.shorten(share_url).short_url rescue ''
    snap.update_attribute :short_url, res unless res.blank?
    res
  end

  def as_json(opts = {})
    {
      share_url: share_url,
      short_url: short_url
    }
  end
end
