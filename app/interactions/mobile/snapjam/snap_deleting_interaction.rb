class Mobile::Snapjam::SnapDeletingInteraction < Mobile::Interaction
  def initialize(args)
    super
    st = Persistence::SnapTrack.find args[:id]
    if user.uri.eql? st.user_uri
      st.destroy
    else
      raise InteractionErrors::Forbidden.new
    end
  end

  def as_json(opts = {})
    {}
  end
end
