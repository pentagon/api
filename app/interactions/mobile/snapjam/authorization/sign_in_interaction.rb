class Mobile::Snapjam::Authorization::SignInInteraction < ::Authorization::SignInInteraction
  include Mobile::Serializers
  include Mobile::Snapjam::Serializers

  def get_params options = {}
    @params = options[:user_params]
    @user_params = options[:user_params]
  end

  def additional_checks; end

  def as_json options = {}
    serialize_user(user)
  end
end
