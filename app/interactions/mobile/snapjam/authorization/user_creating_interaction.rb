class Mobile::Snapjam::Authorization::UserCreatingInteraction < ::Authorization::UserCreatingInteraction
  include Mobile::Serializers
  include Mobile::Snapjam::Serializers

  def get_base_params options = {}
    @params, @remote_ip, @user_params = options[:params], options[:remote_ip], options[:params]
  end

end
