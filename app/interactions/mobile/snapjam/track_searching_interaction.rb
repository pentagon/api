class Mobile::Snapjam::TrackSearchingInteraction < Mobile::Interaction
  include SearchFiltering::Track
  COLOR_TUNE_KEYS = [:white, :yellow, :grey, :red, :purple, :orange, :green, :blue, :black]
  attr_accessor :results

  def initialize args
    super
    @results = track_by_colors_search(args.slice(*COLOR_TUNE_KEYS, :th), args[:amount] || 50,
      args[:offset].to_i)
  end

  def track_by_colors_search(colors = {}, amount, offset)
    threshold = colors[:th].to_i || 10
    color_keys = COLOR_TUNE_KEYS.select { |key| colors[key] }
    color_filters = color_keys.map do |key|
      value = colors[key].to_i
      start_val, end_val = value > threshold ? [value - threshold, value + threshold] : [0, threshold]
      {range: {"color_tune.#{key}" => {gte: start_val, lte: end_val}}}
    end
    Track.search index: indices_to_search_in.compact.join(",") do |s|
      s.query {|q| q.raw_dsl :function_score, scoring_dsl}
      s.filter :bool, track_filter_params(color_filters)
      s.sort {by :popularity, 'desc'}
      s.size amount
      s.from offset
    end
  end

  def scoring_dsl
    # TODO use per country popularity field when we get them filled in in ES
    {
      query: {match_all: {}},
      functions: [
        {filter: {term: {'rights.hit' => 1}},boost_factor: 2},
        {filter: {not: {term: {'popularity.popularity' => 0}}},
          field_value_factor: {field: 'popularity.popularity', modifier: 'log1p', factor: 1.5}}
      ]
    }
  end

  def indices_to_search_in
    [labeled_index]
  end

  def serialize_track track
    {
      id: track.id,
      title: track.title,
      artist_name: track.artist.try(:title),
      duration: track.duration,
      image_url: track.image_url,
      preview_track_url: track.music_url
    }
  end

  def filtered_results
    results.group_by {|r| r.artist['id']}.values.map(&:shuffle).map(&:first).flatten
  end

  def as_json opts = {}
    filtered_results.map {|t| serialize_track t}
  end
end
