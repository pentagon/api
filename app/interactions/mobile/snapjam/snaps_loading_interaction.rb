class Mobile::Snapjam::SnapsLoadingInteraction < Mobile::Interaction
  attr_accessor :snaps

  def initialize(args)
    super
    @snaps = Persistence::SnapTrack.where(user_uri: user.uri).desc(:created_at).
      limit(args[:amount] || 20).offset(args[:offset] || 0)
  end

  def image_url(snap)
    File.join Settings.storage_url, snap.uri.split(':')[0..1], snap.image.filename
  end

  def preview_url(snap)
    File.join Settings.storage_url, snap.uri.split(':')[0..1], snap.preview.filename
  end

  def share_url(snap)
    File.join Settings.mobile.snap_track_base_url, snap.id
  end

  def track_for_snap(snap)
    Track.fetch(snap.track_uri) or snap.track
  end

  def serialize_snap(snap)
    {
      id: snap.id,
      geo_location: snap.id,
      image_url: image_url(snap),
      preview_url: preview_url(snap),
      share_url: share_url(snap),
      short_url: snap.short_url,
      created_at: snap.created_at,
      updated_at: snap.updated_at,
      track: track_for_snap(snap)
    }
  end

  def as_json(opts = {})
    snaps.map {|s| serialize_snap s}
  end
end
