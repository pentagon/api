module Mobile::AstroRadio
  extend ActiveSupport::Concern
  attr_accessor :station, :pool_mgr, :radio_stream, :subscription

  USER_REACTIONS_MAP = {
    'skip' => 'add_negative_influence_song',
    'like' => 'add_cached_positive_influence'
  }

  # Override user country with initial station territory
  def catalog
    @station.country
  end

  def explicit_level
    @explicit_level = 0
  end

  def age_filters_hash
    dob_year = user.personal_info.date_of_birth.try(:year) || DateTime.now.year - 24
    {
      past_min_year: dob_year + 16,
      past_max_year: dob_year + 20,
      present_min_year: DateTime.now.year - 2
    }
  end

  def region
    station.country.eql?('us') ? 'us' : 'uk'
  end

  def pool_mgr
    @pool_mgr ||= begin
      res = Radio::PoolManager.new station
      res.command_class = MisCommand::GetAstroRadioData
      res
    end
  end

  def init_start_tracks
    if station.seed_track_uris.any?
      pool_mgr.send_sync_mis_request do |r|
        station.seed_track_uris.each do |uri|
          request = age_filters_hash.merge(type: 'add_astro_positive_influence', seed_track_uri: uri, region: region,
            apply_dmca: false, tracks_to_add: 100)
          r.add_request_data request
        end
      end
    end
  end

  def get_radio_stream_for_track track
    ::Radio::StreamResolver.new(ip_address: user_ip, station_uri: @station.uri, user: @user,
      track: track, agent: agent_string, user_country: @country).get_radio_location
  end

  def increment_tracks_counter_on_subscription
    if subscription && subscription.usable?
      subscription.custom_options["track_requests"] ||= 0
      subscription.custom_options["track_requests"] += 1
      subscription.save
    end
  end

  def radio_stream_url
    radio_stream[:location]
  end

  def track
    @track ||= begin
      track = nil
      loop do
        track_id = pool_mgr.pick_next_track_id
        track = fetch_items_by_ids track_id
        unless track
          Rails.logger.error "cannot fetch track: '#{track_id}', trying next one..."
          next
        end
        if (@radio_stream = get_radio_stream_for_track track)[:success]
          increment_tracks_counter_on_subscription
          break
        end
        Rails.logger.error "cannot fetch MN stream for track: '#{track_id}', trying next one..."
      end
      track
    rescue Radio::PoolManager::QueueExhausted => e
      Rails.logger.error e.message
      raise InteractionErrors::RadioQueueEmpty.new "Radio queue exhausted for radio station: #{@station.uri}"
    end
  end
end
