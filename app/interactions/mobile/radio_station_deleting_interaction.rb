class Mobile::RadioStationDeletingInteraction < Mobile::Interaction
  def initialize args
    super
    station = Persistence::Station.find_by(user_uri: @user.uri, uri: args[:station_id])
    raise InteractionErrors::NotFound.new args[:station_id] unless station
    station.destroy
  end

  def as_json opts = {}
    {sucess: true}
  end
end
