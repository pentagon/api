class Mobile::Radio::Authorization::SignInInteraction < ::Authorization::SignInInteraction
  include Mobile::Serializers
  include Mobile::Radio::Authorization::Serializers

  def get_params options
    @params = options[:user_params]
    @user_params = options[:user_params]
  end

  def as_json options = {}
    {
      user: serialize_user(@user)
    }
  end

  def additional_checks
    raise ::Authorization::Exceptions::UnverifiedUser unless @user.is_verified
  end
end
