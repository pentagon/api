module Mobile::Radio::Authorization::Serializers

  include Persistence::Helpers

  def serialize_user user
    {
      id: user.id,
      email: user.email,
      display_name: user.display_name,
      avatar_url: user.image_url,
      uri: user.uri,
      superuser: user.superuser?,
      subscription: serialize_subscription(Persistence::Subscription.get_latest_of_type_for_user 'radio', user.uri),
      contact_info: {
        country: country_name(user.contact_info.country),
        state: state_name(user.contact_info.country, user.contact_info.state),
        city: user.contact_info.city,
        postal_code: user.contact_info.postal_code
      },
      personal_info: {
        id: user.personal_info.id,
        first_name: user.personal_info.first_name,
        last_name: user.personal_info.last_name
      },
      authentication_token: user.authentication_token
    }
  end
end
