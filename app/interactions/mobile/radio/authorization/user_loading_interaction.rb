class Mobile::Radio::Authorization::UserLoadingInteraction < ::Authorization::UserLoadingInteraction
  include Mobile::Serializers
  include Mobile::Radio::Authorization::Serializers

  def as_json options = {}
    {
      user: serialize_user(@current_user)
    }
  end
end
