class Mobile::Radio::Authorization::UserCreatingInteraction < ::Authorization::UserCreatingInteraction
  include Mobile::Serializers
  include Mobile::Radio::Authorization::Serializers

  def initialize options = {}
    @params, @remote_ip, @tracking = options[:params], options[:remote_ip], options[:params][:tracking]
    @user_params = @params
    @user_params.merge!(get_tracking_params) if @params[:tracking].present?
    update_from_geo_info unless @user_params[:contact_info_attributes].present?
    @user_params[:email] = @user_params[:email].try(:downcase)
    @user = Persistence::User.new(@user_params)
    @user.generate_verification_token
    if @user.save
      send_verification_mail(@user)
    end
    @errors = @user.errors
  end

  def as_json options = {}
    {
      user: serialize_user(@user)
    }
  end

  def send_verification_mail user
    AccountsMailer.radio_verification_instructions(user).deliver
  end
end
