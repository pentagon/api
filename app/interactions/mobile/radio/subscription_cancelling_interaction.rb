class Mobile::Radio::SubscriptionCancellingInteraction < Mobile::SubscriptionCancellingInteraction

private

  def after_cancel
    ::RadioMailer.subscription_cancelled(@subscription).deliver
  end
end
