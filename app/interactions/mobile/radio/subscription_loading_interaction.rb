class Mobile::Radio::SubscriptionLoadingInteraction < Mobile::SubscriptionLoadingInteraction
  def subscription_type
    'radio'
  end
end
