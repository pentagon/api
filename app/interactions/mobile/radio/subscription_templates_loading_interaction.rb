class Mobile::Radio::SubscriptionTemplatesLoadingInteraction < Mobile::Interaction
  def initialize args
    super
    @templates = SubscriptionTemplate.select args.merge(type: 'radio')
  end

  def as_json opts = {}
    {templates: @templates}
  end
end
