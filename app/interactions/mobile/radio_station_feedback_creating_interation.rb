# Like/Dislike/Play
#   Request: [POST] https://restapi.tunehog.com/api/radio/stations/api:station:1398687881474462/send_request
#            [POST] {"track_uri":"medianet:track:1694149","type":"like"}
class Mobile::RadioStationFeedbackCreatingInteraction < Mobile::Interaction
  def initialize args
    super
    request_type = args[:type]
    raise InteractionErrors::WrongArgument.new 'station_id is not specified!' if args[:station_id].blank?
    raise InteractionErrors::WrongArgument.new 'track_uri is not specified!' if args[:track_uri].blank?
    raise InteractionErrors::WrongArgument.new 'Invalid request type' unless
      @station.mis_requests_manager.class::TRACK_REQUEST_TYPES.keys.map(&:to_s).include?(request_type)
    @station = Persistence::Station.find_by uri: args[:station_id], user_uri: @user.uri
    raise InteractionErrors::NotFound.new args[:station_id] unless @station
    filters = args[:filters].present? ? JSON.parse(args[:filters]) : {}
    @station.mis_requests_manager.send_track_request(request_type, args[:track_uri], filters)
  end

  def as_json opts = {}
    {success: true, message: "Request sent."}
  end
end
