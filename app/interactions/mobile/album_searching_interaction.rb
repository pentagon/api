class Mobile::AlbumSearchingInteraction < AlbumSearchingInteraction
  include Mobile::Serializers

  def initialize args
    super
    raise InteractionErrors::WrongArgument.new "Query is empty for searching!" if params[:q].blank?
    @results = album_simple_search args[:q], args[:page], args[:per_page]
  end

  def as_json opts = {}
    @results.map {|t| serialize_album t}
  end
end
