class Mobile::SubscriptionCancellingInteraction < Mobile::Interaction
  include SubscriptionUtils::Cancelling
  include Mobile::Serializers


  def initialize args
    super
    @subscription = Persistence::Subscription.find_by uri: args[:subscription_id]
    raise InteractionErrors::NotFound.new args[:subscription_id] unless @subscription
    raise InteractionErrors::Forbidden.new unless @subscription.user_uri.eql?(user.uri)
    raise InteractionErrors::UnprocessableEntity.new unless cancel_subscription(@subscription)
    after_cancel
  end

  def as_json opts = {}
    {
      subscription: serialize_subscription(@subscription)
    }
  end

private

  def after_cancel
    # Override to do something useful.
  end
end
