class Mobile::Startune::ReportPurchasing < Mobile::Interaction

  STARTUNE_DEPARTMENT_CODE = "505"

  def initialize(args = {})
    super
    @args = args
    @controller = args[:controller]
    @report = ::StartuneReport.digital_goods_find_by_id(args[:id], country: country)
    @customer = user
  end

  def buy
    return handle_error("item_not_found") unless @report
    params = {
      ip_address: user_ip,
      country: country,
      agent: (agent_string.presence || @args[:agent]),
      department_code: STARTUNE_DEPARTMENT_CODE,
      mobile: true,
      custom_options: {
        "hdn_id"      => @args[:hdn_id],
        "first_name"  => @args[:first_name],
        "last_name"   => @args[:last_name],
        "user_email"  => @args[:user_email],
        "dd_day"      => @args[:dd_day],
        "dd_month"    => @args[:dd_month],
        "dd_year"     => @args[:dd_year],
      }
    }
    payment = ::Persistence::PaymentOrder.create_temporary_payment!(@report, @customer, params)

    url_params = {
      host: Settings.domain,
      protocol: @controller.request.protocol,
      params: {
        th_token: @args[:token],
        ip_address: user_ip
      }
    }
    confirm_purchase_url = @controller.confirm_purchase_payment_url(payment.uri, url_params)
    confirm_cancel_url = @controller.confirm_cancel_payment_url(payment.uri, url_params)
    payment_redirect_url = payment.purchase_redirect_url(confirm_purchase_url, confirm_cancel_url)

    @controller.redirect_to payment_redirect_url
  rescue ::Persistence::PaymentOrder::PaymentError => e
    reason = e.payments_message_code if e.respond_to?(:payments_message_code)
    handle_error(reason)
  end

private

  def handle_error(reason = nil)
    redirect_url = @args[:error_url].presence || @controller.result_payments_url({
      host: Settings.domain,
      protocol: @controller.request.protocol,
      status: 'error',
      params: {reason: reason, token: @args[:token]}
    })
    @controller.redirect_to redirect_url
  end
end
