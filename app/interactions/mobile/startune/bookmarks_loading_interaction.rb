class Mobile::Startune::BookmarksLoadingInteraction < Mobile::Interaction
  include Mobile::Startune::Serializers

  def initialize args
    super
    @track_uris = Persistence::BookmarkedTrackPool.find_or_create_by(user_uri: @user.uri, application: 'startune').track_uris
  end

  def explicit_level
    @explicit_level = 0
  end

  def tracks
    @tracks ||= fetch_items_by_ids @track_uris
  end

  def as_json opts = {}
    {tracks: tracks.map {|t| serialize_track t}}
  end
end
