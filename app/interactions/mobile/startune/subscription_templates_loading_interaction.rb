class Mobile::Startune::SubscriptionTemplatesLoadingInteraction < Mobile::Interaction
  def initialize args
    super
    @templates = SubscriptionTemplate.select args.merge(type: 'startune')
  end

  def as_json opts = {}
    {templates: @templates}
  end
end
