class Mobile::Startune::SubscribeInitiatingInteraction < ::Mobile::SubscribeInitiatingInteraction

  def department_code
    "505"
  end

  def return_url
    Rails.application.routes.url_helpers.subscribe_confirm_mobile_startune_subscriptions_url(
      host: Settings.domain, params: subscription_params)
  end

  def success_url
    Rails.application.routes.url_helpers.result_success_mobile_startune_subscriptions_url(
      host: Settings.domain)
  end

  def cancel_url
    Rails.application.routes.url_helpers.result_cancel_mobile_startune_subscriptions_url(
      host: Settings.domain)
  end

  def error_url
    Rails.application.routes.url_helpers.result_error_mobile_startune_subscriptions_url(
      host: Settings.domain)
  end
end
