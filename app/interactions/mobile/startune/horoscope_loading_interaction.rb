class Mobile::Startune::HoroscopeLoadingInteraction < HoroscopeLoadingInteraction
  include Mobile::Startune::Serializers

  def explicit_level
    @explicit_level = 0
  end

  def consumer
    'startune'
  end

  def get_data_for_sign
    horoscope.data.detect {|i| i['sign'].to_s == ALLOWED_SIGNS.index(sign).to_s}
  end

  def fetch_data_from_service
    RestClient.get Settings.startune.horoscope_url
  rescue Exception => e
    Rails.logger.error "Failed to fetch horoscope! Error: #{e.message}"
    raise e
  end

  def as_json opts = {}
    data = get_data_for_sign
    {
      date: horoscope.date_from,
      zodiac_id: sign,
      title: '',
      text: data['present_desc'],
      tracks: tracks.map {|t| serialize_track t}
    }
  end
end
