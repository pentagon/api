class Mobile::Startune::ConstraintsChecker < Mobile::ConstraintsChecker
  def subscription_name
    'startune'
  end

  def app_name
    'startune'
  end
end
