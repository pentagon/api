class Mobile::Startune::TemplatesLoadingInteraction < Mobile::Interaction

  def get_list
    @items ||= ::StartuneReport.templates
  rescue ::Persistemce::PaymentOrder::ItemProviderError
    raise InteractionErrors::ThirdPartyFailure
  end

  def serialize_item(item)
    item
  end

  def as_json(opts = {})
    get_list.map { |item| serialize_item(item) }
  end
end
