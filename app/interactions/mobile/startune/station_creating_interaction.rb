class Mobile::Startune::StationCreatingInteraction < Mobile::Interaction
  include Radio::RadioRequest
  include Mobile::AstroRadio
  attr_accessor :station

  def initialize args
    super
    @errors = InteractionErrors::CodedErrors.new
    @errors.add :invalid_params if args[:title].blank? or args[:seed_track_uris].blank?
    @errors.add :no_valid_subscription unless can_user_create_commercial_stations?
    raise InteractionErrors::UnprocessableEntity.new @errors if @errors.any?
    params = args.slice :seed_track_uris, :title
    params.update user_uri: @user.uri, country: @country, is_free: false, source: 'startune'
    @station = Persistence::Station.find_or_create_by params
    raise InteractionErrors::UnprocessableEntity.new @station.errors if @station.errors.any?
    init_start_tracks
  end

  def can_user_create_commercial_stations?
    user.has_usable_subscription? 'startune'
  end

  def as_json opts = {}
    {
      id: station.uri,
      title: station.title,
      image_url: station.image_url,
      seed_track_uris: station.seed_track_uris
    }
  end
end
