class Mobile::Startune::SubscribeConfirmingInteraction < ::Mobile::SubscribeConfirmingInteraction

private

  def after_subscribe
    ::StartuneMailer.subscription_confirmation(@subscription).deliver
  end

end
