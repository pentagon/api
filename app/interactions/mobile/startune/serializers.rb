module Mobile::Startune::Serializers
  extend ActiveSupport::Concern

  def serialize_track track
    {
      id: track.id,
      title: track.title,
      album_uri: track.album.try(:id),
      album_name: track.album.try(:title),
      artist_uri: track.artist.try(:id),
      artist_name: track.artist.try(:title),
      duration: track.duration,
      image_url: track.image_url,
      music_url: track.music_url,
      rights: transform_rights(track.rights)
    }
  end

  def serialize_user user
    #TODO: Ping Kovalsky when removed it
    subscription = Persistence::Subscription.get_latest_of_type_for_user 'startune', user.uri
    result = {
        uri: user.uri,
        user: {
          uri: user.uri,
          email: user.email.downcase,
          avatar_url: user.image_url,
          display_name: user.display_name,
          subscription: serialize_subscription(subscription),
          date_of_birth: user.personal_info.try(:date_of_birth).try {|t| t.strftime("%Y-%m-%d")},
          zodiac_sign: user.personal_info.try(:date_of_birth).try(:zodiac_sign),
          authentication_token: user.authentication_token
        },
        email: user.email.downcase,
        avatar_url: user.image_url,
        display_name: user.display_name,
        subscription: serialize_subscription(subscription),
        date_of_birth: user.personal_info.try(:date_of_birth).try {|t| t.strftime("%Y-%m-%d")},
        zodiac_sign: user.personal_info.try(:date_of_birth).try(:zodiac_sign),
        authentication_token: user.authentication_token
    }
  end

  def serialize_subscription(subscription)
    return {} if subscription.blank?
    tracks_listened = subscription.custom_options['track_requests'].to_i
    tracks_limit_base = subscription.custom_options['labeled_tracks_limit'].to_i
    tracks_limit_bonus = subscription.custom_options['bonus_track_requests'].to_i
    tracks_limit_total = tracks_limit_base + tracks_limit_bonus
    tracks_left = tracks_limit_total - tracks_listened
    trial_end_date = subscription.created_at + subscription.trial_length if subscription.is_internal_trial
    {
      id: subscription.uri,
      status: subscription.status,
      creation_date: subscription.created_at,
      start_date: (subscription.last_pay_date || subscription.created_at),
      end_date: subscription.next_payment_date,
      seconds_listened: (tracks_listened * 10 * 3600 / 172),
      seconds_left: (tracks_left  * 10 * 3600 / 172),
      seconds_alacarte: (tracks_limit_bonus * 10 * 3600 / 172),
      country: subscription.country,
      price: subscription.price,
      trial_end_date: trial_end_date,
      is_trial: subscription.is_internal_trial,
      is_usable: subscription.usable?,
      currency: subscription.currency
    }
  end

end
