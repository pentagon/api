class Mobile::Startune::SubscriptionCancellingInteraction < Mobile::SubscriptionCancellingInteraction

private

  def after_cancel
    ::StartuneMailer.subscription_cancelled(@subscription).deliver
  end

end
