# curl -XPOST -F "email=test@user.me" -F "report=@report.pdf" -F "id=11122333" \
# http://restapi.tunehog.com/api/mobile/startune/reports.json

class Mobile::Startune::ReportCreatingInteraction
  include Mobile::Startune::ReportUtils
  attr_accessor :id, :email, :report

  def initialize(args)
    @report = Persistence::StartuneReport.find_by order_number: args[:id]
    raise InteractionErrors::WrongArgument.new "Invalid Order No: '#{args[:id]}'" unless report
    @report.update_attributes status: 'COMPLETE', report_file: store_file(args[:report]),
      email: args[:email]
    send_email_notification args[:email]
  end

  def store_file(file)
    if file.is_a?(ActionDispatch::Http::UploadedFile)
      Rails.logger.info "[Startune::ReportCreatingInteraction] gonna create dest_dir '#{dest_dir_name}'..."
      FileUtils.mkpath dest_dir_name
      Rails.logger.info "[Startune::ReportCreatingInteraction] done."
      dest_fname = File.join(dest_dir_name, @report.uri.split(':').last) + File.extname(file.original_filename)
      Rails.logger.info "[Startune::ReportCreatingInteraction] gonna save incoming file to '#{dest_fname}'..."
      FileUtils.move file.path, dest_fname
      Rails.logger.info "[Startune::ReportCreatingInteraction] done."
      FileUtils.chmod 0664, dest_fname, verbose: true
      File.basename dest_fname
    else
      Rails.logger.info "[Startune::ReportCreatingInteraction] Invalid file: '#{file}'"
      raise InteractionErrors::WrongArgument.new 'Invalid file'
    end
  end

  def dest_dir_name
    File.join Settings.storage_path, @report.uri.split(':')[0..1]
  end

  def send_email_notification(email)
    product_name = StartuneReport.templates.detect {|t|
      t[:product_id] == report.report_type}.try {|t| t[:product_name]}
    StartuneMailer.report_received(email, report_file_url(report), product_name).deliver
  end

  def as_json(opts = {})
    {}
  end
end
