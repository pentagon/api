class Mobile::Startune::ReportsLoadingInteraction < Mobile::Interaction
  include Mobile::Startune::ReportUtils
  attr_accessor :reports

  def initialize(args)
    super
    @reports = Persistence::StartuneReport.where(user_uri: user.uri)
  end

  def serialize_report(report)
    {
      order_no: report.order_number,
      creation_date: report.created_at,
      report_type: report.report_type,
      status: report.status,
      report_file: report_file_url(report)
    }
  end

  def as_json(opts = {})
    reports.map {|r| serialize_report r}
  end
end
