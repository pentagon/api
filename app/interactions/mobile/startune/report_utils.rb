module Mobile::Startune::ReportUtils
  def report_file_url(report)
    return unless report.report_file.present?
    File.join(Settings.storage_url, report.uri.split(':')[0..1], report.report_file)
  end
end
