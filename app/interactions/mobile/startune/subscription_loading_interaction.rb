class Mobile::Startune::SubscriptionLoadingInteraction < Mobile::SubscriptionLoadingInteraction
  include Mobile::Startune::Serializers

  def subscription_type
    'startune'
  end
end
