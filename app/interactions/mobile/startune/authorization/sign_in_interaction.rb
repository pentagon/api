class Mobile::Startune::Authorization::SignInInteraction < ::Authorization::SignInInteraction
  include Mobile::Serializers
  include Mobile::Startune::Serializers

  def get_params options
    @params = options[:user_params]
    @user_params = if @params[:user].present?
      @params[:user]
    else
      @params
    end
  end

  def additional_checks
    raise Authorization::Exceptions::UnverifiedUser unless true
    raise Authorization::Exceptions::TooManyDevices unless true
  end

  def as_json options = {}
    serialize_user(user)
  end

end
