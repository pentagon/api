class Mobile::Startune::Authorization::RequestVerificationInteraction < ::Authorization::RequestVerificationInteraction
  def send_verification_mail user
    AccountsMailer.startune_verification_instructions(user).deliver
  end
end
