class Mobile::Startune::BookmarkCreatingInteraction < Mobile::Interaction
  def initialize args
    super
    raise InteractionErrors::WrongArgument.new "track_id is not given!" if args[:track_id].blank?
    Persistence::BookmarkedTrackPool.find_or_create_by(user_uri: @user.uri, application: 'startune').add_track args[:track_id]
  end

  def explicit_level
    @explicit_level = 0
  end

  def as_json opts = {}
    {}
  end
end
