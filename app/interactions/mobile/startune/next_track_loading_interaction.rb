class Mobile::Startune::NextTrackLoadingInteraction < Mobile::Interaction
  include Mobile::Startune::Serializers
  include Mobile::AstroRadio
  attr_accessor :curr_track, :action, :user_reaction

  def initialize args
    super
    @headers = args[:headers]
    validate_args_and_set_attrs! args
    if curr_track.blank?
      init_start_tracks
    else
      request = [{type: 'add_played_song', song_id: curr_track, region: region}]
      request.push({type: user_reaction, song_id: curr_track, region: region}) if user_reaction
      pool_mgr.send_async_mis_request {|r| r.add_request_data request}
    end
  end

  def validate_args_and_set_attrs! args
    sid = args[:station_id]
    raise InteractionErrors::WrongArgument.new 'Insufficient station_id!' if sid.blank?
    @curr_track = args[:current_track]
    @action = args[:action_type]
    @user_reaction = Mobile::AstroRadio::USER_REACTIONS_MAP[action]
    @station = Persistence::Station.find_by user_uri: @user.uri, uri: args[:station_id]
    raise InteractionErrors::WrongArgument.new(
      "Cannot load station for user_uri: '#{@user.uri}', station_id: #{sid}") if @station.blank?
    cons_checker = Mobile::Startune::ConstraintsChecker.new action: action, user: @user, country: @country,
      mobile_device_id: mobile_device_id, station: station
    cons_checker.check
    raise InteractionErrors::UnprocessableEntity.new cons_checker.errors if cons_checker.errors.any?
    @subscription = @user.subscription('startune')
  end

  def as_json opts = {}
    track.present? ? serialize_track(track).update(full_track_url: radio_stream_url) : {}
  end
end
