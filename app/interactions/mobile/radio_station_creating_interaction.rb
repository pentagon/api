# body {"station":{"user_uri":"accounts:user:1377241937549302","seed_track_uris":["medianet:track:10000059"],
#   "title":"Elf Power - O What A Beautiful Dream","is_free":0}}

class Mobile::RadioStationCreatingInteraction < Mobile::Interaction
  include Mobile::Serializers

  def initialize args
    super
    @station = Persistence::Station.create args.slice(:seed_track_uris, :title, :is_free).merge(user_uri: @user.uri,
      country: @country)
    @station.init_station
  end

  def as_json opts = {}
    serialize_station @station
  end
end
