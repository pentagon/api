class Mobile::ArtistLoadingInteraction < Mobile::Interaction
  include Mobile::Serializers

  def initialize args
    super
    @artist = fetch_items_by_ids args[:artist_id]
    raise InteractionErrors::NotFound args[:artist_id], @country
  end

  def as_json opts = {}
    serialize_artist @artist
  end
end
