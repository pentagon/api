class Mobile::SubscriptionLoadingInteraction < Mobile::Interaction
  include Mobile::Serializers

  def initialize args
    super
    @subscription = Persistence::Subscription.get_latest_of_type_for_user(subscription_type, @user.uri)
    raise InteractionErrors::NotFound.new if @subscription.blank?
  end

  def subscription_type
  end

  def as_json(opts = {})
    {
      subscription: serialize_subscription(@subscription)
    }
  end
end
