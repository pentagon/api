class Mobile::SubscriptionActivatingInteraction < Mobile::Interaction
  include SubscriptionUtils::Activating
  include Mobile::Serializers

  def initialize args
    super
    @subscription = Persistence::Subscription.find_by uri: args[:subscription_id]
    raise InteractionErrors::NotFound.new args[:subscription_id] unless @subscription
    raise InteractionErrors::Forbidden.new unless @subscription.user_uri.eql?(user.uri)
    raise InteractionErrors::UnprocessableEntity.new unless activate_subscription(@subscription)
  end

  def as_json opts = {}
    serialize_subscription(@subscription)
  end
end
