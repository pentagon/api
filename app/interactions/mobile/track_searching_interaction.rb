class Mobile::TrackSearchingInteraction < TrackSearchingInteraction
  include Mobile::Serializers

  def initialize(args)
    super
    @results = if args[:autocomplete]
      limit = args[:per_page].presence || 10
      track_autocomplete_search args[:q], limit
    else
      track_simple_search args[:q], args[:page], args[:per_page]
    end
  end

  def as_json opts = {}
    @results.map {|t| serialize_track t}
  end
end
