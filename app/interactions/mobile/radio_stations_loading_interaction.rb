# Response:
#      @SerializedName("id") private String id;
#      @SerializedName("title") private String title;
#      @SerializedName("user_uri") private String userUri;
#      @SerializedName("seed_track_uris") private List<String> seedTrackUris;
#      @SerializedName("image_url") private String imageUrl;
#      @SerializedName("is_free") private boolean free;
#      @SerializedName("parent_station_uri") private String parentStationUri;
#      @SerializedName("station_show_url") private String stationShowUrl;
#      @SerializedName("created_at") private String createdAt;
#      @SerializedName("updated_at") private String updatedAt;
#      @SerializedName("children_count") private int childrenCount;

class Mobile::RadioStationsLoadingInteraction < Mobile::Interaction
  include Mobile::Serializers

  def initialize args
    super
    @stations = Persistence::Station.where user_uri: @user.uri
  end

  def as_json opts = {}
    @stations.map {|s| serialize_station s}
  end
end
