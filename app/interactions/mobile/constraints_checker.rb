class Mobile::ConstraintsChecker
  attr_accessor :action, :country, :station, :station_params, :mobile_device_id, :user, :errors

  class << self
    def human_attribute_name attr, options = {}
      attr.to_s.humanize
    end

    def lookup_ancestors
      [self]
    end
  end

  def initialize args
    @user = args[:user]
    @action = args[:action]
    @country = args[:country]
    @station = args[:station]
    @station_params = args[:station_params]
    @mobile_device_id = args[:mobile_device_id]
    @errors = InteractionErrors::CodedErrors.new
    @errors.add :invalid_params if args.has_key?(:station_params) and station_params.blank?
  end

  def explicit_level
    @explicit_level = 0
  end

  def check
    check_if_subscription_valid if user
    check_if_track_limit_reached if user && station
    check_and_set_device_constraint if station && mobile_device_id
    register_skip_check_if_can_skip if action.eql?('skip')
    check_if_can_stream_for_country if station
  end

  def register_skip_check_if_can_skip
    if RedisDb.client.keys("#{stream_skip_key_prefix}*").count < 5
      RedisDb.client.setex "#{stream_skip_key_prefix}_#{DateTime.now.to_i}", 1.hour, DateTime.now
    else
      errors.add :max_skips_achieved
    end
  end

  def check_and_set_device_constraint
    if RedisDb.client.keys("#{device_mutex_key_prefix}*").count < 5
      RedisDb.client.setex "#{device_mutex_key_prefix}_#{mobile_device_id}", 24.hours, DateTime.now
    else
      errors.add :already_streaming
    end
  end

  def check_if_can_stream_for_country
    unless country.eql? station.country
      errors.add :cannot_stream_for_different_country
    end
    unless %w(us gb).include? country
      errors.add :cannot_stream_for_requested_country
    end
  end

  def check_if_subscription_valid
    @errors.add :no_valid_subscription unless user.has_usable_subscription? subscription_name
  end

  def check_if_track_limit_reached
    return if user.admin? || user.superuser? || station.is_free
    if user.has_usable_subscription?(subscription_name) && user.subscription(subscription_name).tracks_limit_reached
      errors.add :no_hours_left
    end
  end

  def stream_mutex_key
    "#{app_name}_stream_#{@user.uri}"
  end

  def device_mutex_key_prefix
    "#{app_name}_device_#{@user.uri}"
  end

  def stream_skip_key_prefix
    "#{app_name}_stream_skip_#{@station.uri}"
  end

  def subscription_name
    raise '[Mobile::ConstraintsChecker] subscription_name should be overriden!'
  end

  def app_name
    raise '[Mobile::ConstraintsChecker] app_name should be overriden!'
  end
end
