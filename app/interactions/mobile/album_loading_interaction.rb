class Mobile::AlbumLoadingInteraction < Mobile::Interaction
  include Mobile::Serializers

  def initialize args
    super
    @album = fetch_items_by_ids args[:album_id]
    raise InteractionErrors::NotFound args[:album_id], @country unless @album
  end

  def as_json opts = {}
    serialize_album @album
  end
end
