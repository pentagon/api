class Mobile::SubscribeConfirmingInteraction < Interaction
  include SubscriptionUtils::Confirming
  attr_accessor :subscription_template, :paypal_token, :error_url, :success_url, :agent, :department_code,
    :user_country

  def initialize(args)
    %w(success_url error_url cancel_url department_code user_country paypal_token token id agent).each do |m|
      raise InteractionErrors::WrongArgument.new "Missed #{m}!" if args[m.to_sym].blank?
    end
    validate_args_and_set_attrs!(args)
    @subscription = begin
      init_subscription setup_profile
    rescue => e
      raise InteractionErrors::RedirectingError.new error_url, e.message
    end
    if @subscription.save
      after_subscribe
      raise InteractionErrors::RedirectingError.new success_url
    else
      raise InteractionErrors::RedirectingError.new error_url
    end
  end

  def as_json(opts = {})
    {}
  end

private

  def after_subscribe
    # Override to do something useful.
  end

end
