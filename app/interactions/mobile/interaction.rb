class Mobile::Interaction < Interaction
  include Mobile::HeadersMixin

  def initialize args
    super
    @headers = args[:headers]
    handle_invalid_headers unless mobile_headers_valid?
  end

  def handle_invalid_headers
    # log something here
  end
end
