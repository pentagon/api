class Mobile::Astro::SubscriptionCancellingInteraction < Mobile::SubscriptionCancellingInteraction

private

  def after_cancel
    ::AstroMailer.subscription_cancelled(@subscription).deliver
  end

end
