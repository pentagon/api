class Mobile::Astro::ConstraintsChecker < Mobile::ConstraintsChecker
  def subscription_name
    'astro'
  end

  def app_name
    'astro'
  end
end
