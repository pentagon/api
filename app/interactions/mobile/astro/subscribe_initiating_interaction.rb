class Mobile::Astro::SubscribeInitiatingInteraction < ::Mobile::SubscribeInitiatingInteraction

  def department_code
    "500"
  end

  def return_url
    Rails.application.routes.url_helpers.subscribe_confirm_mobile_astro_subscriptions_url(
      host: Settings.domain, params: subscription_params)
  end

  def success_url
    Rails.application.routes.url_helpers.result_mobile_astro_subscriptions_url(
      host: Settings.domain, status: "success")
  end

  def cancel_url
    Rails.application.routes.url_helpers.result_mobile_astro_subscriptions_url(
      host: Settings.domain, status: "cancel")
  end

  def error_url
    Rails.application.routes.url_helpers.result_mobile_astro_subscriptions_url(
      host: Settings.domain, status: "error")
  end
end
