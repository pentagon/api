class Mobile::Astro::SubscribeConfirmingInteraction < ::Mobile::SubscribeConfirmingInteraction

private

  def after_subscribe
    ::AstroMailer.subscription_confirmation(@subscription).deliver
  end

end
