# TODO review user feedback stuff and finalize this
class Mobile::Astro::FeedbackCreatingInteraction < Mobile::Interaction
  def initialize args
    super
    raise InteractionErrors::WrongArgument.new "track_id is not given!" unless args[:track_id]
    status = args[:feedback]
    raise InteractionErrors::WrongArgument.new "Invalid feedback '#{status}'" unless %w(like dislike).include?(status)
    # feedback = Persistence::UserTrackFeedback.find_or_create_by user_uri: @user.uri, track_uri: args[:track_id] do |f|
    # end
    # track = fetch_items_by_ids args[:track_id]
  end

  def explicit_level
    @explicit_level = 0
  end

  def as_json opts = {}
    {}
  end
end
