class Mobile::Astro::HoroscopeLoadingInteraction < HoroscopeLoadingInteraction
  include AstroUtils
  include Mobile::Astro::Serializers

  def as_json opts = {}
    data = get_data_for_sign
    {
      forecast: {
        date: horoscope.date_from,
        zodiac_id: sign,
        title: data['header'],
        text: data['interp']
      },
      tracks: tracks.map {|t| serialize_track t}
    }
  end
end
