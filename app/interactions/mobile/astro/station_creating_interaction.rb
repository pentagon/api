class Mobile::Astro::StationCreatingInteraction < Mobile::Interaction
  include Radio::RadioRequest
  include Mobile::AstroRadio
  attr_accessor :station

  def initialize args
    super
    cons_checker = Mobile::Astro::ConstraintsChecker.new user: @user, station_params: args[:station]
    cons_checker.check
    raise InteractionErrors::UnprocessableEntity.new cons_checker.errors if cons_checker.errors.any?
    params = args[:station].slice :seed_track_uris, :title
    params.update user_uri: @user.uri, country: @country, is_free: false, source: 'astro'
    @station = Persistence::Station.find_or_create_by params
    raise InteractionErrors::UnprocessableEntity.new @station.errors if @station.errors.any?
    init_start_tracks
  end

  def as_json opts = {}
    {
      station: {
        id: station.uri,
        title: station.title,
        image_url: station.image_url
      }
    }
  end
end
