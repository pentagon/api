class Mobile::Astro::Authorization::UserCreatingInteraction < ::Authorization::UserCreatingInteraction
  include Mobile::Serializers
  include Mobile::Astro::Serializers

  def send_verification_mail user
    AccountsMailer.astro_verification_instructions(user).deliver
  end

  def get_base_params options = {}
    @params, @remote_ip = options[:params], options[:remote_ip]
    @user_params = if @params[:user].present?
      @params[:user]
    else
      @params
    end
  end

  def get_additional_params options = {}
    super
    @user_params[:personal_info_attributes] = {date_of_birth: @user_params[:birthday]} if @user_params[:birthday].present?
  end

end
