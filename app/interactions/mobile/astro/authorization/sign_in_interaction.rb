class Mobile::Astro::Authorization::SignInInteraction < ::Authorization::SignInInteraction
  include Mobile::Serializers
  include Mobile::Astro::Serializers

  def get_params options
    @params = options[:user_params]
    @user_params = options[:user_params]
  end

  def additional_checks
    # raise ::Authorization::Exceptions::UnverifiedUser unless @user.is_verified
  end

  def as_json options = {}
    serialize_user(user)
  end
end
