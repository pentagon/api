class Mobile::Astro::Authorization::RequestVerificationInteraction < ::Authorization::RequestVerificationInteraction
  def send_verification_mail user
    AccountsMailer.astro_verification_instructions(user).deliver
  end
end
