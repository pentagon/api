class Mobile::Astro::Authorization::ResetPasswordInteraction < ::Authorization::ResetPasswordInteraction
  def initialize options = {}
    @user_params = if options[:user_params][:user].present?
      options[:user_params][:user]
    else
      options[:user_params]
    end
    @user_params[:email] = @user_params[:email].try(:downcase)
    @user = send_instructions
  end

  def errors
    {
      error: "no_user"
    }
  end

  def send_instructions
    Persistence::User.send_reset_password_instructions(@user_params) do |user, token|
      raise InteractionErrors::UnprocessableEntity.new user.errors unless user.errors.empty?
      AccountsMailer.astro_reset_password_instructions(user, token).deliver
    end
  end

end
