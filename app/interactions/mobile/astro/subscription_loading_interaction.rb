class Mobile::Astro::SubscriptionLoadingInteraction < Mobile::SubscriptionLoadingInteraction
  include Mobile::Astro::Serializers

  def subscription_type
    'astro'
  end
end
