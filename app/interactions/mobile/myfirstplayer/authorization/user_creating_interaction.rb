class Mobile::Myfirstplayer::Authorization::UserCreatingInteraction < ::Authorization::UserCreatingInteraction
  def send_verification_mail user
    AccountsMailer.myfirstplayer_verification_instructions(user).deliver
  end
end
