class Mobile::Myfirstplayer::Authorization::RequestVerificationInteraction < ::Authorization::RequestVerificationInteraction
  def send_verification_mail user
    AccountsMailer.myfirstplayer_verification_instructions(user).deliver
  end
end
