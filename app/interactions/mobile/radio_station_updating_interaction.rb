# Body: {"station":
#   {"user_uri":"accounts:user:1377241937549302","seed_track_uris":["medianet:track:8276187"],
#     "title":"Ludacris - Holidae","is_free":0}}

class Mobile::RadioStationUpdatingInteraction < Mobile::Interaction
  include Mobile::Serializers

  def initialize args
    super
    @station = Persistence::Station.find_by user_uri: @user.uri, uri: args[:station_id]
    raise InteractionErrors::NotFound.new args[:station_id] unless @station
    @station.update_attributes args.slice(:title, :seed_track_uris, :is_free)
    @station.request_tracks_for_new_seeds
  end

  def as_json opts = {}
    serialize_station @station
  end
end
