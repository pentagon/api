class Mobile::RecommendationLoadingInteraction < RecommendationLoadingInteraction
  include Mobile::Serializers

  def as_json params = {}
    {
      success: true,
      wheels: {
        future: compiled_tracks.select {|t| t[:id].split(':').first != 'medianet'}.first(20),
        present: compiled_tracks.select {|t| t[:release_year] >= 3.years.ago.year and t[:id].split(':').first == 'medianet'}.first(20),
        past: compiled_tracks.select {|t| t[:release_year] < 3.years.ago.year and t[:id].split(':').first == 'medianet'}.first(20)
      }
    }
  end
end
