class Mobile::DeviceRegisteringInteraction < Mobile::Interaction
  def initialize args
    super
    @device_token = args[:device_token]
    raise InteractionErrors::WrongArgument.new 'No token' if @device_token.blank?
    fetch_or_register_device
  end

  def fetch_or_register_device
    device = Persistence::PushSubscription.find_or_create_by user_uri: user.uri, app_name: mobile_app_name,
      app_platform: mobile_device_platform, app_version: mobile_app_version, device_id: mobile_device_id,
        device_token: @device_token
    device.update_attributes device_time_zone: mobile_device_time_zone, device_locale: mobile_device_locale
    device.touch
  end

  def handle_invalid_headers
    raise InteractionErrors::WrongArgument.new "Invalid mobile headers: #{sliced_headers}!"
  end

  def as_json opts = {}
    {}
  end
end
