class Mobile::SubscribeInitiatingInteraction < Mobile::Interaction
  include SubscriptionUtils::Initiating
  attr_accessor :subscription_template

  def initialize(args)
    super
    @subscription_template = ::SubscriptionTemplate.find args[:id]
    raise InteractionErrors::NotFound.new args[:id] unless subscription_template
    raise InteractionErrors::RedirectingError.new build_auth_redirect_url(return_url, cancel_url)
  end

  def handle_invalid_headers
    raise InvalidHeaders.new
  end

  def department_code
    "000"
  end

  def agent
    agent_string
  end

  def return_url
    # Override to return URL
    ""
  end

  def success_url
    # Override to return URL
    ""
  end

  def cancel_url
    # Override to return URL
    ""
  end

  def error_url
    # Override to return URL
    ""
  end

  def mobile?
    true
  end

  # we never should get it called
  def as_json(opts = {})
    {}
  end
end
