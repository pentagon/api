class Mobile::Contextual::TrackLoadingInteraction < Mobile::Interaction
  attr_accessor :track

  def initialize(args)
    super
    @track = Track.fetch args[:track_id]
    raise InteractionErrors::NotFound.new args[:track_id] unless track
  end

  def as_json(opts = {})
    {
      id: track.id,
      title: track.title,
      artist_name: track.try(:artist).try(:title),
      image_url: track.image_url,
      url: track.music_url
    }
  end
end
