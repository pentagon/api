module Mobile::Contextual::Serializers
  def serialize_user user
    {
      user: {
        uri: user.uri,
        email: user.email.downcase,
        avatar_url: user.image_url,
        date_of_birth: user.personal_info.try(:date_of_birth).try {|t| t.strftime("%Y-%m-%d")},
        authentication_token: user.authentication_token
      }
    }
  end
end
