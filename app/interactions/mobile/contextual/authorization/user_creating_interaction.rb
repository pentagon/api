class Mobile::Contextual::Authorization::BirthdayAbsent < ::StandardError; end

class Mobile::Contextual::Authorization::UserCreatingInteraction < ::Authorization::UserCreatingInteraction
  include Mobile::Serializers
  include Mobile::Contextual::Serializers

  def get_base_params options = {}
    @params, @remote_ip, @user_params = options[:params], options[:remote_ip], options[:params]
  end

  def get_additional_params options = {}
    super
    @user_params[:personal_info_attributes] = {date_of_birth: @user_params[:birthday]}
  end

  def check_params
    raise Mobile::Contextual::Authorization::BirthdayAbsent unless @user_params[:birthday].present?
  end

end
