class Mobile::Kurio::RecommendationLoadingInteraction < RecommendationLoadingInteraction
  include Mobile::Kurio::Serializers

  def default_wheels
    @@default_wheels ||= YAML.load_file File.join Rails.root, 'config', 'kurio_wheels.yml'
  end

  def default_set_ids
    default_wheels[explicit_level]['future'] + default_wheels[explicit_level]['present'] +
      default_wheels[explicit_level]['past']
  end

  def track_pool
    @track_pool ||= begin
      res = super
      res += fetch_items_by_ids default_set_ids if res.count < 30
      res
    end
  end

  def future_set
    # Workaround to add default MN tracks to future set
    res = compiled_tracks.select {|t| t[:id].split(':').first != 'medianet'}
    if res.count < 10
      addition_tracks = fetch_items_by_ids default_wheels[explicit_level]['future']
      res += addition_tracks.map {|t| serialize_track t}
    end
    res
  end

  def present_set
    compiled_tracks.select {|t| t[:release_year] >= 3.years.ago.year and t[:id].split(':').first == 'medianet'}
  end

  def past_set
    compiled_tracks.select {|t| t[:release_year] < 3.years.ago.year and t[:id].split(':').first == 'medianet'}
  end

  def as_json params = {}
    {
      success: true,
      wheels: {
        future: future_set,
        present: present_set,
        past: past_set
      }
    }
  end
end
