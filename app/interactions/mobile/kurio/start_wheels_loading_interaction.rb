class Mobile::Kurio::StartWheelsLoadingInteraction < Mobile::Interaction
  include Mobile::Kurio::Serializers

  def initialize params
    super
    # override user explicit level until Kurio app gets updated
    @explicit_level = params[:explicit_filter].to_i if params[:explicit_filter].present?
    @@data ||= YAML.load_file File.join Rails.root, 'config', 'kurio_wheels.yml'
    @track_set = @@data[explicit_level] || {}
  end

  def tracks_pool
    @tracks_pool ||= fetch_items_by_ids @track_set.values.flatten
  end

  def wheels
    @track_set.inject({}) do |res, (name, ids)|
      res[name] = []
      ids.each do |id|
        if (track = tracks_pool.detect {|t| t.id == id}).present?
          res[name].append serialize_track track
        end
      end
      res
    end
  end

  def as_json params = {}
    {
      success: true,
      wheels: wheels
    }
  end
end
