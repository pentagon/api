class Mobile::RadioStationNextTrackLoadingInteraction < Mobile::Interaction
  include Mobile::Serializers

  def initialize args
    super
    raise InteractionErrors::WrongArgument.new "user_ip is not specified!" if user_ip.blank?
    @station = Persistence::Station.find_by uri: args[:station_id], user_uri: @user.uri
    raise InteractionErrors::NotFound args[:station_id] if @station.blank?
    if @station.medianet_tracks_limit_reached?(@user) and not @station.is_free
      raise InteractionErrors::TrackLimitReached.new 'User reached allowed limit for MediaNet tracks!'
    end
    @track = @station.pending_tracks_manager.get_next_track
    raise InteractionErrors::NotFound.new 'Unable to find a track for station' if @track.blank?
  end

  def stream_url
    # TODO: Check if we need to add subscription here, and which one if we do.
    # SEE: Radio::StreamResolver#increment_tracks_counter_on_subscription
    Radio::PumpedStreamResolver.new(ip_address: @user_ip, station_uri: @station.uri, user: @user,
      track: @track, headers: headers, user_country: @country).get_radio_location[:location]
  end

  def reactions
    reactions = Persistence::RadioTrackReaction.where(station_uri: @station.uri, track_uri: @track.id).pluck :reaction_type
    {liked: reactions.include?('like'), disliked: reactions.include?('dislike')}
  end

  def as_json opts = {}
    res = serialize_station_track @track
    res.update full_track_url: stream_url
    res.update reactions
  end
end
