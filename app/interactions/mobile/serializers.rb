module Mobile::Serializers
  extend ActiveSupport::Concern

  def image_url_for_station station
    Track.fetch(station.seed_track_uris.first).try(:image_url)
  end

  def show_page_url_for_station station
    File.join Settings.station_show_page_url, station.uri
  end

  def show_page_url_for_track track
    track.unsigned? ? File.join(::Settings.track_show_page_url, track.id) : ''
  end

  def show_page_url_for_artist artist_id
    Api::Base.get_source_from_uri(artist_id).eql?('unsigned') ? File.join(::Settings.artist_show_page_url, artist_id) : ''
  end

  def serialize_station station
    {
      id: station.uri,
      title: station.title,
      user_uri: station.user_uri,
      seed_track_uris: station.seed_track_uris,
      image_url: image_url_for_station(station),
      is_free: station.is_free,
      parent_station_uri: station.parent_station_uri,
      station_show_url: show_page_url_for_station(station),
      created_at: station.created_at,
      updated_at: station.updated_at,
      children_count: station.clone_counter
    }
  end

  def serialize_station_track track
    {
      found: true,
      id: track.id,
      type: Api::Base.get_source_from_uri(track.id),
      title: track.title,
      album_name: track.album.try(:name),
      album_uri: track.album.try(:id),
      artist_name: track.artist.try(:name),
      artist_uri: track.artist.try(:id),
      genre: track.genre,
      release_year: track.release_date.try {|d| d[0..3]},
      duration: track.duration,
      image_url: track.image_url,
      link_to_track_page: show_page_url_for_track(track),
      link_to_artist_page: show_page_url_for_station(track.artist.try :id),
      preview_track_url: track.music_url,
      allowed: allowed?(track),
      rights: transform_rights(track.rights)
    }
  end

  def serialize_track track
    {
      id: track.id,
      title: track.title,
      album_name: track.album.try(:title),
      artist_name: track.artist.try(:title),
      genre: track.genre,
      allowed: allowed?(track),
      image_url: track.image_url,
      music_url: track.music_url,
      # like:
      # dislike:
      rights: transform_rights(track.rights),
      release_year: track.release_date.try {|d| d[0..3]}.to_i
    }
  end

  def serialize_album album
    {
      id: album.id,
      title: album.title,
      artist: album.artist.try(:title),
      tracks: album.tracks,
      image_url: album.image_url,
      release_date: album.release_date
    }
  end

  def serialize_artist artist
    {
      id: artist.id,
      title: artist.title,
      albums: artist.albums,
      image_url: artist.image_url,
      allowed: allowed?(artist)
    }
  end

  def serialize_subscription(subscription)
    return {} if subscription.blank?
    tracks_listened = subscription.custom_options['track_requests'].to_i
    tracks_limit_base = subscription.custom_options['labeled_tracks_limit'].to_i
    tracks_limit_bonus = subscription.custom_options['bonus_track_requests'].to_i
    tracks_limit_total = tracks_limit_base + tracks_limit_bonus
    tracks_left = tracks_limit_total - tracks_listened
    trial_end_date = subscription.created_at + subscription.trial_length if subscription.is_internal_trial
    {
      id: subscription.uri,
      status: subscription.status,
      creation_date: subscription.created_at,
      start_date: (subscription.last_pay_date || subscription.created_at),
      end_date: subscription.next_payment_date,
      seconds_listened: (tracks_listened * 10 * 3600 / 172),
      seconds_left: (tracks_left  * 10 * 3600 / 172),
      seconds_alacarte: (tracks_limit_bonus * 10 * 3600 / 172),
      country: subscription.country,
      price: subscription.price,
      trial_end_date: trial_end_date,
      is_trial: subscription.is_internal_trial,
      is_usable: subscription.usable?,
      currency: subscription.currency
    }
  end
end
