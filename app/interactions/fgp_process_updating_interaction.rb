class FgpProcessUpdatingInteraction < Interaction
  attr_reader :track

  class << self
    def process_response response
      new response
    end
  end

  def initialilze response
    super
    if response['results']['status'].eql? 'OK'
      response['results']['tracks'].each do |r|
        @track = ::Persistence::Track.find_by uri: r['track_uri']
        if track
          apply_fgp_results fetch_track_info(r['found_track_uri'])
        else
          Rails.logger.error "[#{name} :: #{Time.now}] failed to fetch track '#{t['track_uri']}' for result: '#{r}'"
        end
      end
    end
  end

  def apply_fgp_results res
    Rails.logger.info "[#{self.class.name}] FGP results: #{res}"
    track.update_attribute :infringement_info, res
  end

  def after_apply_fgp_results res
    TmmMailer.infringement(user: track.user, track: track).deliver if track.infringement?
  end

  def fetch_track_info track_uri
    res = HashWithIndifferentAccess.new track_uri: track_uri
    track = Track.fetch track_uri
    res.update title: track.title, artist_title: track.artist.title if track
    res
  end
end
