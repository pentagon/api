class Slm::Authorization::VerifyInteraction < ::Authorization::VerifyInteraction
  def url
    @success ? "#{Settings.slm_url}/user/confirmed" : "#{Settings.slm_url}/user/failed"
  end
end
