class Slm::Authorization::UserCreatingInteraction < ::Authorization::UserCreatingInteraction
  def send_verification_mail user
    AccountsMailer.slm_verification_instructions(user).deliver
  end
end
