class Slm::Authorization::RequestVerificationInteraction < ::Authorization::RequestVerificationInteraction
  def send_verification_mail user
    AccountsMailer.slm_verification_instructions(user).deliver
  end
end
