module Sync::HssMixin
  extend ActiveSupport::Concern

  def redis_id_for_track track_id
    "sync_hss_#{track_id}"
  end

  def blank_hss_for_track id
    {
      'track_uri' => id,
      'has_error' => false,
      'error_message' => '',
      'hss_score' => {
        'uk' => 0,
        'us' => 0
      },
      'similar_tracks' => [],
      'created_at' => Time.now,
      'updated_at' => Time.now,
      'is_persisted' => false
    }
  end

  def fetch_record track_id
    JSON.parse RedisDb.client.get redis_id_for_track track_id
  rescue
    blank_hss_for_track track_id
  end

  def destroy_record track_id
    RedisDb.client.del redis_id_for_track track_id
  end

  def store_record record
    record['is_persisted'] = true
    RedisDb.client.set redis_id_for_track(record['track_uri']), record.to_json
  end
end
