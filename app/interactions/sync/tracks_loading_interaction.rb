class Sync::TracksLoadingInteraction
  include Sync::Serializers

  def initialize params
    @tracks = Persistence::Track.any_in uri: params[:ids]
  end

  def as_json params = {}
    @tracks.map {|t| serialize_track t}
  end
end
