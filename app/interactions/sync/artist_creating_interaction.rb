class Sync::ArtistCreatingInteraction
  include Sync::Serializers

  def initialize params
    @artist = Persistence::Artist.create params[:artist].merge(source: 'sync')
  end

  def as_json opts = {}
    serialize_artist @artist
  end
end
