class Sync::TrackLoadingInteraction
  include Sync::Serializers
  include Sync::Errors

  def initialize params
    @track = Persistence::Track.find_by uri: params[:track_id]
    raise Sync::Errors::NotFound.new unless @track
  end

  def as_json params = {}
    serialize_track @track
  end
end
