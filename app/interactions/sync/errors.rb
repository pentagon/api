module Sync::Errors
  extend ActiveSupport::Concern
  class NotFound < Exception; end
end
