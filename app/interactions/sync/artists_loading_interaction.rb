class Sync::ArtistsLoadingInteraction
  include Sync::Serializers

  def initialize params
    @artists = Persistence::Artist.where user_uri: params[:user_uri]
  end

  def as_json params = {}
    @artists.map {|a| serialize_artist a}
  end
end
