class Sync::HssUpdatingInteraction
  include Sync::HssMixin

  class << self
    def process_mis_response response
      new response
    end
  end

  def initialize params
    if (track_id = params['track_uri']).present?
      @hss = update_hss fetch_record(track_id), params
      store_record @hss
      notify_sync track_id
      @hss
    else
      raise Exception.new('No track ID specified!')
    end
  end

  def notify_sync track_id
    if track_id.start_with?('sync', 'rrmusic')
      Thread.new do
        begin
          Rails.logger.info "#{log_tag} Gonna send SYNC notification for track: '#{track_id}'..."
          RestClient.post Settings.sync_notify_url, {track_uri: track_id}
          Rails.logger.info "#{log_tag} SYNC notification sent to '#{Settings.sync_notify_url}'."
        rescue Exception => e
          Rails.logger.error "#{log_tag} FAILED to send SYNC notification to '#{Settings.sync_notify_url}'. Error: #{e}"
        end
      end
    end
  end

  def update_hss hss, data
    if data['status'].eql? 'OK'
      result = data['results']
      hss['hss_score']['uk'] = result['hss3']['hss_score']
      hss['hss_score']['us'] = result['hss3_us']['hss_score']
      tracks = result.values_at(*%w(future past present past_hits past_discovery present_hits present_discovery)).flatten
      # collect ids for tracks with a distance less than 1
      ids = []
      similar_tracks = tracks.inject([]) do |a,h|
        if h['d'] <= 1
          ids << h['uri']
          a << {'track_uri' => h['uri'], 'distance' => h['d']}
        end
        a
      end
      # enrich similar_tracks entities with title and artist
      tracks = Track.fetch ids
      hss['similar_tracks'] = similar_tracks.inject([]) do |a, v|
        track = tracks.detect {|trk| trk.id == v['track_uri']}
        a << v.merge('title' => track.title, 'artist' => track.artist_name) if track
        a
      end
    else
      hss['has_error'] = true
      hss['error_message'] = data['message']
    end
    hss['updated_at'] = Time.now
    hss
  end

  def as_json params = {}
    @hss
  end
end
