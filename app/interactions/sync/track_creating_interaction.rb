class Sync::TrackCreatingInteraction
  include Sync::Serializers

  def initialize params
    params.reverse_merge! release_date: DateTime.now, source: 'sync', is_private: true
    @track = Persistence::Track.create params
  end

  def as_json params = {}
    serialize_track @track
  end
end
