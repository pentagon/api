class Sync::ArtistUpdatingInteraction
  include Sync::Serializers
  include Sync::Errors

  def initialize params
    @artist = Persistence::Artist.find_by uri: params[:artist_id]
    raise Sync::Errors::NotFound.new unless @artist
    @artist.update_attributes params['artist']
  end

  def as_json
    serialize_artist @artist
  end
end
