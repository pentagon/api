module Sync::Serializers
  extend ActiveSupport::Concern

  def serialize_track track
    {
      track_uri: track.uri,
      artist: track.artist_name,
      title: track.title,
      recommendation_uri: '' #track.recommendation.try(:uri)
    }
  end

  def serialize_artist artist
    artist.as_json
  end
end
