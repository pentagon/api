class Sync::HssLoadingInteraction
  include Sync::HssMixin

  def initialize params
    @hss = fetch_record params[:track_id]
    ## TODO raise exception if no track_id given
    unless @hss['is_persisted']
      @hss = {status: 'queued'}
      track = Persistence::Track.find_by uri: params[:track_id]
      raise Exception.new "Cannot find track with id: '#{params[:track_id]}'!"
      query = {track_id: params[:track_id], track_url: track.music_url}
      MisService.enqueue MisCommand::GetHssInfo.new Sync::HssUpdatingInteraction, :process_mis_response, query
    end
  end

  def as_json params = {}
    @hss
  end
end
