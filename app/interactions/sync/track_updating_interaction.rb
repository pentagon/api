class Sync::TrackUpdatingInteraction
  include Sync::Serializers
  include Sync::Errors

  def initialize params
    params.reverse_merge! release_date: DateTime.now, source: 'sync'
    @track = Persistence::Track.find_by uri: params[:track_id]
    raise Sync::Errors::NotFound.new unless @track
    @track.update_attributes params[:track]
  end

  def as_json params = {}
    serialize_track @track
  end
end
