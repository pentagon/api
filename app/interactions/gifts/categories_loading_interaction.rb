class Gifts::CategoriesLoadingInteraction < Gifts::BaseInteraction

  def initialize(params)
    @categories = Gifts::Category.all
  end

  def as_json(params = {})
    {
      categories: @categories.map {|c| serialize_category(c)}.compact,
      templates: @categories.map(&:templates).flatten.map {|t| serialize_template(t)}.compact
    }
  end
end
