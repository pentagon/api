class Gifts::MixtapeFullTrackLoadingInteraction < Gifts::BaseInteraction
  attr_reader :location

  def initialize(params)
    super
    @mixtape = Mixtape.find_by uri: params[:mixtape]
    raise InteractionErrors::NotFound.new params[:id] unless @mixtape &&
      @mixtape.successfully_paid? && @mixtape.track_uris.include?(params[:id])
    resolver = Radio::PumpedStreamResolver.new track_uri: params[:id], user_country: @country, track: params[:track],
      ip_address: params[:remote_user_ip], agent: @mixtape.source.upcase, user: @user
    result = resolver.get_radio_location
    raise InteractionErrors::NotFound.new params[:id] unless result[:success]
    @mixtape.inc(:mixtape_played_count, 1)
    @location = result[:location]
  end
end
