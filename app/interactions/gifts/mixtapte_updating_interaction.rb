class Gifts::MixtapeUpdatingInteraction < Gifts::BaseInteraction

  def initialize(params)
    super
    @mixtape = Mixtape.find_by uri: params[:id]
    raise InteractionErrors::NotFound.new params[:id] if @mixtape.blank?
    raise InteractionErrors::Forbidden.new params[:id] unless @mixtape.user_uri == @user.uri or @mixtape.status == 'temporary'
    @mixtape.update_attributes params[:mixtape]
    raise InteractionErrors::UnprocessableEntity.new @mixtape.errors if @mixtape.errors.any?
  end

  def as_json(params = {})
    serialize_mixtape @mixtape
  end
end
