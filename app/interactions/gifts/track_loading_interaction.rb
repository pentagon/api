class Gifts::TrackLoadingInteraction < Gifts::BaseInteraction

  attr_reader :track

  def initialize(params = {})
    super
    @track = fetch_items_by_ids params[:track_id]
    raise NotFound.new params[:track_id] if @track.blank?
  end

  def as_json(params = {})
    {track: serialize_track(@track)}
  end
end
