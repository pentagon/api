class Gifts::EcardLoadingInteraction < Gifts::BaseInteraction

  attr_reader :ecard

  def initialize(params)
    @ecard = Ecard.find_by uri: params[:id]
    raise InteractionErrors::NotFound.new params[:id] if @ecard.blank?
  end

  def as_json(params = {})
    serialize_ecard(@ecard)
  end
end
