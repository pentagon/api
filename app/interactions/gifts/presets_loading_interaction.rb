class Gifts::PresetsLoadingInteraction < Gifts::BaseInteraction

  def initialize(params)
    super
    @presets = Gifts::Preset.all
    @presets = @presets.select {|p| p.type == params["type"]} if params["type"].present?
    @tracks_ids = @presets.map(&:track_ids).flatten.compact.uniq
  end

  def tracks_pool
    @tracks_pool ||= fetch_items_by_ids(@tracks_ids)
  end

  def templates
    @templates ||= @presets.map(&:template).compact
  end

  def as_json(params = {})
    {
      presets: @presets.map {|p| serialize_preset(p)},
      templates: templates.map {|t| serialize_template(t)},
      tracks: tracks_pool.map {|t| serialize_track(t)}
    }
  end

  def catalog
    country
  end
end
