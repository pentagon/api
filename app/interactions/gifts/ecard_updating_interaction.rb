class Gifts::EcardUpdatingInteraction < Gifts::BaseInteraction

  def initialize(params)
    super
    @ecard = Ecard.find_by uri: params[:id]
    raise InteractionErrors::NotFound.new params[:id] if @ecard.blank?
    raise InteractionErrors::Forbidden.new params[:id] unless @ecard.user_uri == @user.uri or @ecard.status == 'temporary'
    @ecard.update_attributes params[:ecard]
    raise InteractionErrors::UnprocessableEntity.new @ecard.errors if @ecard.errors.any?
  end

  def as_json(params = {})
    serialize_ecard @ecard
  end
end
