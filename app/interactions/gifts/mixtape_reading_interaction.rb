class Gifts::MixtapeReadingInteraction < Gifts::MixtapeLoadingInteraction

  def initialize(params)
    super
    @mixtape.read
  end
end
