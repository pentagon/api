class Gifts::CassetteLoadingInteraction < Gifts::BaseInteraction

  def initialize(params)
    @cassette = Gifts::Cassette.find params[:id]
    raise InteractionErrors::NotFound.new params[:id] if @cassette.blank?
  rescue ActiveHash::RecordNotFound
    raise InteractionErrors::NotFound.new
  end

  def as_json(params = {})
    {cassette: serialize_cassette(@cassette)}
  end
end
