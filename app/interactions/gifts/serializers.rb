module Gifts::Serializers
  # Need for mixtape_full_gifts_track_url method
  include Rails.application.routes.url_helpers

  def default_url_options
    {host: Settings.domain}
  end

  def serialize_track(track, mixtape = nil)
    return unless track
    track_file = (mixtape && mixtape.successfully_paid?) ? mixtape_full_gifts_track_url(track.id, mixtape: mixtape.id) : track.music_url
    {
      id: track.id,
      track_file: track_file,
      preview_file: track.music_url,
      cover_url: track.image_url,
      artist_name: track.artist.title,
      track_name: track.title,
      document_id: track.id,
      rights: transform_rights(track.rights),
      duration: track.duration.to_s,
      artist_uri: track.artist_uri,
      album_uri: track.album_uri
    }
  end

  def serialize_cassette(cassette)
    return unless cassette
    a = %W{id title image_url image_large_url}
    Hash[a.map {|x| [x, cassette.send(x)]}]
  end

  def serialize_pack(pack)
    return unless pack
    a = %W{id image_url image_back_url} + Gifts::Pack::AVAILABLE_IMAGES.map{|m| "image_#{m}_url".to_sym}
    Hash[a.map {|x| [x, pack.send(x)]}]
  end

  def serialize_category(category)
    return unless category
    a = %W{id image_url image_hover_url title}
    Hash[a.map {|x| [x, category.send(x)]}].merge(template_ids: category.templates.map{|t| t.id})
  end

  def serialize_template(template)
    return unless template
    a = %W{id category_id image_url title}
    Hash[a.map {|x| [x, template.send(x)]}]
  end

  def serialize_payment_order(payment_order)
    return unless payment_order
    a = %W{price_currency price_amount status item_uri user_uri}
    {id: payment_order.uri}.merge(Hash[a.map{|x|[x, payment_order.send(x) ]}])
  end

  def serialize_ecard(ecard)
    return unless ecard
    gift_serialized = serialize_gift ecard

    is_downloaded = ecard.download_request && ecard.download_request.is_downloaded
    download_url = ecard.download_request.try(:download_url) unless is_downloaded

    gift_serialized.merge!({
      tracks: [serialize_track(ecard.track)].compact
    })

    gift_serialized[:gift].merge!({
      track_uri: ecard.track_uri,
      picture_uri: ecard.picture_uri,
      template_id: ecard.template_id,
      is_downloaded: is_downloaded,
      download_url: download_url
    })

    gift_serialized[:ecard] = gift_serialized.delete :gift
    gift_serialized
  end

  def serialize_mixtape(mixtape)
    return unless mixtape
    gift_serialized = serialize_gift mixtape

    gift_serialized.merge!({
      tracks: mixtape.tracks.map{|t| serialize_track(t, mixtape)},
      cassettes: [serialize_cassette(mixtape.cassette)].compact
    })

    gift_serialized[:gift].merge!({
      cassette_id: mixtape.cassette_id,
      preset_id: mixtape.preset_id,
      mixtape_title: mixtape.mixtape_title,
      mixtape_played_count: mixtape.mixtape_played_count,
      track_uris: mixtape.track_uris
    })
    gift_serialized.merge!({presets:[serialize_preset(mixtape.preset)]}) if mixtape.preset.present?
    gift_serialized[:mixtape] = gift_serialized.delete :gift
    gift_serialized
  end

  def serialize_gift(gift)
    return unless gift
    a = %W{user_uri sender_name recipient_name recipient_email title message status sent_at read_at pack_id template_id picture_uri payment_order_id}
    {
      gift: {id: gift.uri}.merge(Hash[a.map{|x|[x, gift.send(x) ]}]),
      payment_orders: [serialize_payment_order(gift.payment_order)].compact,
      templates: [serialize_template(gift.template)].compact,
      pictures: [serialize_picture(gift.picture)].compact,
      packs: [serialize_pack(gift.pack)].compact
    }
  end

  def serialize_picture(picture)
    return unless picture
    {
      id: picture.uri,
      image_url: "#{picture.image.url(:original)}?#{Time.now.to_i}",
      image_cropped_url: "#{picture.image.url(:thumb)}?#{Time.now.to_i}"
    }
  end

  def serialize_preset(preset)
    preset.as_json["attributes"]
  end
end
