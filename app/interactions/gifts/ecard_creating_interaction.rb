class Gifts::EcardCreatingInteraction < Gifts::BaseInteraction

  def initialize(params)
    super
    @ecard = Ecard.create(params[:ecard].merge(user_uri: @user.uri, user_country: @country))
    raise InteractionErrors::UnprocessableEntity.new @ecard.errors if @ecard.errors.any?
  end

  def as_json(params = {})
    serialize_ecard @ecard
  end
end
