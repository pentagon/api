class Gifts::PictureCreatingInteraction < Gifts::BaseInteraction

  DEFAULT_OPTIONS = {
    source: 'gifts'
  }

  def initialize(params)
    super
    @picture = Gifts::Picture.new DEFAULT_OPTIONS
    @picture.image = params[:picture][:image] if params[:picture]
    @picture.set_raw_image(params[:request].raw_post) if params[:raw]
    @picture.user_uri = @user.uri
    @picture.save
    raise InteractionErrors::UnprocessableEntity.new @picture.errors if @picture.errors.any?
  end

  def as_json options = {}
    {
      picture: serialize_picture(@picture)
    }
  end
end
