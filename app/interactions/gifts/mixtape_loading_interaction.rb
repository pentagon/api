class Gifts::MixtapeLoadingInteraction < Gifts::BaseInteraction

  attr_reader :mixtape

  def initialize(params)
    super
    @mixtape = Mixtape.find_by uri: params[:id]
    raise InteractionErrors::NotFound.new params[:id] if @mixtape.blank?
  end

  def as_json(params = {})
    serialize_mixtape(@mixtape)
  end
end
