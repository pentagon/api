class Gifts::EcardReadingInteraction < Gifts::EcardLoadingInteraction

  def initialize(params)
    super
    @ecard.read
  end
end
