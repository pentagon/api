class Gifts::TemplatesLoadingInteraction < Gifts::BaseInteraction

  def initialize(params)
    @templates = Gifts::Template.all
  end

  def as_json(params = {})
    {templates: @templates.map {|c| serialize_template c}.compact}
  end
end
