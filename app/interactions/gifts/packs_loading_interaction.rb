class Gifts::PacksLoadingInteraction < Gifts::BaseInteraction

  def initialize(params)
    @packs = Gifts::Pack.all
  end

  def as_json(params = {})
    {packs: @packs.map {|c| serialize_pack c}.compact}
  end
end
