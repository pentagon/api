class Gifts::PictureUpdatingInteraction < Gifts::BaseInteraction

  def initialize(params)
    super
    @picture = Gifts::Picture.find_by(uri: params[:id])
    raise InteractionErrors::NotFound.new unless @picture
    raise InteractionErrors::Forbidden.new unless @picture.user_uri == @user.uri
    @picture.update_attributes(params[:picture].except(:image_url,:image_cropped_url))
    @picture.image.recreate_versions!
    raise InteractionErrors::UnprocessableEntity.new @picture.errors unless @picture.valid?
  end

  def as_json options = {}
    {
      picture: serialize_picture(@picture)
    }
  end
end
