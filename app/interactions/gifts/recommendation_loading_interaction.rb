class Gifts::RecommendationLoadingInteraction < RecommendationLoadingInteraction
  include Gifts::Serializers

  def as_json params = {}
    {
      recommendation: {
        id: @track_id,
        status: 'ok',
        track_ids: track_pool.map(&:id)
      },
      tracks: track_pool.map {|t| serialize_track(t)}
    }
  end

  def track_pool
    @track_pool ||= begin
      res = fetch_items_by_ids track_uris, {preview: true}
      res.reject! {|t| t.id.include?('medianet') and not t.rights[:radio].include?(country)}
      res
    end
  end
end
