class Gifts::MixtapeCreatingInteraction < Gifts::BaseInteraction

  def initialize(params)
    super
    @mixtape = Mixtape.create(params[:mixtape].merge(user_uri: @user.uri, user_country: @country))
    raise InteractionErrors::UnprocessableEntity.new @mixtape.errors if @mixtape.errors.any?
  end

  def as_json(params = {})
    serialize_mixtape @mixtape
  end
end
