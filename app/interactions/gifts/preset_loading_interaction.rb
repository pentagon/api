class Gifts::PresetLoadingInteraction < Gifts::BaseInteraction

  def initialize(params)
    super
    @preset = Gifts::Preset.find(params[:id])
    raise InteractionErrors::NotFound.new(params[:id]) if @preset.blank?
    @track_ids = @preset.track_ids
  end

  def tracks_pool
    @track_pool ||= fetch_items_by_ids(@track_ids.flatten.uniq)
  end

  def as_json(params = {})
    {
      preset: serialize_preset(@preset),
      tracks: tracks_pool.map{|t| serialize_track(t)},
      templates: [serialize_template(@preset.template)]
    }
  end

  def catalog
    country
  end
end
