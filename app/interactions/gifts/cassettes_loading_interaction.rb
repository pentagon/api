class Gifts::CassettesLoadingInteraction < Gifts::BaseInteraction

  def initialize(params)
    @cassettes = Gifts::Cassette.all
  end

  def as_json(params = {})
    { cassettes: @cassettes.map {|c| serialize_cassette(c)}.compact }
  end
end
