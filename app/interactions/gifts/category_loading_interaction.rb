class Gifts::CategoryLoadingInteraction < Gifts::BaseInteraction

  def initialize(params)
    @category = Gifts::Category.find params[:id]
    raise InteractionErrors::NotFound.new params[:id] if @category.blank?
  rescue ActiveHash::RecordNotFound
    raise InteractionErrors::NotFound.new
  end

  def as_json(params = {})
    {
      category: serialize_category(@category),
      templates: @category.templates.map {|t| serialize_template t}
    }
  end
end
