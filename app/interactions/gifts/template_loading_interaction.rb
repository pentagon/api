class Gifts::TemplateLoadingInteraction < Gifts::BaseInteraction

  def initialize(params)
    raise InteractionErrors::WrongArgument.new "template id is not specified" if params[:id].blank?
    @template = Gifts::Template.find params[:id]
    raise InteractionErrors::NotFound.new params[:id] if @template.blank?
  end

  def as_json(params = {})
    {template: serialize_template(@template)}
  end
end
