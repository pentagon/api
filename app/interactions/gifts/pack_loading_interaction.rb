class Gifts::PackLoadingInteraction < Gifts::BaseInteraction

  def initialize(params)
    @pack = Gifts::Pack.find params[:id]
    raise InteractionErrors::NotFound.new params[:id] if @pack.blank?
  rescue ActiveHash::RecordNotFound
    raise InteractionErrors::NotFound.new
  end

  def as_json(params = {})
    {pack: serialize_pack(@pack)}
  end
end
