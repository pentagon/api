class Gifts::PictureDestroyingInteraction < Gifts::BaseInteraction

  def initialize(params)
    super
    @picture = Gifts::Picture.find_by(uri: params[:id])
    raise InteractionErrors::NotFound.new unless @picture
    raise InteractionErrors::Forbidden.new unless @picture.user_uri == @user.uri
    @picture.destroy
  end

  def as_json options = {}
    {}
  end
end
