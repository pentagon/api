class Gifts::TracksSearchingInteraction < TrackSearchingInteraction
  include Gifts::Serializers

  attr_reader :tracks

  def initialize(params)
    super
    raise InteractionErrors::WrongArgument.new "Specify 'query' or 'chart' param" if
      params["query"].blank? && params["chart"].blank?
    # transform rights to match single catalog essentials
    @rights = if params["rights"]
      params["rights"].inject({}) do |a,(k,v)|
        if PER_TERRITORY_SETS.include?(k)
          a[k] = catalog if v
        else
          a[k] = v
        end
        a
      end
    else
      {}
    end
    @chart = params["chart"]
    @query = params['query'].presence
    @tracks = track_simple_search @query, params["page"], 100
  end

  def as_json param = {}
    {tracks: @tracks.map {|t| serialize_track t}.compact}
  end

  def track_default_must_filters_array
    filters = @rights.map {|r,v| {term: {"rights.#{r}" => catalog}}}
    filters << [{term: {"charts.group" => @chart}}] if @chart.present?
    filters
  end

  def catalog
    country
  end
end
