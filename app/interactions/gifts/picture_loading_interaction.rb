class Gifts::PictureLoadingInteraction < Gifts::BaseInteraction

  def initialize(params)
    @picture = Picture.find_by uri: params[:id]
    raise InteractionErrors::NotFound.new params[:id] if @picture.blank?
  end

  def as_json options = {}
    {
      picture: serialize_picture(@picture)
    }
  end
end
