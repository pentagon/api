module CatalogUtils
  extend ActiveSupport::Concern

  PER_TERRITORY_SETS = [:available, :purchase, :preview, :radio, :stream]
  ACCESSABILITY_FLAGS = [:mis_online, :public]

  def labeled_index
    'labeled'
  end

  def catalog_for_country c
    %w(gb us ca).include?(c) ? c : 'gb'
  end

  def item_type_from_uri id
    id.split(':').second
  end

  def explicit_filters_by_type type
    res = []
    if self.explicit_level > 0
      res << {term: {'rights.explicit' => false}}
      res << {term: {'genre' => "Children's"}} if self.explicit_level == 2 && type.to_s.eql?('track')
    end
    res
  end

  def fetch_items_by_ids ids, flags = {}
    return (ids.respond_to?(:any?) ? [] : nil) if ids.blank?
    type = item_type_from_uri ids.respond_to?(:any?) ? ids.first : ids
    klass = type.to_s.classify.constantize
    territory_filters = flags.slice(*PER_TERRITORY_SETS).inject([]) do |a,(k,v)|
      a << {term: {"rights.#{k}" => self.catalog}} if v
      a
    end
    availability_filters = flags.slice(*ACCESSABILITY_FLAGS).inject([]) do |a,(k,v)|
      a << {term: {"rights.#{k}" => v}}
    end
    if (size = flags[:size].to_i) > 0
      klass.fetch_by_size ids, size, territory_filters + availability_filters + explicit_filters_by_type(type)
    else
      klass.fetch ids, territory_filters + availability_filters + explicit_filters_by_type(type)
    end
  end

  def fetch_related_for_items_by_type items, type, page = 1, per_page = 10000
    klass = type.to_s.classify.constantize
    index = items.respond_to?(:any?) ? items.map(&:source_index).uniq.join(',') : items.source_index
    ids = items.respond_to?(:any?) ? items.map(&:id).uniq : items.id
    item_type = item_type_from_uri ids.respond_to?(:any?) ? ids.first : ids
    must_filters = explicit_filters_by_type(item_type)
    must_filters << {term: {'rights.available' => self.catalog}}
    klass.search(index: index, page: page, per_page: per_page) do |s|
      if ids.respond_to?(:any?)
        s.filter :or, ids.map {|i| {term: {"#{item_type}.id" => i.split(':').last}}}
      else
        must_filters << {term: {"#{item_type}.id" => ids}}
      end
      s.filter :bool, must: must_filters
    end
  end

  def transform_rights rights
    transformed = rights.to_hash.slice(*PER_TERRITORY_SETS).inject({}) do |a,(k,v)|
      a[k] = (v.respond_to?(:any?) ? v.include?(self.country) : v)
      a
    end
    rights.merge(transformed).to_hash
  end
end
