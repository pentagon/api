class FgpProcessCreatingInteraction < Interaction
  attr_reader :track

  def initialilze params
    super
    @track = params[:track]
    query = {}
    md5sum = `md5sum #{track.track_file_path}`.split(' ').first
    if md5sum
      `mad3 --fgp1 #{track.track_file_path}`
      # fgp service expects array or tracks to verify
      query[:tracks] = [{track_uri: track.uri, track_md5: md5sum}]
      MisService.enqueue MisCommand::GetFgpInfo.new update_interaction_class, :process_response, query
    end
  end

  def update_interaction_class
    FgpProcessUpdatingInteraction
  end
end
