class Tiles::ArtistsSearchingInteraction < Interaction
  include Searchers::Artist
  attr_accessor :artists, :albums, :tracks

  def initialize args
    super
    @page = args[:page] || 1
    @per_page = args[:per_page] || 10
    @artists = artist_simple_search args[:q], @page, @per_page
  end

  def indices_to_search_in
    [labeled_index]
  end

  def albums
    @albums ||= fetch_related_for_items_by_type artists, 'album'
  end

  def serialize_artist artist
    {
      id: artist.id,
      title: artist.title,
      image_url: artist.image_url,
      albums: albums.inject([]) {|a, al| a << al.id if al.artist['id'] == artist.id; a}
    }
  end

  def as_json opts = {}
    {
      artists: artists.map {|a| serialize_artist a},
      meta: {
        total: artists.total,
        page: @page,
        per_page: @per_page
      }
    }
  end
end
