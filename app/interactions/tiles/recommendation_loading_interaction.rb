class Tiles::RecommendationLoadingInteraction < RecommendationLoadingInteraction
  include Tiles::Serializers

  def as_json params = {}
    {
      success: true,
      tracks: compiled_tracks
    }
  end
end
