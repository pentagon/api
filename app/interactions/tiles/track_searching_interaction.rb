class Tiles::TrackSearchingInteraction < TrackSearchingInteraction
  include Tiles::Serializers

  def initialize args
    super
    @results = track_simple_search args[:query], args[:page], args[:per_page]
  end

  def track_default_must_filters_array
    [
      {term: {"rights.mis_online" => true}},
      {term: {"rights.purchase" => catalog}},
      {term: {"rights.preview" => catalog}}
    ]
  end

  def as_json opts = {}
    @results.map {|t| serialize_track t}
  end
end
