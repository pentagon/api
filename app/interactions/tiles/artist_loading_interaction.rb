class Tiles::ArtistLoadingInteraction < Interaction
  def initialize args
    super
    artist_id = args[:artist_id]
    @artist = fetch_items_by_ids artist_id
    raise InteractionErrors::NotFound.new artist_id unless @artist
  end

  def albums
    @albums ||= fetch_related_for_items_by_type @artist, 'album'
  end

  def tracks
    @tracks ||= fetch_related_for_items_by_type @artist, 'track'
  end

  def album_ids
    @album_ids ||= albums.map &:id
  end

  def track_ids
    @track_ids ||= tracks.map &:id
  end

  def serialize_album album
    {
      id: album.id,
      title: album.title,
      image_url: album.image_url,
      tracks: tracks.inject([]) {|a, t| a << t.id if t.album['id'] == album.id; a},
      artist: album.artist['id']
    }
  end

  def serialize_track track
    {
      id: track.id,
      title: track.title,
      image_url: track.image_url,
      music_url: track.music_url,
      artist: track.artist['id'],
      album: track.album['id'],
      allowed: allowed?(track),
      rights: transform_rights(track.rights)
    }
  end

  def as_json opts = {}
    {
      artist: {
        id: @artist.id,
        title: @artist.title,
        image_url: @artist.image_url,
        albums: album_ids,
        tracks:  track_ids
      },
      albums: albums.map {|a| serialize_album a},
      tracks: tracks.map {|t| serialize_track t}
    }
  end
end
