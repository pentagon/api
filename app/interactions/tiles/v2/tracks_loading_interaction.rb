class Tiles::V2::TracksLoadingInteraction < Tiles::V2::BaseInteraction
  include Searchers::Track
  include Tiles::V2::Serializers

  def initialize args
    super
    @responder = if args[:ids]
      fetch_items_by_ids args[:ids]
    elsif args[:q]
      track_autocomplete_search args[:q], 100
    end
  end

  def as_json opts = {}
    {
      tracks: @responder.map { |a| serialize_track a }
    }
  end
end
