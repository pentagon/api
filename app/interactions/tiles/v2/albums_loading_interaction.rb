class Tiles::V2::AlbumsLoadingInteraction < Tiles::V2::BaseInteraction
  include Searchers::Album
  include Tiles::V2::Serializers

  def initialize args
    super
    @responder = if args[:ids]
      fetch_items_by_ids args[:ids]
    elsif args[:q]
      album_advanced_search args[:q], 1, 100
    end
  end

  def as_json opts = {}
    {
      albums: @responder.map { |a| serialize_album a }
    }
  end
end
