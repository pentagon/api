class Tiles::V2::SearchesLoadingInteraction < Tiles::V2::BaseInteraction
  include Searchers::Artist
  include Searchers::Album
  include Searchers::Track

  def initialize args
    super
    unless TorqueBox::ServiceRegistry.lookup("jboss.messaging.default").nil?
      backwriter_queue.publish({key: 'search', data: 
        {
          search_term: query_string,
          search_type: 'standard',
          user_ip: args[:user_ip],
          user_uri: args[:user].try(:uri)
        }
      })
    end
    @artists = Thread.new { Thread.current[:value] = (artist_simple_search query_string, 1, 8) }
    @albums  = Thread.new { Thread.current[:value] = (album_advanced_search  query_string, 1, 9) }
    @tracks  = Thread.new { Thread.current[:value] = (track_simple_search  query_string, 1, 15) }
    [@artists, @albums, @tracks].map(&:join)
  end

  def artists
    @artists[:value]
  end

  def albums
    @albums[:value]
  end

  def tracks
    @tracks[:value]
  end

  def backwriter_queue
    @backwriter_queue ||= TorqueBox::Messaging::Queue.new('/queues/backwriter')
  end

  def as_json opts = {}
    {
      searches: [
        {
          id: @query_string,
          track_ids: tracks.map(&:id),
          album_ids: albums.map(&:id),
          artist_ids: artists.map(&:id)
        }
      ],
      tracks: tracks.map {|t| serialize_track t},
      albums: albums.map {|t| serialize_album t},
      artists: artists.map {|t| serialize_artist t},
      meta: {
        artists_total: artists.total,
        albums_total: albums.total,
        tracks_total: tracks.total
      }
    }
  end
end
