module Tiles::V2::Serializers
  extend ActiveSupport::Concern

  def serialize_track track
    {
      id: track.id,
      title: track.title,
      image_url: track.image_url,
      music_url: track.music_url,
      artist_id: track.artist['id'],
      artist_title: track.artist['title'],
      album_id: track.album['id'],
      album_title: track.album['title'],
      color_tune: track.color_tune.max_by{|_,b| b}.try(:first) || 'grey',
      genre: track.genre,
      rights: transform_rights(track.rights),
      release_year: track.release_date.to_i,
      bpm: track.bpm,
      energy: track.energy || 0
    }.with_indifferent_access
  end

  def serialize_album album, includes = []
    hash = {
      id: album.id,
      title: album.title,
      image_url: album.image_url,
      artist_id: album.artist['id'],
      artist_name: album.artist.title
    }
    hash[:track_ids] = fetch_related_for_items_by_type(album, 'track').map(&:id) if includes.include?('tracks')
    hash
  end

  def serialize_artist artist, includes = []
    hash = {
      id: artist.id,
      title: artist.title,
      image_url: artist.image_url
    }
    hash[:albums] = fetch_related_for_items_by_type(artist, 'album').map(&:id) if includes.include?('albums')
    hash
  end
end
