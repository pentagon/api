class Tiles::V2::BaseInteraction < Interaction
  include Tiles::V2::Serializers

  attr_accessor :query_string, :page, :per_page

  def initialize args
    super
    @query_string = args[:q]
    @page = 1
    @per_page = 25
  end

  def indices_to_search_in
    [labeled_index]
  end
end
