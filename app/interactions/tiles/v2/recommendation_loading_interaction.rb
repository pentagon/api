class Tiles::V2::RecommendationLoadingInteraction < RecommendationLoadingInteraction
  include Tiles::V2::Serializers

  def as_json params = {}
    {
      recommendation: {
        id: @track_id,
        track_ids: compiled_tracks.map {|a| a['id'] },
      },
      tracks: compiled_tracks
    }
  end
end
