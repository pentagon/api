class Tiles::V2::SearchLoadingInteraction < Tiles::V2::BaseInteraction
  include Searchers::Artist
  include Searchers::Album
  include Searchers::Track

  #attr_accessor :artists, :albums, :tracks

  def initialize args
    super
    unless TorqueBox::ServiceRegistry.lookup("jboss.messaging.default").nil?
      backwriter_queue.publish({key: 'search', data: 
        {
          search_term: query_string,
          search_type: 'autocomplete',
          user_ip: args[:user_ip],
          user_uri: args[:user].try(:uri)
        }
      })
    end
    @artists = Thread.new { Thread.current[:value] = (artist_autocomplete_search query_string, args[:artists_limit] || 5) }
    @albums  = Thread.new { Thread.current[:value] = (album_autocomplete_search  query_string, args[:albums_limit]  || 6) }
    @tracks  = Thread.new { Thread.current[:value] = (track_autocomplete_search  query_string, args[:tracks_limit]  || 6) }
    [@artists, @albums, @tracks].map(&:join)
  end

  def artists
    @artists[:value]
  end

  def albums
    @albums[:value]
  end

  def tracks
    @tracks[:value]
  end

  def backwriter_queue
    @backwriter_queue ||= TorqueBox::Messaging::Queue.new('/queues/backwriter')
  end

  def as_json opts = {}
    {
      search: {
        id: query_string,
        track_ids: tracks.map(&:id),
        album_ids: albums.map(&:id),
        artist_ids: artists.map(&:id)
      },
      tracks: tracks.map {|t| serialize_track t},
      albums: albums.map {|t| serialize_album t},
      artists: artists.map {|t| serialize_artist t}
    }
  end
end
