class Tiles::V2::TrackLoadingInteraction < Tiles::V2::BaseInteraction
  include Tiles::V2::Serializers

  def initialize args
    super
    @responder = fetch_items_by_ids args[:id]
    raise InteractionErrors::NotFound.new args[:id] unless @responder
  end

  def as_json opts = {}
    {
      track: serialize_track(@responder)
    }
  end
end
