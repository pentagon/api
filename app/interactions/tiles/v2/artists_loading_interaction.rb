class Tiles::V2::ArtistsLoadingInteraction < Tiles::V2::BaseInteraction
  include Searchers::Artist
  include Tiles::V2::Serializers

  def initialize args
    super
    @results = if args[:ids]
      fetch_items_by_ids args[:ids]
    elsif args[:q]
      artist_autocomplete_search args[:q], 100
    end
  end

  def as_json opts = {}
    {
      artists: @results.map { |a| serialize_artist a}
    }
  end
end
