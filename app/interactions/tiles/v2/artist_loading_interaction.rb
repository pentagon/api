class Tiles::V2::ArtistLoadingInteraction < Interaction
  include Tiles::V2::Serializers

  def initialize args
    super
    @artist = fetch_items_by_ids args[:id]
    raise InteractionErrors::NotFound.new args[:id] unless @artist
  end

  def albums
    @albums ||= fetch_related_for_items_by_type @artist, 'album'
  end

  def tracks
    @tracks ||= fetch_related_for_items_by_type @artist, 'track'
  end

  def album_ids
    @album_ids ||= albums.map &:id
  end

  def track_ids
    @track_ids ||= tracks.map &:id
  end

  def as_json opts = {}
    {
      artist: {
        id: @artist.id,
        title: @artist.title,
        image_url: @artist.image_url,
        album_ids: album_ids,
        track_ids:  track_ids
      },
      tracks: tracks.map { |a| serialize_track a },
      albums: albums.map { |a| serialize_album a}
    }
  end
end
