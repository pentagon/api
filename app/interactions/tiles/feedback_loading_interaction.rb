class Tiles::FeedbackLoadingInteraction < Interaction
  def initialize params
    super
    raise WrongArgument.new "Unable to load feedbacks for non existing user" if @user.blank?
    raise WrongArgument.new "Unable to load feedbacks for empty ids list" if params[:ids].blank?
    uris = params[:ids].is_a?(Array) ? params[:ids] : Array.wrap(params[:ids].try :split, ',')
    limit = params[:limit] || 150
    @feedbacks = if uris.any?
      Persistence::UserTrackFeedback.where(user_uri: @user.uri).in(track_uri: uris).limit(limit)
        .pluck(:status, :track_uri).collect {|r| {status: r.first, track_uri: r.last}}
    else
      []
    end
  end

  def as_json params = {}
    @feedbacks
  end
end
