module Tiles::Serializers
  extend ActiveSupport::Concern

  def serialize_track track
    {
      id: track.id,
      title: track.title,
      image_url: track.image_url,
      music_url: track.music_url,
      label: track.label,
      color: track.color,
      bpm: track.bpm,
      color_tune: track.color_tune.to_hash,
      genre_cloud: track.genre_cloud || [],
      energy: track.energy || 0,
      genre: track.genre,
      release_date: track.release_date,
      release_year: track.release_date.try {|d| d[0..3]}.to_i,
      hss_rating: 0,
      is_seed: false,
      is_random: false,
      album: track.album.present? ? {
        id: track.album.id,
        title: track.album.title,
        release_date: track.release_date
      } : {},
      artist: track.artist.present? ? {
        id: track.artist.id,
        title: track.artist.title
      } : {},
      allowed: allowed?(track),
      rights: transform_rights(track.rights)
    }
  end
end
