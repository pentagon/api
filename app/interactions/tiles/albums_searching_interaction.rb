class Tiles::AlbumsSearchingInteraction < Interaction
  include SearchFiltering::Album
  attr_accessor :artists, :albums, :tracks

  def initialize args
    super
    @page = args[:page] || 1
    @per_page = args[:per_page] || 10
    @albums = album_simple_search args[:q], @page, @per_page
  end

  def indices_to_search_in
    [labeled_index]
  end

  def tracks
    @tracks ||= fetch_related_for_items_by_type albums, 'track'
  end

  def serialize_album album
    {
      id: album.id,
      title: album.title,
      image_url: album.image_url,
      artist: album.artist['id'],
      tracks: tracks.inject([]) {|a, t| a << t.id if t.album['id'] == album.id; a}
    }
  end

  def as_json opts = {}
    {
      albums: albums.map {|a| serialize_album a},
      meta: {
        total: albums.total,
        page: @page,
        per_page: @per_page
      }
    }
  end
end
