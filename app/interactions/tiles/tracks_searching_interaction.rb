class Tiles::TracksSearchingInteraction < Interaction
  include SearchFiltering::Track

  attr_accessor :artists, :albums, :tracks

  def initialize args
    super
    @page = args[:page] || 1
    @per_page = args[:per_page] || 10
    @tracks = track_simple_search args[:q], @page, @per_page
  end

  def indices_to_search_in
    [labeled_index]
  end

  def serialize_track track
    {
      id: track.id,
      title: track.title,
      image_url: track.image_url,
      music_url: track.music_url,
      artist: track.artist['id'],
      album: track.album['id']
    }
  end

  def as_json opts = {}
    {
      tracks: tracks.map {|t| serialize_track t},
      meta: {
        total: tracks.total,
        page: @page,
        per_page: @per_page
      }
    }
  end
end
