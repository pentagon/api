class Authorization::RequestVerificationInteraction
  def initialize options = {}
    @current_user = options[:current_user]
    raise ::Authorization::Exceptions::UnauthorizedException unless @current_user.present?
    @current_user.generate_verification_token!
    send_verification_mail(@current_user)
  end

  def as_json options = {}
    {}
  end

  def send_verification_mail user
    AccountsMailer.global_verification_instructions(user).deliver
  end
end
