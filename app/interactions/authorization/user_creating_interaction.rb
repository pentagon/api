module Authorization
  class UserCreatingInteraction
    include Authorization::Serializers
    attr_reader :errors, :user

    def initialize options = {}
      get_base_params options
      get_additional_params options
      check_params
      @user = Persistence::User.new(@user_params)
      @user.assign_role(:superuser) if @user.email =~ /@eventim\.de/
      @user.generate_verification_token
      if @user.save
        send_verification_mail(@user)
      end
      @errors = @user.errors
    end

    def get_base_params options = {}
      @params, @remote_ip = options[:params], options[:remote_ip]
      @user_params = @params[:user]
    end

    def get_additional_params options = {}
      @tracking = options[:params][:tracking] || @user_params[:tracking]
      @user_params.merge!(get_tracking_params @tracking) if @tracking.present?
      update_from_geo_info unless @user_params[:contact_info_attributes].present?
      begin
        @user_params[:internal_user] = Settings.ips.map { |a| a.include? IPAddr.new(@remote_ip) }.include?(true)
      rescue
        @user_params[:internal_user] = false
      end
      @user_params[:email] = @user_params[:email].try(:downcase)
    end

    def check_params; end

    def as_json options = {}
      serialize_user @user
    end

    def get_tracking_params tracking_param
      res = {}
      if tracking_param.present?
        tracking = Base64.decode64(tracking_param).split(';')
        res = {
          partner_tracking_date: Time.at(tracking[0].try(:to_i)),
          partner_tracking_page: tracking[1],
          partner: tracking[2]
        }
      end
      res
    end

    def update_from_geo_info
      geo_info = ::MaxmindGeoip.get_geolocation_from_ip(@remote_ip)
      @user_params[:contact_info_attributes] ||= {}
      @user_params[:contact_info_attributes][:country] =
        geo_info["country_code"] if geo_info["country_code"].present? and
          @user_params[:contact_info_attributes][:country].blank?
      @user_params[:contact_info_attributes][:state] =
        geo_info["region_name"] if geo_info["region_name"].present? and geo_info["country_code"] == "US" and
          @user_params[:contact_info_attributes][:state].blank?
      @user_params[:contact_info_attributes][:city] =
        geo_info["city"] if geo_info["city"].present? and @user_params[:contact_info_attributes][:city].blank?
    end

    def send_verification_mail user
      AccountsMailer.global_verification_instructions(user).deliver
    end
  end
end
