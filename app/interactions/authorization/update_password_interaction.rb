module Authorization
  class UpdatePasswordInteraction
    include InteractionErrors
    attr_reader :user

    def initialize options = {}
      @user_params = options[:params][:user]
      @user = Persistence::User.reset_password_by_token(@user_params)
      raise InteractionErrors::UnprocessableEntity.new(@user.errors) unless @user.errors.empty?
    end

    def as_json options = {}
      {}
    end
  end
end
