module Authorization
  class SignOutInteraction
    def initialize options = {}
      @current_user = options[:user]
      raise Authorization::Exceptions::UnauthorizedException unless @current_user.present?
      @current_user.reset_authentication_token!
    end

    def as_json options = {}
      {}
    end
  end
end
