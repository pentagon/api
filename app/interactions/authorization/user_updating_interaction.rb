class Authorization::UserUpdatingInteraction
  include Authorization::Serializers
  include InteractionErrors

  def initialize options = {}
    @params = options[:params]
    @user_params = @params[:user]
    @current_user = options[:current_user]
    raise ::Authorization::Exceptions::UnauthorizedException unless @current_user.present?
    @current_user.contact_info_attributes  = @user_params.delete(:contact_info) if @user_params.key? :contact_info
    @current_user.personal_info_attributes = @user_params.delete(:personal_info) if @user_params.key? :personal_info
    @current_user.assign_attributes(@user_params) if @user_params.present?
    if @current_user.valid?
      @current_user.save
    else
      raise InteractionErrors::UnprocessableEntity.new @current_user.errors
    end
  end

  def as_json options = {}
    serialize_user @current_user
  end
end
