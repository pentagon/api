module Authorization
  module Exceptions
    WrongEmailOrPasswordException = Class.new(StandardError)
    UnauthorizedException = Class.new(StandardError)
    UserCreatingException = Class.new(StandardError)
    UnverifiedUser = Class.new(StandardError)
  end
end
