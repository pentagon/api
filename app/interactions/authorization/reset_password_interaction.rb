module Authorization
  class ResetPasswordInteraction
    include InteractionErrors

    def initialize options = {}
      @user_params = options[:user_params][:user]
      @user_params[:email] = @user_params[:email].try(:downcase)
      @user = send_instructions
      raise InteractionErrors::UnprocessableEntity.new @user.errors unless @user.errors.empty?
    end

    def success?
      @user.errors.empty?
    end

    def errors
      @user.errors
    end

    def as_json options = {}
      {}
    end

    # TODO refactor this because it's ridiculous that this method return user
    def send_instructions
      Persistence::User.send_reset_password_instructions(@user_params)
    end
  end
end
