module Authorization::Serializers
  extend ActiveSupport::Concern

  def serialize_user user
    user.attributes.except('roles', 'admin', 'superuser', 'social_info').merge({
      id: user.uri,
      uri: user.uri,
      personal_info: user.personal_info.as_json,
      oauth_uid: user.oauth_uid,
      full_name: user.full_name,
      avatar_url: user.image_url,
      display_name: user.display_name,
      developer: user.developer?,
      superuser: user.superuser?,
      admin: user.admin?,
      zodiac_sign: user.zodiac_sign
    })
  end
end
