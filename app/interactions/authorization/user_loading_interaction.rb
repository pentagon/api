class Authorization::UserLoadingInteraction
  include AddServiceName
  include Authorization::Serializers
  attr_reader :current_user

  def initialize options = {}
    @params = options[:params]
    @current_user = options[:current_user]
    raise ::Authorization::Exceptions::UnauthorizedException unless @current_user.present?
    add_service_name(current_user, @params[:source]) if @params[:source].present?
  end

  def as_json options = {}
    serialize_user @current_user
  end
end
