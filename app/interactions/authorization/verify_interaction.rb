class Authorization::VerifyInteraction

  def initialize options = {}
    @params = options[:params]
    @verification_token = @params[:verification_token]
    @user = Persistence::User.find_by(verification_token: @verification_token)
    if @user.present?
      @success = @user.verify!
    else
      @success = false
    end
  end

  def url
    @success ? "#{Settings.accounts_url}/?verification=success" : "#{Settings.accounts_url}/?verification=failure"
  end

end