module Authorization
  class SignInInteraction
    include Authorization::Serializers
    attr_reader :user

    include AddServiceName

    def initialize options = {}
      get_params(options)
      @user = Persistence::User.find_by(email: @user_params[:email].try(:downcase))
      raise Authorization::Exceptions::WrongEmailOrPasswordException unless @user.present? and @user.valid_password?(@user_params[:password])
      additional_checks
      add_service_name(@user, @params[:source]) if @params[:source].present?
    end

    def get_params options
      @params = options[:user_params]
      @user_params = options[:user_params][:user]
    end

    def additional_checks
    end

    def as_json options = {}
      serialize_user @user
    end
  end
end
