class HitMaker::UserLoadingInteraction < Interaction
  include HitMaker::Serializers

  def initialize args
    super
    @usr = Persistence::User.find_by uri: args[:id]
  end

  def as_json opts = {}
    {
      user: serialize_user(@usr),
      tracks: @usr.tracks.map { |t| serialize_track(t) },
      reports: @usr.hit_maker_reports.map { |t| serialize_report(t) }
    }
  end
end
