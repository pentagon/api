class HitMaker::ReportUpdatingInteraction
  class << self
    def process_mis_response response
      new response
    end
  end

  attr_accessor :territory

  def initialize response
    track_uri = response['track_uri']
    results = response['results']
    status = response['status']
    report_uri = response['report_uri']
    @territory = response['region']
    r = Persistence::HitMakerReport.find_by uri: report_uri
    status.eql?('OK') ? update_report_from_hss_data(r, results) : set_error_from_hss_data(r, response['message'])
  end

  def update_report_from_hss_data report, data
    Persistence::HitMakerReport::get_mapping_for_region(territory)
      .each_pair {|key, val| report.send "#{key}=", extract_value_by_path(data, val)}
    report.genre_cloud = Array.wrap(report.genre_cloud).sort_by {|a| -a['probability']}
    tracks_ids = Array.wrap(report.similar_hits).collect {|h| h['track_uri']}
    tracks_pool = Track.fetch tracks_ids
    report.similar_hits.each do |h|
      if (track = tracks_pool.detect {|t| t.id == h['track_uri']})
        h['image_url'] = track.image_url
        h['music_url'] = track.music_url
      else
        h['image_url'] = ''
        h['music_url'] = ''
      end
    end
    report.is_processed = true
    report.save
  end

  def set_error_from_hss_data report, message
    report.error_message = message
    report.has_error = true
    report.is_processed = true
    report.save
    refund_user report.user
  end

  def extract_value_by_path hash, path
    keys = path.split('.')
    key = keys.shift
    val = hash[key]
    val.is_a?(Hash) ? keys.empty? ? val : extract_value_by_path(val, keys.join('.')) : val
  end

  def refund_user user
    user.update_attribute :available_reports, user.available_reports + 1
  end
end
