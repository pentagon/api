module HitMaker::Serializers
  extend ActiveSupport::Concern

  def serialize_user user, attributes = nil
    {
      id: user.uri,
      email: user.email,
      mongoid_slug: user.mongoid_slug,
      display_name: user.display_name,
      token: user.authentication_token,
      image_url: user.image_url,
      track_ids: user.tracks.pluck(:uri),
      report_ids: user.hit_maker_reports.reject { |r| r.track.nil? }.map(&:uri),
      available_reports: user.available_reports,
      reports_statistics: {
        min: user.hit_maker_reports.min('data.hss_score'),
        avg: user.hit_maker_reports.avg('data.hss_score').to_i,
        max: user.hit_maker_reports.max('data.hss_score')
      },
      bsonid: user._id
    }
  end

  def serialize_track track
    {
      id: track.uri,
      title: track.title,
      image_url: track.image_url,
      is_private: track.is_private,
      user_id: track.user_uri,
      created_at: track.created_at,
      report_ids: track.hit_maker_reports.pluck(:uri),
      release_date: track.release_date.to_time.to_i,
      music_url: track.music_url,
      artist_name: track.user.display_name,
      user_uri: track.user_uri,
      album_name: track.try(:album).try(:title),
      album_uri: track.try(:album).try(:id),
      duration: track.duration.to_i,
      track_status: track.track_status
    }
  end

  def serialize_report report
    return {} unless report
    {
      id: report.uri,
      hit_score: report.data['hss_score'],
      created_at: report.created_at,
      is_private: report.is_private,
      is_pending: !report.is_processed,
      similar_tracks: report.data['similar_hits'],
      genre_cloud: report.data['genre_cloud'],
      energy_mood: report.data['energy'],
      catalog: report.territory,
      user_id: report.user_uri,
      track_id: report.track_uri,
      hss_category: report.hss_category,
      color: (report.data['color'].sort_by{|_,v| v}.last.first rescue nil),
      hss_avg_year: report.data['hss_avg_year'].to_i,
      is_newest: report.is_new,
      energy_mood_chart: report.data['energy_mood_chart'],
      past_report_uri: report.user.reports.find_by(track_uri: report.track_uri, hss_version: (report.territory.eql?('uk') ? 3 : 2)).try(:uri),
      album_id: report.track.try(:album).try(:id),
      album_name: report.track.try(:album).try(:title),
      track_image_url:    report.track.try(:image_url),
      track_duration:     report.track.try(:duration).try(:to_i),
      track_title:        report.track.try(:title),
      track_release_date: report.track.try(:release_date).try(:to_time).try(:to_i ),
      user_display_name: report.user.display_name
    }
  end
end
