class HitMaker::Authorization::UserCreatingInteraction < ::Authorization::UserCreatingInteraction
  def send_verification_mail user
    AccountsMailer.hitlogic_verification_instructions(user).deliver
  end
end
