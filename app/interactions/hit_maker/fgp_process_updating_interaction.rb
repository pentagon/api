class HitMaker::FgpProcessUpdatingInteraction < FgpProcessUpdatingInteraction
  def after_apply_fgp_results res
    track.update_attribute :is_private, true
  end
end
