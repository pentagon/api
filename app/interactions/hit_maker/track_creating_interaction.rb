class HitMaker::TrackCreatingInteraction < Interaction
  attr_reader :track

  def initialize params
    super
    @params = params[:track_params]
    if @params[:file].content_type =~ /image/
      store_image
    else
      store_track
    end
  end

  def store_image
    @track = @user.tracks.find_by uri: @params[:track_uri]
    if @track
      @track.image = @params[:file]
      @track.save
    end
  end

  def store_track
    return false if @params[:params].blank?
    track_params = JSON.parse @params[:params]
    track_params['release_date'] = Date.today.strftime('%Y-%m-%d') if track_params['release_date'].to_i <= 0
    @track = @user.tracks.build track_params.merge(artist_uri: @user.default_artist.uri)
    track.track_source_file = @params[:file].tempfile.path
    track.track_source_file_ext = File.extname(@params[:file].original_filename).downcase
    track.is_explicit = track_params['is_explicit']
    track.is_private = track_params['is_private']
    track.is_merchantable = track_params['is_merchantable']
    track.source = 'hitlogic'

    if @track.save
      content_type = @params[:file].content_type
      if content_type.eql? "audio/wav" or content_type.eql? "audio/x-wav"
        track.update_attribute(:track_status, 1)
        Thread.new { WavToMp3.convert @track.uri }
      end

      @params[:file].tempfile.close(true)
      HitMaker::FgpProcessCreatingInteraction.new track: @track
    end
  end
end
