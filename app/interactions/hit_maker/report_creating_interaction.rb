class HitMaker::ReportCreatingInteraction < Interaction
  include HitMaker::Serializers
  attr_accessor :reports

  def initialize params
    super
    @reports = []
    request = params[:request]
    reports_requested = request.values.flatten.count
    raise InteractionErrors::InsufficientReportsAvailableError.new if !params[:dont_charge_user] && @user.available_reports < reports_requested
    init_reports request
    enqueue_jobs
    charge_user unless params[:dont_charge_user]
  end

  # accepts request hash like {track_uri_1: ['us', 'uk'], track_uri_2: ['us']}
  def init_reports request
    @reports = request.map {|k, v| init_report_for_track k, v}.flatten
  end

  def init_report_for_track track_uri, territories
    territories.map do |terr|
      Persistence::HitMakerReport.create user_uri: @user.uri, track_uri: track_uri, territory: terr
    end
  end

  def enqueue_jobs
    reports.each do |r|
      MisService.enqueue MisCommand::GetHssInfo.new HitMaker::ReportUpdatingInteraction.name, :process_mis_response,
        track_uri: r.track_uri, track_url: r.track.music_url, version: '3.2', region: r.territory, app: 'stream',
          report_uri: r.uri
    end
  end

  def charge_user
    @user.update_attribute :available_reports, @user.available_reports - @reports.count
  end

  def as_json params = {}
    reports.map { |r| serialize_report(r) }
  end
end
