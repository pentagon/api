class Astroweb::Authorization::VerifyInteraction < ::Authorization::VerifyInteraction
  def url
    @success ? "#{Settings.astro_url}/auth/success_verification?token=#{@user.authentication_token}" : "#{Settings.astro_url}/auth/failed_verification"
  end
end
