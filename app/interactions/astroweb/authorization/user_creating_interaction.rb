class Astroweb::Authorization::UserCreatingInteraction < ::Authorization::UserCreatingInteraction
  include Astroweb::Authorization::Serializers

  def send_verification_mail user
    AccountsMailer.astro_verification_instructions(user).deliver
  end
end
