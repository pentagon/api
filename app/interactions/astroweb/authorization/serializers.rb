module Astroweb::Authorization::Serializers
  def serialize_user user
    {
      id: user.id,
      _slugs: user._slugs,
      authentication_token: user.authentication_token,
      avatar_url: user.image_url,
      contact_info: serialize_contact_info(user.contact_info),
      created_at: user.created_at,
      date_of_birth: user.date_of_birth,
      display_name: user.display_name,
      email: user.email,
      full_name: user.full_name,
      is_verified: user.is_verified,
      personal_info: serialize_personal_info(user.personal_info),
      slug: user.slug,
      source: user.source,
      status: user.status,
      superuser: user.superuser?,
      uri: user.uri,
      verification_token: user.verification_token,
      zodiac_sign: user.zodiac_sign,
      astro_subscription_active: (Persistence::Subscription.get_latest_of_type_for_user('astro', user.uri).try(:status) == 'active')
    }
  end

  def serialize_contact_info contact_info
    {
      phone: contact_info.phone,
      country: contact_info.country,
      state: contact_info.state,
      city: contact_info.city,
      street: contact_info.street,
      address: contact_info.address,
      postal_code: contact_info.postal_code
    }
  end

  def serialize_personal_info personal_info
    {
      nick_name: personal_info.nick_name,
      first_name: personal_info.first_name,
      last_name: personal_info.last_name,
      gender: personal_info.gender,
      date_of_birth: personal_info.date_of_birth,
      biography: personal_info.biography,
      place_of_birth: personal_info.place_of_birth,
      birth_country: personal_info.birth_country,
      birth_city: personal_info.birth_city,
      birth_state: personal_info.birth_state
    }
  end
end
