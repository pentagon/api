class Astroweb::ForecastLoadingInteraction < HoroscopeLoadingInteraction
  include AstroUtils
  include Astroweb::Serializers

  def as_json opts = {}
    data = get_data_for_sign
    {
      forecast: {
        date: horoscope.date_from,
        zodiac_id: sign,
        title: data['header'],
        text: data['interp']
      },
      tracks: tracks.first(20).map {|t| serialize_track t}
    }
  end
end
