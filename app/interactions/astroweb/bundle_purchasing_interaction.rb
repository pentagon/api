class Astroweb::BundlePurchasingInteraction < Interaction
  attr_accessor :payment

  def initialize(args)
    @item = digital_goods_item
    return handle_payments_error("item_not_found") unless @item

    @payment = ::Persistence::PaymentOrder.create_temporary_payment!(@item, user, args)

    redirect_to purchase_redirect_url
  rescue Persistence::PaymentOrder::PaymentError => e
    reason = e.payments_message_code if e.respond_to?(:payments_message_code)
    handle_payments_error(reason)
  end

  def digital_goods_item
    # RadioStreamBundle
    type = params[:type].gsub("|", "/").camelize.constantize
    type.digital_goods_find_by_id(params[:id], country: country)
  rescue ::Persistence::PaymentOrder::ItemNotAvailableForPurchase
    raise
  rescue
    nil
  end

  def handle_payments_error(reason = nil)
    redirect_url = if params[:error_url].blank?
      url_params = {
        host: Settings.domain,
        protocol: request_protocol,
        status: 'error',
        params: {reason: reason, token: params[:token]}
      }
      url_params[:params].merge!(popup: 1) if params[:popup]
      result_payments_url(url_params)
    else
      response.headers["X-Payments-Error"] = reason.to_s
      params[:error_url]
    end
    redirect_to redirect_url
  end

  def url_params
    {
      host: Settings.domain,
      protocol: request_protocol,
      params: {
        th_token: user.authentication_token,
        ip_address: user_ip
      },
      popup: 1
    }
  end

  def gateway_options
    res = {
      items: paypal_order_items,
      currency: payment.price_currency,
      return_url: confirm_purchase_url,
      cancel_return_url: confirm_cancel_url
    }
    res [:no_shipping] = true if payment.custom_options['popup']
    res
  end

  def purchase_redirect_url
    raise InternalPaymentError unless payment.temporary?
    response = paypal_gateway.setup_purchase(payment.price_amount, gateway_options)
    unless response.success?
      @@logger.error "Setup purchase error for #{item_type} #{item_uri}:\n#{response.params}"
      Rails.logger.error "[PaymentOrder] setup purchase error: #{response.params}"
      raise PaymentProviderError
    end
    paypal_gateway.redirect_url_for(response.token, mobile: !!payment.custom_options['mobile'])
  end

  def confirm_purchase_url
    confirm_purchase_payment_url(payment.uri, url_params)
  end

  def confirm_cancel_url
    confirm_cancel_payment_url(payment.uri, url_params)
  end
end
