class Astroweb::SubscriptionCancellingInteraction < Interaction
  include SubscriptionUtils::Cancelling
  include Mobile::Serializers
  include AstroNotificationUtils

  def initialize args
    super
    @subscription = Persistence::Subscription.find_by uri: args[:subscription_id]
    raise InteractionErrors::NotFound.new args[:subscription_id] unless @subscription
    raise InteractionErrors::Forbidden.new unless @subscription.user_uri.eql?(user.uri)
    raise InteractionErrors::UnprocessableEntity.new unless cancel_subscription(@subscription)
    send_cancelling_notification @subscription.user_uri
    # Deliver cancelling notification email?
  end

  def as_json opts = {}
    serialize_subscription(@subscription)
  end
end
