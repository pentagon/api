class Astroweb::SubscribeConfirmingInteraction < Interaction
  include SubscriptionUtils::Confirming
  include AstroNotificationUtils
  attr_accessor :subscription_template, :paypal_token, :error_url, :success_url, :agent, :department_code,
    :user_country

  def initialize(args)
    %w(success_url error_url cancel_url department_code user_country th_token token id agent).each do |m|
      raise InteractionErrors::WrongArgument.new "Missed #{m}!" if args[m.to_sym].blank?
    end
    validate_args_and_set_attrs! args
    subscription = begin
      init_subscription setup_profile
    rescue => e
      raise InteractionErrors::RedirectingError.new error_url, e.message
    end
    if subscription.save
      send_purchase_notification @subscription.user_uri
      ::AstroMailer.subscription_confirmation(@subscription).deliver
      raise InteractionErrors::RedirectingError.new success_url
    else
      raise InteractionErrors::RedirectingError.new error_url
    end
  end

  def as_json(opts = {})
    {}
  end
end
