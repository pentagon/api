class Astroweb::TrackLoadingInteraction < Interaction
  include Astroweb::Serializers

  def initialize params
    super
    @uris = params[:track_uris]
  end

  def tracks
    @tracks ||= fetch_items_by_ids @uris
  end

  def as_json params = {}
    {
      tracks: (tracks.map {|t| serialize_track t}).presence
    }
  end
end
