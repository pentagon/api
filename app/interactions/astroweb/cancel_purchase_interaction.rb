class Astroweb::CancelPurchaseInteraction < Interaction
  attr_accessor :payment, :token

  def initialize(args)
    @token = args[:token]
    @payment = ::Persistence::PaymentOrder.find_by(uri: args[:id])
    return handle_payments_error("internal_payment_error") unless payment

    payment.process_cancel
  end

  def url_params
    {
      host: Settings.domain,
      protocol: request_protocol,
      status: 'cancel',
      params: {
        token: token,
        payment_id: payment.uri,
        popup: payment.custom_options["popup"]
      }
    }
  end

  def cancel_url
    payment.custom_options["cancel_url"] or result_payments_url(url_params)
  end

  def as_json(opts = {})
    {
      redirect_url: cancel_url
    }
  end
end
