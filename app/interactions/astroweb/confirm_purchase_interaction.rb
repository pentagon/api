class Astroweb::ConfirmPurchaseInteraction < Interaction
  attr_accessor :payment, :error_url, :popup, :success_url, :token, :logger

  def initialize(args)
    super
    @logger = Persistence::PaymentOrder.logger
    @payment = ::Persistence::PaymentOrder.find_by(uri: params[:id])
    return handle_payments_error("internal_payment_error") unless payment

    @error_url = payment.custom_options["error_url"]
    @popup = payment.custom_options["popup"]

    @token = args[:token]

    raise InternalPaymentError unless item.acts_like? :digital_goods

    if payment.payment_provider_payment_id.blank?
      resp = do_purchase(token, args[:PayerID])
      resp.success? ? update_order_with_transaction_details(resp) :
        raise_purchase_error(resp.params['ErrorCode'].to_i)
    end

    # rescue Persistence::PaymentOrder::PaymentProviderFundingFailureError
    #   return redirect_to payment.funding_error_recover_redirect_url(params[:paypal_token])
    # end
    # return handle_payments_error("payment_not_successful") unless payment.successful?

    delivery_result = deliver_item

    if delivery_result[:success]
      @success_url = payment.custom_options["success_url"]
      if success_url.present?
        @success_url = add_params_to_url(success_url, payment_uri: payment.uri,
          payment_provider_payment_id: payment.payment_provider_payment_id)
      else
        success_url = result_payments_url(url_params)
      end
      redirect_to success_url
    else
      handle_payments_error("delivery_error")
    end
  rescue Persistence::PaymentOrder::PaymentError => e
    reason = e.payments_message_code if e.respond_to?(:payments_message_code)
    handle_payments_error(reason)

  end


  def handle_payments_error(reason = nil)
    redirect_url = if @error_url.blank?
      p = url_params.dup
      p[:status] = 'error'
      p[:params][:reason] = reason
      p[:params][:popup] =  1 if popup?
      result_payments_url(url_params)
    else
      response.headers["X-Payments-Error"] = reason.to_s
      @error_url
    end
    redirect_to redirect_url
  end

  def add_params_to_url(url, params)
    parsed = URI.parse(url)
    query_params = Rack::Utils.parse_query(parsed.query).merge(params)
    "#{url.split("?").first}?#{query_params.to_query}"
  end

  def url_params
    res = {
      host: Settings.domain,
      protocol: request_protocol,
      status: 'success',
      params: {
        token: token,
        payment_id: payment.uri
      }
    }
    res[:params][:popup] = 1 if popup?
    res
  end

  def item
    return nil unless payment.item_type && payment.item_uri
    @item ||= payment.item_class.digital_goods_find_by_id(payment.item_uri,
      country: payment.purchase_country) if payment.item_class
  end

  def deliver_item
    if item
      item.digital_goods_deliver(self)
    else
      {success: false, errors: ["Item #{payment.item_type} | #{payment.item_uri} not found."]}
    end
  end

  def paypal_order_items
    hash = {
      name: item.digital_goods_name,
      amount: item.digital_goods_price(country: purchase_country, discount_code: discount_code)[:amount],
      description: item.digital_goods_description,
      number: 1,
      quantity: payment.quantity
    }
    hash[:category] = 'Digital' if payment.custom_options['popup']
    [hash]
  end

  def invoice_id
    "#{payment.department_code}-#{payment.nominal_code}-TH#{payment.increment_code}"
  end

  def paypal_gateway_by_type(popup = false)
    environment = Settings.paypal.use_sandbox ? "sandbox" : "production"
    account_name = payment.item_class.digital_goods_use_payment_account
    options = {
      login:      Settings.paypal[environment][account_name]["login"],
      password:   Settings.paypal[environment][account_name]["password"],
      signature:  Settings.paypal[environment][account_name]["signature"]
    }
    if popup
      ActiveMerchant::Billing::PaypalDigitalGoodsPatchedGateway.new options
    else
      ActiveMerchant::Billing::PaypalExpressGateway.new options
    end
  end

  def paypal_gateway
    @paypal_gateway ||= paypal_gateway_by_type(payment.custom_options["popup"])
  end

  def fetch_payment_provider_transaction_details(payment_id)
    res = paypal_gateway.transaction_details(payment_id)
    res.params.merge("success" => res.success?)
    res
  end

  def do_purchase(payer_id)
    options = {
      items: paypal_order_items,
      currency: payment.price_currency,
      token: token,
      invoice_id: invoice_id,
      payer_id: payer_id
    }
    res = paypal_gateway.purchase(payment.price_amount, options)
    logger.info "Commit payment (#{payment.uri}) for #{item.class.digital_goods_type} #{item.digital_goods_item_id}."
    res
  end

  def raise_purchase_error error_code
    case error_code
    when 10415
      logger.info "Payment already completed:\n#{response.params}"
    when 10486
      logger.info "Payment provider funding failure:\n#{response.params}"
      raise Persistence::PaymentOrder::PaymentProviderFundingFailureError
    else
      logger.error "Payment provider error:\n#{response.params}"
      raise Persistence::PaymentOrder::PaymentProviderError
    end
  end

  def update_order_with_transaction_details response
    payment_id = response.params['transaction_id']
    payment.update_attributes(
      payment_provider_order_info: fetch_payment_provider_transaction_details(payment_id),
      payment_provider_payment_id: payment_id,
      payment_provider_payment_token: response.params['token'],
      payment_provider_response: response.params,
      status: Persistence::PaymentOrder::STATUSES.key(response.params['payment_status']))
  end
end
