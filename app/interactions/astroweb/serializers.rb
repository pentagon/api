module Astroweb::Serializers
  def serialize_track track
    {
      id: track.id,
      title: track.title,
      track_url: track.music_url,
      cover_uri: track.image_url,
      album_uri: track.album.id,
      album_title: track.album.title,
      artist_uri: track.artist.id,
      artist_title: track.artist.title,
      rights: transform_rights(track.rights),
      allowed: allowed?(track),
      user_uri: track.artist.try(:user_uri).to_s
    }
  end
end
