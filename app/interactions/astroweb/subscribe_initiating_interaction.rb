class Astroweb::SubscribeInitiatingInteraction < Interaction
  include SubscriptionUtils::Initiating
  attr_accessor :subscription_template

  def initialize(args)
    super
    @subscription_template = ::SubscriptionTemplate.find args[:id]
    raise InteractionErrors::NotFound.new args[:id] unless subscription_template
    raise InteractionErrors::RedirectingError.new build_auth_redirect_url(return_url, cancel_url)
  end

  def handle_invalid_headers
    raise InvalidHeaders.new
  end

  def department_code
    "500"
  end

  def agent
    ::AstroNotificationUtils::NOTIFY_APP_NAME
  end

  def return_url
    Rails.application.routes.url_helpers.subscribe_confirm_mobile_startune_subscriptions_url(
      host: Settings.domain, params: subscription_params)
  end

  def success_url
    Rails.application.routes.url_helpers.result_success_mobile_startune_subscriptions_url(
      host: Settings.domain)
  end

  def cancel_url
    Rails.application.routes.url_helpers.result_error_mobile_startune_subscriptions_url(
      host: Settings.domain)
  end

  def error_url
    Rails.application.routes.url_helpers.result_cancel_mobile_startune_subscriptions_url(
      host: Settings.domain)
  end

  def mobile?
    true
  end

  # we never should get it called
  def as_json(opts = {})
    {}
  end
end
