class TestRadio::MisResponseProcessingInteraction
  include InteractionErrors

  class << self
    def process_mis_response(response)
      new response
    end
  end

  def initialize(response)
    if response['status'].eql? 'success'
      station_uri = response['station_id']
      if response['playlist'].blank?
        error_msg = "Empty playlist for station_id: '#{station_id}', response: '#{response}'"
        Rails.logger.error error_msg
        raise InteractionErrors::WrongArgument.new error_msg
      else
        track_ids = response['playlist'].map { |t| t['uri'] }
        station = Persistence::TestRadioStation.find_by(uri: station_uri)
        station.radio_queue = track_ids
        station.save
      end
    else
      raise "Failed response: #{response}"
    end
  end
end
