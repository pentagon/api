class TestRadio::StationTrackRatingInteraction < Interaction

  def initialize(args = {})
    super
    @station = args[:station]
  end

  def rate(track_uri, rating)
    search_args = {
      station_uri: @station.uri,
      track_uri: track_uri,
      user_uri: @user.uri,
    }
    value_args = {value: rating}
    reaction = Persistence::TestRadioTrackReaction.find_by(search_args)
    if reaction
      reaction.update_attributes(value_args)
    else
      Persistence::TestRadioTrackReaction.create(search_args.merge(value_args))
    end
    self
  end

  def as_json(opts = {})
    average_ratings_per_track = @station.average_ratings_per_track
    user_reactions = @station.reactions(user_uri: @user.uri).entries
    @station.radio_queue.map do |track_uri|
      user_reaction = user_reactions.select { |r| r.track_uri == track_uri }.first
      {
        track_id: track_uri,
        average_rating: average_ratings_per_track[track_uri],
        current_user_rating: user_reaction.try(:value),
      }
    end
  end
end
