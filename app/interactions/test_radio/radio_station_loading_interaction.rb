class TestRadio::RadioStationLoadingInteraction < Interaction
  include Admin::Serializers

  def initialize(args = {})
    super
    @station = Persistence::TestRadioStation.find_by(seed_track_uri: args[:seed_track_uri])
    if @station.blank?
      args[:environment] = Rails.env
      @station = Persistence::TestRadioStation.create(args)
      init_station
    end
  end

  def as_json(opts = {})
    {test_stations: [serialize_test_station(@station)]}
  end

private

  def init_station
    query = {
      action: "station:update",
      popularity_filter: false,
      priority: 10,
      requests: [{
        region: 'uk',
        song_id: @station.seed_track_uri,
        type: "add_cached_positive_influence",
      }],
      station_id: @station.uri,
      subuniverse_uri: Settings.radio.mis_api.subuniverse_uris.commercial.other,
    }
    command = MisCommand::GetTestRadioData.new(TestRadio::MisResponseProcessingInteraction,
      :process_mis_response, query)
    MisService.enqueue(command)
  end
end
