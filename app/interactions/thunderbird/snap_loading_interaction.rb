class Thunderbird::SnapLoadingInteraction < Interaction
  include Thunderbird::Serializers

  def initialize params
    super
    raise WrongArgument.new "Unable to get snap with no id" if params[:snap_id].blank?
    @snap = Persistence::SnapTrack.find params[:snap_id]
    raise NotFound.new params[:snap_id] if @snap.blank?
  end

  def track
    @track ||= fetch_items_by_ids @snap.track_uri
  end

  def as_json params = {}
    {
      user: serialize_user(@snap.user),
      snap: serialize_snap(@snap),
      track: track.present? ? serialize_track(track) : nil
    }
  end
end
