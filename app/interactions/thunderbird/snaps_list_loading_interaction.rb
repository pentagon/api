class Thunderbird::SnapsListLoadingInteraction < Interaction
  include Thunderbird::Serializers

  def initialize args
    super
    raise InteractionErrors::WrongArgument.new 'Insufficient user_id' if args[:user_id].blank?
    page = args[:page] || 1
    per_page = args[:per_page] || 20
    @snaps = Persistence::SnapTrack.where(user_uri: args[:user_id]).desc(:updated_at).page(page).per(per_page)
  end

  def track_uris
    @snaps.collect(&:track_uri).uniq
  end

  def tracks_pool
    @tracks_pool ||= Track.fetch track_uris
  end

  def serialize_mobile_snap snap
    {
      id: snap.id,
      image_url: snap.image_url,
      preview_url: snap.preview_url,
      share_url: snap.page_url,
      short_url: snap.short_url,
      geo_location: {lat: snap.geo_location[0], lng: snap.geo_location[1]},
      user_uri: snap.user_uri,
      created_at: snap.created_at,
      updated_at: snap.updated_at,
      md5sum: snap.md5sum
    }
  end

  def as_json opts = {}
    @snaps.map {|s| serialize_mobile_snap(s).merge(serialize_snap_track tracks_pool.detect {|t| t.id == s.track_uri})}
  end
end
