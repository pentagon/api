module Thunderbird::Serializers
  extend ActiveSupport::Concern

  def serialize_user user
    {
      id: user.uri,
      full_name: user.full_name
    }
  end

  def serialize_track track
    {
      id: track.id,
      album_uri: track.album.try(:uri).to_s,
      album_name: track.album.try(:title) || '',
      allowed: allowed?(track),
      artist_name: track.artist_name,
      artist_uri: track.artist_uri,
      cover_uri: track.image_url,
      rights: transform_rights(track.rights),
      track_name: track.title.to_s,
      track_url: track.music_url.to_s,
      user_uri: track.artist.try(:user_uri).to_s
    }
  end

  def serialize_snap snap
    {
      id: snap.id,
      image_url: snap.image_url,
      share_url: snap.page_url,
      short_url: snap.short_url,
      user_uri: snap.user_uri
    }
  end

  def serialize_snap_track track
    {
      music_url: track.try(:music_url),
      title: track.try(:title),
      artist_name: track.try(:artist).try(:title),
      duration: track.try(:duration),
      track_image_url: track.try(:image_url)
    }
  end
end
