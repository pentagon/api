class Thunderbird::UserSnapsLoadingInteraction < Interaction
  include Thunderbird::Serializers

  def initialize params
    super
    raise WrongArgument.new 'No user_id specified' if params[:user_id].blank?
    @usr = Persistence::User.find_by(uri: params[:user_id])
    raise NotFound.new params[:user_id] if @usr.blank?
  end

  def tracks_pool
    @tracks_pool ||= Track.fetch @usr.snap_tracks.pluck(:track_uri)
  end

  def as_json params = {}
    {
      user: serialize_user(@usr),
      snaps: @usr.snap_tracks.map {|s| serialize_snap(s).merge(
        serialize_snap_track tracks_pool.detect {|t| t.id == s.track_uri})}
    }
  end
end
