class Thunderbird::TrackSearchingInteraction < TrackSearchingInteraction

  COLOR_TUNE_KEYS = [:white, :yellow, :grey, :red, :purple, :orange, :green, :blue, :black]
  attr_accessor :results

  def initialize args
    super
    args[:per_page] = 200
    @results = track_by_colors_search(args[:colors], args[:page] || 1, args[:per_page] || 50)
  end

  def track_by_colors_search(colors = {}, page, per_page)
    threshold = colors[:th].to_i || 10
    color_keys = COLOR_TUNE_KEYS.select { |key| colors[key] }
    color_filters = color_keys.map do |key|
      value = colors[key].to_i
      start_val, end_val = value > threshold ? [value - threshold, value + threshold] : [0, threshold]
      {range: {"color_tune.#{key}" => {gte: start_val, lte: end_val}}}
    end
    Track.search page: page, per_page: per_page, index: indices_to_search_in.compact.join(",") do |s|
      s.query {|q| q.raw_dsl :function_score, scoring_dsl}
      s.filter :bool, track_filter_params(color_filters)
      s.sort {by :popularity, 'desc'}
    end
  end

  def scoring_dsl
    # TODO use per country popularity field when we get them filled in in ES
    {
      query: {match_all: {}},
      functions: [
        {
          filter: {term: {'rights.hit' => 1}},
          boost_factor: 2
        },
        {
          filter: {not: {term: {'popularity.popularity' => 0}}},
          field_value_factor: {field: 'popularity.popularity', modifier: 'log1p', factor: 1.5}
        }
      ]
    }
  end

  def indices_to_search_in
    [labeled_index]
  end

  def serialize_track track
    {
      found: true,
      id: track.id,
      type: track.class.get_source_from_uri(track.id),
      title: track.title,
      album_name: track.album.try(:title),
      album_uri: track.album.try(:id),
      artist_name: track.artist.try(:title),
      artist_uri: track.artist.try(:id),
      genre: track.genre,
      release_year: track.release_date.split('-').first,
      duration: track.duration,
      image_url: track.image_url,
      link_to_track_page: track.show_page_url,
      link_to_artist_page: track.artist_show_page_url,
      preview_track_url: track.music_url,
      allowed: allowed?(track),
      rights: transform_rights(track.rights)
    }
  end

  def filtered_results
    results.group_by {|r| r.artist['id']}.values.map(&:shuffle).map(&:first).flatten
  end

  def as_json opts = {}
    filtered_results.map {|t| serialize_track t}
  end
end
