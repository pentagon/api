module Radio::Serializers
  extend ActiveSupport::Concern

  def serialize_track track
    {
      found: true,
      id: track.id,
      type: track.class.get_source_from_uri(track.id),
      title: track.title,
      album_name: track.album.try(:title),
      album_uri: track.album.try(:id),
      artist_name: track.artist.try(:title),
      artist_uri: track.artist.try(:id),
      genre: track.genre,
      release_year: track.release_date.split('-').first,
      duration: track.duration,
      image_url: track.image_url,
      link_to_track_page: track.show_page_url,
      link_to_artist_page: track.artist_show_page_url,
      preview_track_url: track.music_url,
      allowed: allowed?(track),
      rights: transform_rights(track.rights)
    }
  end
end
