class Radio::MoreByArtistLoadingInteraction < Interaction
  include Radio::Serializers

  def initialize args
    super
    track = Track.fetch args[:track_id]
    raise InetractionErrors::NotFound.new args[:track_id] unless track
    artist = Artist.fetch track.artist['id']
    raise InetractionErrors::NotFound.new track.artist['id'] unless artist
    @results = fetch_related_for_items_by_type artist, 'track', args[:page], args[:per_page]
    @tracks = @results.to_a.reject {|t| t.id.eql? args[:track_id]}
  end

  def as_json opts = {}
    {
      tracks: @tracks.map {|t| serialize_track t},
      total: @results.total - 1
    }
  end
end
