class Radio::LatestPlayedForStationLoadingInteraction < Interaction
  include Radio::Serializers

  def initialize(args)
    super
    station = Persistence::Station.find_by uri: args[:station_id]
    raise InetractionErrors::NotFound.new args[:station_id] unless station
    size = (args[:size] || 5).to_i
    @tracks = station.pending_tracks_manager.latest_played_tracks size
  end

  def as_json opts = {}
    @tracks.map {|t| serialize_track t}
  end
end
