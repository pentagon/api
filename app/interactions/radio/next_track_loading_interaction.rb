class Radio::NextTrackLoadingInteraction < Interaction
  include Radio::Serializers
  include Radio::Utils

  def initialize args
    super
    remote_user_ip = args[:remote_user_ip]
    raise InteractionErrors::UnprocessableEntity.new if remote_user_ip.blank?
    previous_track_uri = args[:track_uri]
    @station = Persistence::Station.find_by uri: args[:station_id]
    raise InteractionErrors::NotFound.new args[:station_id] unless @station
    @user ||= Persistence::User.find_by uri: @station.user_uri
    raise InteractionErrors::UnprocessableEntity.new unless @station.is_free || !medianet_tracks_limit_reached?(user)
    @track = @station.pending_tracks_manager.get_next_track(previous_track_uri)
    raise InteractionErrors::NotFound.new 'track' unless @track
    @resolver = Radio::PumpedStreamResolver.new(ip_address: remote_user_ip, station_uri: @station.uri, user: user,
      track: @track, headers: args[:headers], subscription: user.subscription('radio'), user_country: country)
  end

  def radio_reactions
    @reactions ||= Persistence::RadioTrackReaction.where(station_uri: @station.uri, track_uri: @track.id).pluck :reaction_type
  end

  # Override allowed flag basing on user subscription
  def allowed? item
    super || (item.labeled? && !@station.is_free && user.has_usable_subscription?('radio'))
  end

  def as_json opts = {}
    res = serialize_track @track
    res[:full_track_url] = @resolver.get_radio_location[:location]
    res[:liked] = radio_reactions.include? 'like'
    res[:disliked] = radio_reactions.include? 'dislike'
    res
  end
end
