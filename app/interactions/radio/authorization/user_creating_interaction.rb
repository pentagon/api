class Radio::Authorization::UserCreatingInteraction < ::Authorization::UserCreatingInteraction
  def send_verification_mail user
    AccountsMailer.radio_verification_instructions(user).deliver
  end
end
