class Radio::Authorization::RequestVerificationInteraction < Authorization::RequestVerificationInteraction
  def send_verification_mail user
    AccountsMailer.radio_verification_instructions(user).deliver
  end
end
