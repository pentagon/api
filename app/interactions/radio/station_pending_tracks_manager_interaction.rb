class Radio::StationPendingTracksManagerInteraction
  include Radio::StationBaseMixin

  def initialize station
    @station = station
  end

  def get_next_track(current_track_uri = nil)
    logger.info("Getting next track for #{@station.uri}.") unless current_track_uri
    track_uri = get_next_track_uri(current_track_uri)
    unless track_uri || current_track_uri
      handle_empty_pending_tracks_list
      track_uri = get_next_track_uri
    end
    if track_uri
      logger.info("Track URI found #{track_uri} for #{@station.uri}.")
      track = Track.fetch track_uri
      remove_pending_tracks(track_uri)
      if track.present?
        logger.info("Track found: '#{track_uri}'. ")
      else
        logger.info("Track NOT found: #{track_uri}. Let's try next one.")
        track = get_next_track(track_uri)
      end
    else
      track = nil
    end
    if current_track_uri.nil? && track.nil?
      logger.error("Next track '#{track_uri}' NOT found for #{@station.uri}.")
    end
    track
  end

  def pending_tracks_uris
    track_uris = RedisDb.client.lrange(redis_id_for(:tracks), 0, -1)
    handle_empty_pending_tracks_list if track_uris.blank?
    filtered_track_uris = track_uris.uniq
    if filtered_track_uris.size != track_uris.size
      logger.error("Duplicate tracks in #{@station.uri}:\n#{track_uris}")
    end
    filtered_track_uris
  end

  def skip_track(track_uri)
    return false unless allowed_to_skip?
    skips = get_skips
    (skips.length + 1 - Settings.radio.number_of_skips_per_hour).times do
      RedisDb.client.rpop(redis_id_for(:skips))
    end
    current_time = Time.now
    RedisDb.client.lpush(redis_id_for(:skips), current_time.to_i)
    @station.mis_requests_manager.send_track_request(:play, track_uri)
    true
  end

  def latest_played_tracks(size = 5)
    Track.fetch Persistence::TrackStatistic.latest_played_tracks_uris(@station.uri, size)
  end

  def set_pending_tracks(ids)
    RedisDb.client.del(redis_id_for(:tracks))
    ids.each { |id| RedisDb.client.rpush(redis_id_for(:tracks), id) }
  end

private

  def get_next_track_uri(current_track_uri = nil)
    track_uris = pending_tracks_uris
    track_index = track_uris.index(current_track_uri)
    if track_index
      if track_index < (track_uris.size - 1)
        track_uris[track_index + 1]
      end
    else
      track_uris.first
    end
  end

  def remove_pending_tracks(track_uris)
    Array.wrap(track_uris).each { |track_uri| RedisDb.client.lrem(redis_id_for(:tracks), 0, track_uri) }
    if RedisDb.client.llen(redis_id_for(:tracks)).to_i < 3 && Settings.radio.fallback_to_mixer
      logger.info("Refreshing mixer tracks for #{@station.uri}")
      set_pending_tracks(get_mixer_tracks)
    end
  end

  def redis_id_for(type)
    case type
      when :tracks then "radio:station_tracks:#{@station.uri}"
      when :skips then "radio:station_skips:#{@station.uri}"
    end
  end

  def handle_empty_pending_tracks_list
    requests_count = @station.mis_requests_manager.resend_last_request
    if (requests_count > 1 && Settings.radio.fallback_to_mixer) || Settings.mis_api.use_mock_requests
      logger.info("Handling empty tracks, using mixer for #{@station.uri}.")
      set_pending_tracks(get_mixer_tracks)
    end
  end

  def get_mixer_tracks
    played_tracks_uris = Persistence::TrackStatistic.latest_played_tracks_uris_since(@station.uri, 3.hours.ago)
    played_tracks = Track.fetch played_tracks_uris
    mixer_country = @station.is_free ? "invalid" : @station.country
    ::RadioMixer::create_playlist_from_seeds(@station.seed_tracks, 8, mixer_country, @station.user, played_tracks)
  end

  def allowed_to_skip?
    current_time = Time.now
    skips = get_skips
    if skips.length >= Settings.radio.number_of_skips_per_hour
      oldest_skip = Time.at skips.last.to_i
      current_time > oldest_skip.advance(hours: 1)
    else
      true
    end
  end

  def get_skips
    RedisDb.client.lrange redis_id_for(:skips), 0, (Settings.radio.number_of_skips_per_hour - 1)
  end

end
