module Radio::StationBaseMixin
  extend ActiveSupport::Concern

  module ClassMethods
    def get_for_station(station)
      @@instances ||= {}
      @@instances[station.uri] ||= {}
      @@instances[station.uri][name] ||= new(station)
    end

    def logger
      @@logger ||= Logger.new(File.join(Rails.root, "log", "radio.log"))
    end
  end

  included do
    delegate :logger, to: 'self.class'

    logger.formatter = proc do |severity, timestamp, progname, msg|
      "\n[#{severity.first}][#{timestamp}] #{String === msg ? msg : msg.inspect}\n"
    end
  end
end
