module Radio::Utils
  extend ActiveSupport::Concern

  def medianet_tracks_limit_reached? user
    false and return if user.admin? || user.superuser?
    user.subscription('radio').tracks_limit_reached if user.has_usable_subscription?('radio')
  end
end
