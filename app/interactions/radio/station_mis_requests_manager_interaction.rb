class Radio::StationMisRequestsManagerInteraction
  include Radio::StationBaseMixin

  TRACK_REQUEST_TYPES = {
    like: "add_cached_positive_influence",
    dislike: "add_negative_influence_song",
    play: "add_played_song"
  }

  FILTER_TYPES = {
    "timeRange" => ["<80s", "80s", "90s", "2000", "present", "future"],
    "genre" => ["same", "discovery"],
    "hss" => ["superHit", "platinum", "gold", "silver", "bronze", "other"]
  }

  DEFAULT_FILTERS = {
    free: {
      "timeRange" => {
        "future" => 100
      },
      "genre" => {},
      "hss" => {
        "superHit" => 0
      }
    },
    commercial: {
      "timeRange" => {
        "future" => 0,
        "present" => 60
      },
      "genre" => {
        "same" => 100
      },
      "hss" => {
        "superHit" => 100
      }
    }
  }

  def self.process_mis_response(params)
    station_id = params["station_id"]
    station = ::Persistence::Station.find_by(uri: station_id) unless station_id.blank?
    logger.info "MIS response received."
    if station
      logger.info "Setting '#{station.uri}' with playlist:\n#{params['playlist']}"
      station.mis_requests_manager.set_pending_tracks_from_data(params["playlist"])
    else
      logger.info "Cannot find station '#{station_id}' from params:\n#{params}"
    end
  end

  def initialize station
    @station = station
  end

  def resend_last_request
    request_timed_out = last_api_request.blank? || Time.now > (last_api_request[:time] + Settings.radio.mis_api.request_waiting_time.minutes)
    requests_count = last_api_request.present? ? last_api_request[:request_count] : 0
    if request_timed_out
      if requests_count < Settings.radio.mis_api.max_number_of_requests
        if last_api_request.present?
          send_api_request(last_api_request[:requests_data])
        else
          @station.request_starting_tracks
        end
      elsif requests_count == Settings.radio.mis_api.max_number_of_requests
        logger.error "Cannot get tracks for #{@station.uri}."
        RedisDb.client.hset(redis_id_for_last_request, :request_count, last_api_request[:request_count] + 1)
      end
    end
    requests_count
  end

  def send_clone_station_request(parent_station_uri)
    requests_data = [{type: "copy_station", preset_uri: parent_station_uri}]
    send_api_request(requests_data)
  end

  def send_track_request(type, track_uris, filters = {})
    track_uris = Array.wrap track_uris
    if ::Persistence::RadioTrackReaction::ALLOWED_REACTIONS.include? type
      track_uris.each { |track_uri| ::Persistence::RadioTrackReaction.create({station_uri: @station.uri, track_uri: track_uri,
        reaction_type: type}).to_yaml }
    end
    if type.to_sym == :play
      @station.last_played_at = DateTime.now
      @station.save
    end
    filters = type.to_sym == :like ? processed_filters(filters) : {}
    requests_data = track_uris.map { |track_uri| {type: TRACK_REQUEST_TYPES[type.to_sym], song_id: track_uri}.merge(filters) }
    send_api_request(requests_data)
  end

  def send_api_request(requests_data = [])
    return Settings.mis_api.mock_api_response.to_json if Settings.mis_api.use_mock_requests
    request_region = @station.country
    request_region = Settings.radio.fallback_country unless Settings.radio.allowed_countries.include?(@station.country)
    request_region = "uk" if request_region == "gb"
    requests_data.each { |entry| entry.update(region: request_region) }
    save_api_request(requests_data)
    query = {
      action: "station:update",
      popularity_filter: false,
      priority: Settings.radio.mis_api.job_priority,
      station_id: @station.uri,
      requests: requests_data,
      subuniverse_uri: get_subuniverse_uri
    }
    result = MisService.enqueue(MisCommand::GetRadioData.new(self.class.name, :process_mis_response, query))
    logger.info "MIS API request sent for '#{request_region}' region:\n#{query}\nResult:\n#{result}"
    result
  end

  def set_pending_tracks_from_data(data)
    new_tracks = data.compact.collect { |item| item["uri"] }
    if new_tracks.any?
      @station.pending_tracks_manager.set_pending_tracks(new_tracks)
    else
      ::MisErrorsMailer.empty_playlist_received(data, @station).deliver if Settings.mis_api.mailer.enabled
      logger.error "Empty callback tracks array received from MIS for #{@station.uri}"
    end
  end

private

  def get_subuniverse_uri
    return Settings.radio.mis_api.subuniverse_uris.free if @station.is_free
    Settings.radio.mis_api.subuniverse_uris.commercial[@station.country] || Settings.radio.mis_api.subuniverse_uris.commercial.other
  end

  def processed_filters(filters = {})
    return {} unless Settings.radio.use_filters
    filters = {} if filters.blank?
    default_filters = @station.is_free ? DEFAULT_FILTERS[:free] : DEFAULT_FILTERS[:commercial]
    custom_filters = {}
    FILTER_TYPES.each do |category, options|
      if filters[category].present?
        custom_filters[category] = {}
        options.each do |option_name|
          custom_filters[category][option_name] = filters[category][option_name].to_i if filters[category][option_name].present?
        end
      end
    end
    if @station.is_free
      custom_filters["timeRange"] = {"future" => 100}
      if custom_filters["hss"]
        custom_filters["hss"]["superHit"] = 0
      else
        custom_filters["hss"] = {"superHit" => 0}
      end
    end
    default_filters.merge(custom_filters)
  end

  def save_api_request(requests_data)
    requests_count = (last_api_request.present? && last_api_request[:requests_data] == requests_data) ? last_api_request[:request_count] + 1 : 1
    @last_api_request = {
      time: Time.now,
      requests_data: requests_data,
      request_count: requests_count
    }
    data_hash = {
      time: @last_api_request[:time].to_i,
      requests_data: @last_api_request[:requests_data].to_json,
      request_count: @last_api_request[:requests_count]
    }
    RedisDb.client.mapped_hmset(redis_id_for_last_request, data_hash)
  end

  def last_api_request
    if @last_api_request.nil?
      data = RedisDb.client.hgetall(redis_id_for_last_request)
      @last_api_request = if data.present? && data["requests_data"].present?
        {
          time: Time.at(data["time"].to_i),
          requests_data: JSON.parse(data["requests_data"]),
          request_count: data["request_count"].to_i
        }
      else
        {}
      end
    end
    @last_api_request
  end

  def redis_id_for_last_request
    "radio:station:last_mis_request:#{@station.uri}"
  end

end
