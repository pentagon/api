class Radio::TrackLoadingInteraction < Interaction
  include Radio::Serializers

  def initialize args
    super
    @track = Track.fetch args[:track_id]
    raise InetractionErrors::NotFound.new args[:track_id] unless @track
  end

  def as_json opts = {}
    serialize_track(@track)
  end
end
