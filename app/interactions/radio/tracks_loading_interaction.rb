class Radio::TracksLoadingInteraction < Interaction
  include Radio::Serializers

  def initialize args
    super
    track_ids = args[:track_ids]
    raise Interaction::WrongArgument.new if track_ids.blank?
    @tracks = Track.fetch track_ids, must_filters
  end

  def must_filters
    [{term: {'rights.available' => catalog}}]
  end

  def as_json opts = {}
    @tracks.map {|t| serialize_track t}
  end
end
