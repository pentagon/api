class Radio::PresetsLoadingInteraction < Interaction
  include Radio::Serializers

  def initialize args
    super
    track_ids, @offset = if args[:random]
      [Settings.radio.preset_track_ids.shuffle.first(args[:per_page]), 0]
    else
      from = (args[:page] - 1) * args[:per_page]
      to = from + args[:per_page] - 1
      [Settings.radio.preset_track_ids[from..to], from]
    end
    @tracks = Track.fetch track_ids
  end

  def as_json opts = {}
    {
      tracks: @tracks.map {|t| serialize_track t},
      offset: @offset,
      total: Settings.radio.preset_track_ids.size
    }
  end
end
