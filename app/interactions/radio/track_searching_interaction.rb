class Radio::TrackSearchingInteraction < TrackSearchingInteraction
  include Radio::Serializers
  attr_accessor :results

  def initialize args
    super
    @results = if args[:query]
      track_simple_search args[:query], args[:page], args[:per_page]
    else
      track_advanced_search args[:search_params], args[:page], args[:per_page]
    end
  end

  def default_must_filters_array
    array = []
    array << {term: {"rights.mis_online" => true}}
    if labeled_index
      array << {
        indices: {
          indices: [labeled_index],
          filter: {
            term: {
              "rights.purchase" => catalog
            }
          },
          no_match_filter: "all"
        }
      }
    end
    array
  end

  def as_json opts = {}
    {
      tracks: @results.map {|t| serialize_track t},
      total: @results.total
    }
  end
end
