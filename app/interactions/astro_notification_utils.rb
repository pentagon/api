module AstroNotificationUtils
  extend ActiveSupport::Concern

  NOTIFY_APP_NAME = 'Astro DJ'
  SUBSCRIPTION_NOTIFICATION_TEMPLATE = {
    'alert' => {
      'body' => 'Bob wants to play poker',
      'action-loc-key' => 'OPEN'
    }
  }

  module ClassMethods
    def notification_messages
      @@notification_messages ||= YAML.load_file(File.join Rails.root, 'config', 'astro.yml')['notifications']
    end
  end

  included do
    delegate :notification_messages, to: 'self.class'
  end

  def build_message(locale, type)
    locale = notification_messages.has_key?(locale) ? locale : notification_messages.keys.first
    res = SUBSCRIPTION_NOTIFICATION_TEMPLATE.dup
    res['alert']['body'] = notification_messages[locale][type]
    res
  end

  def send_push_notifications(user_uri, type)
    devices_to_notify = Persistence::PushSubscription.where(app_name: NOTIFY_APP_NAME,
      user_uri: user_uri).pluck(:device_token, :device_locale)
    notifications = devices_to_notify.map do |t|
      Mobile::APNS::Notification.new t.first, build_message(t.last, type)
    end
    Mobile::APNS.send_notifications notifications
  end

  def send_in_advance_expiration_notification(subscription)
    send_push_notifications subscription.user_uri, 'trial_days_left'
  end

  def send_expiration_notification(subscription)
    send_push_notifications subscription.user_uri, 'trial_over'
  end

  def send_purchase_notification(user_uri)
    send_push_notifications user_uri, 'subscription_purchase'
  end

  def send_cancelling_notification(user_uri)
    send_push_notifications user_uri, 'subscription_purchase'
  end

  def send_alacarte_purchase_notification(user_uri)
    send_push_notifications user_uri, 'additional_10_hrs'
  end
end
