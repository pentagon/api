class Locate::ArtistsController < ApplicationController
  def search
    respond_with_interaction Locate::ArtistSearchingInteraction
  rescue InteractionErrors::NotFound, InteractionErrors::WrongArgument
    head :not_found
  end
end
