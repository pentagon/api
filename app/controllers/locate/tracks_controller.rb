class Locate::TracksController < ApplicationController
  def load_by_uris
    respond_with_interaction Locate::TrackLoadingInteraction, params.slice(:uris, :page, :per_page)
  end

  def search
    respond_with_interaction Locate::TrackSearchingInteraction, params.slice(:q, :page, :per_page)
  end
end
