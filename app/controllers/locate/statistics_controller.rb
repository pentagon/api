class Locate::StatisticsController < ApplicationController
  def listen_geo_by_period
    respond_with_interaction Locate::StatisticInteraction,
      params.slice(:start, :end).update(type: 'geo_by_period')
  end

  def realtime_listen
    respond_with_interaction Locate::StatisticInteraction,
      params.slice(:start, :counter).update(type: 'realtime_listen')
  end

  def listen_by_artist
    respond_with_interaction Locate::StatisticInteraction,
      params.slice(:start, :end, :id).update(type: 'artist_listen')
  end

  def listen_by_track
    respond_with_interaction Locate::StatisticInteraction,
      params.slice(:id, :start, :end).update(type: 'track_listen')
  end

  def listen_by_user
    respond_with_interaction Locate::StatisticInteraction,
      params.slice(:id, :start, :end).update(type: 'user_listen')
  end
end
