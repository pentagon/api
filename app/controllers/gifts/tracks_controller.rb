class Gifts::TracksController < Gifts::BaseController

  def index
    respond_with_interaction Gifts::TracksSearchingInteraction, params.slice(:query, :rights, :chart)
  rescue InteractionErrors::WrongArgument
    head :not_found
  end

  def show
    respond_with_interaction Gifts::TrackLoadingInteraction, {track_id: params[:id]}
  end

  def mixtape_full
    track_interaction = ::Gifts::TrackLoadingInteraction.new(default_interaction_params.merge(track_id: params[:id]))
    redirect_to Gifts::MixtapeFullTrackLoadingInteraction.new(default_interaction_params.merge(id: params[:id], track: track_interaction.track, mixtape: params[:mixtape], remote_user_ip: remote_user_ip)).location
  end
end
