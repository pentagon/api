class Gifts::CategoriesController < Gifts::BaseController

  def index
    respond_with_interaction Gifts::CategoriesLoadingInteraction, params
  end

  def show
    respond_with_interaction Gifts::CategoryLoadingInteraction, params.slice(:id)
  end
end
