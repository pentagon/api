class Gifts::BaseController < ApplicationController
  rescue_from InteractionErrors::NotFound, :with => :show_not_found
  rescue_from InteractionErrors::Forbidden, :with => :show_forbidden
  rescue_from InteractionErrors::UnprocessableEntity, :with => :show_unprocessable_entity

  protected

  def params_with_defaults
    default_interaction_params.merge(params).merge(request: request)
  end

  def show_forbidden
    head :forbidden
  end

  def show_not_found
    head :not_found
  end

  def show_unprocessable_entity e
    render json: {errors: e.errors}, status: :unprocessable_entity, location: false
  end

  def format_errors errors
    errors_hash = {}
    errors.to_hash.each{ |k,v| errors_hash[k] = v.map{ |m| errors.full_message(k,m) } }
    errors_hash
  end
end
