class Gifts::TemplatesController < Gifts::BaseController

  def index
    respond_with_interaction Gifts::TemplatesLoadingInteraction, params
  end

  def show
    respond_with_interaction Gifts::TemplateLoadingInteraction, params.slice(:id)
  end
end
