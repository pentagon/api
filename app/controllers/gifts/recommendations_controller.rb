class Gifts::RecommendationsController < ApplicationController
  def show
    res = Gifts::RecommendationLoadingInteraction.new(user: current_user, country: user_country, track_id: params[:id])
    respond_with res
    rescue RecommendationLoadingInteraction::RequestSent
      respond_with({recommendation: {id: params[:id], status: 'queued', track_ids: []}, tracks: []})
  end
end
