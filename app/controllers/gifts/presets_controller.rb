class Gifts::PresetsController < Gifts::BaseController

  def index
    respond_with_interaction Gifts::PresetsLoadingInteraction, params.slice(:type)
  end

  def show
    respond_with_interaction Gifts::PresetLoadingInteraction, params.slice(:id)
  end
end
