class Gifts::PacksController < Gifts::BaseController

  def index
    respond_with_interaction Gifts::PacksLoadingInteraction, params
  end

  def show
    respond_with_interaction Gifts::PackLoadingInteraction, params.slice(:id)
  end
end
