class Gifts::CassettesController < Gifts::BaseController

  def index
    respond_with_interaction Gifts::CassettesLoadingInteraction, params
  end

  def show
    respond_with_interaction Gifts::CassetteLoadingInteraction, params.slice(:id)
  end
end
