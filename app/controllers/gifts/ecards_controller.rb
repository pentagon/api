class Gifts::EcardsController < Gifts::BaseController

  before_filter :authenticate_user!, only: [:create]

  def create
    respond_with_interaction Gifts::EcardCreatingInteraction, params.slice(:ecard)
  rescue InteractionErrors::UnprocessableEntity => e
    respond_with({errors: format_errors(e.errors)}, status: :unprocessable_entity, location: false)
  end

  def update
    respond_with_interaction Gifts::EcardUpdatingInteraction, params.slice(:ecard, :id)
  rescue InteractionErrors::UnprocessableEntity => e
    respond_with({errors: format_errors(e.errors)}, status: :unprocessable_entity, location: false)
  end

  def show
    respond_with_interaction Gifts::EcardLoadingInteraction, params.slice(:id)
  end

  def read
    respond_with_interaction Gifts::EcardReadingInteraction, params
  end

  def get_price
    result = Ecard.digital_goods_base_price_by_track_uri params[:track_uri], user_country
    respond_with result.merge({success: true, user_country: user_country})
  rescue ::Persistence::PaymentOrder::ItemNotAvailableForPurchase => e
    respond_with success: false, message: e.message
  end
end
