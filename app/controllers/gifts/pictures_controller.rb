class Gifts::PicturesController < Gifts::BaseController
  before_filter :authenticate_user!, except: [:show]

  def create
    interaction = Gifts::PictureCreatingInteraction.new(params_with_defaults)
    respond_to do |format|
      format.html {render json: interaction, content_type: 'text/html', layout: false}
      format.json {render json: interaction}
    end
  end

  def update
    respond_with_interaction Gifts::PictureUpdatingInteraction, params
  end

  def show
    respond_with_interaction Gifts::PictureLoadingInteraction, params
  end

  def destroy
    respond_with_interaction Gifts::PictureDestroyingInteraction, params
  end
end
