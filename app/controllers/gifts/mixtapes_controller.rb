class Gifts::MixtapesController < Gifts::BaseController

  before_filter :authenticate_user!, only: [:create]

  def create
    respond_with_interaction Gifts::MixtapeCreatingInteraction, params.slice(:mixtape)
  rescue InteractionErrors::UnprocessableEntity => e
    respond_with({errors: format_errors(e.errors)}, status: :unprocessable_entity, location: false)
  end

  def update
    respond_with_interaction Gifts::MixtapeUpdatingInteraction, params.slice(:mixtape, :id)
  rescue InteractionErrors::UnprocessableEntity => e
    respond_with({errors: format_errors(e.errors)}, status: :unprocessable_entity, location: false)
  end

  def show
    respond_with_interaction Gifts::MixtapeLoadingInteraction, params.slice(:id)
  end

  def read
    respond_with_interaction Gifts::MixtapeReadingInteraction, params
  end

  def get_prices
    result = {
      ranges: Mixtape.prices.to_a.map { |p| [p.first.to_s, p.last] },
      currency: Mixtape.currency(user_country)
    }
    respond_with result
  end
end
