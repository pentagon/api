class Tmm::BaseController < AuthorizedController
  before_filter :set_request_format
  skip_before_filter :authenticate_user!, only: [:index, :show]


  def respond_after_destroy entity
    if entity.destroy
      render json: true
    else
      head :unprocessable_entity
    end
  end

  def set_request_format
    request.format = "json" unless params[:format]
  end
  private :set_request_format
end
