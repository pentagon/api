class Tmm::ReferralsController < Tmm::BaseController
  skip_before_filter :authenticate_user!, only: [:index]

  def index
    respond_with Tmm::ReferralLoadingInteraction.new
  end

  def by_initiator
    respond_with ::Persistence::Referral.where initiator_uri: params[:id]
  end
end
