class Tmm::ReportsController < Tmm::BaseController
  skip_before_filter :authenticate_user!

  def index
    respond_with Tmm::ReportsLoadingInteraction.new(ids: params[:ids], user: current_user)
  end

  def show
    respond_with Tmm::ReportLoadingInteraction.new(id: params[:id], user: current_user)
  end
end
