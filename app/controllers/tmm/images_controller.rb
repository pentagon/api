class Tmm::ImagesController < Tmm::BaseController

  def create
    render json: Tmm::ImageCreatingInteraction.new(params, current_user)
  rescue InteractionErrors::UnprocessableEntity => e
    render json: {errors: e.errors.full_messages}, status: :unprocessable_entity
  end

  def destroy
    gallery = Persistence::Gallery.find_by('images.uri' => params[:id], gallerieable_uri: current_user.uri)
    image = gallery.images.find_by uri: params[:id]
    respond_after_destroy image
  end

end
