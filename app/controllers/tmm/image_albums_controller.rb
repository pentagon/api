class Tmm::ImageAlbumsController < Tmm::BaseController

  def show
    render json: Tmm::ImageAlbumLoadingInteraction.new(params)
  end

  def create
    render json: Tmm::ImageAlbumCreatingInteraction.new(params, current_user)
  end

  def update
   render json: Tmm::ImageAlbumUpdatingInteraction.new(params, current_user)
  end

  def destroy
    img_album = current_user.image_albums.find_by uri:params[:id]
    respond_after_destroy img_album
  end
end
