class Tmm::UsersController < Tmm::BaseController
  skip_before_filter :authenticate_user!, only: [:show]

  def show
    respond_with Tmm::UserLoadingInteraction.new(id: params[:id], user: current_user)
  end

  def update
    respond_with_interaction Tmm::UserUpdatingInteraction, user_params: params[:user]
  end
end
