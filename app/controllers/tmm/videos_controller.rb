class Tmm::VideosController < Tmm::BaseController
  skip_before_filter :authenticate_user!, only: [:index, :show]

  def index
    respond_with Tmm::VideosLoadingInteraction.new(ids: params[:ids], user: current_user)
  end

  def show
    respond_with Tmm::VideoLoadingInteraction.new(id: params[:id], user: current_user)
  end

  def create
    creating_interaction = Tmm::VideoCreatingInteraction.new(video_params: params, user: current_user)
    respond_with creating_interaction do |format|
      format.json {
        if creating_interaction.valid?
          render json: creating_interaction, location: false and return
        else
          render json: creating_interaction, status: :unprocessable_entity and return
        end
      }
    end
  end

  def destroy
    video = current_user.videos.find_by uri: params[:id]
    respond_after_destroy(video) if video
  end

end
