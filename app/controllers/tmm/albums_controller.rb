class Tmm::AlbumsController < Tmm::BaseController
  def index
    respond_with Tmm::AlbumsLoadingInteraction.new(ids: params[:ids], user: current_user)
  end

  def show
    respond_with Tmm::AlbumLoadingInteraction.new(id: params[:id], user: current_user)
  end

  def create
    creating_interaction = Tmm::AlbumCreatingInteraction.new(album_params: params, user: current_user)
    respond_with creating_interaction do |format|
      format.json {
        if creating_interaction.valid?
          render json: creating_interaction, location: false and return
        else
          render json: creating_interaction, status: :unprocessable_entity and return
        end
      }
    end
  end

  def destroy
    artist = current_user.default_artist
    album = artist.albums.find_by_slug params[:id]
    respond_after_destroy album
  end

  def update
    updating_interaction = Tmm::AlbumUpdatingInteraction.new(user: current_user, album_params: params)
    respond_with updating_interaction do |format|
      format.json {
        if updating_interaction.valid?
          render json: updating_interaction, location: false
        else
          render json: updating_interaction, status: :unprocessable_entity
        end
      }
    end
  end
end
