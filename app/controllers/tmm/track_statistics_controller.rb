class Tmm::TrackStatisticsController < Tmm::BaseController
  skip_before_filter :authenticate_user!, only: [:show]

  def show
    respond_with Tmm::TrackStatisticLoadingInteraction.new(id: params[:id], user: current_user)
  end
end
