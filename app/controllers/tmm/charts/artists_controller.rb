class Tmm::Charts::ArtistsController < Tmm::BaseController
  skip_before_filter :authenticate_user!

  def index
    render json: Tmm::ChartsArtistsLoadingInteraction.new(params)
  end


end
