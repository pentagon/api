class Tmm::Charts::TracksController < Tmm::BaseController
  skip_before_filter :authenticate_user!

  def index
    render json:  Tmm::ChartsTracksIndexInteraction.new(params)
  end

end
