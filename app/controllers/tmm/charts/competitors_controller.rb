class Tmm::Charts::CompetitorsController < Tmm::BaseController

  def index
    render json: Tmm::ChartsArtistCompetitorsLoadingInteraction.new(params)
  end

end
