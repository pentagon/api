class Tmm::ConversationsController < Tmm::BaseController
  def index
    respond_with Tmm::ConversationsLoadingInteraction.new(user: current_user)
  end

  def show
    respond_with Tmm::ConversationLoadingInteraction.new(id: params[:id], user: current_user)
  end
end

