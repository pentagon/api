class Tmm::ShareLinksController < Tmm::BaseController
  skip_before_filter :authenticate_user!, except: [:index, :show]

  def index
    respond_with Tmm::SharelinksLoadingInteraction.new(user: current_user)
  end

  def show
    respond_with Tmm::SharelinkLoadingInteraction.new(id: params[:id], user: current_user)
  end

  def create
    creating_interaction = Tmm::SharelinkCreatingInteraction.new(user: current_user, sharelink_params: params)
    respond_with creating_interaction do |format|
      format.json {
        if creating_interaction.valid?
          render json: creating_interaction, location: false and return
        else
          render json: creating_interaction, status: :unprocessable_entity and return
        end
      }
    end
  end

  def destroy
    share_link = current_user.share_links.find_by uri: params[:id]
    share_link.destroy if share_link
    respond_with share_link
  end
end
