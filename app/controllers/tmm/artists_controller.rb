class Tmm::ArtistsController < Tmm::BaseController

  def index
    respond_with Tmm::ArtistsLoadingInteraction.new(params: params)
  end

  def show
    respond_with Tmm::ArtistLoadingInteraction.new(id: params[:id], user: current_user)
  end
end

