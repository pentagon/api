class Tmm::TracksController < Tmm::BaseController
  skip_before_filter :authenticate_user!, only: [:index, :show]

  EMBER_UNNEEDED_FIELDS = %w|user_id media_url|
  DEFAULT_OPTIONS = {
    source: 'fms'
  }

  def index
    respond_with Tmm::TracksLoadingInteraction.new(ids: params[:ids], user: current_user)
  end

  def show
    track_interaction = Tmm::TrackLoadingInteraction.new(id: params[:id], user: current_user)
    respond_with track_interaction, status: track_interaction.status
  end

  def create
    track_interaction = Tmm::TrackCreatingInteraction.new(user: current_user, coutry: user_country, track_params: params)
    respond_with track_interaction, location: false
  end

  def destroy
    track = current_user.tracks.find_by_slug params[:id]
    respond_after_destroy track
  end

  def update
    render json: Tmm::TrackUpdatingInteraction.new(user: current_user, country: user_country, track_params: params)
  end
end
