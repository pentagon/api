class UsersController < ApplicationController
  before_filter :authenticate_user!, only: [:confirm_social, :subscribe, :unsubscribe, :with_roles, :validate_contact_info]
  before_filter :admin_required!, only: [:with_roles]
  before_filter :authorize_by_client!, only: [:mobile_authentication]

  include AddServiceName

  def show
    resource = ::Persistence::User.find_by_id_or_slug(params[:id]) || ::Persistence::User.find_by(uri: params[:id])
    head :not_found and return if resource.blank?
    res = resource.as_json(json_type: params[:as]).merge geolocation_info: geo_location
    respond_with res, callback: params[:callback]
  end

  def find_by_email
    resource = ::Persistence::User.find_by_email params[:email]
    head :not_found and return if resource.blank?
    res = resource.as_json.merge geolocation_info: geo_location
    respond_with res
  end

  # TODO figure out if we need it
  def validate_contact_info
    user = ::Persistence::User.find_by uri: params[:id]
    user.contact_info.force_valid? ? render(json: user, json_type: :full) :
      render(json: {success: false, errors: user.contact_info.errors.full_messages, errors_with_keys: user.contact_info.errors}, status: :unprocessable_entity)
  end

  def confirm_social
    current_user.add_social_auth(uid: params[:uid], provider: params[:provider])
    if current_user.save
      add_service_name(current_user) if params[:source].present?
      render json: ({token: current_user.authentication_token}), status: :created
    else
      render json: ({}), status: :not_acceptable
    end
  end

  def bulk_load
    head :unprocessable_entity and return if params[:ids].blank?
    respond_with ::Persistence::User.any_in(uri: params[:ids]), location: false
  end

  def subscribe
    @message = if current_user.subscribed?
      I18n.t('messages.notices.already_subscribed')
    else
      current_user.subscribe ? I18n.t('messages.notices.subscribed') : I18n.t('messages.errors.subscribe')
    end
    respond_to do |format|
      format.json { render json: {message: @message} }
      format.html { render text: @message }
    end
  end

  def unsubscribe
    @message = if current_user.unsubscribed?
      I18n.t('messages.notices.already_unsubscribed')
    else
      current_user.unsubscribe ? I18n.t('messages.notices.unsubscribed') : I18n.t('messages.errors.unsubscribe')
    end
    respond_to do |format|
      format.json { render json: {message: @message} }
      format.html { render text: @message }
    end
  end

  def with_roles
    required_params(:roles) do
      respond_with Persistence::User.with_roles(*params[:roles]).order_by(:email.asc).pluck(:email)
    end
  end

  def mobile_authentication
    required_params(:data) do
      required_params(:id, :email, scope: :data) do
        render json: Persistence::User.find_or_create_from_mobile(params[:provider], params[:data])
      end
    end
  end
end
