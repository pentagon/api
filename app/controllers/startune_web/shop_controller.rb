class StartuneWeb::ShopController < ApplicationController

  before_filter :authenticate_user!, only: [:buy]

  def templates
    render json: ::StartuneReport.templates
  rescue ::Persistemce::PaymentOrder::ItemProviderError
    head :unprocessable_entity
  end

  def buy
    ::StartuneWeb::ReportPurchasing.new(self, current_user, params).buy
  end
end
