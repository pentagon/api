class HitMakerReportsController < ApplicationController
  def show
    respond_with Persistence::HitMakerReport.find_by uri: params[:id]
  end
end
