class Thunderbird::TracksController < ApplicationController
  def search
    respond_with_interaction Thunderbird::TrackSearchingInteraction, params.slice(:colors, :page, :per_page)
  end
end
