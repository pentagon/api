class Thunderbird::SnapTracksController < ApplicationController
  before_filter :authenticate_user!, only: [:create, :destroy]

  # def show
  #   track = Persistence::SnapTrack.find(params[:id])
  #   head :not_found and return if track.blank?
  #   user = Persistence::User.find_by(uri: track.user_uri)
  #   track_info = {
  #     id: track.id,
  #     image_url: track.image.filename,
  #     track_uri: track.track_uri,
  #     user_uri: track.user_uri,
  #     full_name: user.full_name,
  #     short_url: track.short_url
  #   }
  #   respond_with track_info
  # end

  def create
    respond_with ::Persistence::SnapTrack.create_from_params(params.update user_uri: current_user.uri), location: false
  end

  def list
    respond_with_interaction Thunderbird::SnapsListLoadingInteraction, {user_id: params[:user_id], page: params[:page],
      per_page: params[:per]}
  rescue InteractionErrors::WrongArgument
    head :unproccessable_entity
  end

  def destroy
    st = Persistence::SnapTrack.find params[:id]
    st.destroy if current_user.uri.eql? st.user_uri
    head :ok
  end
end
