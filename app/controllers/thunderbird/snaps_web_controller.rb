class Thunderbird::SnapsWebController < ApplicationController
  before_filter :authenticate_user!, only: [:create, :destroy]

  def index
    respond_with_interaction Thunderbird::UserSnapsLoadingInteraction, {user_id: params[:user_id]}
  rescue InteractionErrors::WrongArgument
    head :not_found
  end

  def show
    respond_with_interaction Thunderbird::SnapLoadingInteraction, {snap_id: params[:id]}
  rescue InteractionErrors::NotFound, InteractionErrors::WrongArgument
    head :not_found
  end

  def create
    respond_with ::Persistence::SnapTrack.create_from_params(params.update user_uri: current_user.uri), location: false
  end

  def destroy
    st = Persistence::SnapTrack.find params[:id]
    st.destroy if current_user.uri.eql? st.user_uri
    head :ok
  end
end
