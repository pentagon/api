class Recommend::TracksController < ApplicationController
  def show
    respond_with Recommend::TrackLoadingInteraction.new params
  rescue InteractionErrors::NotFound => e
    respond_with({success: false, message: e.message}, status: :not_found)
  end

  def autocomplete
    respond_with Recommend::TracksAutocompleteInteraction.new params
  rescue InteractionErrors::NotFound => e
    respond_with({success: false, message: e.message}, status: :not_found)
  end

  def search
    respond_with Recommend::TracksSearchInteraction.new params
  rescue InteractionErrors::NotFound => e
    respond_with({success: false, message: e.message}, status: :not_found)
  end
end
