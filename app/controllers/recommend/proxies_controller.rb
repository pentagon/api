class Recommend::ProxiesController < ApplicationController

  def index
    method = params[:recommend_method].present? ? params[:recommend_method].downcase.to_sym : :post
    req_params = [:get, :delete].include?(method) ? { params: params[:recommend_params] } : params[:recommend_params]
    render json: RestClient.send(method, File::join(::Settings.recommend_api_url, params[:recommend_action]), req_params)
  end

end
