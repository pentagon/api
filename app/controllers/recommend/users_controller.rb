class Recommend::UsersController < ApplicationController
  before_filter :authenticate_user!, only: [:search]

  def show
    respond_with Recommend::UserLoadingInteraction.new(id: params[:id], user: current_user)
  rescue InteractionErrors::NotFound => e
    respond_with({success: false, message: e.message}, status: :not_found)
  end

  def search
    render :json => Recommend::UserSearchInteraction.new(default_interaction_params.merge(field: params[:field], value: params[:value]))
  rescue InteractionErrors => e
    render :json => {success: false, message: e.message}, status: :not_found, :location => nil
  end
end
