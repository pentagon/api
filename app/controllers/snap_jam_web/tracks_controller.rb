class SnapJamWeb::TracksController < ApplicationController

  def show
    respond_with SnapJamWeb::TrackLoadingInteraction.new track_id: params[:id]
  rescue InteractionErrors::NotFound, InteractionErrors::WrongArgument
    head :not_found
  end

end
