class SnapJamWeb::UsersController < ApplicationController

  def show
    respond_with SnapJamWeb::UserLoadingInteraction.new user_id: params[:id]
  rescue InteractionErrors::NotFound, InteractionErrors::WrongArgument
    head :not_found
  end

end
