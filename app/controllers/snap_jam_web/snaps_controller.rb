class SnapJamWeb::SnapsController < ApplicationController

  def index
    respond_with SnapJamWeb::SnapsLoadingInteraction.new snap_ids: params[:ids]
  end

  def show
    respond_with SnapJamWeb::SnapLoadingInteraction.new snap_id: params[:id]
  rescue InteractionErrors::NotFound, InteractionErrors::WrongArgument
    head :not_found
  end
end
