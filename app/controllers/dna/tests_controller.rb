class Dna::TestsController < AuthorizedController
  def index
    tracks = {}
    user_test = Persistence::UserDna.get_last_for_user(current_user.uri)
    tracks = UserDnaTrackMap.get_tracks_for_user(current_user.uri, params[:per_page], params[:page])
    respond_with user_dna: user_test, suggested_tracks: tracks
  end

  def update
    tracks = {}
    dna = Persistence::UserDna.create_test_for_user current_user.uri
    head :not_found and return unless dna
    dna.add_test_result params[:result]
    tracks = UserDnaTrackMap.get_tracks_for_user(current_user.uri, params[:per_page], params[:page])
    render json: { suggested_tracks: tracks }
  end

  def random
    random_tracks = UserDnaTrackMap.random_without_user_tracks(current_user.uri, params[:count])
    respond_with random_tracks: random_tracks
  end

  def user_liked_tracks
    liked_tracks_ids = Persistence::UserTrackFeedback.last_liked_user_tracks(current_user.uri, params[:liked_count] || 10)
    tracks = Track.fetch liked_tracks_ids
    respond_with liked_tracks: tracks.map {|t| t.as_json_for_player(user_uri: current_user.uri)}
  end
end

