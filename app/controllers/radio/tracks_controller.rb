# encoding: UTF-8
class Radio::TracksController < Radio::BaseController

  before_filter :set_pagination_variables, only: [:more_by_artist, :simple_search, :advanced_search, :discovery, :radio_presets]

  def show
    respond_with Radio::TrackLoadingInteraction.new default_interaction_params.merge(track_id: params[:id])
  rescue InteractionErrors::NotFound
    # TODO do we really need such a response?
    respond_with({found: false, id: params[:id]}, status: :not_found)
  end

  def more_by_artist
    res = Radio::MoreByArtistLoadingInteraction.new default_interaction_params.merge(track_id: params[:id],
      page: @page, per_page: @per_page).as_json
    if res[:tracks].any?
      offset = (@page - 1) * @per_page
      set_pagination_headers total: res[:total], offset: offset, limit: @per_page
      respond_with res[:tracks]
    else
      head :unprocessable_entity
    end
  rescue InteractionErrors::NotFound
    head :not_found
  end

  def simple_search
    render nothing: true and return if params[:query].blank?
    res = Radio::TrackSearchingInteraction.new(default_interaction_params.merge(query: params[:query],
      page: @page, per_page: @per_page)).as_json
    if res[:tracks]
      offset = (@page - 1) * @per_page
      set_pagination_headers total: res[:total], offset: offset, limit: @per_page
      respond_with res[:tracks]
    else
      head :unprocessable_entity
    end
  end

  def advanced_search
    res = Radio::TrackSearchingInteraction.new(default_interaction_params.merge(search_params: params,
      page: @page, per_page: @per_page)).as_json
    offset = (@page - 1) * @per_page
    set_pagination_headers total: res[:total], offset: offset, limit: @per_page
    respond_with res[:tracks]
  end

  def discovery
    # TODO review this stub since 'discovery' method of track seems to be malfunctioning
    res = Radio::TrackSearchingInteraction.new(default_interaction_params.merge(search_params: params,
      page: @page, per_page: @per_page)).as_json
    offset = (@page - 1) * @per_page
    set_pagination_headers total: res[:total], offset: offset, limit: @per_page
    respond_with res[:tracks]
  end

  def latest_played_for_station
    respond_with Radio::LatestPlayedForStationLoadingInteraction.new(default_interaction_params.merge(
      station_id: params[:station_uri]))
  rescue InteractionErrors::NotFound
    head :not_found
  end

  def next_for_station
    respond_with Radio::NextTrackLoadingInteraction.new(default_interaction_params.merge(track_id: params[:track_uri],
      remote_user_ip: remote_user_ip, station_id: params[:station_uri], headers: request.headers))
  rescue InteractionErrors::UnprocessableEntity
    head :unprocessable_entity
  rescue InteractionErrors::NotFound
    head :not_found
  end

  def radio_presets
    res = Radio::PresetsLoadingInteraction.new(default_interaction_params.merge(random: params[:random],
      page: @page, per_page: @per_page)).as_json
    set_pagination_headers total: res[:total], offset: res[:offset], limit: @per_page
    respond_with res[:tracks]
  end

  def multi_load
    respond_with Radio::TracksLoadingInteraction.new(default_interaction_params.merge(track_ids: params[:ids]))
  rescue InteractionErrors::WrongArgument
    head :unprocessable_entity
  end

  private

  def set_pagination_variables
    @page = params[:page].to_i
    @per_page = params[:per_page].blank? ? 20 : params[:per_page].to_i
    @page = 1 if @page < 1
    @per_page = 1 if @per_page < 1
  end
end
