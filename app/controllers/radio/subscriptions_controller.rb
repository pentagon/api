# DEPRECATED
# TODO: Remove this controller after mobile apps stop using it.

class Radio::SubscriptionsController < Radio::BaseController

  before_filter :warn_deprecated

  def index
    per_page = params[:per_page] || 20
    subscriptions = ::Persistence::Subscription.load_from_params(params).page(params[:page]).per(per_page)
    set_pagination_headers_for(subscriptions)
    respond_with SubscriptionSerializer.new(subscriptions)
  end

  def show
    subscription = ::Persistence::Subscription.find_by uri: params[:id]
    options = subscription.blank? ? {status: :not_found} : {}
    respond_with SubscriptionSerializer.new(subscription), options
  end

  def update
    subscription = ::Persistence::Subscription.find_by(uri: params[:id])
    if subscription.update_attributes(params[:subscription])
      respond_with SubscriptionSerializer.new(subscription)
    else
      render json: {errors: subscription.errors.full_messages}, status: :unprocessable_entity
    end
  end

  def create
    params[:subscription][:type] = 'radio'
    subscription = ::Persistence::Subscription.new params[:subscription]
    if subscription.save
      RadioMailer.subscription_confirmation(subscription).deliver
      respond_with SubscriptionSerializer.new(subscription), location: false
    else
      render json: {errors: subscription.errors.full_messages}, status: :unprocessable_entity
    end
  end

  def destroy
    subscription = ::Persistence::Subscription.find_by uri: params[:id]
    if subscription.destroy
      head :ok
    else
      head :unprocessable_entity
    end
  end

  private

  def warn_deprecated
    ActiveSupport::Deprecation.warn "#{self.class} is deprecated. Use #{::SubscriptionsController} instead."
  end

end
