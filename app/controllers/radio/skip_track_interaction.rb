class Radio::SkipTrackInteraction < Interaction
  include Radio::Serializers
  include Radio::Utils

  def initialize args
    super
    track_id = args[:track_id]
    raise InteractionErrors::NotFound.new 'track_id' if track_id.blank?
    @station = Persistence::Station.find_by uri: args[:station_id]
    raise InteractionErrors::NotFound.new args[:station_id] unless @station
    @user ||= Persistence::User.find_by uri: @station.user_uri
    raise InteractionErrors::UnprocessableEntity.new unless @station.is_free || !medianet_tracks_limit_reached?(user)
    raise InteractionErrors::UnprocessableEntity.new unless @station.pending_tracks_manager.skip_track(track_id)
    @track = @station.pending_tracks_manager.get_next_track(track_id)
    @resolver = Radio::PumpedStreamResolver.new(ip_address: args[:remote_user_ip], station_uri: @station.uri,
      user: user, track: @track, headers: args[:headers], subscription: user.subscription('radio'), user_country: country)
  end

  def as_json opts = {}
    res = serialize_track @track
    res[:full_track_url] = @resolver.get_radio_location[:location]
    res
  end
end
