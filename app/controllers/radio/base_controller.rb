class Radio::BaseController < ApplicationController

private

  def medianet_tracks_limit_reached? user
    false and return if user.admin? || user.superuser?
    user.subscription('radio').tracks_limit_reached if user.has_usable_subscription?('radio')
  end

  def user_country
    country = params[:country].to_s.downcase.presence || super
    country = Settings.radio.fallback_country if !Settings.radio.allowed_countries.include?(country) && current_user.try(:superuser?)
    country
  end

  def user_country_allowed
    Settings.radio.allowed_countries.include? user_country
  end

  def user_subscribed(user = nil)
    user ||= current_user
    user.has_usable_subscription? 'radio' if user
  end
end
