class Radio::RadioPlaylistsController < Radio::BaseController
  before_filter :authenticate_user!, only: [:set]

  def index
    per_page = params[:per_page] || 20
    playlists = ::Persistence::RadioPlaylist.where(user_uri: params[:user_uri]).page(params[:page]).per(per_page)
    set_pagination_headers_for playlists
    respond_with playlists
  end

  def show
    playlist = ::Persistence::RadioPlaylist.find_by uri: params[:id]
    options = playlist.blank? ? {status: :not_found} : {}
    respond_with playlist, options
  end

  def update
    playlist = ::Persistence::RadioPlaylist.find_by uri: params[:id]
    if playlist.update_attributes params[:radio_playlist]
      respond_with playlist
    else
      render json: {errors: playlist.errors.full_messages}, status: :unprocessable_entity
    end
  end

  def create
    playlist = ::Persistence::RadioPlaylist.new params[:radio_playlist]
    if playlist.save
      respond_with playlist
    else
      render json: {errors: playlist.errors.full_messages}, status: :unprocessable_entity
    end
  end

  def set
    user_uri = current_user.uri
    radio_playlist = ::Persistence::RadioPlaylist.find_by user_uri: user_uri
    success = if radio_playlist
      params[:radio_playlist] ||= {}
      params[:radio_playlist][:id] = radio_playlist.id
      params[:radio_playlist][:station_uris] ||= []
      radio_playlist.update_attributes params[:radio_playlist]
    else
      radio_playlist = ::Persistence::RadioPlaylist.new params[:radio_playlist]
      radio_playlist.user_uri = user_uri
      radio_playlist.save
    end
    respond_with({success: !!success}, location: nil)
  end

end
