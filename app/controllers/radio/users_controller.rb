class Radio::UsersController < Radio::BaseController

  def show
    @user = ::Persistence::User.find_by_uri_or_slug params[:id]
    respond_with @user, json_type: :radio
  end

end
