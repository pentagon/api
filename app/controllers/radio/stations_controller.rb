class Radio::StationsController < Radio::BaseController

  before_filter :set_paginnation_params, only: [:index, :most_popular]
  before_filter :find_station_by_id, only: [:show, :update, :destroy, :send_request, :skip_track, :tracks_uris]

  def index
    stations = ::Persistence::Station.load_from_params(params).page(@page).per(@per_page)
    set_pagination_headers_for stations
    respond_with stations
  end

  def most_popular
    stations = ::Persistence::Station.most_popular.page(@page).per(@per_page)
    set_pagination_headers_for stations
    respond_with stations
  end

  def playlist
    stations, options = if (user_uri = params[:user_uri])
      playlist = ::Persistence::RadioPlaylist.find_by user_uri: user_uri
      stations = playlist ? playlist.stations : []
      [stations, {}]
    else
      [[], {status: :unprocessable_entity}]
    end
    respond_with stations, options
  end

  def show
    respond_with @station
  end

  def create
    params[:station][:filters] = JSON.parse(params[:station][:filters]) if params[:station][:filters].present?
    station = ::Persistence::Station.find_or_create_from_params params[:station].merge(country: user_country)
    if station.persisted?
      respond_with station, location: false
    else
      render json: {errors: station.errors.full_messages}, status: :unprocessable_entity
    end
  end

  def update
    params[:station][:filters] = JSON.parse(params[:filters]) if params[:filters].present?
    if @station.update_attributes params[:station]
      @station.request_tracks_for_new_seeds
      respond_with @station
    else
      render json: {errors: @station.errors.full_messages}, status: :unprocessable_entity
    end
  end

  def destroy
    head(@station.destroy ? :ok : :unprocessable_entity)
  end

  def multi_load
    params[:ids].is_a?(Array) ? respond_with(Persistence::Station.any_in uri: params[:ids]) : head(:unprocessable_entity)
  end

  def send_request
    request_type = params[:type]
    if params[:track_uri].present? && @station.mis_requests_manager.class::TRACK_REQUEST_TYPES.keys.map(&:to_s).include?(request_type)
      filters = params[:filters].present? ? JSON.parse(params[:filters]) : {}
      @station.mis_requests_manager.send_track_request(request_type, params[:track_uri], filters)
      render json: {success: true, message: "Request sent."}
    else
      render json: {success: false, message: "Invalid request type or some parameters missing."}, status: :unprocessable_entity
    end
  end

  def skip_track
    respond_with Radio::SkipTrackInteraction.new(default_interaction_params.merge(track_id: params[:track_uri],
      station_id: params[:id], remote_user_ip: remote_user_ip, headers: request.headers)), location: false
  rescue InteractionErrors::UnprocessableEntity
    head :unprocessable_entity
  rescue InteractionErrors::NotFound
    head :not_found
  end

  def tracks_uris
    user ||= Persistence::User.find_by uri: @station.user_uri
    unless user.superuser?
      unless @station.is_free || (user_country_allowed && user_subscribed(user))
        head :forbidden and return
      end
    end
    respond_with @station.pending_tracks_manager.pending_tracks_uris
  end

private

  def set_paginnation_params
    @page = params[:page].presence || 1
    @per_page = params[:per_page].presence || 20
  end

  def find_station_by_id
    options = {uri: params[:id]}
    @station = Persistence::Station.find_by options
    raise Mongoid::Errors::DocumentNotFound.new(Persistence::Station, options) unless @station
  end

end
