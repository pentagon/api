require 'maxmind_geoip'

class MainController < ApplicationController
  def index
    respond_with ::ApiStatus.get
  end

  def geoip
    respond_with ::MaxmindGeoip.get_geolocation_from_ip(remote_user_ip), callback: params[:callback]
  end

  def geosearch
    result = RestClient.get "http://gd.geobytes.com/AutoCompleteCity", params: params.except('controller', 'action', 'format')
    respond_with JSON.parse(result).map { |c| c.split(',').first }
  end

  def status
    respond_with ::ApiStatus.get false
  end
end
