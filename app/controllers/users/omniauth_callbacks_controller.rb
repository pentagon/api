class Users::OmniauthCallbacksController < ApplicationController
  include AddServiceName
  include ModulizeApplication

  def create
    @auth = request.env['omniauth.auth']
    unless @auth and @auth['info']
      render json: {error: 'messages.fatal.service_temporary_unavailable', status: :service_unavailable} and return
    end
    if params[:new]
      new_authentication
    else
      old_authentication
    end
  end

  private

  def add_to_visited_and_sign_in
    add_service_name(@user, params[:source]) if params[:source].present?
    sign_in(@user)
  end

  def new_authentication
    case (authentication_type = detect_auth_type)
    when :bound
      Rails.logger.info 'bound'
      add_to_visited_and_sign_in
      render json: @user
    when :unbound
      Rails.logger.info 'unbound'
      render json: unbound_auth
    when :absent
      Rails.logger.info 'absent'
      add_to_visited_and_sign_in
      render json: @user
    else
      Rails.logger.info 'error'
      render json: ({errors: @user.errors}), status: :not_acceptable
    end
  end

  def old_authentication
    auth_hash = @auth.merge(get_tracking_params)
    auth_hash = auth_hash.merge({source: params[:source]}) if params[:source].present?
    auth_hash = auth_hash.merge({remote_ip: request.remote_ip})
    @user = find_or_create_from_oauth(auth_hash, current_user)
    if @user.valid?
      add_to_visited_and_sign_in
      render json: serialize_user(@user)
    else
      render_errors @user.errors
    end
  end

  def serialize_user user
    user.as_json(:json_type => :full)
  end

  def find_or_create_from_oauth auth_hash, current_user
    Persistence::User.find_or_create_from_oauth(auth_hash, current_user) do |user|
      user.update_attributes(internal_user: true) if Settings.ips.map { |a| a.include? @remote_ip }.include?(true)
      user.assign_role(:superuser) if user.email =~ /@eventim\.de/
      user.save
      find_interaction('request_verification_interaction').new(current_user: user)
    end
  end

  def detect_auth_type
    return :bound if (@user = Persistence::User.find_by_auth(@auth.provider, @auth.uid))
    return :unbound if (@user = Persistence::User.find_by(email: @auth.info.email))
    return :absent if (@user = Persistence::User.create_from_oauth(@auth, parse_additional_params))
    :error
  end

  def parse_additional_params
    additional_params = {}
    additional_params[:source] = session[:user_source] || params[:source]
    additional_params[:utm_medium] = session[:utm_medium] || params[:utm_medium]
    additional_params[:utm_campaign] = session[:utm_campaign] || params[:utm_campaign]
    if params[:referral_token].present?
      additional_params[:referral_token] = params[:referral_token]
      additional_params[:remote_ip] = request.remote_ip
    end
    additional_params[:internal_user] = Settings.ips.map { |a| a.include? @remote_ip }.include?(true)
    additional_params.merge!(get_tracking_params) if params[:tracking].present?
    begin
      geo_info = geo_location
      Rails.logger.info geo_info.inspect
      additional_params[:contact_info_attributes] = {}
      additional_params[:contact_info_attributes][:country] = geo_info["country_code"] if geo_info["country_code"].present?
      additional_params[:contact_info_attributes][:state] = geo_info["region_name"] if geo_info["region_name"].present? and geo_info["country_code"] == "US"
      additional_params[:contact_info_attributes][:city] = geo_info["city"] if geo_info["city"].present?
      Rails.logger.info additional_params[:contact_info_attributes].inspect
    rescue Exception => e
      Rails.logger.info "User create geocoding exception: #{e}"
    end
    additional_params
  end

  def unbound_auth
    hash = {provider: @auth.provider, uid: @auth.uid, info: @auth.info.to_hash, gender: @auth.extra.raw_info.gender, status: 'unbound', token: @user.authentication_token}
    additional = case @auth.provider
    when 'facebook'
      {bio: @auth.extra.raw_info.bio, birthday: @auth.extra.raw_info.birthday}
    when 'google_oauth2'
      {bio: @auth.extra.raw_info.biography}
    end
    hash.merge(additional)
  end

  alias_method :facebook, :create
  alias_method :google_oauth2, :create
end
