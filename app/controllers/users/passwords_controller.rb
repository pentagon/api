class Users::PasswordsController < ApplicationController
  include ModulizeApplication
  skip_before_filter :require_no_authentication, only: [:edit]

  def new
    redirect_to accounts_url+'#restore_password'
  end

  def edit
    token_param = "?reset_password_token=#{params[:reset_password_token]}"
    redirect_url = params[:edit_password_url] ? params[:edit_password_url] + token_param : accounts_url + "/settings/password" + token_param + "#edit_password"
    redirect_to redirect_url
  end

  def create
    render json: find_interaction('reset_password_interaction').new(user_params: params), status: :created, location: false and return
  rescue InteractionErrors::UnprocessableEntity => e
    user_not_found e
  end

  def update
    res = find_interaction('update_password_interaction').new(params: params)
    sign_in res.user
    respond_with res, status: :no_content, location: false
  rescue InteractionErrors::UnprocessableEntity => e
    render json: ({errors: e.errors}), status: :unprocessable_entity, location: false
  end

  protected

  def user_not_found e
    render json: ({errors: e.errors}), status: :unprocessable_entity, location: false
  end
end
