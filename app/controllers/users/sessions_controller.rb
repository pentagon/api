class Users::SessionsController < ApplicationController
  include ModulizeApplication
  before_filter :authenticate_user!, only: [:destroy, :me, :update_me]

  def new
    head :ok and return if signed_in?
    head :unauthorized
  end

  def create
    authorization_interaction = find_interaction('sign_in_interaction').new(user_params: params)
    sign_in(authorization_interaction.user)
    respond_with authorization_interaction, location: false
  rescue ::Authorization::Exceptions::WrongEmailOrPasswordException
    wrong_email_response
  end

  def destroy
    find_interaction('sign_out_interaction').new(user: current_user)
    request.env['current_user'] = nil
    render json: ({}), status: :ok
  rescue Authorization::Exceptions::UnauthorizedException
    render json: ({error: 'You have to be authorized to do this'}), status: :unauthorized, location: false
  end

  def me
    loading_interaction = find_interaction('user_loading_interaction').new(params: params, current_user: current_user)
    respond_with loading_interaction
  rescue Authorization::Exceptions::UnauthorizedException
    render json: ({error: 'You have to be authorized to do this'}), status: :unauthorized, location: false
  end

  def update_me
    render json: find_interaction('user_updating_interaction').new(params: params, current_user: current_user), status: :ok, location: false
  rescue Authorization::Exceptions::UnauthorizedException
    render json: ({error: 'You have to be authorized to do this'}), status: :unauthorized, location: false
  rescue InteractionErrors::UnprocessableEntity => e
    render json: ({errors: e.errors.full_messages}), status: :unprocessable_entity, location: false
  end

  protected

  def wrong_email_response
    render json: ({error: 'Invalid email or password.'}), status: :unauthorized, location: false
  end
end
