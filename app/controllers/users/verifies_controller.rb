class Users::VerifiesController < ApplicationController
  include ModulizeApplication
  before_filter :authenticate_user!, except: [:verify]

  def send_verification_email
    respond_with find_interaction('request_verification_interaction').new(current_user: current_user), status: :created, location: false
  rescue Authorization::Exceptions::UnauthorizedException
    respond_with ({}), status: :unprocessable_entity, location: false
  end

  def verify
    verification_interaction = find_interaction('verify_interaction').new(params: params)
    redirect_to verification_interaction.url
  end

  def confirm_verification
    respond_with {}
  end
end
