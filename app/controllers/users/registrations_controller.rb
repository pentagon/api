class Users::RegistrationsController < ApplicationController
  include ModulizeApplication
  prepend_before_filter :require_no_authentication, only: [:new, :create, :cancel]
  prepend_before_filter :authenticate_user!, only: [:edit, :update, :destroy]
  before_filter :set_referral_token

  def new
    redirect_to accounts_url+'#sign_up'
  end

  def create
    user_creating_interaction = find_interaction('user_creating_interaction').new(params: params, remote_ip: remote_user_ip)
    respond_with user_creating_interaction do |format|
      format.json do
        if user_creating_interaction.errors.blank?
          sign_in(user_creating_interaction.user)
          render json: user_creating_interaction, status: :created, location: false and return
        else
          render json: ({errors: user_creating_interaction.errors}), status: :unprocessable_entity, location: false and return
        end
      end
    end
  end

  def sign_up_params
    (super || {}).merge({referral_uri: session[:referral_uri], referral_ip: remote_user_ip})
  end

  def set_referral_token
    if params[:referral_token].present?
      referral = Persistence::Referral.find_or_create(params[:referral_token], request.remote_ip)
      session[:referral_uri] = referral.uri
    end
  end
  protected :set_referral_token

end
