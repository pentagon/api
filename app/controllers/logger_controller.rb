class LoggerController < ApplicationController
  def index
    @entries = LogEntry.load_from_params(params)
    respond_with @entries
  end

  def create
    (queue.publish({key: 'logger', data: (params[:entries].presence || params)})) unless TorqueBox::ServiceRegistry.lookup("jboss.messaging.default").nil?
    head :created
  end

  private

  def queue
    @queue ||= TorqueBox::Messaging::Queue.new('/queues/backwriter')
  end
end
