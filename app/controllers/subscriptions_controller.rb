class SubscriptionsController < ApplicationController

  # TODO: Refactor this into interactions.

  prepend_before_filter :fix_ambigious_tokens, only: [:subscribe_confirm]
  before_filter :authenticate_user!, except: [:templates, :result_success, :result_error, :result_cancel]
  before_filter :admin_required!, only: [:index, :show, :destroy]
  before_filter :get_subscription_data, only: [:subscribe, :subscribe_confirm]

  def templates
    respond_with ::SubscriptionTemplate.select(params.merge(country: user_country))
  end

  def trial_template
    if params[:type].blank?
      return render json: {error: "no_type_provided"}, status: :unprocessable_entity
    elsif !trial_available(params[:type])
      return render json: {error: "trial_used"}, status: :unprocessable_entity
    end
    template = ::SubscriptionInternalTrialTemplate.find(params[:type], user_country)
    return head :not_found if template.blank?
    render json: template
  end

  def latest_for_user
    return head :unprocessable_entity if params[:type].blank?
    subscription = Persistence::Subscription.get_latest_of_type_for_user params[:type], current_user.uri
    status = subscription ? :ok : :not_found
    respond_with SubscriptionSerializer.new(subscription), status: status
  end

  def subscribe
    url_params = {host: Settings.domain, params: @subscription_params.merge({th_token: params[:token]})}
    return_url = subscribe_confirm_subscriptions_url(url_params)
    mobile = params[:mobile].present?
    auth_redirect_url = Persistence::Subscription.auth_redirect_url(
      @subscription_template,
      return_url,
      @subscription_params[:cancel_url],
      mobile
    )
    redirect_to auth_redirect_url
  end

  def subscribe_confirm
    begin
      profile_id = Persistence::Subscription.setup_profile @subscription_template, params[:paypal_token]
      return handle_subscription_error(:paypal) unless profile_id
    rescue
      return handle_subscription_error(:paypal)
    end

    @subscription = Persistence::Subscription.new(@subscription_template.to_hash.except(:valid_from, :valid_to))
    @subscription.user = current_user
    @subscription.payment_prov_id = profile_id
    @subscription.agent = @subscription_params[:agent]
    @subscription.department_code = @subscription_params[:department_code]
    @subscription.country = user_country

    if @subscription.save
      case @subscription.type
      when 'radio'
        RadioMailer.subscription_confirmation(@subscription).deliver
      when 'astro'
        AstroMailer.subscription_confirmation(@subscription).deliver
      when 'startune'
        StartuneMailer.subscription_confirmation(@subscription).deliver
      end
      redirect_to @subscription_params[:success_url]
    else
      logger.error "Purchased subscription was not saved"
      logger.error @subscription
      handle_subscription_error(:subscription_not_saved)
    end
  end

  def cancel
    @subscription = Persistence::Subscription.find_by(uri: params[:id])
    return head(:not_found) unless @subscription
    return head(:forbidden) unless @subscription.user_uri == current_user.uri
    options = {location: false, status: @subscription.cancel ? :ok : :unprocessable_entity}
    respond_with SubscriptionSerializer.new(@subscription), options
  end

  def reactivate
    @subscription = Persistence::Subscription.find_by(uri: params[:id])
    return head(:not_found) unless @subscription
    return head(:forbidden) unless @subscription.user_uri == current_user.uri
    options = {location: false, status: @subscription.bill_outstanding_amount ? :ok : :unprocessable_entity}
    respond_with SubscriptionSerializer.new(@subscription), options
  end

  def activate_trial
    errors = []
    template = ::SubscriptionInternalTrialTemplate.find(params[:type], user_country)
    errors << "no_type_provided" if params[:type].blank?
    errors << "template_not_found" unless template
    errors << "trial_used" unless trial_available(params[:type])
    return render json: {success: false, errors: errors}, status: :unprocessable_entity if errors.present?

    @subscription = Persistence::Subscription.create(template.to_hash.merge({
      agent: params[:agent],
      department_code: params[:department_code],
      is_internal_trial: true,
      repeat_frequency: 1,
      status: "active",
      trial_cycles: 1,
      trial_price: 0,
      user_uri: current_user.uri,
    }))
    render json: {success: true, subscription: SubscriptionSerializer.new(@subscription)}
  end

  def result_success
    render json: {result: {status: 'success'}}
  end

  def result_error
    render json: {result: {status: 'error'}}
  end

  def result_cancel
    render json: {result: {status: 'cancel'}}
  end

  # Admin actions
  # TODO: Remove them
  def index
    per_page = params[:per_page] || 20
    subscriptions = ::Persistence::Subscription.load_from_params(params).page(params[:page]).per(per_page)
    set_pagination_headers_for(subscriptions)
    respond_with SubscriptionSerializer.new(subscriptions)
  end

  def show
    subscription = ::Persistence::Subscription.find_by(uri: params[:id])
    options = subscription.blank? ? {status: :not_found} : {}
    respond_with SubscriptionSerializer.new(subscription), options
  end

  def destroy
    subscription = ::Persistence::Subscription.find_by(uri: params[:id])
    if subscription.cancel && subscription.destroy
      head :ok
    else
      head :unprocessable_entity
    end
  end

private

  # TODO: Rework it
  def handle_subscription_error(reason = nil)
    response.headers['X-Subscription-Error'] = reason.to_s
    redirect_to params[:error_url]
  end

  # TODO: Refactor this shit
  def get_subscription_data
    select_params = ::SubscriptionTemplate::SELECT_PARAMS
    config_params = [:success_url, :error_url, :cancel_url, :agent, :department_code]
    url_params = {host: Settings.domain}
    params[:country] = user_country
    params[:agent] = 'UNDEFINED' unless params[:agent].present?
    params[:success_url] = result_success_subscriptions_url(url_params) if params[:success_url].blank?
    params[:error_url] = result_error_subscriptions_url(url_params) if params[:error_url].blank?
    params[:cancel_url] = result_cancel_subscriptions_url(url_params) if params[:cancel_url].blank?
    @subscription_params = params.slice(*(select_params + config_params)).reject { |_, value| value.blank? }

    @subscription_params[:repeat_frequency] = @subscription_params[:repeat_frequency].to_i if @subscription_params[:repeat_frequency].present?
    @subscription_template = if @subscription_params[:id].present?
      ::SubscriptionTemplate.find @subscription_params[:id]
    else
      ::SubscriptionTemplate.select(@subscription_params).first
    end
    handle_subscription_error(:invalid_params) and return unless @subscription_template
  end

  # TODO: Rework it
  def user_country
    country = params[:country].to_s.downcase.presence || super
    unless ["gb", "us"].include? country
      if current_user.present? && current_user.superuser?
        country = "gb"
      end
    end
    country
  end

  def trial_available(type)
    return false unless current_user
    args = {
      user_uri: current_user.uri,
      type: type,
    }
    Persistence::Subscription.where(args).limit(1).last.nil?
  end

end
