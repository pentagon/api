class Player::V2::PlaylistsController < AuthorizedController
  def index
    respond_with Player::PlaylistsLoadingInteraction.new default_params
  end

  def show
    respond_with Player::PlaylistLoadingInteraction.new default_params.merge(playlist_id: params[:id])
  rescue InteractionErrors::NotFound
    head :not_found
  end

  def create
    respond_with Player::PlaylistCreatingInteraction.new(default_params.merge user_playlist: params[:user_playlist]),
      location: false
  end

  def update
    Player::PlaylistUpdatingInteraction.new default_params.merge(playlist_id: params[:id], user_playlist: params[:user_playlist])
    head :ok
  end

  def destroy
    Player::PlaylistDestroyingInteraction.new default_params.merge(playlist_id: params[:id])
    head :ok
  end

  def default_params
    {
      user: current_user,
      country: user_country
    }
  end
  private :default_params
end
