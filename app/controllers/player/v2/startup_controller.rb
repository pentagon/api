class Player::V2::StartupController < AuthorizedController
  def show
    respond_with Player::StartupLoadingInteraction.new user: current_user, country: user_country
  end
end
