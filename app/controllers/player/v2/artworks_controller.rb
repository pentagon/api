class Player::V2::ArtworksController < ApplicationController
  def index
    respond_with Player::ArtworkLoadingInteraction.new artist_id: params[:artist_id], country: user_country
  rescue InteractionErrors::NotFound
    head :not_found
  end
end
