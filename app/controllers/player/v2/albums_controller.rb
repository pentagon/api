class Player::V2::AlbumsController < ApplicationController
  def show
    respond_with Player::AlbumLoadingInteraction.new(uri: params[:id], country: user_country, user: current_user)
  rescue InteractionErrors::NotFound
    head :not_found
  end
end
