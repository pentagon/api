class Player::V2::TracksController < ApplicationController
  before_filter :authenticate_user!, only: [:index]

  def show
    respond_with Player::TrackLoadingInteraction.new track_id: params[:id], user: current_user, country: user_country
  rescue InteractionErrors::NotFound
    head :not_found
  end

  def feedback
    Persistence::UserTrackFeedback.create_or_update_from_params params.merge track_uri: params[:id]
    head :created
  end
end
