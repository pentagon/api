class Player::V2::ArtistsController < ApplicationController
  def tracks
    respond_with Player::ArtistTracksLoadingInteraction.new artist_id: params[:id], user: current_user, country: user_country
  rescue InteractionErrors::NotFound
    head :not_found
  end
end
