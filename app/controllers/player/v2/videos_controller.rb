class Player::V2::VideosController < AuthorizedController
  def create
    respond_with Player::VideoCreatingInteraction.new(params.slice(:video).update(user: current_user)), location: nil
  end

  def destroy
    Player::VideoDestroyingInteraction.new user: current_user, uri: params[:id]
    head :ok
  end
end
