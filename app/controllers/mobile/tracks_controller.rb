class Mobile::TracksController < ApplicationController
  before_filter :authenticate_user!, only: [:ban]

  def ban
    head :created, location: false
  end

  def search
    respond_with_interaction Mobile::TrackSearchingInteraction,
      params.slice(:q, :page, :per_page, :autocomplete)
  end
end
