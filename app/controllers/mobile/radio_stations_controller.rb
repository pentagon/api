class Mobile::RadioStationsController < AuthorizedController
  def index
    respond_with_interaction Mobile::RadioStationsLoadingInteraction
  end

  def create
    respond_with_interaction Mobile::RadioStationCreatingInteraction,
      params.slice(:seed_track_uris, :title, :is_free)
  end

  def update
    respond_with_interaction Mobile::RadioStationUpdatingInteraction,
      params.slice(:title, :seed_track_uris, :is_free).merge(station_id: params[:id])
  rescue InteractionErrors::NotFound => e
    render json: {status: 'error', message: e.message}, status: :not_found
  end

  def destroy
    respond_with_interaction Mobile::RadioStationDeletingInteraction, {station_id: params[:id]}
  rescue InteractionErrors::NotFound => e
    respond_with({status: 'error', message: e.message}, status: :not_found)
  end

  def next_track
    respond_with_interaction Mobile::RadioStationNextTrackLoadingInteraction, {station_id: params[:id]}
  rescue InteractionErrors::NotFound => e
    respond_with({status: 'error', message: e.message}, status: :not_found)
  rescue InteractionErrors::WrongArgument => e
    respond_with({status: 'error', message: e.message}, status: :unprocessable_entity)
  rescue InteractionErrors::TrackLimitReached => e
    respond_with({status: 'error', message: e.message}, status: :unprocessable_entity)
  end

  def feedback
    respond_with_interaction Mobile::RadioStationFeedbackCreatingInteraction,
      params.slice(:type, :track_uri, :filters).merge(station_id: params[:id])
  rescue InteractionErrors::WrongArgument, InteractionErrors::NotFound
    respond_with({status: 'error', message: $!.message}, status: :unprocessable_entity)
  end
end
