require 'maxmind_geoip'

class Mobile::AstroController < ApplicationController
  before_filter :authorize_by_client!, only: [:mobile_authentication]

  def mobile_authentication
    required_params(:data) do
      required_params(:id, :email, scope: :data) do
        render json: Persistence::User.find_or_create_from_mobile(params[:provider], params[:data])
      end
    end
  end

  def geoip
    respond_with ::MaxmindGeoip.get_geolocation_from_ip(remote_user_ip), callback: params[:callback]
  end
end
