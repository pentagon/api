class Mobile::RecommendationsController < ApplicationController
  def start
    respond_with_interaction Mobile::StartupLoadingInteraction, params.slice(:random)
  end

  def show
    res = Rails.cache.read cache_key unless current_user.try :superuser
    res ||= Mobile::RecommendationLoadingInteraction.new(default_interaction_params.merge(track_id: params[:id])).to_json
    Rails.cache.write cache_key, res, expires_in: 12.hours unless current_user.try :superuser
    respond_with res
  rescue RecommendationLoadingInteraction::RequestSent
    respond_with({status: 'queued'})
  rescue RecommendationLoadingInteraction::HasError => e
    respond_with({error: true, error: e.message})
  end

  def cache_key
    cache_key = "mobile_#{params[:id]}:#{user_country}"
    explicit_level = current_user ? current_user.explicit_filter.to_i : 0
    cache_key += ":filtered_#{explicit_level}"
  end
  private :cache_key
end
