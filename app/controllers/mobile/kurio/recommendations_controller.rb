class Mobile::Kurio::RecommendationsController < AuthorizedController
  def show
    cache_key = "kurio_#{params[:id]}:#{user_country}:expl_lvl_#{current_user.explicit_filter}"
    res = Rails.cache.read cache_key
    res ||= Mobile::Kurio::RecommendationLoadingInteraction.new(default_interaction_params.merge(track_id: params[:id])).to_json
    Rails.cache.write cache_key, res, expires_in: 12.hours
    respond_with res
  rescue RecommendationLoadingInteraction::RequestSent
    respond_with({status: 'queued'})
  rescue RecommendationLoadingInteraction::HasError => e
    respond_with({error: true, error: e.message})
  end
end
