class Mobile::Kurio::StartController < ApplicationController
  def index
    respond_with Mobile::Kurio::StartWheelsLoadingInteraction.new default_interaction_params
  end
end
