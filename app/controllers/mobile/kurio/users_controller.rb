class Mobile::Kurio::UsersController < AuthorizedController
  def update
    return head :unauthorized if current_user.nil? or current_user.uri != params[:id]
    return head :unauthorized unless current_user.valid_password? params[:password]
    # TODO consider params to update and move it to model
    current_user.update_attribute :explicit_filter, params[:explicit_filter]
    head :ok
  end
end
