class Mobile::Kurio::SubscriptionsController < ApplicationController
  def index
    head :unauthorized unless current_user
    respond_with current_user.subscriptions
  end
end
