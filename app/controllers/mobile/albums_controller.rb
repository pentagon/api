class Mobile::AlbumsController < ApplicationController
  def search
    respond_with_interaction Mobile::AlbumSearchingInteraction, params.slice(:q, :page, :per_page)
  end

  def show
    respond_with_interaction Mobile::AlbumLoadingInteraction, {album_id: params[:id]}
  rescue InteractionErrors::NotFound => e
    respond_with({status: 'error', message: e.message}, status: :not_found)
  end
end
