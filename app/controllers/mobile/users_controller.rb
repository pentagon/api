class Mobile::UsersController < ApplicationController
  before_filter :authenticate_user!, only: [:me, :update]

  #
  # @params:
  #   {"subject": "blah-blah", "message": "blah-blah-blah"}
  #
  # TODO authorize app before emailing to disallow behaving like a gateway for various jerks spamming our users
  #
  def mail
    user = Persistence::User.find_by uri: params[:id]
    head :not_found and return unless user
    head :unprocessable_entity and return if user.email.blank?
    MobileMailer.notification(user: user, subject: params[:subject], message: params[:message]).deliver
    head :ok
  end

  def me
    res = current_user.as_json
    res[:radio_subscribed] = current_user.has_usable_subscription? 'radio'
    respond_with res
  end

  # Quick fix for Kovalskiy. Review it
  def update
    current_user.explicit_filter = params[:explicit_filter].to_i
    if current_user.save
      render json: current_user.as_json
    else
      render json: {error: current_user.errors.full_messages}, status: :unprocessable_entity
    end
  end
end
