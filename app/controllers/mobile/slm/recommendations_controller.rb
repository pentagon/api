class Mobile::Slm::RecommendationsController < ApplicationController
  def index
    respond_with_interaction Mobile::Slm::RecommendationsLoadingInteraction,
      params.slice(:activity, :page, :per_page)
  end
end
