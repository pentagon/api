class Mobile::Slm::ProfilesController < AuthorizedController
  def show
    respond_with_interaction Mobile::Slm::ProfileLoadingInteraction
  end

  def create
    respond_with_interaction Mobile::Slm::ProfileCreatingInteraction,
      params.slice(:birthdate, :birthplace, :gender, :school_list)
  rescue InteractionErrors::WrongArgument => e
    respond_with({error: true, message: e.message}, status: :unprocessable_entity, location: false)
  end
end
