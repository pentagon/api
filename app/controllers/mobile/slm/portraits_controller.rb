class Mobile::Slm::PortraitsController < AuthorizedController
  def create
    respond_with_interaction Mobile::Slm::PortraitCreatingInteraction, params.slice(:track_feedback)
  end
end
