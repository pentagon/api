class Mobile::Contextual::Users::RegistrationsController < ::Users::RegistrationsController
  def create
    super
  rescue Mobile::Contextual::Authorization::BirthdayAbsent
    render json: ({error: "birthday_absent"}), status: :unprocessable_entity
  end
end
