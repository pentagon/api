class Mobile::Contextual::Users::SessionsController < ::Users::SessionsController
  protected

  def wrong_email_response
    render json: ({error: 'invalid_email_or_password'}), status: :unauthorized, location: false
  end
end
