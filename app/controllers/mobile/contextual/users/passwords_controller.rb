class Mobile::Contextual::Users::PasswordsController < ::Users::PasswordsController
  protected

  def user_not_found e
    render json: ({error: 'no_user'}), status: :not_found, location: false
  end
end
