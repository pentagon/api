class Mobile::Contextual::TracksController < ApplicationController
  def index
    respond_with_interaction Mobile::Contextual::TrackLoadingInteraction, {track_id: params[:id]}
  rescue InteractionErrors::NotFound => e
    render json: {message: e.message}, status: :not_found
  end
end
