class Mobile::Snapjam::TracksController < ApplicationController
  def index
    respond_with_interaction Mobile::Snapjam::TrackSearchingInteraction, params
  end
end
