class Mobile::Snapjam::SnapsController < AuthorizedController
  def create
    respond_with_interaction Mobile::Snapjam::SnapCreatingInteraction, params.slice(:track_id,
      :image, :preview, :geo_location)
  rescue InteractionErrors::UnprocessableEntity => e
    render json: {message: e.message}, status: :unprocessable_entity
  end

  def delete
    respond_with_interaction Mobile::Snapjam::SnapDeletingInteraction, params.slice([:id])
  rescue InteractionErrors::Forbidden
    head :forbidden
  end

  def history
    respond_with_interaction Mobile::Snapjam::SnapsLoadingInteraction, params.slice(:amount, :offset)
  end
end
