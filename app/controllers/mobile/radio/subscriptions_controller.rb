class Mobile::Radio::SubscriptionsController < AuthorizedController
  def templates
    respond_with Mobile::Radio::SubscriptionTemplatesLoadingInteraction.new default_interaction_params
  end

  def current
    respond_with Mobile::Radio::SubscriptionLoadingInteraction.new(default_interaction_params)
  rescue InteractionErrors::NotFound
    respond_with({error: 'no subscriptions found'}, status: :not_found)
  end

  def activate
    respond_with Mobile::Radio::SubscriptionActivatingInteraction.new(default_interaction_params.merge(
      subscription_id: params[:id])), location: false
  rescue InteractionErrors::NotFound
    head :not_found
  rescue InteractionErrors::Forbidden
    head :forbidden
  rescue InteractionErrors::UnprocessableEntity
    head :unprocessable_entity
  end

  def cancel
    respond_with Mobile::Radio::SubscriptionCancellingInteraction.new(default_interaction_params.merge(
      subscription_id: params[:id])), location: false
  rescue InteractionErrors::NotFound
    head :not_found
  rescue InteractionErrors::Forbidden
    head :forbidden
  rescue InteractionErrors::UnprocessableEntity
    head :unprocessable_entity
  end
end
