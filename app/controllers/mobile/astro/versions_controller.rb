class Mobile::Astro::VersionsController < ApplicationController
  def latest
    respond_with({
      'latest' => {
        'version' => params[:version],
        'critical' => false,
        'platform' => params[:platform],
        'deviceId' => params[:deviceId]
      }
    })
  end
end
