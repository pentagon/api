class Mobile::Astro::SubscriptionsController < AuthorizedController

  prepend_before_filter :fix_ambigious_tokens, only: [:subscribe_confirm]
  before_filter :authenticate_user!, except: [:templates, :result]

  rescue_from InteractionErrors::NotFound do
    respond_with({error: 'subscription not found'}, status: :not_found)
  end
  rescue_from InteractionErrors::Forbidden do
    head :forbidden
  end
  rescue_from InteractionErrors::UnprocessableEntity do
    head :unprocessable_entity
  end
  rescue_from InteractionErrors::InvalidHeaders do
    render json: {result: {status: 'invalid_headers'}}, status: :unprocessable_entity
  end
  rescue_from InteractionErrors::WrongArgument do |e|
    render json: {error: e.message}, status: :unprocessable_entity
  end
  rescue_from InteractionErrors::RedirectingError do |e|
    redirect_to e.redirect_url
  end

  # It's actually "reactivate", but for some reason mobile team uses this naming.
  def activate
    respond_with_interaction Mobile::Astro::SubscriptionActivatingInteraction,
      {subscription_id: params[:id]}
  end

  def cancel
    respond_with_interaction Mobile::Astro::SubscriptionCancellingInteraction,
      {subscription_id: params[:id]}
  end

  def current
    respond_with_interaction Mobile::Astro::SubscriptionLoadingInteraction
  end

  def result
    render json: {result: {status: params[:status]}}
  end

  def subscribe
    respond_with_interaction Mobile::Astro::SubscribeInitiatingInteraction, params.slice(:id)
  end

  def subscribe_confirm
    respond_with_interaction Mobile::Astro::SubscribeConfirmingInteraction,
      params.slice(:id, :paypal_token, :token, :agent, :department_code, :success_url, :error_url,
        :cancel_url, :user_country)
  end

  def templates
    args = params.merge(country: user_country, type: "astro")
    render json: SubscriptionTemplate.select(args)
  end
end
