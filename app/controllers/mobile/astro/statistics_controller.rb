class Mobile::Astro::StatisticsController < ApplicationController
  def create
    # this needs proxy_set_header CLIENT_IP $remote_addr; in nginx conf
    args = (params[:track] || params).merge geo_location
    args = args.reverse_merge user_ip: request.remote_ip, client_id: 'ASTRO_MOBILE'
    stat = Persistence::TrackStatistic.create_or_update_from_params args
    respond_with stat, location: false
  end
end
