class Mobile::Astro::UsersController < ApplicationController
  before_filter :authorize_by_client!, only: [:mobile_authentication]
  before_filter :authenticate_user!, only: [:me]

  def mobile_authentication
    required_params(:data) do
      required_params(:id, :email, scope: :data) do
        render json: Persistence::User.find_or_create_from_mobile(params[:provider], params[:data])
      end
    end
  end

  def me
    respond_with current_user, callback: params[:callback]
  end
end
