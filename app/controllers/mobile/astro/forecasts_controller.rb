class Mobile::Astro::ForecastsController < ApplicationController
  def index
    respond_with_interaction Mobile::Astro::HoroscopeLoadingInteraction, params.slice(:id)
  rescue InteractionErrors::WrongArgument => e
    respond_with({message: e.message}, status: :unprocessable_entity)
  end
end
