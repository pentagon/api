class Mobile::Astro::Users::SessionsController < Users::SessionsController

  def create
    super
  rescue ::Authorization::Exceptions::UnverifiedUser
    respond_with ({error: "user_not_verified"}), status: :forbidden, location: false
  end

  protected

  def wrong_email_response
    render json: ({error: 'invalid_email_or_password'}), status: :unauthorized, location: false
  end
end
