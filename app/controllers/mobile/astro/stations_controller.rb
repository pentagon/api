class Mobile::Astro::StationsController < AuthorizedController
  def create
    respond_with_interaction Mobile::Astro::StationCreatingInteraction, params.slice(:station)
  rescue InteractionErrors::UnprocessableEntity => e
    respond_with({error: e.message}, status: :unprocessable_entity, location: false)
  rescue InteractionErrors::WrongArgument => e
    respond_with({error: e.message}, status: :unprocessable_entity, location: false)
  rescue InteractionErrors::StationCreatingDisallowed => e
    respond_with({error: e.message}, status: :unprocessable_entity, location: false)
  end

  def next_track
    respond_with_interaction Mobile::Astro::NextTrackLoadingInteraction,
      params.slice(:station_id, :current_track, :action_type)
  rescue InteractionErrors::WrongArgument => e
    respond_with({error: e.message}, status: :unprocessable_entity)
  rescue InteractionErrors::UnprocessableEntity => e
    respond_with({error: e.message}, status: :unprocessable_entity)
  # TODO consider proper response with mobile team
  rescue InteractionErrors::RadioQueueEmpty
    respond_with({error: 'no_track_in_station'}, status: :not_found)
  end
end
