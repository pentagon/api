class Mobile::Astro::TracksController < AuthorizedController
  def get_bookmarks
    respond_with_interaction Mobile::Astro::BookmarksLoadingInteraction
  end

  def add_bookmark
    respond_with_interaction Mobile::Astro::BookmarkCreatingInteraction, params.slice(:track_id)
  rescue InteractionErrors::WrongArgument => e
    render json: {error: e.message}, status: :unprocessable_entity
  end

  def remove_bookmark
    respond_with_interaction Mobile::Astro::BookmarkDeletingInteraction, params.slice(:track_id)
  rescue InteractionErrors::WrongArgument => e
    render json: ({error: e.message}), status: :unprocessable_entity
  end

  def feedback
    respond_with_interaction Mobile::Astro::FeedbackCreatingInteraction,
      params.slice(:track_id, :feedback)
  rescue InteractionErrors::WrongArgument => e
    render json: {error: e.message}, status: :unprocessable_entity
  end
end
