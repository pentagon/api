class Mobile::Startune::SubscriptionsController < ApplicationController

  prepend_before_filter :fix_ambigious_tokens, only: [:subscribe_confirm]
  before_filter :authenticate_user!, except: [:subscribe_confirm, :result_success, :result_error,
    :result_cancel]

  def current
    respond_with_interaction Mobile::Startune::SubscriptionLoadingInteraction
  rescue InteractionErrors::NotFound
    respond_with({error: 'no subscriptions found'}, status: :not_found)
  end

  def templates
    respond_with_interaction Mobile::Startune::SubscriptionTemplatesLoadingInteraction
  end

  def activate
    respond_with_interaction Mobile::Startune::SubscriptionActivatingInteraction,
      {subscription_id: params[:id]}
  rescue InteractionErrors::NotFound
    head :not_found
  rescue InteractionErrors::Forbidden
    head :forbidden
  rescue InteractionErrors::UnprocessableEntity
    head :unprocessable_entity
  end

  def cancel
    respond_with_interaction Mobile::Startune::SubscriptionCancellingInteraction,
      {subscription_id: params[:id]}
  rescue InteractionErrors::NotFound
    head :not_found
  rescue InteractionErrors::Forbidden
    head :forbidden
  rescue InteractionErrors::UnprocessableEntity
    head :unprocessable_entity
  end

  def subscribe
    respond_with_interaction Mobile::Startune::SubscribeInitiatingInteraction, params.slice(:id)
  rescue InteractionErrors::RedirectingError => e
    redirect_to e.redirect_url
  rescue InteractionErrors::InvalidHeaders
    render json: {result: {status: 'invalid_headers'}}, status: :unprocessable_entity
  rescue InteractionErrors::WrongArgument => e
    render json: {error: e.message}, status: :unprocessable_entity
  end

  def subscribe_confirm
    respond_with_interaction Mobile::Startune::SubscribeConfirmingInteraction,
      params.slice(:id, :paypal_token, :token, :agent, :department_code, :success_url, :error_url,
        :cancel_url, :user_country)
  rescue InteractionErrors::RedirectingError => e
    redirect_to e.redirect_url
  rescue InteractionErrors::WrongArgument => e
    render json: {error: e.message}, status: :unprocessable_entity
  end

  def result_success
    render json: {result: {status: 'success'}}
  end

  def result_error
    render json: {result: {status: 'error'}}
  end

  def result_cancel
    render json: {result: {status: 'cancel'}}
  end
end
