class Mobile::Startune::ForecastsController < ApplicationController
  def index
    respond_with_interaction Mobile::Startune::HoroscopeLoadingInteraction, params.slice(:id)
  rescue InteractionErrors::WrongArgument => e
    respond_with({message: e.message}, status: :unprocessable_entity)
  end
end
