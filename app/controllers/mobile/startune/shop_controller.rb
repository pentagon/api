class Mobile::Startune::ShopController < AuthorizedController

  before_filter :authenticate_user!, only: [:buy]

  def templates
    respond_with_interaction Mobile::Startune::TemplatesLoadingInteraction
  rescue InteractionErrors::ThirdPartyFailure
    head :unprocessable_entity
  end

  def buy
    args = default_interaction_params.merge(params).merge(controller: self)
    Mobile::Startune::ReportPurchasing.new(args).buy
  end
end
