class Mobile::Startune::TracksController < AuthorizedController
  def get_bookmarks
    respond_with_interaction Mobile::Startune::BookmarksLoadingInteraction
  end

  def add_bookmark
    respond_with_interaction Mobile::Startune::BookmarkCreatingInteraction, params.slice(:track_id)
  rescue InteractionErrors::WrongArgument => e
    render json: {error: e.message}, status: :unprocessable_entity
  end

  def remove_bookmark
    respond_with_interaction Mobile::Startune::BookmarkDeletingInteraction, params.slice(:track_id)
  rescue InteractionErrors::WrongArgument => e
    render json: ({error: e.message}), status: :unprocessable_entity
  end
end
