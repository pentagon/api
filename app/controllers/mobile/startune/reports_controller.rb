class Mobile::Startune::ReportsController < ApplicationController
  before_filter :authenticate_user!, only: [:index]

  def index
    respond_with_interaction Mobile::Startune::ReportsLoadingInteraction
  end

  def create
    respond_with_interaction Mobile::Startune::ReportCreatingInteraction, params.slice(:id, :report, :email)
  end
end
