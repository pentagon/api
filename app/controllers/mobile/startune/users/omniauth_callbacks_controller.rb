class Mobile::Startune::Users::OmniauthCallbacksController < ::Users::OmniauthCallbacksController
  include Mobile::Startune::Serializers

  alias_method :facebook_token, :create
end
