class Mobile::Startune::Users::SessionsController < ::Users::SessionsController

  def create
    super
  rescue Authorization::Exceptions::UnverifiedUser
    render json: ({error: "user_not_verified"}), status: :unprocessable_entity, location: false
  rescue Authorization::Exceptions::TooManyDevices
    render json: ({error: "too_many_devices"}), status: :unprocessable_entity, location: false
  end

  def wrong_email_response
    render json: ({error: "invalid_email_or_password"}), status: :unauthorized
  end

end
