class Mobile::DevicesController < AuthorizedController
  def register
    respond_with_interaction Mobile::DeviceRegisteringInteraction, params.slice(:device_token)
  rescue InteractionErrors::WrongArgument => e
    render json: {error: e.message}, location: false
  end
end
