class Tiles::AlbumsController < ApplicationController
  def index
    if params[:q]
      respond_with Tiles::AlbumsSearchingInteraction.new default_interaction_params.merge(params.slice :q, :page, :per_page)
    else
      head :not_found
    end
  end
end
