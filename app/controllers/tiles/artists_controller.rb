class Tiles::ArtistsController < ApplicationController
  def index
    if params[:q]
      respond_with Tiles::ArtistsSearchingInteraction.new default_interaction_params.merge(params.slice :q, :page, :per_page)
    else
      head :not_found
    end
  end

  def show
    respond_with Tiles::ArtistLoadingInteraction.new default_interaction_params.merge(artist_id: params[:id])
  rescue InteractionErrors::NotFound
    head :not_found
  end
end
