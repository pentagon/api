class Tiles::V2::ArtistsController < ApplicationController
  def index
    respond_with_interaction Tiles::V2::ArtistsLoadingInteraction, params
  end

  def show
    respond_with_interaction Tiles::V2::ArtistLoadingInteraction, params
  end
end
