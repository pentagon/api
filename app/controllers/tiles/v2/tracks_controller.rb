class Tiles::V2::TracksController < ApplicationController
  def index
    respond_with_interaction Tiles::V2::TracksLoadingInteraction, params
  end

  def show
    respond_with_interaction Tiles::V2::TrackLoadingInteraction, params
  end
end
