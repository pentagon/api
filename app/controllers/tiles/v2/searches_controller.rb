class Tiles::V2::SearchesController < ApplicationController
  def index
    respond_with_interaction Tiles::V2::SearchesLoadingInteraction, q: params[:q], artists_limit: 100, albums_limit: 100, tracks_limit: 100
  end

  def show
    respond_with_interaction Tiles::V2::SearchLoadingInteraction, q: params[:id]
  end
end
