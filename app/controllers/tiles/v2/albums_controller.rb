class Tiles::V2::AlbumsController < ApplicationController
  def index
    respond_with_interaction Tiles::V2::AlbumsLoadingInteraction, params
  end

  def show
    respond_with_interaction Tiles::V2::AlbumLoadingInteraction, params
  end
end
