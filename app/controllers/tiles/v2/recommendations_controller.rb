class Tiles::V2::RecommendationsController < ApplicationController
  def show
    res = Tiles::V2::RecommendationLoadingInteraction.new(user: current_user, country: user_country, track_id: params[:id])
    respond_with res
  end
end
