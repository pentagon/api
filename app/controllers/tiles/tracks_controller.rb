class Tiles::TracksController < ApplicationController
  before_filter :authenticate_user!, only: [:feedback]

  def index
    if params[:q]
      respond_with Tiles::TracksSearchingInteraction.new default_interaction_params.merge(params.slice :q, :page, :per_page)
    else
      head :not_found
    end
  end

  def feedback
    respond_with Tiles::FeedbackLoadingInteraction.new(user: current_user, country: user_country, ids: params[:ids]),
      callback: params[:callback]
  end

  def search
    respond_with Mobile::TrackSearchingInteraction.new(default_interaction_params.merge(params.slice :q, :page, :per_page))
  end
end
