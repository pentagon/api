class Dwl::Users::VerifiesController < ::Users::VerifiesController
  before_filter :authenticate_user!, except: [:verify, :send_verification_email]

  def send_verification_email
    respond_with find_interaction('request_verification_interaction').new(email: params[:email]), status: :created
  rescue InteractionErrors::NotFound
    respond_with({error: 'Invalid email.'}, status: :unprocessable_entity)
  end
end
