class RecommendationsController < ApplicationController
  def show
    if params[:id].include?('recommendation')
      respond_with Persistence::Recommendation.find_by uri: params[:id]
    else
      respond_with Persistence::Recommendation.where track_uri: params[:id]
    end
  end

  def destroy
    res = Persistence::Recommendation.where(track_uri: params[:id])
    res = res.where type: params[:type] if %w(wheels hss3).include?(params[:type])
    res.map(&:destroy) if res.any?
    head :ok
  end

  def bulk_destroy
    head :unprocessable_entity if params[:ids].blank?
    res = Persistence::Recommendation.where(track_uri: params[:ids])
    res = res.where type: params[:type] if %w(wheels hss3).include?(params[:type])
    res.map(&:destroy) if res.any?
    head :ok
  end

  def precache
    %w(us, uk).each {|c| RecommendationLoadingInteraction.new country: c, track_id: params[:id], priority: 10}
    head :ok
  end
end
