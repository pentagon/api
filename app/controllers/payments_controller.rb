class PaymentsController < ApplicationController

  prepend_before_filter :fix_ambigious_tokens, only: [:confirm_purchase, :confirm_cancel]
  before_filter :authenticate_user!, only: [:buy, :check_billing_address, :update_billing_address]

  def process_ipn
    ipn = OffsitePayments.integration(:paypal).notification(request.raw_post)
    logger.info("IPN received")
    logger.info(ipn.params)
    if ipn.acknowledge && (ipn.test? == test_mode?)
      logger.info("IPN verified")
      ::Persistence::PaymentOrder.process_ipn(ipn)
      head :ok
    else
      logger.info("IPN invalid")
      head :unprocessable_entity
    end
  end

  def buy
    @item = digital_goods_item
    return handle_payments_error("item_not_found") unless @item

    params.merge!(ip_address: remote_user_ip, country: user_country)
    payment = ::Persistence::PaymentOrder.create_temporary_payment!(@item, current_user, params)

    url_params = {
      host: Settings.domain,
      protocol: request.protocol,
      params: {
        th_token: params[:token],
        ip_address: remote_user_ip
      }
    }
    url_params[:params].merge!({popup: 1}) if params[:popup]
    confirm_purchase_url = confirm_purchase_payment_url(payment.uri, url_params)
    confirm_cancel_url = confirm_cancel_payment_url(payment.uri, url_params)
    payment_redirect_url = payment.purchase_redirect_url(confirm_purchase_url, confirm_cancel_url)

    redirect_to payment_redirect_url
  rescue Persistence::PaymentOrder::PaymentError => e
    reason = e.payments_message_code if e.respond_to?(:payments_message_code)
    handle_payments_error(reason)
  end

  def confirm_cancel
    payment = ::Persistence::PaymentOrder.find_by(uri: params[:id])
    return handle_payments_error("internal_payment_error") unless payment

    payment.process_cancel

    cancel_url = payment.custom_options["cancel_url"]
    if cancel_url.blank?
      url_params = {
        host: Settings.domain,
        protocol: request.protocol,
        status: 'cancel',
        params: {
          token: params[:token],
          payment_id: payment.uri,
          popup: payment.custom_options["popup"]
        }
      }
      cancel_url = result_payments_url(url_params)
    end
    redirect_to cancel_url
  end

  def confirm_purchase
    payment = ::Persistence::PaymentOrder.find_by(uri: params[:id])
    return handle_payments_error("internal_payment_error") unless payment

    params[:error_url] = payment.custom_options["error_url"]
    params[:popup] = payment.custom_options["popup"]
    begin
      payment.do_purchase(params[:paypal_token], params[:PayerID])
    rescue Persistence::PaymentOrder::PaymentProviderFundingFailureError
      return redirect_to payment.funding_error_recover_redirect_url(params[:paypal_token])
    end
    return handle_payments_error("payment_not_successful") unless payment.successful?

    delivery_result = payment.deliver_item

    if delivery_result[:success]
      success_url = payment.custom_options["success_url"]
      if success_url.present?
        success_url = add_params_to_url(success_url, payment_uri: payment.uri, payment_provider_payment_id: payment.payment_provider_payment_id)
      else
        url_params = {
          host: Settings.domain,
          protocol: request.protocol,
          status: 'success',
          params: {
            token: params[:token],
            payment_id: payment.uri
          }
        }
        url_params[:params].merge!(popup: 1) if params[:popup]
        success_url = result_payments_url(url_params)
      end
      redirect_to success_url
    else
      handle_payments_error("delivery_error")
    end
  rescue Persistence::PaymentOrder::PaymentError => e
    reason = e.payments_message_code if e.respond_to?(:payments_message_code)
    handle_payments_error(reason)
  end

  def get_price
    @location_override_needed = true
    @item = digital_goods_item
    return head :not_found unless @item
    params[:country] = user_country
    price = @item.digital_goods_price(params)
    render json: {price: price.merge(success: true)}
  rescue Exception => e
    render json: {price: {success: false, message: e.message}}
  end

  def get_prices
    @location_override_needed = true
    type = params[:type].gsub("|", "/").camelize.constantize
    prices = []
    params[:country] = user_country
    type.digital_goods_find_by_ids(params[:ids], params).compact.each do |item|
      id = item.digital_goods_item_id
      prices << {id: id, price: item.digital_goods_price(params)}
    end
    render json: {prices: {success: true, items: prices}}
  rescue Exception => e
    render json: {prices: {success: false, message: e.message}}
  end

  def check_billing_address
    errors = billing_address_errors
    if errors.any?
      render json: {result: {success: false, errors: errors}}
    else
      render json: {result: {success: true}}
    end
  end

  def update_billing_address
    errors = billing_address_errors
    if errors.any? || !current_user.set_billing_address(params).save
      render json: {result: {success: false, errors: errors}}, status: :unprocessable_entity
    else
      render json: {result: {success: true}}
    end
  end

  def result
    @data = {type: :payments, token: params[:token], result: params[:status]}
    @data[:reason] = params[:reason] if params[:reason]
    if params[:payment_id].present? && payment = ::Persistence::PaymentOrder.find_by(uri: params[:payment_id])
      @data.merge!({
        payment_id: params[:payment_id],
        price_amount: payment.price_amount,
        price_currency: payment.price_currency
      })
    end
    if params[:popup]
      render "close_popup"
    else
      render json: {result: @data}
    end
  end

  def countries_list
    respond_with ::Persistence::PaymentOrder.valid_country_states
  end

  private

  def handle_payments_error(reason = nil)
    redirect_url = if params[:error_url].blank?
      url_params = {
        host: Settings.domain,
        protocol: request.protocol,
        status: 'error',
        params: {reason: reason, token: params[:token]}
      }
      url_params[:params].merge!(popup: 1) if params[:popup]
      result_payments_url(url_params)
    else
      response.headers["X-Payments-Error"] = reason.to_s
      params[:error_url]
    end
    redirect_to redirect_url
  end

  def add_params_to_url(url, params)
    parsed = URI.parse(url)
    query_params = Rack::Utils.parse_query(parsed.query).merge(params)
    "#{url.split("?").first}?#{query_params.to_query}"
  end

  def digital_goods_item
    type = params[:type].gsub("|", "/").camelize.constantize
    type.digital_goods_find_by_id(params[:id], country: user_country)
  rescue ::Persistence::PaymentOrder::ItemNotAvailableForPurchase
    raise
  rescue
    nil
  end

  def test_mode?
    Settings.paypal.use_sandbox
  end

  def billing_address_errors
    billing_address = current_user.billing_address_hash(params)
    errors = ::Persistence::PaymentOrder.check_billing_address(billing_address)
    billing_country = billing_address[:country].to_s.downcase
    ip_country = user_country.to_s.downcase
    unless billing_country == ip_country
      errors[:country] ||= :does_not_match unless current_user.superuser?
    end
    errors
  end

  def user_country
    params[:country].to_s.downcase.presence || super
  end

end
