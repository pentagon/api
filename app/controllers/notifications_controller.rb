class NotificationsController < ApplicationController
  def index
    respond_with ::Notification::Base.load_from_params params
  end
end
