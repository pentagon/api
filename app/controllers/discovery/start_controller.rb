class Discovery::StartController < ApplicationController
  def show
    res = Discovery::StartupLoadingInteraction.new user: current_user, country: user_country, random: params[:random]
    respond_with res, callback: params[:callback]
  end
end
