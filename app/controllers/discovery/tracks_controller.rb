class Discovery::TracksController < ApplicationController
  before_filter :authenticate_user!, only: [:send_mail]
  before_filter :update_params, only: [:index, :show, :search]

  def index
    respond_with Discovery::TracksLoadingInteraction.new params
  rescue InteractionErrors::NotFound => e
    respond_with({success: false, message: e.message}, status: :not_found)
  end

  def show
    respond_with Discovery::TrackLoadingInteraction.new params
  rescue InteractionErrors::NotFound => e
    respond_with({success: false, message: e.message}, status: :not_found)
  end

  def search
    respond_with_interaction Discovery::TrackSearchingInteraction, params
  rescue InteractionErrors::WrongArgument, InteractionErrors::NotFound
    head :not_found
  end

private

  def update_params
    params.update user: current_user, country: user_country
  end
end
