class Discovery::AdsController < ApplicationController
  # Returns json of requested tracks.
  # @params ids: ['','','', ...]
  def show
    render json: Ads.find_by_params(params, user_country)
  end
end
