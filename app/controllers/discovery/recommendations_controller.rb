class Discovery::RecommendationsController < ApplicationController
  def show
    res = Rails.cache.read cache_key unless current_user.try :superuser
    res ||= Discovery::RecommendationLoadingInteraction.new(user: current_user,
      country: user_country, track_id: params[:id]).to_json
    Rails.cache.write cache_key, res, expires_in: 12.hours unless current_user.try :superuser
    respond_with res, callback: params[:callback]
  rescue RecommendationLoadingInteraction::RequestSent
    respond_with({status: 'queued'}, callback: params[:callback])
  rescue RecommendationLoadingInteraction::HasError => e
    respond_with({error: true, error: e.message}, callback: params[:callback])
  end

  def cache_key
    cache_key = "discovery_#{params[:id]}:#{user_country}"
    explicit_level = current_user ? current_user.explicit_filter.to_i : 0
    cache_key += ":filtered_#{explicit_level}"
  end
  private :cache_key
end
