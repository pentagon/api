class Discovery::DownloadRequestsController < ApplicationController
  before_filter :authenticate_user!, only: [:create]

  def set_downloaded
    respond_with is_downloaded: download_request.set_downloaded(remote_user_ip)
  end

  def download
    head :not_found and return unless download_request
    track_url = download_request.purchase_get_url
    render json: track_url
  rescue => e
    logger.error "#{e.class} (#{e.message}):\n  #{e.backtrace.join("\n  ")}"
    respond_with success: false, message: e.message
  end

  protected

  def download_request
    @download_request ||= Persistence::DownloadRequest.find_for_download(params[:id])
  end
end
