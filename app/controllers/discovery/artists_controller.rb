class Discovery::ArtistsController < ApplicationController
  before_filter :update_params

  def show
    respond_with Discovery::ArtistLoadingInteraction.new params.merge artist_id: params[:id]
  rescue InteractionErrors::NotFound
    head :not_found
  end

  def search
    respond_with Discovery::ArtistSearchingInteraction.new params
  rescue InteractionErrors::WrongArgument, InteractionErrors::NotFound
    head :not_found
  end

  def update_params
    params.update user: current_user, country: user_country
  end
  private :update_params
end
