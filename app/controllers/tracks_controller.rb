class TracksController < ApplicationController
  def wavejson
    json_name = File.join(params[:id].to_s.split ':') + '.json'
    if File.exists?(File.join(::Settings.storage_path, json_name))
      redirect_to File.join(::Settings.storage_url, json_name)
    else
      render json: {waveform: WaveForm.make_json_from_params(params)}
    end
  rescue ::WaveForm::ValidationException
    render nothing: true, status: 400, content: 'Bad Request'
  rescue ::WaveForm::FileNotFoundException
    ask_storage_for_track
    head :not_found
  end

  def ask_storage_for_track
    file_name = File.join(params[:id].to_s.split ':') + '.mp3'
    Thread.new {RestClient.head File.join(::Settings.storage_url, file_name) rescue nil}
  end
  protected :ask_storage_for_track
end
