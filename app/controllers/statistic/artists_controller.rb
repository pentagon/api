class Statistic::ArtistsController < ApplicationController
  def listen
    respond_with Persistence::TrackStatistic.with_artist.mr_artist.sort(value: -1).to_a
  end

  def genre_chart
    respond_with Persistence::TrackStatistic.with_artist.charts(genre: params[:genre], mode: "artist")
  end

  def aggregated_listen_stat
    respond_with Persistence::TrackStatistic.where(artist_uri: params[:artist_uri]).mr_track(mode: "date").to_a
  end
end
