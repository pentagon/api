class Statistic::TracksController < ApplicationController
  def show
    respond_with params[:id] => Persistence::TrackStatistic.load_stat_from_params(params)
  end

  def genre_chart
    respond_with ::Persistence::TrackStatistic.charts(genre: params[:genre], source: 'unsigned', mode: 'track')
  end

  def create
    # this needs proxy_set_header CLIENT_IP $remote_addr; in nginx conf
    args = (params[:track] || params).merge geo_location
    args = args.reverse_merge user_ip: request.remote_ip
    stat = Persistence::TrackStatistic.create_or_update_from_params args
    respond_with stat, location: false
  end
end
