class Statistic::DashboardsController < ApplicationController
  CACHE_TIME = 1440.minutes

  def registrations
    model_scope = Persistence::User.scoped
    from = params[:from]
    to = params[:to]
    model_scope = model_scope.where(:'created_at'.gte => from.to_date) if from.present?
    model_scope = model_scope.where(:'created_at'.lte => to.to_date) if to.present?
    result = Rails.cache.fetch("registrations_#{from}_#{to}", expires_in: CACHE_TIME) do
      model_scope.count
    end
    render json: ({count: result})
  end

  def activities
    model_scopes = {
      hitmaker: Persistence::HitMakerReport.scoped,
      tmm_songs: Persistence::Track.scoped,
      tmm_plays: Persistence::TrackStatistic.scoped.where(:source_page => /#{Settings.hosts.musicmanager}/),
      radio: Persistence::TrackStatistic.scoped.where(:source_page => /#{Settings.hosts.radio}/)
    }

    model_scopes.each_pair do |key, model|
      model = Rails.cache.fetch("activities_#{key}_#{params[:from]}_#{params[:to]}", expires_in: CACHE_TIME) do
        result = model
        result = model.where(:'created_at'.gte => params[:from].to_date) if params[:from].present?
        result = result.where(:'created_at'.lte => params[:to].to_date) if params[:to].present?
        result.count
      end
      model_scopes[key] = model
    end
    render json: model_scopes
  end

  def purchases
    model_scopes = {}
    %w(bronze silver gold platinum).each do |type|
      model_scopes[:"hitmaker_#{type}"] = Persistence::PaymentOrder.scoped.where(
        item_type: 'hitmaker_reports_bundle',
        item_provider_item_id: type
      )
    end
    model_scopes.each_pair do |key, model|
      fetcher = Rails.cache.fetch("purchases_#{key}_#{params[:from]}_#{params[:to]}", expires_in: CACHE_TIME) do
        result = model
        result = model.where(:'created_at'.gte => params[:from].to_date) if params[:from].present?
        result = result.where(:'created_at'.lte => params[:to].to_date) if params[:to].present?
        (result.sum(:price_amount) * 0.01).round(2)
      end
      model_scopes[key] = fetcher
    end
    render json: model_scopes
  end

  def analytics
    if (params[:to] && params[:from]) and (params[:to].to_date < params[:from].to_date)
      respond_with {} and return
    end

    from = params[:from].to_date.strftime('%Y-%m-%d') if params[:from].present?
    to = params[:to].to_date.strftime('%Y-%m-%d') if params[:to].present?
    results = {}
    client = LazyGoogleAnalytics::Client.new()
    Settings.analytics.apps.each_pair do |app, id|
      results[app] = Rails.cache.fetch("analytics_#{app}_#{from}_#{to}", expires_in: CACHE_TIME) do
        client.parameters({'ids' => "ga:#{id}",
                           'start-date' =>  from || "2012-01-01",
                           'end-date' => to || Date.today.strftime('%Y-%m-%d'),
                           'dimensions' => "ga:day,ga:month",
                           'metrics' => "ga:visits,ga:users",
                           'sort' => "ga:month,ga:day" })
        client.results.data.totalsForAllResults.to_hash
      end
    end
    respond_with results
  end
end
