class Blog::InitsController < Blog::BaseController
  def show
    respond_with_interaction Blog::InitsLoadingInteraction, params
  end
end
