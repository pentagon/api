class Blog::BaseController < ApplicationController
  def respond_with_interaction interaction_class, *params
    interaction = interaction_class.new *params
    if interaction.responder
      render json: interaction and return
    else
      render json: {errors: ['Resource not found']}, status: :not_found and return
    end
  end
end
