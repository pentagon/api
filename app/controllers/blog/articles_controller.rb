class Blog::ArticlesController < ApplicationController

  def index
    respond_with news
  end

  def latest
    respond_with news.first
  end

  private

  def news
    @news ||= JSON.parse(RestClient.get(File::join(::Settings.blog_url, 'articles.json')))
  end
end
