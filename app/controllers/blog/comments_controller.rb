class Blog::CommentsController < Blog::BaseController
  before_filter :authenticate_user!, only: [:create]

  def index
    respond_with_interaction Blog::CommentsLoadingInteraction, params
  end

  def create
    respond_with_interaction Blog::CommentCreatingInteraction, user: current_user, comment_params: params[:comment]
  end
end
