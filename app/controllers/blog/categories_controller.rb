class Blog::CategoriesController < Blog::BaseController
  def index
    respond_with_interaction Blog::CategoriesLoadingInteraction, params
  end

  def show
    respond_with_interaction Blog::CategoryLoadingInteraction, params
  end
end
