class Blog::PostsController < Blog::BaseController
  def index
    respond_with_interaction Blog::PostsLoadingInteraction, params
  end

  def show
    respond_with_interaction Blog::PostLoadingInteraction, params
  end
end
