class ZendeskController < ApplicationController
  #include Zendesk::RemoteAuthHelper
  before_filter :authenticate_user!

  def new
    sign_into_zendesk(current_user)
    #redirect_to zendesk_remote_auth_url(current_user)
  end

  def destroy
    #redirect_to destroy_session_path(resource_name)
  end

  private

  def sign_into_zendesk(user)
    iat = Time.now.to_i
    jti = "#{iat}/#{rand(36**64).to_s(36)}"
    payload = JWT.encode({
      iat: iat,
      jti: jti,
      name: user.full_name,
      email: user.email,
      external_id: user.uri
      }, Settings.zendesk.shared_secret)

    redirect_to zendesk_sso_url(payload)
  end

  def zendesk_sso_url(payload)
    "https://#{Settings.zendesk.subdomain}.zendesk.com/access/jwt?jwt=#{payload}"
  end
end
