class Astroweb::TracksController < ApplicationController
  def load_by_uris
    respond_with_interaction Astroweb::TrackLoadingInteraction, params.slice(:track_uris)
  end
end
