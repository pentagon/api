class Astroweb::SubscriptionsController < ApplicationController
  before_filter :authenticate_user!, except: [:subscribe_confirm, :result_success, :result_error,
    :result_cancel]

  def cancel
    respond_with_interaction Astroweb::SubscriptionCancellingInteraction,
      {subscription_id: params[:id]}
  rescue InteractionErrors::NotFound
    head :not_found
  rescue InteractionErrors::Forbidden
    head :forbidden
  rescue InteractionErrors::UnprocessableEntity
    head :unprocessable_entity
  end

  def subscribe
    respond_with_interaction Astroweb::SubscribeInitiatingInteraction, params.slice(:id)
  rescue InteractionErrors::RedirectingError => e
    redirect_to e.redirect_url
  rescue InteractionErrors::InvalidHeaders
    render json: {result: {status: 'invalid_headers'}}, status: :unprocessable_entity
  rescue InteractionErrors::WrongArgument => e
    render json: {error: e.message}, status: :unprocessable_entity
  end

  def subscribe_confirm
    respond_with_interaction Astroweb::SubscribeConfirmingInteraction,
      params.slice(:id, :token, :th_token, :agent, :department_code, :success_url, :error_url,
        :cancel_url, :user_country)
  rescue InteractionErrors::RedirectingError => e
    redirect_to e.redirect_url
  rescue InteractionErrors::WrongArgument => e
    render json: {error: e.message}, status: :unprocessable_entity
  end

  def result_success
    render json: {result: {status: 'success'}}
  end

  def result_error
    render json: {result: {status: 'error'}}
  end

  def result_cancel
    render json: {result: {status: 'cancel'}}
  end
end
