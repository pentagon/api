class Astroweb::ForecastsController < ApplicationController
  def index
    respond_with_interaction Astroweb::ForecastLoadingInteraction, params.slice(:id)
  rescue InteractionErrors::WrongArgument => e
    render json: {message: e.message}, status: :unprocessable_entity
  end
end
