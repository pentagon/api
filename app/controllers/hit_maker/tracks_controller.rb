class HitMaker::TracksController < AuthorizationsController
  include HitMaker::Serializers
  before_filter :authenticate_user!, except: [:show]

  def create
    track_interaction = HitMaker::TrackCreatingInteraction.new user: current_user, coutry: user_country, track_params: params
    respond_with serialize_track(track_interaction.track), location: nil
  end

  def show
    track = Persistence::Track.find_by uri: params[:id]
    track_json = { track: serialize_track(track) }
    respond_with track_json, location: nil
  end
end
