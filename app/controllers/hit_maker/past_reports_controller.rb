class HitMaker::PastReportsController < ApplicationController
  def show
    report = Persistence::Report.find_by(uri: params[:id])
    respond_with report, root: false, serializer: Hitmaker::ReportSerializer
  end
end
