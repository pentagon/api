class HitMaker::UsersController < UsersController
  def show
    respond_with_interaction HitMaker::UserLoadingInteraction, params.slice(:id)
  end
end
