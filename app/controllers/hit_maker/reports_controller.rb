class HitMaker::ReportsController < ApplicationController
  include HitMaker::Serializers

  before_filter :authenticate_user!, except: [:show]
  before_filter :find_resource, only: [:update, :destroy]

  def create
    reports = HitMaker::ReportCreatingInteraction.new user: current_user, country: user_country, request: params[:request]
    respond_with reports, location: false
  rescue InteractionErrors::InsufficientReportsAvailableError
    respond_with({message: "Shit!!! You want it too much!"}, status: :unprocessable_entity, location: false)
  end

  # Todo: move to interaction
  def show
    report = Persistence::HitMakerReport.find_by uri: params[:id]
    # report = nil if report && report.private? && !report.user_uri.eql?(current_user.try(:uri))
    respond_with({ report: serialize_report(report) }, status: (report.nil? ? :not_found : :ok))
  end

  def update
    @report.is_private = params[:report][:is_private]
    @report.is_new = params[:report][:is_newest]
    respond_with @report.save do |format|
      format.json { render json: {report: serialize_report(@report)}, location: false }
    end
  end

  def destroy
    head @report.destroy ? :ok : :unprocessable_entity
  end

  def find_resource
    @report = current_user.hit_maker_reports.find_by uri: params[:id]
  end
  private :find_resource
end
