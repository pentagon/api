class Admin::SubscriptionsController < Admin::BaseController

  before_filter :find_model_by_id, only: [:show, :destroy]

  def index
    subscriptions = if params[:ids]
      Persistence::Subscription.in(uri: params[:ids])
    else
      page = params[:page] || 1
      per_page = params[:per_page] || 20
      unless params[:is_internal_trial].nil?
        params[:is_internal_trial] = ['1', 1, 'true', true].include?(params[:is_internal_trial])
      end
      Persistence::Subscription.load_from_params(params).page(page).per(per_page)
    end
    respond_with Admin::SubscriptionSerializer.new(subscriptions, meta: {
      total_count: subscriptions.total_count,
    })
  end

  def show
    respond_with Admin::SubscriptionSerializer.new(@model)
  end

  def create
    allowed_params = [:user_id, :type, :country]
    attributes = params[:subscription].slice(*allowed_params).merge({
      agent: "ADMIN",
      custom_options: {
        bonus_track_requests: 0,
        labeled_tracks_limit: nil,
        track_requests: 0,
      },
      is_dummy: true,
      status: :active
    })
    attributes[:user_uri] = attributes[:user_id]
    attributes[:country] = attributes[:country].downcase if attributes[:country]
    @model = Persistence::Subscription.new(attributes)
    if @model.save
      render json: Admin::SubscriptionSerializer.new(@model)
    else
      respond_with @model
    end
  end

  def destroy
    if @model.is_dummy
      respond_with @model.destroy
    else
      head :forbidden
    end
  end

  def csv
    unless params[:is_internal_trial].nil?
      params[:is_internal_trial] = ['1', 1, 'true', true].include?(params[:is_internal_trial])
    end
    send_data ::Admin::SubscriptionCSVInteraction.new(params).to_csv, filename: "subscriptions.csv"
  end

private

  def find_model_by_id
    @model ||= Persistence::Subscription.find_by(uri: params[:id])
  end

end
