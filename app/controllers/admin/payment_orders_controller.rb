class Admin::PaymentOrdersController < Admin::BaseController

  def index
    page = params[:page] || 1
    per_page = params[:per_page] || 20
    criteria = ::Persistence::PaymentOrder.load_from_params params
    paged_documents = criteria.page(page).per(per_page)
    respond_with Admin::PaymentOrderSerializer.new(paged_documents, meta: {
      total_count: paged_documents.total_count,
      aggregated_price: ::Persistence::PaymentOrder.aggregate_price(criteria)
    })
  end

  def show
    respond_with Admin::PaymentOrderSerializer.new(payment_order)
  end

  def refresh_status
    payment_order.update_attributes payment_provider_order_info: payment_order.fetch_payment_provider_transaction_details
    payment_order.refresh_status
    render json: Admin::PaymentOrderSerializer.new(payment_order)
  end

  def additional_info
    result = {}
    payment_details = payment_order.payment_provider_order_info
    if payment_details.blank?
      payment_details = payment_order.fetch_payment_provider_transaction_details
      payment_order.update_attributes payment_provider_order_info: payment_details if payment_details.present?
    end
    success = payment_details["success"]
    result[:payment_provider_info] = {success: success}
    if success
      payer_info = payment_details["PaymentTransactionDetails"]["PayerInfo"]
      payment_info = payment_details["PaymentTransactionDetails"]["PaymentInfo"]
      result[:payment_provider_info].merge!({
        customer: {
          email: payer_info["Payer"],
          name: {
            salutation:   payer_info["PayerName"]["Salutation"],
            first_name:   payer_info["PayerName"]["FirstName"],
            middle_name:  payer_info["PayerName"]["MiddleName"],
            last_name:    payer_info["PayerName"]["LastName"],
            suffix:       payer_info["PayerName"]["Suffix"],
          },
          address: {
            street1: payer_info["Address"]["Street1"],
            street2: payer_info["Address"]["Street2"],
            city: payer_info["Address"]["CityName"],
            state: payer_info["Address"]["StateOrProvince"],
            postal_code: payer_info["Address"]["PostalCode"],
            country: payer_info["Address"]["Country"]
          }
        },
        payment: {
          gross_amount: (payment_info["GrossAmount"].to_f * 100).to_i,
          fee_amount: (payment_info["FeeAmount"].to_f * 100).to_i
        }
      })
      result[:payment_provider_info][:payment][:net_amount] = result[:payment_provider_info][:payment][:gross_amount] - result[:payment_provider_info][:payment][:fee_amount]
    end

    if payment_order.item_type == "track"
      details = payment_order.item_provider_item_info
      if details.blank?
        details = Medianet::Base.get_track_info payment_order.item_provider_item_id, payment_order.purchase_country
        payment_order.update_attributes item_provider_item_info: details if details.present?
      end
      success = details["Success"]
      result[:item_provider_info] = {success: success}
      if success
        result[:item_provider_info].merge!({
          track_id: details["Track"]["MnetId"],
          track_title: details["Track"]["Title"],
          artist_id: details["Track"]["Artist"]["MnetId"],
          artist_name: details["Track"]["Artist"]["Name"],
          album_id: details["Track"]["Album"]["MnetId"],
          album_title: details["Track"]["Album"]["Title"],
          album_label: details["Track"]["Album"]["Label"],
          price: {
            currency: details["Track"]["PriceTag"]["Currency"],
            fixed: details["Track"]["PriceTag"]["IsSetPrice"]
          }
        })
        price = details["Track"]["PriceTag"]
        result[:item_provider_info][:price][:amount] = (price["Amount"] * 100).to_i if price["Amount"]
        result[:item_provider_info][:price][:wholesale_price] = (price["WholesalePrice"] * 100).to_i if price["WholesalePrice"]
        result[:item_provider_info][:price][:publishing_cost] = (price["PublishingCost"] * 100).to_i if price["PublishingCost"]
      end
      if payment_order.item_provider_response["Success"]
        result[:item_provider_info][:order_id] = payment_order.item_provider_response["Order"]["ID"]
      end
    end

    respond_with result
  end

  def csv
    send_data ::Admin::PaymentOrderCSVInteraction.new(params).to_csv, filename: "payments_report.csv"
  end

  private

  def payment_order
    options = {uri: params[:id]}
    @payment_order ||= Persistence::PaymentOrder.find_by options
    raise ::Mongoid::Errors::DocumentNotFound.new(Persistence::PaymentOrder, options) unless @payment_order
    @payment_order
  end

end
