class Admin::PlaylistsController < Admin::BaseController

  def index
    respond_with_interaction Admin::PlaylistsLoadingInteraction
  rescue WrongArgument => e
    render json: {error: e.error}, :status => :unprocessable_entity
  end

  def show
    respond_with_interaction Admin::PlaylistLoadingInteraction, playlist_id: params[:id]
  rescue InteractionErrors::NotFound
    head :not_found
  end

  def create
    respond_with_interaction Admin::PlaylistCreatingInteraction, user_playlist: params[:user_playlist],
      location: false
  rescue WrongArgument => e
    render json: {error: e.error}, :status => :unprocessable_entity
  end

  def update
    Admin::PlaylistUpdatingInteraction.new default_interaction_params.merge(playlist_id: params[:id], user_playlist: params[:user_playlist])
    head :no_content
  rescue WrongArgument => e
    render json: {error: e.error}, :status => :unprocessable_entity
  end

  def destroy
    Admin::PlaylistDestroyingInteraction.new default_interaction_params.merge(playlist_id: params[:id])
    head :ok
  rescue WrongArgument => e
    render json: {error: e.error}, :status => :unprocessable_entity
  end

  def csv
    filename = "#{params[:title]}.csv"
    send_data Admin::TrackLoadingInteraction.new(params).to_csv, filename: filename
  end

end
