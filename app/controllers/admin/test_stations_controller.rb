class Admin::TestStationsController < Admin::BaseController
  include Admin::Serializers

  before_filter :add_user_param
  before_filter :find_station_by_id, only: [:show, :track_ratings, :rate_track]

  def index
    respond_with TestRadio::RadioStationLoadingInteraction.new(params)
  end

  def show
    return head :not_found unless @station
    respond_with test_station: serialize_test_station(@station)
  end

  def track_ratings
    return head :not_found unless @station
    params[:station] = @station
    respond_with TestRadio::StationTrackRatingInteraction.new(params)
  end

  def rate_track
    return head :not_found unless @station
    params[:station] = @station
    interaction = TestRadio::StationTrackRatingInteraction.new(params)
    render json: interaction.rate(params[:track_uri], params[:rating])
  end

private

  def add_user_param
    params[:user] = current_user
  end

  def find_station_by_id
    @station = Persistence::TestRadioStation.find_by(uri: params[:id])
  end

end
