class Admin::ArtistsController < Admin::BaseController

  def index
    respond_with Admin::ArtistLoadingInteraction.new(params)
  end

  def show
    respond_with Admin::ArtistLoadingInteraction.new(params)
  rescue InteractionErrors::NotFound
    head :not_found
  end

end
