class Admin::BaseController < ApplicationController

  before_filter :authenticate_user!
  before_filter :require_admin

private

  def require_admin
    unless current_user && current_user.admin?
      message = 'You are not allowed to access this resource.'
      render_errors(message, status: :forbidden)
    end
  end

  def require_thirdparty
    unless current_user && (current_user.thirdparty? || current_user.admin?)
      message = 'You are not allowed to access this resource.'
      render_errors(message, status: :forbidden)
    end
  end

end
