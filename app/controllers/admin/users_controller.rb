class Admin::UsersController < Admin::BaseController

  skip_before_filter :require_admin, only: [:me]
  before_filter :find_by_id, only: [:show, :update, :destroy]
  before_filter :wrap_nested_params, only: [:update]

  def index
    page = params[:page] || 1
    per_page = params[:per_page] || 20
    if params[:ids].present?
      users = Persistence::User.in(uri: params[:ids])
      options = {}
    else
      users = Persistence::User.load_from_params(params).page(page).per(per_page)
      options = {meta: {total_count: users.total_count}}
    end
    respond_with Admin::UserSerializer.new(users, options)
  end

  def show
    respond_with Admin::UserSerializer.new(@user)
  end

  def me
    respond_with Admin::UserSerializer.new(current_user)
  end

  def update
    if params[:user].keys.include?('roles')
      @user.roles = (params[:user][:roles] || []).to_set
    end
    UserAccounting.new(@user).calculate!(params[:user][:available_credits]) 
    @user.update_attributes(params[:user])
    respond_with @user
  end

  def registrations
    total = 0
    from = params[:from].try(:to_date) || Date.today.beginning_of_month
    till = params[:till].try(:to_date) || Date.today
    all_sources = Persistence::User.where(:created_at.gte => from, :created_at.lte => till.end_of_day, internal_user: false).pluck(:source).uniq
    only = params[:only]
    if only.present?
      sources  = all_sources.select { |s| only.include? s }
    else
      sources = all_sources.reject { |s| s == "api" }
    end
    reg_hash = Persistence::User.collection.aggregate(
      {"$match" => {created_at: {"$gte" => from.to_time, "$lte" => till.to_time.end_of_day}, source: {"$in" => sources}, internal_user: false}},
      {"$group" => {_id: {year: {"$year" => "$created_at"}, day: {"$dayOfYear" => "$created_at"}, source: "$source"}, signed: {"$sum" => 1}}},
      {"$sort" => {"_id" => 1}})
    data = [["Date", *sources]]
    (from..till).map do |d|
      row = [d.to_s]
      sources.each do |s|
        if_not_found = -> { return ({"signed" => 0}) }
        signed = reg_hash.find(if_not_found) { |h| h["_id"] == {"year" => d.year, "day" => d.yday, "source" => s} }
        row << signed["signed"]
        total += signed["signed"]
      end
      data << row
    end
    respond_with ({all_sources: all_sources, sources: sources, data: data, total: total})
  end

private

  def find_by_id
    options = {uri: params[:id]}
    @user = Persistence::User.find_by(options)
    raise ::Mongoid::Errors::DocumentNotFound.new(Persistence::User, options) unless @user
    @user
  end

  def wrap_nested_params
    skip_fields = ["_id", "created_at", "updated_at"]
    nested_mapping = {
      contact_info: Persistence::UserData::ContactInfo.fields.keys.reject { |k| skip_fields.include?(k) },
      personal_info: Persistence::UserData::PersonalInfo.fields.keys.reject { |k| skip_fields.include?(k) }
    }
    nested_mapping.each do |level, attributes|
      attributes.each do |key|
        if params[:user][key].present?
          params[:user][level] ||= {}
          params[:user][level][key] = params[:user][key]
        end
      end
    end
  end

end
