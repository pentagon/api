class Admin::StationsController < Admin::BaseController

  include Admin::Serializers

  before_filter :find_model_by_id, only: [:show, :destroy, :next_track]

  def create
    allowed_params = [:is_free, :seed_track_ids, :country, :title]
    attributes = params[:station].slice(*allowed_params)
    attributes["seed_track_uris"] = attributes.delete("seed_track_ids")
    attributes["user_uri"] = current_user.uri
    track = Track.fetch(attributes["seed_track_uris"].first)
    attributes["title"] = "#{track.title}, by #{track.artist_name}"
    @model = Persistence::Station.new(attributes)
    if @model.save
      render json: {station: serialize_station(@model)}
    else
      respond_with @model
    end
  end

  def destroy
    respond_with @model.destroy
  end

  def index
    stations = if params[:ids]
      Persistence::Station.in(uri: params[:ids])
    else
      page = params[:page] || 1
      per_page = params[:per_page] || 20
      Persistence::Station.load_from_params(params).page(page).per(per_page)
    end
    respond_with stations: stations.map { |s| serialize_station(s) }
  end

  def next_track
    track_id = RadioEngine::StreamProvider.new(@model)
      .fetch_next_track(params[:track_id], params[:feedback])
    track = Track.fetch(track_id)
    if track
      respond_with track: serialize_track(track)
    else
      head :not_found
    end
  end

  def show
    respond_with station: serialize_station(@model)
  end

  def used_tracks
    ids = ::Persistence::RadioUsedTrack.where(station_uri: params[:id])
      .desc(:created_at).limit(20).pluck(:track_uri)
    tracks = Track.fetch(ids)
    respond_with tracks: tracks.map { |t| serialize_track(t) }
  end

  def track_feedbacks
    feedbacks = ::Persistence::RadioTrackReaction.where(station_uri: params[:id])
      .desc(:created_at).limit(20)
    render json: feedbacks
  end

private

  def find_model_by_id
    @model ||= Persistence::Station.find_by(uri: params[:id])
  end

end
