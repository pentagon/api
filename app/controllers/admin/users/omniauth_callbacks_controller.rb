class Admin::Users::OmniauthCallbacksController < ::Users::OmniauthCallbacksController

  private
  def serialize_user user, attributes = nil
    Admin::UserSerializer.new(users).as_json
  end
end
