class Admin::VideosController < Admin::BaseController
  def index
    respond_with_interaction Admin::VideosLoadingInteraction, params.slice(:ids)
  end
end
