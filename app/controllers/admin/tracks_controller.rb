class Admin::TracksController < Admin::BaseController

  skip_before_filter :require_admin
  before_filter :require_thirdparty

  def index
    if params[:test_data].present?
      respond_with Admin::TestDataTrackSearchingInteraction.new(params)
    elsif params[:ids].present? || params[:album_id].present?
      respond_with Admin::TrackLoadingInteraction.new(params)
    else
      respond_with Admin::TrackSearchingInteraction.new(params)
    end
  end

  def csv
    send_data Admin::TrackSearchingInteraction.new(params).to_csv, filename: "tracks.csv"
  end

  def page_csv
    filename = "#{params[:page]}_page.csv"
    send_data Admin::TrackLoadingInteraction.new(params).to_csv, filename: filename
  end

  def show
    respond_with Admin::TrackLoadingInteraction.new(params)
  rescue InteractionErrors::NotFound
    head :not_found
  end

end
