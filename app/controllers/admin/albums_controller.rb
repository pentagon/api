class Admin::AlbumsController < Admin::BaseController

  def index
    respond_with Admin::AlbumLoadingInteraction.new(params)
  end

  def show
    respond_with Admin::AlbumLoadingInteraction.new(params)
  rescue InteractionErrors::NotFound
    head :not_found
  end

end
