class Admin::PaymentPriceModifiersController < Admin::BaseController

  before_filter :set_pagination_params, only: [:index]
  before_filter :format_params, only: [:create, :update]
  before_filter :get_model, only: [:show, :destroy, :update]

  def index
    criteria = Persistence::PaymentPriceModifier.load_from_params(params)
    paged_documents = criteria.page(@page).per(@per_page)
    respond_with Admin::PaymentPriceModifierSerializer.new(paged_documents, meta: {
      total_count: paged_documents.total_count
    })
  end

  def show
    respond_with Admin::PaymentPriceModifierSerializer.new(@model)
  end

  def destroy
    respond_with @model.destroy
  end

  def create
    @model = Persistence::PaymentPriceModifier.new(params[:payment_price_modifier])
    if @model.save
      render json: Admin::PaymentPriceModifierSerializer.new(@model)
    else
      respond_with @model
    end
  end

  def update
    @model.update_attributes(params[:payment_price_modifier])
    respond_with @model
  end

private

  def set_pagination_params
    @page = params[:page] || 1
    @per_page = params[:per_page] || 20
  end

  def get_model
    options = {code: params[:id]}
    @model ||= Persistence::PaymentPriceModifier.find_by options
    raise ::Mongoid::Errors::DocumentNotFound.new(Persistence::PaymentPriceModifier, options) unless @model
    @model
  end

  def format_params
    if params[:payment_price_modifier]
      params[:payment_price_modifier][:digital_goods_type] = params[:payment_price_modifier][:item_type]
      params[:payment_price_modifier][:digital_goods_id] = params[:payment_price_modifier][:item_id]
    end
  end

end
