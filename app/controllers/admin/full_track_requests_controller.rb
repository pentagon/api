class Admin::FullTrackRequestsController < Admin::BaseController

  before_filter :filter_params

  def index
    per_page = params[:per_page].presence || 20
    requests = ::Persistence::FullTrackRequest.load_from_params(params).page(params[:page]).per(params[:per_page])
    respond_with Admin::FullTrackRequestSerializer.new(requests, meta: {
      total_count: requests.respond_to?(:total_count) ? requests.total_count : 0
    })
  end

  def csv
    send_data ::Admin::FullTrackRequestsCSVInteraction.new(params).to_csv(params[:csv_type]), filename: "#{params[:csv_type]}.csv"
  rescue ArgumentError
    head :unprocessable_entity
  end

  def show
    respond_with Admin::FullTrackRequestSerializer.new(full_track_request)
  end

  def countries_list
    respond_with ::Persistence::FullTrackRequest.get_countries
  end

private

  def filter_params
    params[:type] = case params[:type]
      when 'labeled', 'unsigned' then params[:type]
      when '1' then 'labeled'
      when '2' then 'unsigned'
      else nil
    end
    params[:success] = case params[:success]
      when '1' then true
      when '2' then false
      else nil
    end
    params[:enviroment] = nil unless ['qa', 'production'].include?(params[:enviroment])
  end

  def full_track_request
    options = {uri: params[:id]}
    @full_track_request ||= Persistence::FullTrackRequest.find_by(options)
    raise ::Mongoid::Errors::DocumentNotFound.new(Persistence::FullTrackRequest, options) unless @full_track_request
    @full_track_request
  end

end
