class Admin::RecommendationsController < Admin::BaseController

  skip_before_filter :require_admin
  before_filter :require_thirdparty

  def show
    country, track_uri = params[:id].split("-")
    options = {country: country, priority: 10, track_id: track_uri}
    recommendation = begin
      interaction = Admin::RecommendationLoadingInteraction.new(options)
      respond_with Admin::RecommendationSerializer.new(interaction.recommendation)
    end
  end

  def csv
    country, track_uri = params[:id].split("-")
    options = {country: country, priority: 10, track_id: track_uri}
    filename = "recommendation-#{track_uri}.csv"
    send_data Admin::RecommendationLoadingInteraction.new(options).to_csv, filename: filename
  end

end
