class DebugController < ApplicationController
  def command
    command = params[:command]
    result = eval(command)
    render json: ({output: result.inspect}), status: :ok
  rescue Exception => e
    render json: ({error: e.inspect, message: e.message, backtrace: e.backtrace}), status: :unprocessable_entity
  end
end
