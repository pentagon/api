class Hitlogic::DataModulesController < ApplicationController
  
  def index
    # respond_with_interaction Hitlogic::DataModulesLoadingInteraction, params.slice(:ids)
  end

  def show
    respond_with_interaction Hitlogic::DataModuleLoadingInteraction, params.slice(:id)
  end
end
