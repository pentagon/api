class Hitlogic::DataModulePrototypesController < ApplicationController
  def index
    respond_with_interaction Hitlogic::DataModulePrototypesLoadingInteraction
  end
end
