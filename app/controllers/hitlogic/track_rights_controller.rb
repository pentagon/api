class Hitlogic::TrackRightsController < ApplicationController
  def show
    respond_with_interaction Hitlogic::TrackRightsLoadingInteraction, params
  end
end
