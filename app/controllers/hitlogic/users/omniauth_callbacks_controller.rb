class Hitlogic::Users::OmniauthCallbacksController < ::Users::OmniauthCallbacksController
  include Hitlogic::Serializers

  private 
  def serialize_user user, attributes = nil
    {user: super}
  end  
end
