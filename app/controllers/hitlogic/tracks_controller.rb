class Hitlogic::TracksController < ApplicationController
  before_filter :authenticate_user!, only: [:create, :index, :destroy]

  def index
    respond_with_interaction Hitlogic::TracksLoadingInteraction, user: current_user
  end

  def update
    respond_with_interaction Hitlogic::TrackUpdatingInteraction, params
  end

  def create
    respond_with_interaction Hitlogic::TrackCreatingInteraction, params
  end

  def show
    respond_with_interaction Hitlogic::TrackLoadingInteraction, id: params[:id]
  end

  def destroy
    respond_with_interaction Hitlogic::TrackDestroyingInteraction, id: params[:id]
  end
end
