class Hitlogic::UsersController < ApplicationController
  before_filter :authenticate_user!, only: [:me]

  def show
    respond_with_interaction Hitlogic::UserLoadingInteraction, params.slice(:id)
  end

  def reports
    u = if params[:email]
      Persistence::User.find_by email: params[:email]
    elsif params[:uri]
      Persistence::User.find_by uri: params[:uri]
    else
        render json: {error: 'please provide uri or email of user to get reports'} and return
    end
    render json: {error: 'User was not found'} and return unless u
    reports = u.hit_maker_reports.where(is_processed: true).page(params[:page] || 1)
    render json: {
      meta: {
        current_page: params[:page] || 1,
        total_pages: reports.total_pages,
        total_count: reports.total_count
      },
      reports: reports
    }
  end
end
