class Hitlogic::DataSetsController < ApplicationController
  def show
    respond_with_interaction Hitlogic::DataSetLoadingInteraction, params
  end

  def create
    respond_with_interaction Hitlogic::DataSetCreatingInteraction, data_set: params[:data_set]
  rescue InteractionErrors::InsufficientReportsAvailableError
    render json: {errors:'The number of credits has exceeded'}, status: :unprocessable_entity
  end

  def update
    respond_with_interaction Hitlogic::DataSetUpdatingInteraction, data_set: params[:data_set], id: params[:id]
  rescue InteractionErrors::InsufficientReportsAvailableError
    render json: {errors:'The number of credits has exceeded'}, status: :unprocessable_entity
  end

  def tags
    ds = Persistence::Hitlogic::DataSet.find params[:id]
    @track = ds.track
    @report_url = "#{Settings.hitlogic_site}/reports/#{ds.id}"
    raise ActiveHash::RecordNotFound unless @track
  rescue
    render text: ''
  end
end
