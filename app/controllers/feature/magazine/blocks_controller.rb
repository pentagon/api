class Feature::Magazine::BlocksController < ApplicationController
  def index
    respond_with Featured::BlocksLoadingInteraction.new params.slice(:type, :country).merge(user: current_user)
  rescue Featured::Errors::CountryCodeMissing
    head :bad_request
  end

  def available_blocks
    blocks = Persistence::MagazineBlock.all.to_a
    respond_with blocks
  end

  def create
    Featured::BlockCreatingInteraction.new block_params
    head :created
  end

  def update
    Featured::BlockUpdatingInteraction.new id: params[:id], block: block_params
    head :ok
  rescue Featured::Errors::NotFound
    head :not_found
  end

  def destroy
    Featured::BlockDeletingInteraction.new params[:id]
    head :ok
  rescue Featured::Errors::NotFound
    head :not_found
  end

  private

  def block_params
    attribs = params.slice(:title, :type, :description, :button_url, :button_text,
      :method_type, :randomized, :fetch_realtime)
    [:items_gb, :items_us, :items_ca].each { |i| attribs[i] = params[i].split(' ') }
    attribs
  end
end
