class Sync::RecommendationsController < ApplicationController
  def show
    respond_with Sync::HssLoadingInteraction.new track_id: params[:id]
  rescue Exception => e
    respond_with({status: 'error', message: e.message})
  end
end
