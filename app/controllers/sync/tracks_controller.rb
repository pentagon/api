class Sync::TracksController < ApplicationController
  def index
    respond_with Sync::TracksLoadingInteraction.new ids: params[:ids]
  end

  def show
    respond_with Sync::TrackLoadingInteraction.new track_id: params[:id]
  rescue Sync::Errors::NotFound
    head :not_found
  end

  def create
    respond_with Sync::TrackCreatingInteraction.new(params[:track]), location: false
  end

  def update
    respond_with Sync::TrackUpdatingInteraction.new track_id: params[:id], track: params[:track]
  rescue Sync::Errors::NotFound
    head :not_found
  end
end
