class Sync::ArtistsController < ApplicationController
  def show
    respond_with Sync::ArtistLoadingInteraction.new artist_id: params[:id]
  rescue Sync::Errors::NotFound
    head :not_found
  end

  def by_user
    respond_with Sync::ArtistsLoadingInteraction.new user_uri: params[:id]
  end

  def create
    respond_with Sync::ArtistCreatingInteraction.new(params), location: false
  end

  def update
    respond_with Sync::ArtistUpdatingInteraction.new artist_id: params[:id], artist: params[:artist]
  rescue Sync::Errors::NotFound
    head :not_found
  end
end
