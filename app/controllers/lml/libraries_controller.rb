class Lml::LibrariesController < AuthorizedController
  def index
    respond_with_interaction Lml::LibrariesLoadingInteraction
  end

  def create
    respond_with_interaction Lml::LibraryCreatingInteraction, params.slice(:library_hash, :port, :name)
  rescue InteractionErrors::WrongArgument => e
    render json: {message: e.message}, status: :unprocessable_entity
  end
end
