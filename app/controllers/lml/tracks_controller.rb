class Lml::TracksController < AuthorizedController
  def index
    respond_with_interaction Lml::TracksLoadingInteraction, params.slice(:library_uri)
  end

  def create
    respond_with_interaction Lml::TracksCreatingInteraction, params.slice(:tracks)
  rescue InteractionErrors::WrongArgument => e
    render json: {message: e.message}, status: :unprocessable_entity
  end

  def show
    respond_with_interaction Lml::TracksLoadingInteraction, params.slice(:library_uri)
  rescue InteractionErrors::WrongArgument => e
    render json: {message: e.message}, status: :unprocessable_entity
  end

  def destroy
    respond_with_interaction Lml::TracksDeletingInteraction, params.slice(:tracks, :library_uri)
  rescue InteractionErrors::WrongArgument => e
    render json: {message: e.message}, status: :unprocessable_entity
  end

  def send_analysis
    respond_with_interaction Lml::TracksSendAnalysisInteraction, params.slice(:tracks)
  end

  def get_analysis
    respond_with_interaction Lml::TracksGetAnalysisInteraction, params.slice(:library_uri)
  end
end
