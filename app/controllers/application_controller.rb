class ApplicationController < ActionController::API
  include ActionController::MimeResponds
  before_filter :user_country #init user ip
  ALLOWED_HEADERS = %w(DNT X-Mx-ReqToken Keep-Alive User-Agent X-Requested-With If-Modified-Since Cache-Control
    Content-Type X-Authentication-Token Accept Origin X-Radio-Agent).join(',')

  respond_to :json

  after_filter :add_custom_headers, if: "signed_in?"
  before_filter :set_access_control_headers
  before_filter :set_replied_host_header
  before_filter :set_current_user_by_token
  prepend_before_filter :set_oauth2_client
  prepend_before_filter :set_current_user_by_bearer

  def persistence_users_url(params={})
    users_url params
  end

  # This dirty hack for devise (?)
  def flash
    {}
  end

  def set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Request-Method'] = '*'
    headers['Access-Control-Allow-Credentials'] = 'true'
    headers['Access-Control-Allow-Headers'] = ALLOWED_HEADERS
  end

  def set_replied_host_header
    headers['X-Replied-Host'] = Socket.gethostname.split(".").first
  end

  def get_tracking_params
    tracking_param = params[:tracking] || (params[:user][:tracking] if params[:user].present?)
    if tracking_param.present?
      tracking = Base64.decode64(tracking_param).split(';')
      {
        partner_tracking_date: Time.at(tracking[0].try(:to_i)),
        partner_tracking_page: tracking[1],
        partner: tracking[2]
      }
    end or {}
  end

  def auth_token
    @auth_token ||= ActionController::HttpAuthentication::Token.token_and_options(request)
  end

  def client_id
    params[:client_id] || request.headers['X-Client-ID']
  end
  helper_method :auth_token, :client_id

  attr_reader :resource, :collection
  helper_method :resource, :collection

  def geo_location
    MaxmindGeoip.get_geolocation_from_ip remote_user_ip
  end

  def user_country
    # This is a black magic, accessig geo_location hash via key name returns nil
    @user_country ||= begin
      user_country = geo_location.fetch(geo_location.keys.first).presence.to_s.downcase
      unless %w(us gb uk).include?(user_country)
        @remote_ip = '205.251.242.54' #amazon.com
        user_country = 'us'
      end
      user_country
    end
  rescue
    nil
  end

  def remote_user_ip
    @remote_ip ||= params[:ip] || params[:remote_ip] || params[:ip_address] || request.remote_ip || '127.0.0.1'
  end

  def accounts_url(resource=nil)
    Settings.accounts_url
  end
  helper_method :accounts_url

  alias_method :after_sign_in_path_for, :accounts_url
  alias_method :after_sign_up_path_for, :accounts_url
  alias_method :after_inactive_sign_up_path_for, :accounts_url
  alias_method :after_resetting_password_path_for, :accounts_url

  def after_sending_reset_password_instructions_path_for(resource)
    accounts_url + '#restored_password'
  end

  def after_sign_out_path_for(resource_name)
    params[:return_to] || accounts_url
  end

  def respond_with_interaction interaction_class, *interaction_params
    render json: interaction_class.new(default_interaction_params.merge(Hash[*interaction_params]))
  end

  def redirect_with_interaction interaction_class, *interaction_params
    res = interaction_class.new(default_interaction_params.merge(Hash[*interaction_params])).as_json
    redirect_to res[:redirect_url]
  end

  protected

  def default_interaction_params
    {
      user: current_user,
      country: user_country,
      headers: request.headers,
      user_ip: remote_user_ip,
      request_protocol: request.protocol
    }.with_indifferent_access
  end

  def add_custom_headers user = nil
    header_user = user || current_user
    headers['X-Authentication-Token'] = header_user.try(:authentication_token)
    headers['X-User-URI'] = header_user.try(:oauth_uid)
    headers['X-Client-ID'] = client_id.presence
  end

  def disable_registration
    flash[:error] = t('messages.errors.registration_disabled')
    redirect_to root_path
  end

  def set_pagination_headers headers
    %w(total offset limit).each do |option|
      response.headers["X-pagination_#{option}"] = headers[option.to_sym].to_s
    end
  end

  def set_pagination_headers_for mongoid_view
    headers = {
      total: mongoid_view.length,
      offset: mongoid_view.options[:skip],
      limit: mongoid_view.options[:limit]
    }
    set_pagination_headers headers
  end

  def scoped_params(scope)
    if scope.present?
      _scope = scope.dup
      _params = params.dup
      until _scope.empty?
        _params = params[_scope.shift]
      end
      _params
    else
      params
    end
  end

  # TODO: finish
  def required_params(*args)
    options = args.extract_options!
    scope = Array.wrap(options.delete(:scope))
    _scoped_params = scoped_params(scope)
    blank_params = args.select do |name|
      _scoped_params[name].blank?
    end
    if blank_params.empty?
      yield if block_given?
    else
      render_errors *blank_params.map {|p| '%s paramater is required' % (scope+[p]).join('.') }
    end
  end

  def render_errors(*args)
    options = args.extract_options!
    status = options[:status] || :bad_request
    render json: { errors: args }, status: status
  end

  def admin_required!
    render_errors 'You must be an admin to access this resource', status: :forbidden unless current_user.admin?
  end

  def authorize_by_client!
    client = (::Client.find_by(identifier: params[:client_id])) || (Persistence::Client.find_by(identifier: params[:client_id]))
    return (render_errors "invalid client_id"), status: :unauthorized if client.nil?
    if client.secret != params[:client_secret]
      render_errors "client secrets not matches", status: :unauthorized
    end
  end

  # Workaround for PayPal and TH-Accounts using the same token parameter.
  def fix_ambigious_tokens
    if params[:th_token]
      params[:paypal_token] ||= params[:token]
      params[:token] = params[:th_token]
    end
  end

  def set_current_user_by_token
    current_user
  end

  def set_current_user_by_bearer
    req = Rack::OAuth2::Server::Resource::Bearer::Request.new(env)
    if req.oauth2?
      req.setup!
      token = AccessToken.find_by(token: req.access_token)
      env['oauth2.client'] = token.client if token
      user = token ? token.user : nil
      request.env['current_user'] = user
    end
  end

  def set_oauth2_client
    if params[:client_id] and params[:client_secret]
      client_id, client_secret = [params[:client_id], params[:client_secret]]
      client = Client.find_by(identifier: client_id)
      if client && client.secret == client_secret
        request.env['oauth2.client'] = client
        if code = client.authorization_codes.find_by(token: params[:code])
          request.env['current_user'] = code.user
        end
        if refresh_token = client.refresh_tokens.find_by(token: params[:refresh_token])
          request.env['oauth2.refresh_token'] = refresh_token
          request.env['current_user'] = refresh_token.user
        end
      end
    end
  end

  def current_user
    return request.env['current_user'] if request.env['current_user'].present?
    return nil unless get_token.present?
    request.env['current_user'] = Rails.cache.fetch("user_#{get_token}", expires_in: 30.minutes) do
      Persistence::User.find_by(authentication_token: get_token)
    end
  end
  helper_method :current_user

  def signed_in?
    current_user.present?
  end
  helper_method :signed_in?

  def get_token
    @token ||= (params[:token] || get_token_from_header || get_token_from_x_header)
    @token = @token.chomp('"').reverse.chomp('"').reverse unless @token.blank?
    @token
  end

  def get_token_from_header
    authorization_header = request.headers['Authorization'] || ''
    scheme, scheme_params = authorization_header.split(' ')
    if scheme == 'Token' and scheme_params.present?
      scheme_params.split(',').each do |scheme_param|
        key, value = scheme_param.split('=')
        return value if key == 'token'
      end
    end
  end

  def get_token_from_x_header
    request.headers['X-Authentication-Token'] || ''
  end

  def authenticate_user!
    if not signed_in?
      render json: ({errors: 'You have to be signed in to do this'}), status: :unauthorized and return
    end
  end

  def require_no_authentication
    if signed_in?
      render json: ({errors: 'You have to be signed out to do this'}), status: :unprocessable_entity and return
    end
  end

  def sign_in user
    request.env['current_user'] = user
  end
end
