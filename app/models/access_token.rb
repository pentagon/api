class AccessToken

  DEFAULT_LIFETIME = 15.minutes

  include Persistence::Document
  store_in collection: 'oauth2_access_tokens'.to_sym

  belongs_to :client, class_name: 'Persistence::Client'
  belongs_to :user, class_name: 'Persistence::User'
  belongs_to :refresh_token, class_name: 'RefreshToken'

  field :token
  field :expires_at, type: Time

  index({token: 1}, {unique: true, background: true})
  index({expires_at: 1}, {background: true})

  validates :expires_at, presence: true
  validates :client, presence: true
  validates :token, presence: true, uniqueness: true
  before_validation :restrict_expires_at, on: :create, if: :refresh_token

  default_scope where(:expires_at.gte => Time.now.utc)

  attr_accessible :refresh_token, :user, :client

  after_initialize :init_token, on: :create, unless: :token?
  after_initialize :init_expires_at, on: :create, unless: :expires_at?

  def expires_in
    (expires_at - Time.now.utc).to_i
  end

  def expired!
    self.expires_at = Time.now.utc
    self.save!
  end

  def token_response
    response = {
      access_token: token,
      token_type: 'bearer',
      expires_in: expires_in
    }
    response[:refresh_token] = refresh_token.token if refresh_token
    response
  end

  private

  def restrict_expires_at
    self.expires_at = [self.expires_at, refresh_token.expires_at].compact.min
  end

  def init_token
    self.token = SecureRandom.hex
  end

  def init_expires_at
    self.expires_at = DEFAULT_LIFETIME.from_now
  end
end
