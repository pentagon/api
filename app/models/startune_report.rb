class StartuneReport

  behaves_like :digital_goods

  private_class_method :new

  class << self
    def templates
      params = {
        "actiontask" => "GetProductPrice",
        "api-key" => Settings.startune.report_api_key,
      }
      result = RestClient.get(Settings.startune.report_api_url, params: params)
      result = JSON.parse(result)
      result['products'].map do |item|
        {
          product_id: item['ProductId'],
          product_name: item['ProductName'],
          price: item['Price'],
          currency_name: item['currency_name'],
          currency_symbol: item['CurrencySymbol'],
          currency_code: item['CurrencyCode'],
          lang_code: item['LangCode']
        }
      end
    rescue Exception => e
      binding.pry
      raise Persistence::PaymentOrder::ItemProviderError
    end

    def digital_goods_find_by_id(id, options = {})
      new templates.detect { |t| t[:product_id] == id }
    end

    def digital_goods_find_by_ids(types_array, options = {})
      templates.select { |t| t[:product_id] == id }.map { |t| new t }
    end
  end

  def initialize(data)
    @data = data
  end

  def digital_goods_item_provider_item_info(options = {})
    @data.clone
  end

  def digital_goods_name
    @data[:product_name]
  end

  def digital_goods_item_id
    @data[:product_id]
  end

  def digital_goods_base_price(options = {})
    {amount: (@data[:price].to_f * 100).to_i, currency: @data[:currency_code]}
  end

  def digital_goods_reporting_description
    "Product ID: #{digital_goods_item_id} (#{digital_goods_name})"
  end

  def digital_goods_item_provider_name
    "STARTUNE"
  end

  def digital_goods_deliver(payment_order)
    errors = []

    begin
      api_result = RestClient.post(Settings.startune.report_api_url, {
        "actiontask" => "PostMiniReportReport",
        "api-key" => Settings.startune.report_api_key,
        "ProductId" => payment_order.item_provider_item_id,
        "hdnId" => payment_order.custom_options["hdn_id"],
        "txtFirstName" => payment_order.custom_options["first_name"],
        "txtLastName" => payment_order.custom_options["last_name"],
        "txtUserEmail" => payment_order.custom_options["user_email"],
        "ddDay" => payment_order.custom_options["dd_day"],
        "ddMonth" => payment_order.custom_options["dd_month"],
        "ddYear" => payment_order.custom_options["dd_year"],
        "transactionid" => payment_order.payment_provider_payment_id,
      })
      api_result = JSON.parse(api_result)
    rescue Exception => e
      errors << e.message
    end

    if api_result["code"].to_i == 0
      payment_order.update_attributes(item_provider_responce: api_result)
      report = Persistence::StartuneReport.create({
        order_number: api_result["orderno"],
        report_type: payment_order.item_provider_item_id,
        transaction_id: payment_order.payment_provider_payment_id,
        user_uri: payment_order.user_uri,
      })
      erorrs = report.errors.full_messages
    else
      errors << api_result["message"]
    end
    {success: errors.blank?, errors: erorrs}
  end

end
