class Statistic::MailingStatistic
  def initialize(queue, options={})
    @queue = queue
    @since = options[:since]
    @til = options[:til]
    @old_users = !!options[:old_users]
  end

  def scoped
    return @scoped if @scoped
    @scoped = Persistence::Activity.scoped
    @scoped = @scoped.where(:trackable_uri => /rrmusic|uplaya/) if @old_users
    @scoped = @scoped.where(:created_at.gt => Time.parse(@since)) if @since.present?
    @scoped = @scoped.where(:created_at.lt => Time.parse(@til)) if @til.present?
    @scoped
  end

  def opened_legacy
    @opened_legacy ||= scoped.where(key: 'mail_opened')
  end

  def opened
    @opened ||= scoped.where(key: 'newsletter', 'parameters.event'=>'opened')
  end

  def opened_count
    return @opened_count if @opened_count
    @opened_count = opened_legacy.distinct(:trackable_uri).count + opened.distinct(:trackable_uri).count
  end

  def visited_legacy
    @visited_legacy ||= scoped.where(key: 'mail_visited')
  end

  def visited
    @visited ||= scoped.where(key: 'newsletter', 'parameters.event'=>'visited')
  end

  def visited_count
    return @visited_count if @visited_count
    @visited_count = visited_legacy.distinct(:trackable_uri).count + visited.distinct(:trackable_uri).count
  end

  def delivered
    @delivered ||= scoped.where(key: 'newsletter', 'parameters.event'=>'delivered')
  end

  def delivered_count
    @delivered_count ||= delivered.distinct(:trackable_uri).count
  end

  def reduce_to_hash(criteria)
    criteria.inject({}) do |hash, item|
      hash[item['_id']] = item['value']
      hash
    end
  end

  def sort_desc(array_of_hashes)
    array_of_hashes.sort do |i, j|
      j.values.first <=> i.values.first
    end
  end

  def visited_urls
    map = <<-JS
      function(){
        if(this.parameters.url) {
          var url = this.parameters.url.replace(/\\/$/, \"\");
          url = url.replace(/https?:\\/\\//, \"\");
          emit(url, 1)
        }
      }
    JS
    reduce = <<-JS
      function(key, values){
        var sum = 0;
        for (var i=0; i<values.length; i++) {
          sum += values[i];
        }
        return sum;
      }
    JS
    visited_legacy_urls = reduce_to_hash visited_legacy.map_reduce(map, reduce).out(inline: true)
    visited_urls = reduce_to_hash visited.map_reduce(map, reduce).out(inline: true)
    urls = (visited_legacy_urls.keys.to_set | visited_urls.keys.to_set).flatten
    data = urls.map do |url|
      {url => visited_legacy_urls[url].to_i + visited_urls[url].to_i}
    end
    sort_desc data
  end

  def as_json(options={})
    {
      delivered: delivered_count,
      opened: opened_count,
      visited: visited_count,
      pending: @queue.count_messages,
      unsubscribed: Persistence::User.unsubscribed.count,
      by_url: visited_urls
    }
  end
end
