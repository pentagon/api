class Ads
  class << self
    def genre_map
      @genre_map ||= {
        'Blues' => 'blues_jazz',
        'Classical/Opera' => 'classical',
        'Country' => 'country',
        'Electronica/Dance' => 'dance_electronic',
        'Jazz' => 'blues_jazz',
        'Pop' => 'pop',
        'Rap/Hip Hop' => 'hip_hop',
        'Rock' => 'rock',
        'Soul/R&B' => 'hip_hop',
        'World' => 'world'
      }
    end

    def find_by_params(params, country)
      track = Track.fetch(params[:id])
      type = genre_map[track.genre] || 'rock'
      get_ordered_banners(type)
    end

    def get_ordered_banners(type)
      storage_path = "#{Settings.storage_path}/ads/#{type}"
      storage_url = "#{Settings.storage_url}/ads/#{type}"
      ads_group = Settings.discovery.ads_groups
      dir = Dir["#{storage_path}/*"]

      dir.map! {|d| d.split "/" }
      mm_ads = dir.select { |d| d.last.include? 'tmm' }
      mm_ad = mm_ads.sample
      mobile_ads = dir.select { |d| d.last.include? 'mobile' }
      mobile_ad = mobile_ads.sample
      dir.delete mm_ad
      dir.delete mobile_ad

      file = dir.sample.last
      key = file.split('-')[0]

      [{source: type, img: "#{storage_url}/#{mobile_ad.last}", link: ads_group['mobile']},
      {source: type, img: "#{storage_url}/#{mm_ad.last}", link: ads_group['tmm']},
      {source: type, img: "#{storage_url}/#{file}", link: ads_group[key.to_s]}]
    end
  end
end
