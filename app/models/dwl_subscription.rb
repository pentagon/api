# TODO: Rewrite this.
# Why don’t we store all the needed info in API?
# Why do we duplicate the info on DWL?
# Why does API send request to DWL just to respond to DWL’s request?! O_o
class DwlSubscription < OpenStruct
  behaves_like :digital_goods

  def digital_goods_base_price(options = {})
    {amount: price_amount, currency: price_currency}
  end

  def digital_goods_reporting_description
    ""
  end

  def digital_goods_name
    "DWL subscription"
  end

  def digital_goods_description
    description
  end

  def digital_goods_deliver(payment_order)
    {success: true}
  end

  class << self
    def resource
      # Add the timeout, cause the DWL does not respond sometimes.
      @resource ||= RestClient::Resource.new("#{Settings.dwl_site}/subscriptions", open_timeout: 2)
    end

    def digital_goods_find_by_id(id, options = {})
      result = resource[id].get
      new JSON.parse(result)['subscription']
    rescue RestClient::Exception
      nil
    end

    def digital_goods_use_payment_account
      "big"
    end

    def digital_goods_nominal_code
      "4001"
    end
  end

end
