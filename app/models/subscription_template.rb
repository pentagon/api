class SubscriptionTemplate < OpenStruct
  SELECT_PARAMS = [
    :id, :type, :price, :currency, :country, :repeat_period, :repeat_frequency,
    :trial_period, :trial_frequency, :trial_cycles, :trial_price
  ]

  def to_hash
    instance_values['table']
  end

  class << self
    def storage
      @storage ||= YAML.load_file(storage_file)
    end

    def find(id, include_invalid = false)
      select(id: id, include_invalid: include_invalid).first
    end

    def all(include_invalid = false)
      subscriptions = storage.map {|x| new x}
      subscriptions.select! {|sub| Date.today.between? Date.parse(sub.valid_from),
        Date.parse(sub.valid_to)} unless include_invalid
      subscriptions
    end

    def select(params = {})
      subscriptions = all params[:include_invalid]
      if params.present?
        where_params = params.slice(*SELECT_PARAMS).reject { |_, value| value.blank? }
        unless where_params.blank?
          subscriptions.select! do |sub|
            where_params.map { |k, v| sub.send(k).nil? || (sub.send(k).to_s.downcase == v.to_s.downcase) }.all?
          end
        end
      end
      subscriptions
    end

  private

    def storage_file
      File.join(Rails.root, 'config', 'subscriptions.yml')
    end
  end
end
