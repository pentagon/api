module ChartPosition
  extend ActiveSupport::Concern

  UPDATE_PERIOD = 1.day

  module ClassMethods
    def charts genre = nil, paginate = true
      Rails.cache.fetch("artist_genre_chart_#{genre}#{"_full" unless paginate}", expires_in: ChartPosition::UPDATE_PERIOD) do
        keys = Persistence::TrackStatistic.agr_charts(genre).map { |x| x["_id"] }
        res = paginate ? Kaminari.paginate_array(keys) : keys
      end
    end
  end

  def chart_position(genre=nil)
    chart_position = self.class.charts(genre, false).index { |t| t == self.uri }
    chart_position += 1 if chart_position.present?
    chart_position
  end

  def competitors genre = nil
    self_position = chart_position(genre)
    return nil unless self_position
    charts_ary = self.class.charts(genre, false)
    ch_count = charts_ary.count

    if ch_count == self_position
      ids = charts_ary.last(3)
    else
      prev_i = self_position-2
      next_i = self_position
      ary_index = self_position == 1 ? 0..2 : prev_i..next_i
      ids = charts_ary[ary_index]
    end
    ids
  end

  def all_competitors
    Rails.cache.fetch("#{self.id}all_competitors", expires_in: UPDATE_PERIOD) do
      default = [{genre: "All genres", competitors: competitors}]
      results = default + sorted_genres.take(2).map {|g| { genre: g, competitors: competitors(g)}  }
    end
  end

  def all_charts_positions
    Rails.cache.fetch("#{self.id}all_charts_positions", expires_in: UPDATE_PERIOD) do
      default = [{genre: "All genres", chart_position: chart_position}]
      result = default + sorted_genres.map {|g| { genre: g, chart_position: chart_position(g) }  }
    end
  end

end
