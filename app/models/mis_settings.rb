class MisSettings < Settingslogic
  source "#{Rails.root}/config/mis_settings.yml"
  namespace Rails.env
  load! if Rails.env.development?
end
