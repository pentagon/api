class MailHandler < TorqueBox::Messaging::MessageProcessor
  def tag
    self.class.name
  end

  def on_message(body)
    Rails.logger.debug "#{tag} :: #{body}"
    raise ArgumentError, "message must be a Hash" unless body.is_a?(Hash)
    mailer_class = body[:mailer]
    raise ArgumentError, "message must have a `:mailer' parameter" if mailer_class.blank?
    mailer = mailer_class.constantize
    mailer_action = body[:action]
    raise ArgumentError, "message must have a `:action' parameter" if mailer_action.blank?
    raise ArgumentError, "mailer #{mailer} doesn't respond to action #{mailer_action}" unless mailer.respond_to?(mailer_action)
    mailer_args = Array.wrap(body[:args])
    Rails.logger.info "#{tag} :: Sending mail: #{mailer}.#{mailer_action}(#{args.map(&:inspect).join(', ')})"
    mailer.send(mailer_action, *args).deliver
  end

  def on_error(exception)
    Rails.logger.error "#{tag} :: #{exception}"
  end
end
