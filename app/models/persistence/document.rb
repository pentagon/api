module Persistence::Document
  extend ActiveSupport::Concern

  included do
    include Persistence::Base
    include Mongoid::Validations
    include Mongoid::Timestamps

    field :uri,     type: String, default: ->{ generate_uri }
    field :source,  type: String, default: ->{ default_source }

    index({uri: 1}, {unique: true})

    scope :by_source, ->(source){ where(source: source.ci) }

    validates :uri, uniqueness: true
  end
end
