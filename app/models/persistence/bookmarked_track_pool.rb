class Persistence::BookmarkedTrackPool
  include Persistence::Document

  field :user_uri,    type: String
  field :track_uris,  type: Array, default: []
  field :application, type: String, default: ''

  index({user_uri: 1})
  index({application: 1})

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  attr_accessible :user_uri, :track_uris, :application

  def add_track track_uri
    update_attribute :track_uris, track_uris.push(track_uri)
  end

  def remove_track track_uri
    update_attribute :track_uris, track_uris.reject { |t| t == track_uri }
  end
end
