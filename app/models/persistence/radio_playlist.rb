class Persistence::RadioPlaylist
  include Persistence::Document

  field :user_uri,     type: String,  default: ''
  field :station_uris, type: Array,   default: []

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  attr_accessible :user_uri, :station_uris
  scope :by_user, ->(user_uri) {where user_uri: user_uri}

  validate :user_exists

  def as_json(options = {})
    {
      id: uri,
      created_at: created_at,
      station_uris: station_uris,
      user_uri: user_uri
    }
  end

  def stations
    @stations ||= ::Persistence::Station.any_in(uri: station_uris).sort_by {|a| station_uris.index a.uri}
  end

  private

  def user_exists
    errors.add :user_uri unless user_uri && user
  rescue ArgumentError
    errors.add :user_uri
  end
end
