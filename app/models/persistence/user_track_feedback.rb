class Persistence::UserTrackFeedback
  include Persistence::Document

  field :track_uri,      type: String,  default: ''
  field :playlist_uri,   type: String,  default: ''
  field :status,         type: String,  default: ''
  field :type,           type: String,  default: ''
  field :user_uri,       type: String,  default: ''
  field :track_duration, type: Integer, default: 0
  field :feedback_time,  type: Integer, default: 0

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  attr_accessible :track_uri, :playlist_uri, :source, :status, :user_uri, :type

  validates_uniqueness_of :track_uri, scope: :user_uri

  class << self
    def create_or_update_from_params params
      # TODO: validate params
      unless %w(like dislike).include? params[:status]
        feedback = find_by user_uri: params[:user_uri], track_uri: params[:track_uri]
        feedback.destroy if feedback
      else
        feedback = find_or_create_by type: params[:type], user_uri: params[:user_uri],
          track_uri: params[:track_uri]
        feedback.update_attributes status: params[:status], playlist_uri: params[:playlist_uri]
      end
      TorqueBox::Messaging::Queue.new('/queues/backwriter').publish key: 'feedback', data: params.slice(:status, :track_uri, :user_uri, :source)
    end

    def liked_track_uris_for_users user_uris
      where(status: 'like').in(user_uri: user_uris).pluck :track_uri
    end

    def last_liked_user_tracks(user_uri, count = 0)
      liked_count = where(status: 'like', user_uri: user_uri).count
      if liked_count < count
        where(status: 'like', user_uri: user_uri).sort({track_uri: -1}).limit(count).pluck :track_uri
      else
        where(status: 'like', user_uri: user_uri).pluck :track_uri
      end
    end

    def liked_user_tracks user_uri
      where(status: 'like', user_uri: user_uri).pluck :track_uri
    end

    def disliked_user_tracks user_uri
      where(status: 'dislike', user_uri: user_uri).pluck :track_uri
    end
  end
end
