require 'maxmind_geoip'
require 'radio_mixer'

class Persistence::Station
  include Persistence::Document
  include Mongoid::Paranoia

  field :country,             type: String,   default: Settings.radio.fallback_country
  field :clone_counter,       type: Fixnum,   default: 0
  field :is_favourite,        type: Boolean,  default: false
  field :is_free,             type: Boolean,  default: true
  field :last_played_at,      type: DateTime, default: nil
  field :parent_station_uri,  type: String
  field :seed_track_uris,     type: Array,    default: []
  field :title,               type: String,   default: ''
  field :user_uri,            type: String,   default: ''
  field :pending_tracks_pool, type: Array,    default: []
  field :rejected_tracks_pool,type: Array,    default: []

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  belongs_to :parent_station, foreign_key: :parent_station_uri, class_name: 'Persistence::Station', primary_key: :uri

  attr_accessible :country, :user_uri, :seed_track_uris, :title, :is_free, :parent_station_uri,
    :is_favourite, :filters
  attr_accessor :filters
  attr_reader :new_seed_uris

  validates :title, presence: true
  validate :preserve_attributes, :preserve_seed_tracks, unless: :new_record?
  validate :user_exists, :seed_tracks_exist

  # after_update :request_tracks_for_new_seeds
  # after_create :init_station
  after_destroy :dispose_station
  delegate :logger, to: 'self.class'

  scope :most_popular, descending(:clone_counter)

  class << self
    def logger
      @@logger ||= Logger.new File.join Settings.log_path, 'radio.log'
    end

    def load_from_params(params)
      sorting_type = params[:sort_by] || 'created_at'
      view = params[:include_deleted] ? all.unscoped : all
      view = params[:descending] ? view.desc(sorting_type) : view.asc(sorting_type)
      params_to_find = [:user_uri, :seed_track_uris, :is_free]
      where_params = params.slice(*params_to_find).reject { |_, value| value.nil? }
      view.where where_params
    end

    def find_or_create_from_params(params)
      params_to_find = [:user_uri, :seed_track_uris, :is_free]
      params_to_find << :country unless params[:is_free]
      where_params = params.slice(*params_to_find).reject { |_, value| value.nil? }
      where(where_params).desc(:created_at).first || create_and_init(params)
    end

    def create_and_init params
      res = create params
      res.init_station
      res
    end
  end

  def mis_requests_manager
    @mis_requests_manager ||= Radio::StationMisRequestsManagerInteraction.get_for_station(self)
  end

  def pending_tracks_manager
    @pending_tracks_manager ||= Radio::StationPendingTracksManagerInteraction.get_for_station(self)
  end

  def seed_tracks
    @seed_tracks ||= ::Track.fetch(seed_track_uris)
  end

  # TODO: Remove this if it’s not used.
  def station_show_page_url
    File.join Settings.station_show_page_url, uri
  end

  def image_url
    ::Track.fetch(seed_track_uris.first).try(:image_url)
  end

  def as_json(options = {})
    hash = {
      id: uri,
      title: title,
      user_uri: user_uri,
      seed_track_uris: seed_track_uris,
      image_url: image_url,
      is_free: is_free,
      is_favourite: is_favourite,
      parent_station_uri: parent_station_uri,
      station_show_url: station_show_page_url,
      country: country,
      created_at: created_at,
      updated_at: updated_at
    }
    hash[:children_count] = clone_counter
    hash
  end

  def request_starting_tracks
    # Remove `&& false` when MIS implements cloning functionality
    if parent_station_uri.present? && false
      mis_requests_manager.send_clone_station_request(parent_station_uri)
    else
      if seed_track_uris.many?
        uris = seed_track_uris.reverse
        mis_requests_manager.send_track_request(:like, [uris.pop], self.filters)
        mis_requests_manager.send_track_request(:like, uris, self.filters)
      else
        mis_requests_manager.send_track_request(:like, seed_track_uris, self.filters)
      end
    end
  end

  def init_station
    parent_station.inc(:clone_counter, 1) if parent_station
    request_starting_tracks
  end

  def request_tracks_for_new_seeds
    if @new_seed_uris && @new_seed_uris.any?
      mis_requests_manager.send_track_request :like, @new_seed_uris, self.filters
      @new_seed_uris = []
    end
  end

private

  def dispose_station
    parent_station.inc(:clone_counter, -1) if parent_station
    remove_from_playlist
  end

  def remove_from_playlist
    playlist = ::Persistence::RadioPlaylist.find_by(user_uri: user_uri)
    if playlist && playlist.station_uris
      playlist.station_uris = playlist.station_uris.reject { |station_uri| station_uri == self.uri }
      playlist.save
    end
  end

  def user_exists
    errors.add :user_uri unless user_uri && user
  rescue ArgumentError
    errors.add :user_uri
  end

  def seed_tracks_exist
    if seed_track_uris.blank? || ::Track.fetch(seed_track_uris).blank?
      errors.add :seed_track_uris, :blank
    end
  rescue ArgumentError
    errors.add :seed_track_uris
  end

  def preserve_attributes
    immutable_attributes = [:user_uri, :is_free, :parent_station_uri]
    immutable_attributes.each do |attribute|
      errors.add attribute, :immutable if self.send "#{attribute}_changed?"
    end
  end

  def preserve_seed_tracks
    @new_seed_uris = seed_track_uris - seed_track_uris_was
  end
end
