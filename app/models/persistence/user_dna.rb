class Persistence::UserDna
  include Persistence::Document

  field :user_uri,      type: String, default: ''
  field :test_results,  type: Hash, default: {}
  field :is_completed,  type: Boolean, default: false
  field :is_expired,    type: Boolean, default: false

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  attr_accessible :user_uri
  scope :actual, ->{where is_completed: true, is_expired: false}

  ANSWERS = ['1', '2', '3', '4', '5']

  class << self
    def get_last_for_user user_id
      find_or_create_by(user_uri: user_id, is_expired: false)
    end

    def get_last_incomplete_for_user user_id
      where(user_uri: user_id, is_completed: false, is_expired: false).last
    end

    def create_test_for_user uri
      where(user_uri: uri).each {|i| i.update_attribute(:is_expired, true)}
      create(user_uri: uri)
    end

    def actual_for_user uri
      actual.where(user_uri: uri).first
    end
  end

  def get_next_test_from_set test_set = {}
    raise new Exception('No tests loaded') if test_set.blank?
    {test_results.keys.count.succ => (test_set - test_results.values)}
  end

  def add_test_result results
    update_attribute :test_results, test_results.merge(results)
    update_attribute :is_completed, true if completed?
    if pre_final_answer?
      last_key = (ANSWERS - test_results.keys).first
      last_value = (ANSWERS - test_results.values).first
      add_test_result({last_key => last_value})
    end
  end

  def completed?
    (ANSWERS - test_results.keys).empty?
  end

  def pre_final_answer?
    (ANSWERS - test_results.keys).length == 1
  end

  def similar_users_map
    keys =  (1..ANSWERS.count.pred).map {|i| test_results.slice *test_results.keys[0..-(i - ANSWERS.count.pred)]}
    found_user_ids = [user_uri]
    keys.inject({}) do |a,l|
      query_keys = l.inject({}) {|s,(k,v)| s["test_results.#{k}"] = v; s}
      uris = self.class.actual.where(query_keys).not_in(user_uri: found_user_ids).pluck :user_uri
      found_user_ids += uris
      found_user_ids.uniq!
      a[l.keys.count] = uris
      a
    end
  end
end
