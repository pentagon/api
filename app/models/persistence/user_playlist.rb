class Persistence::UserPlaylist
  include Persistence::Document

  field :user_uri,          type: String,  default: ''
  field :title,             type: String,  default: 'Default Playlist'
  field :track_uris,        type: Array,   default: []
  field :items,             type: Array,   default: []
  field :position,          type: Fixnum,  default: 0

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  attr_accessible :title, :track_uris, :position, :user_uri
  after_destroy :update_positions

  def track_uris= value
    super value || []
  end

  def update_positions
    self.class.where(user_uri: user_uri).asc(:position).each_with_index {|pl, idx| pl.update_attribute :position, idx}
  end
end
