class Persistence::Referral
  include Persistence::Document

  field :initiator_uri
  field :responder_uri
  field :ip

  belongs_to :responder, inverse_of: :responder_referral, class_name: 'Persistence::User', foreign_key: :user_uri,
    primary_key: :uri
  belongs_to :initiator, inverse_of: :initiator_referral, class_name: 'Persistence::User', foreign_key: :user_uri,
    primary_key: :uri
  attr_accessible :initiator_uri, :responder_uri, :ip, :referral_token

  validates :initiator_uri, uniqueness: { scope: [:ip, :responder_uri] }

  def self.find_or_create(referral_token, ip)
    if initiator = Persistence::User.find_by(referral_token: referral_token)
      find_or_create_by(initiator_uri: initiator.uri, ip: ip, responder_uri: nil)
    end
  end
end
