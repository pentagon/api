module Persistence::TrackData
  class BlockInfo
    include Persistence::EmbeddedDocument

    field :code,             type: String, default: ''
    field :reason,           type: String, default: ''
    field :blocked_by,       type: String, default: 'system'
    field :blocked_at,       type: DateTime, default: ->{Time.now}
    field :requested_by,     type: String, default: 'system'
    field :resolved_by,      type: String, default: ''
    field :resolved_at,      type: DateTime

    embedded_in :track, class_name: 'Persistence::Track'

    def resolved?
      not resolved_at.to_i.zero?
    end
  end
end
