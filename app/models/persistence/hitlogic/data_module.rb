class Persistence::Hitlogic::DataModule
  include Persistence::Document

  attr_accessible :module_type, :cost_of_credit, :value_attribute, :description,
                  :title, :visible, :category, :is_multiple_charts, :catalog_id,
                  :priority, :sub_description
  field :value_attribute, type: String,  default: ''
  field :module_type, type: String,  default: ''
  field :title, type: String,  default: ''
  field :description, type: String,  default: ''
  field :sub_description, type: String,  default: ''
  field :category, type: String,  default: ''
  field :visible, type: Boolean, default: false
  field :is_prototype, type: Boolean, default: false
  field :is_multiple_charts, type: Boolean, default: false
  field :cost_of_credit, type: Integer, default: 1
  field :priority, type: Integer, default: 1000

  belongs_to :catalog, class_name: "Persistence::Hitlogic::Catalog"
  belongs_to :data_set, class_name: "Persistence::Hitlogic::DataSet"

  accepts_nested_attributes_for :catalog

  accepts_nested_attributes_for :catalog

  scope :prototypes, where(is_prototype: true)

  index({is_prototype: 1})

  def prototype
    Persistence::Hitlogic::DataModule.prototypes.find_by module_type: module_type
  end

  def  get_data_for_module
    territory = prototype.is_multiple_charts && catalog? ? catalog.name : Persistence::Hitlogic::DataSet::DEFAULT_CATALOG
    report = data_set.reports.find_by territory: territory unless data_set.nil?
    report.send(prototype.value_attribute.to_sym) if prototype.value_attribute.present? && !report.nil?
  end
end
