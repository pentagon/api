class Persistence::Hitlogic::DataSet
  include Persistence::Document

  DEFAULT_CATALOG = 'uk'

  attr_accessible :data_modules_attributes

  field :user_uri, default: ''
  field :track_uri, default: ''
  field :is_private, type: Boolean, default: false
  field :is_new, type: Boolean, default: true

  index({user_uri: 1})
  index({track_uri: 1})

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  belongs_to :track, class_name: 'Persistence::Track', foreign_key: :track_uri, primary_key: :uri
  has_many :reports, class_name: "Persistence::HitMakerReport", dependent: :destroy
  has_many :data_modules, class_name: 'Persistence::Hitlogic::DataModule', dependent: :destroy
  accepts_nested_attributes_for :data_modules

  def catalogs
    data_modules.map {|a| a.catalog.try(:name) || DEFAULT_CATALOG}.uniq
  end

  def available_catalogs
    reports.map &:territory
  end

  def is_processed
    return false if reports.blank?
    reports.all? &:is_processed
  end
end
