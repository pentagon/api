class Persistence::Hitlogic::Catalog
  include Persistence::Document

  attr_accessible :title, :name, :description, :year, :priority, :tooltip, :sub_description

  field :title, type: String,  default: ''
  field :name, type: String,  default: ''
  field :priority, type: Integer
  field :description, type: String,  default: ''
  field :sub_description, type: String,  default: ''
  field :tooltip, type: String,  default: ''
  field :year, type: String,  default: ''

  has_many :data_modules, class_name: "Persistence::Hitlogic::DataModule"
end
