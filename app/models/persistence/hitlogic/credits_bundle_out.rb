class Persistence::Hitlogic::CreditsBundleOut
  include Persistence::Document

  field :amount,      type: Integer
  field :user_uri,    type: String
  field :report_id,    type: String

  attr_accessible :amount, :user_uri, :report_id

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  belongs_to :hitlogic_credits_bundle, class_name: 'Persistence::HitlogicCreditsBundle'
end
