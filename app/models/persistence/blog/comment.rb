class Persistence::Blog::Comment
  include Persistence::Document

  attr_accessible :text

  field :created_at, type: DateTime
  field :text, type: String

  belongs_to :post, class_name: 'Persistence::Blog::Post'
  belongs_to :user, class_name: 'Persistence::User'

  def source
    'blog'
  end
end
