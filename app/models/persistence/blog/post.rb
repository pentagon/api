class Persistence::Blog::Post
  include Persistence::Document
  include Mongoid::Taggable
  include Tire::Model::Search
  include Tire::Model::Callbacks
  include Mongoid::Slug
  require 'tire/queries/fuzzy_like_this'
  require 'tire/queries/more_like_this'

  index_name 'blog'
  document_type 'post'

  field :title, type: String
  field :published_at, type: Date
  field :text, type: String
  field :brief_text, type: String
  field :master, type: Boolean
  field :permalink, type: String
  field :enabled, type: Boolean, default: false
  field :created_by_name, type: String

  belongs_to :category, class_name: 'Persistence::Blog::Category'
  has_many :comments, class_name: 'Persistence::Blog::Comment'



  validates :title, :published_at, :text, :category_id, :created_by_name, :brief_text, presence: true
  validates :image, presence: true, :on => :create

  slug :title, reserve: ['new', 'edit', 'delete', 'create', 'destroy']
  scope :enabled, -> { where enabled: true }

  mapping do
    indexes :tags, :boost => 2.0, :analyzer => 'keyword'
    indexes :title, :analyzer => 'snowball', :boost => 1.0
    indexes :text, :analyzer => 'snowball'
    indexes :brief_text, :analyzer => 'snowball'
    indexes :enabled, index: :not_analyzed
    indexes :created_by_name, index: :not_analyzed
    indexes :published_at, index: :not_analyzed
    indexes :permalink, index: :not_analyzed
    indexes :slug, index: :not_analyzed
    indexes :image_url, index: :not_analyzed
  end

  def to_indexed_json
    to_indexed_hash.to_json
  end

  def to_indexed_hash params = {}
    {
      title: title,
      text: text,
      category_id: category_id.to_s,
      published_at: published_at,
      master: master,
      tags: tags_array,
      disabled: disabled,
      brief_text: brief_text,
      created_by_name: created_by_name,
      image_url: image_url,
      permalink: permalink,
      slug: slug
    }
  end

  def disabled
    !enabled || !category.enabled
  end

  def image_url
    "/uploads/post/image/#{id}/#{self['image']}"
  end

  def source
    'blog'
  end
end
