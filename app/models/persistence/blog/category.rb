class Persistence::Blog::Category
  include Persistence::Document
  include Mongoid::Slug

  field :name, type: String
  field :color, type: String
  field :permalink, type: String
  field :enabled, type: Boolean, default: false

  has_many :posts, class_name: 'Persistence::Blog::Post'

  slug :name, reserve: ['new', 'edit', 'delete', 'create', 'destroy']

  scope :enabled, -> { where enabled: true }

  validates :name, :color, presence: true
end
