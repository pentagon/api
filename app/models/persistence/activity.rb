class Persistence::Activity
  include Persistence::Base
  include Mongoid::Validations
  include Mongoid::Timestamps


  field :key,         type: String
  field :parameters,  type: Hash, default: {}
  field :trackable_uri
  field :trackable_type

  belongs_to :trackable, polymorphic: true, foreign_key: :trackable_uri, primary_key: :uri

  scope :by_key, ->(key){where key: key.to_s}

  validates :trackable_uri, presence: true
  validates :key, presence: true

  attr_accessible :trackable_uri, :trackable_type, :key, :parameters

  def self.create_activity(trackable, options)
    trackable.activities.create! options
  end
end
