module Persistence::LoginStatistic
  extend Penetrator::Concern

  included do
    include Persistence::Statistic

    field :source_page,  type: String, default: ''
    field :activity,     type: String, default: ''

    attr_accessible :source_page, :service, :activity
    scope :by_activity, ->(activity){where activity: activity}
  end
end
