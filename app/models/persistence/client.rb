class Persistence::Client < Client
  belongs_to :user, class_name: 'Persistence::User'
  has_many :access_tokens, class_name: '::AccessToken'
  has_many :refresh_tokens, class_name: '::RefreshToken'
  has_many :authorization_codes, class_name: '::AuthorizationCode'

  store_in collection: "oauth2_clients".to_sym
end
