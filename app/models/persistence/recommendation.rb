class Persistence::Recommendation
  include Persistence::Document

  field :track_uri,                    default: ''
  field :territory
  field :tracks,        type: Array,   default: []
  field :version,       type: Integer, default: 1
  field :is_processed,  type: Boolean, default: false
  field :mis_error
  field :has_error,     type: Boolean, default: false

  index({track_uri: 1}, {})
  index({territory: 1}, {})
  index({is_processed: 1}, {})

  attr_accessible :track_uri, :is_processed, :has_error, :tracks, :territory

  alias_method :processed?, :is_processed
  def unprocessed?; not processed? end
  def has_no_error?; not has_error end
  def new?; created_at == updated_at end
end
