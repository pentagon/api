module Persistence::Statistic
  extend Penetrator::Concern

  included do
    include Persistence::Document

    store_in collection: :statistics

    field :created_at,          type: DateTime,   default: -> { Time.now.utc }
    field :user_uri,            type: String,     default: ''
    field :user_ip,             type: String,     default: ''
    field :location,            type: Array,      default: []
    field :city,                type: String,     default: ''
    field :region_code,         type: String,     default: ''
    field :zipcode,             type: String,     default: ''
    field :country_code,        type: String,     default: ''
    field :service,             type: String,     default: ''
    field :type,                type: String,     default: self.default_type_name

    index({location: '2d'}, {})

    default_scope where(type: self.default_type_name) unless self.default_type_name == 'statistic'

    scope :by_user,  ->(user)  { where(user_uri: user)}
    scope :by_service,  ->(service)   { where(service: service)   }

    attr_accessible :created_at, :user_uri, :user_ip, :location,
      :city, :region_code, :zipcode, :country_code

    def country_name
      Persistence::Helpers::COUNTRIES_MAP[country_code.to_sym] if country_code
    end
  end

  module ClassMethods
    def default_type_name
      name.demodulize.underscore
    end
  end
end
