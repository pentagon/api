class Persistence::Image
  include Persistence::Document
  include ::Utils::CarrierWave::Glue
  include HasStoredImage
  has_attachment :image, Tmm::ImageUploader

  field :title,         default: ''
  field :description,   default: ''

  attr_accessible :title, :description

  embedded_in :gallery, class_name: 'Persistence::Gallery'

  def as_json opts = {}
    {title: title, description: description, image_url: image_url}
  end
end
