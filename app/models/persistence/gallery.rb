class Persistence::Gallery
include Persistence::Document

  field :title,         default: ''
  field :description,   default: ''
  field :gallerieable_uri
  field :gallerieable_type
  field :cover_uri,     default: ''

  index({gallerieable_uri: 1})
  index({gallerieable_type: 1})

  belongs_to :gallerieable, polymorphic: true, foreign_key: :gallerieable_uri, primary_key: :uri

  embeds_many :images, class_name: 'Persistence::Image'

  attr_accessible :title, :description, :gallerieable_uri, :gallerieable_type, :cover_uri
end

