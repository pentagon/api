class Persistence::Oauth2::Client# < Devise::Oauth2Providable::Client
  include Persistence::Document

  field :name
  field :redirect_uri
  field :website
  field :identifier
  field :secret
  field :support_email
  field :contact_email
  field :app_description
  field :sign_out_uri
  field :source_label

  index({identifier: 1}, {unique: true, background: true})

  attr_accessible :name, :redirect_uri, :sign_out_uri, :website, :support_email, :contact_email, :app_description, :source_label

  validates :name, :identifier, :secret, presence: true, uniqueness: true
  validates :website, presence: true
  validates :source_label, presence: true

  store_in collection: "oauth2_clients".to_sym

  def generate_secret
    self.identifier = self.class.generate_secrets('!self.identifier')
    self.secret = self.class.generate_secrets('!self.secret')
  end

  class << self
    def generate_secrets(hash)
      Digest::MD5.hexdigest(Time.now.strftime("%Y%m%d%j%p%H%S%M%L%12N") << hash)
    end
  end
end
