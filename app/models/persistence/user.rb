class Persistence::User
  include Persistence::Document
  include Mongoid::Taggable
  include Mongoid::Slug
  include Tire::Model::Search
  include Tire::Model::Callbacks

  class EmbeddedValidator < ActiveModel::Validator
    def validate(record)
      relations = record.relations
      relations.each_pair do |k, v|
        if v[:relation].parent == Mongoid::Relations::Embedded and record.send(k).respond_to?(:errors)
          embedded_errors = record.send(k).errors.to_hash
          embedded_errors.each_pair do |name, messages|
            messages.each { |message| record.errors.add(:"#{k}.#{name}", message) }
          end
        end
      end
    end
  end

  class RolesValidator < ActiveModel::Validator
    def validate(record)
      unless record.roles.subset?(Persistence::User::ROLES)
        diff = record.roles - Persistence::User::ROLES
        record.errors[:roles] << (options[:message] || "contains invalid items: %s") % diff.to_a.join(', ')
      end
    end
  end

  CONTACT_INFO_KEYS = [:country, :city, :postal_code, :state]
  BILLING_ADDRESS_KEYS = [:address, :country, :city, :postal_code, :state]
  ROLES = %w(superuser developer admin thirdparty).to_set.freeze unless defined?(ROLES)

  tags_separator ' '

  field :slug,                                   default: ''
  field :email,                                  default: ''
  field :social_info,             type: Hash,    default: {}
  field :available_reports,       type: Integer, default: 1
  field :status,                                 default: 'enabled'
  field :roles,                   type: Set,     default: Set.new
  field :visited_services,        type: Set,     default: Set.new
  field :superuser_token
  field :referral_token
  field :failed_attempts,         type: Integer, default: 0
  field :locked_at,               type: Time
  field :unlock_token
  field :authentication_token
  field :subscribed,              type: Boolean, default: true
  field :encrypted_password,                     default: ""
  field :reset_password_token
  field :reset_password_sent_at,  type: DateTime
  field :remember_created_at,     type: DateTime
  field :provider
  field :uid
  field :explicit_filter,         type: Integer, default: 0
  field :followers_uris,          type: Array,   default: []
  field :utm_medium,              type: String,  default: ''
  field :utm_campaign,            type: String,  default: ''
  field :is_verified,             type: Boolean, default: false
  field :verification_token,      type: String,  default: nil
  field :default_artist_uri,      type: String

  field :partner_tracking_date,   type: DateTime
  field :partner_tracking_page,   type: String,  default: ''
  field :partner,                 type: String,  default: ''

  field :internal_user,           type: Boolean, default: false

  index({unlock_token: 1},         {sparse: true, unique: true})
  index({verification_token: 1},   {sparse: true, unique: true})
  index({authentication_token: 1}, {sparse: true, unique: true})
  # index({uid: 1}, {unique: true})
  index({uri: 1, slug: 1})
  index({email: 1})
  index({referral_token: 1},       {sparse: true, unique: true})
  index({slug: 1})
  index({_slug: 1})
  index({uid: 1, provider: 1})
  index({reset_password_token: 1}, {sparse: true, unique: true})
  index({created_at: -1})

  include ::Utils::CarrierWave::Glue
  has_attachment :image, Tmm::ImageUploader, extensions: :crop
  include HasStoredImage

  slug :slug, reserve: ['new', 'edit', 'delete', 'create', 'destroy'], history: true do |record|
    [record.slug, record.uri].compact.reject(&:empty?).first.to_url
  end

  behaves_like :token_generatable
  behaves_like :token_authenticatable
  behaves_like :database_authenticatable
  behaves_like :oauth2_providable
  behaves_like :subscrible
  behaves_like :trackable
  behaves_like :referralable

  embeds_many :billing_addresses, class_name: 'Persistence::UserData::BillingAddress'
  embeds_one :contact_info,  autobuild: true, class_name: 'Persistence::UserData::ContactInfo'
  embeds_one :personal_info, autobuild: true, class_name: 'Persistence::UserData::PersonalInfo'
  embeds_many :social_authentications, class_name: 'Persistence::UserData::SocialAuthentication'

  has_many :tracks, class_name: 'Persistence::Track', foreign_key: :user_uri, primary_key: :uri
  has_many :artists, class_name: 'Persistence::Artist', foreign_key: :user_uri, primary_key: :uri
  has_many :user_playlists, class_name: 'Persistence::UserPlaylist', foreign_key: :user_uri, primary_key: :uri
  has_many :messages, class_name: 'Persistence::User', foreign_key: :owner_uri, primary_key: :uri
  has_many :videos, class_name: 'Persistence::Video', foreign_key: :user_uri, primary_key: :uri
  has_many :share_links, class_name: 'Persistence::ShareLink', foreign_key: :user_uri, primary_key: :uri
  has_many :galleries, as: :gallerieable
  has_many :image_albums, as: :gallerieable, foreign_key: :gallerieable_uri, primary_key: :uri, class_name: 'Persistence::Gallery'
  has_many :responder_referrals, foreign_key: :initiator_uri, inverse_of: :responder,
    class_name: 'Persistence::Referral', primary_key: :uri
  has_one :initiator_referral, foreign_key: :responder_uri, inverse_of: :initiator,
    class_name: 'Persistence::Referral', primary_key: :uri
  has_many :activities, as: :trackable, class_name: 'Persistence::Activity', foreign_key: :trackable_uri, primary_key: :uri,
    order: {created_at: :asc}
  has_many :subscriptions, class_name: 'Persistence::Subscription', foreign_key: :user_uri, primary_key: :uri
  has_many :reports, class_name: 'Persistence::Report', foreign_key: :user_uri, primary_key: :uri
  has_many :hit_maker_reports, class_name: 'Persistence::HitMakerReport', foreign_key: :user_uri, primary_key: :uri
  has_many :snap_tracks, class_name: 'Persistence::SnapTrack', foreign_key: :user_uri, primary_key: :uri
  has_many :hitlogic_credits_bundles, class_name: "Persistence::HitlogicCreditsBundle",  foreign_key: :user_uri, primary_key: :uri

  attr_accessible :slug, :email, :contact_info_attributes, :personal_info_attributes, :subscribed, :provider,
    :uid, :personal_info, :contact_info, :tags_array, :explicit_filter, :verification_token, :is_verified,
    :utm_medium, :utm_campaign, :partner_tracking_date, :partner_tracking_page, :partner, :internal_user

  delegate :first_name, :last_name, :nick_name, :name, :gender, :date_of_birth, :biography,            to: :personal_info
  delegate :phone, :site_url, :country, :state, :city, :address, :street, :postal_code, :full_address, to: :contact_info

  accepts_nested_attributes_for :billing_addresses
  accepts_nested_attributes_for :contact_info
  accepts_nested_attributes_for :personal_info
  accepts_nested_attributes_for :social_authentications

  validates_with RolesValidator
  validates :slug, uniqueness: { case_sensitive: false }, allow_blank: true,
    format: {
      with: /^[([a-z]|[A-Z])0-9_-]{1,40}$/,
      message: 'only english letters or digits, "_" or "-" symbols and length must by less than 40!'
    }
  validates :site_url, allow_blank: true,
    format: {
      with: /(^$)|(^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$)/ix
    }
  validates :terms, acceptance: true
  validates :email,
    length:     { minimum: 6 },
    uniqueness: { case_sensitive: false },
    presence:   true,
    format:     { with: /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/ }, unless: ->{ defined?(Devise) }
  validates :uid, uniqueness: {scope: :provider}, if: :provider?
  validates :referral_token, uniqueness: true, allow_nil: true
  validates_with EmbeddedValidator
  validates_presence_of     :password, if: :password_required?
  validates_confirmation_of :password, if: :password_required?
  validates_length_of       :password, :within => 8..32, :allow_blank => true

  scope :with_roles, ->(*roles){ where(:roles.all => roles) }
  scope :by_email, ->(email){ where(email: email.ci ) }
  scope :by_slug, ->(slug){ where(slug: slug.ci) }
  scope :subscribed, where(subscribed: true)
  scope :unsubscribed, where(subscribed: false)
  scope :verified, where(is_verified: true)
  scope :unverified, where(is_verified: false)

  # Search options
  index_name 'unsigned'
  document_type 'user'

  mapping do
    indexes :id, index: :not_analyzed
    indexes :full_name, analyzer: 'snowball', boost: 100
    indexes :slug, analyzer: 'snowball', boost: 50
    indexes :image_url, index: :not_analyzed
  end

  # Callbacks
  # Gift for user from cadenza, we give 5 free
  after_create do |u|
    bundle = Persistence::HitlogicCreditsBundle.digital_goods_find_by_id('bundle_3')
    bundle.update_attributes({amount: 5, user_uri: u.uri, type: 'first5'})
  end

  after_destroy do |u|
    Rails.cache.delete("user_#{u.authentication_token}")
    u.save_tags_index!
    ::Persistence::Artist.where(user_uri: u.uri).destroy_all
    u.tracks.destroy_all
    u.tire.index.remove u
  end

  after_save do |u|
    u.update_index if u.changed_attributes.keys.include?('slug') or u.personal_info.changed?
  end

  before_save do |u|
    Rails.cache.delete("user_#{u.authentication_token}")
    u.is_verified = false if u.changed.include?('email') and u.is_verified == true
    true
  end

  after_initialize :ensure_authentication_token, if: 'respond_to?(:ensure_authentication_token)'
  before_validation :add_protocol_to_site_url, if: 'site_url.present?'

  ROLES.each do |role|
    scope role.pluralize, where(:roles.all => [role])
    define_method :"#{role}?" do         # def superuser?
      roles.include? role.to_s           #   roles.include? 'superuser'
    end                               # end
    alias_method role, :"#{role}?"          # alias_method :superuser, :superuser?
  end

  %w(stage_name artist_roles).each do |a|
    define_method("#{a}=") do |value|
      prev_value = send "#{a}"
      if prev_value != value and (prev_value.present? or value.present?)
        default_artist.send "#{a}=", value
        default_artist.save
      end
    end
    define_method(a) do
      artists.blank? ? nil : default_artist.send(a)
    end
  end

  {support: 'support@tunehog.com'}.each do |user_alias, email|
      define_method user_alias do
      find_by(email: email)
    end
  end

  # Class methods definitions
  class << self

    def find_or_initialize_with_errors required_attributes, attributes, error = :invalid
      attributes = attributes.slice(*required_attributes)
      attributes.delete_if { |key, value| value.blank? }

      record = find_by(attributes) if attributes.size == required_attributes.size

      unless record
        record = new
        required_attributes.each do |key|
          value = attributes[key]
          record.send("#{key}=", value)
          record.errors.add(key, value.present? ? error: :blank)
        end
      end

      record
    end

    def verification_token
      generate_token(:verification_token)
    end

    def send_reset_password_instructions(attributes={}, &block)
      from = attributes.delete(:from)
      recoverable = find_or_initialize_with_errors([:email], attributes, :not_found)
      if recoverable.persisted?
        recoverable.send_reset_password_instructions(attributes[:edit_password_url], from, &block)
      else
        yield recoverable, nil if block_given?
      end
      recoverable
    end

    def reset_password_by_token(attributes = {})
      token = attributes[:reset_password_token]
      recoverable = find_or_initialize_with_errors([:reset_password_token], {reset_password_token: token})
      if recoverable.persisted?
        if recoverable.reset_password_period_valid?
          recoverable.reset_password!(attributes[:password], attributes[:password_confirmation])
        else
          recoverable.errors.add(:reset_password_token, :expired)
        end
      end
      recoverable
    end

    def find_for_authentication(conditions = {})
      conditions[:email].downcase! if conditions[:email].present?
      find_by conditions
    end

    def load_from_params(params)
      where_params = [:email]
      view = all
      where_params.each do |key|
        view = view.where({key => params[key]}) if params[key].present?
      end
      view = view.where(email: params[:email]) if params[:email].present?
      view = params[:ascending] ? view.asc(:created_at) : view.desc(:created_at)
      view = view.gte(created_at: Time.parse(params[:date_from])) if params[:date_from].present?
      view = view.lte(created_at: Time.parse(params[:date_till]).end_of_day) if params[:date_till].present?
      view = view.limit params[:limit].to_i if params[:limit].present?
      view
    end

    def find_by_auth provider, uid
      find_by :'social_authentications.provider' => provider, :'social_authentications.uid' => uid
    end

    def find_by_uri_or_slug uri_or_slug
      return nil if uri_or_slug.blank?
      find_by(uri: uri_or_slug) or find_by(slug: uri_or_slug.case_insensitive)
    end

    def find_by_id_or_slug id_or_slug
      return nil if id_or_slug.blank?
      if id_or_slug.respond_to?(:case_insensitive)
        find_by(slug: id_or_slug)
      else
        find(id_or_slug)
      end
    end
  end

  def generate_verification_token!
    generate_verification_token
    save
    #update_attribute(:verification_token, Devise.token_generator.generate(Persistence::User, :verification_token).first)
    #ActiveSupport::Deprecation.warn('generate_verification_token uses Devise.token_generator')
  end

  def generate_verification_token
    self.verification_token = self.class.verification_token
  end

  def verify!
    unset('verification_token')
    assign_attributes(is_verified: true)
    save
  end

  def add_protocol_to_site_url
    unless site_url =~ /^https?:\/\//ix
      self.contact_info.site_url = "http://#{site_url}"
    end
  end

  def assign_role(role)
    role = role.to_s
    raise ArgumentError, "role #{role.inspect} is not valid" unless ROLES.include?(role)
    self.roles = roles << role
  end

  def revoke_role(role)
    role = role.to_s
    raise ArgumentError, "role #{role.inspect} is not valid" unless ROLES.include?(role)
    self.roles = roles.delete(role)
  end

  def role?(role)
    roles.include? role.to_s
  end
  alias_method :has_role?, :role?

  def find_and_set_default_artist!
    res = if (arts = artists.where user_uri: uri).any?
      arts.first
    else
      Persistence::Artist.create source: source, user_uri: uri
    end
    update_attribute :default_artist_uri, res.uri
    res
  end

  def default_artist
    default_artist_uri.blank? ? find_and_set_default_artist! : artists.find_by(uri: default_artist_uri)
  end

  alias_method :artist, :default_artist

  def send_reset_password_instructions(edit_password_url = nil, from = nil, &block)
    raw = self.class.generate_token(:reset_password_token)
    self.reset_password_token   = raw
    self.reset_password_sent_at = Time.now.utc
    self.save(:validate => false)
    if block_given?
      yield self, raw
    else
      if from.present?
        send_notification(:reset_password_instructions, raw, edit_password_url: edit_password_url, from: from, reply_to: from)
      else
        send_notification(:reset_password_instructions, raw, edit_password_url: edit_password_url)
      end
    end
    raw
  end

  def full_name
    [nick_name, stage_name, name, slug, email.to_s.split('@').first].compact.reject(&:empty?).first
  end

  def artist_name
    stage_name.presence or full_name
  end

  def to_param
    slug.presence or uri
  end

  def follows
    self.class.in followers_uris: uri
  end

  def followers
    self.class.any_in uri: followers_uris
  end

  def can_follow? user
    user.uri != uri and user.followers_uris.exclude? uri
  end

  def can_unfollow? user
    user.uri != uri and user.followers_uris.include? uri
  end

  def follow! user
    user.set_followed_by! self if user
  end

  def unfollow! user
    user.set_unfollowed_by! self if user
  end

  def set_followed_by! user
    push :followers_uris, user.uri if user.can_follow? self
  end

  def set_unfollowed_by! user
    pull :followers_uris, user.uri if user.can_unfollow? self
  end

  def site_url
    val = contact_info.site_url.to_s.strip
    (val.start_with?('http://') ? val : "http://#{val}") unless val.blank?
  end

  def total_tracks_size
    tracks.sum(&:total_size)
  end

  def default_image_file_name
    'user.png'
  end

  def to_indexed_hash
    {
      id: uri,
      type: 'user',
      slug: slug,
      full_name: display_name,
      image_url: image_url
    }
  end

  def to_referrals_hash
    {
      accepted_referral:
      {
        full_name: display_name,
        email: email,
        registered: created_at.to_i
      }
    }
  end

  def to_indexed_json
    to_indexed_hash.to_json
  end

  def create_image_from_params params
    image = Persistence::Image.new(params)
    gallery = params[:gallery_id].present? ?  galleries.find_by(uri: params[:gallery_id]) : self.gallery
    gallery.images.push image
    image
  end

  def galleries
    @galleries ||= Persistence::Gallery.where gallerieable_uri: uri
  end

  def gallery
    @gallery ||= galleries.first || galleries.create
  end

  def images
    @images ||= gallery.present? ? gallery.images : []
  end

  def as_json(options = {})
    hash = case options[:json_type].to_s.to_sym
    when :full
      attributes.except('roles', 'admin', 'superuser', 'social_info').merge({
        id: uri,
        uri: uri,
        personal_info: personal_info.as_json,
        oauth_uid: oauth_uid,
        full_name: full_name,
        avatar_url: image_url,
        display_name: display_name,
        developer: developer?,
        superuser: superuser?,
        admin: admin?,
        zodiac_sign: zodiac_sign
      })
    when :radio
      {
        id: uri,
        source: source,
        email: email,
        first_name: personal_info.first_name,
        last_name: personal_info.last_name,
        display_name: display_name,
        image_url: image_url,
        date_of_birth: personal_info.date_of_birth,
        gender: personal_info.gender,
        developer: developer?,
        superuser: superuser?,
        admin: admin?,
        station_uris: []
      }
    else
      attributes.except('roles', 'admin', 'superuser', 'social_info').merge({
        id: uri,
        uri: uri,
        oauth_uid: oauth_uid,
        full_name: full_name,
        personal_info: personal_info.as_json,
        avatar_url: image_url,
        display_name: display_name,
        date_of_birth: personal_info.date_of_birth,
        developer: developer?,
        superuser: superuser?,
        admin: admin?,
        has_radio_subscription: has_radio_subscription?,
        zodiac_sign: zodiac_sign
      })
    end
    hash
  end

  def display_name
    [stage_name, name, email.to_s.split('@').first, slug].compact.reject(&:empty?).first
  end

  def zodiac_sign
    personal_info.date_of_birth.present? ? personal_info.date_of_birth.zodiac_sign : ""
  end

  def referral_url
    "#{Settings.tunehog_url}/referral/#{referral_token}"
  end

  def accepted_referrals
    @referrals ||= Persistence::Referral.where initiator_uri: uri
    ::Persistence::User.in(uri: @referrals.map{|h| h['responder_uri']}.compact)
  end

  def subscription(type)
    @subscriptions_cache ||= {}
    @subscriptions_cache[type] ||= Persistence::Subscription.get_latest_of_type_for_user type, uri
  end

  def has_usable_subscription?(type)
    !!subscription(type).try(:usable?)
  end

  def billing_address_hash(override = {})
    address = self.billing_addresses.first.try(:attributes) || contact_info.attributes
    address.merge(override).reject { |key| BILLING_ADDRESS_KEYS.exclude? key.to_sym }.with_indifferent_access
  end

  def set_billing_address(address = {})
    filtered_address = address.reject { |key| BILLING_ADDRESS_KEYS.exclude? key.to_sym }
    if self.billing_addresses.blank?
      self.billing_addresses = [filtered_address]
    else
      self.billing_addresses.first.update_attributes filtered_address
    end
    self
  end

  # from Accounts

  # Build Mail::Address object from user data
  # @return [Mail::Address]
  def mail_address
    return @mail_address unless @mail_address.nil?
    @mail_address = Mail::Address.new
    @mail_address.address = email.to_s.squish
    @mail_address.display_name = full_name.to_s.squish
    @mail_address
  end

  # Represents user in email recipient format
  # @example
  #   #=> "John Appleseed <john.appleseed@example.com>"
  # @return [String]
  def email_with_name
    mail_address.format
  end

  def get_playlist_by_uri uri
    user_playlists.find_by uri: uri or user_playlists.asc(:position).first()
  end

  def has_radio_subscription?
    has_usable_subscription?('radio')
  end

  def verification_success_url service = nil
    case service
    when :dwl
      #TODO: Fix this with APolyakov
      "#{Settings.accounts_url}/settings/access?verification=success"
    when :webastro
      "#{Settings.astro_url}/auth/success_verification"
    else
      "#{Settings.accounts_url}/settings/access?verification=success"
    end
  end

  def verification_failure_url service = nil
    case service
    when :dwl
      "#{Settings.accounts_url}/settings/access?verification=failure"
    when :webastro
      "#{Settings.astro_url}/auth/failed_verification"
    else
      "#{Settings.accounts_url}/settings/access?verification=failure"
    end
  end

  def mailer
    UserDeviseMailer
  end

  def send_notification(notification, *args)
    mailer.send(notification, self, *args).deliver
  end

  def reset_password_period_valid?
    reset_password_sent_at && reset_password_sent_at.utc >= 6.hours.ago
  end

  def reset_password!(new_password, new_password_confirmation)
    self.password = new_password
    self.password_confirmation = new_password_confirmation

    if valid?
      self.reset_password_token = nil
      self.reset_password_sent_at = nil
    end

    save
  end

  def password_required?
    !persisted? || !password.nil? || !password_confirmation.nil?
  end

  alias_method :oauth_uid, :uri
  alias_method :to_s, :email_with_name
  alias_method :zendesk_id, :oauth_uid
end
