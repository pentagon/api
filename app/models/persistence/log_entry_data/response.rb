module Persistence::LogEntryData
  class Response
    include Persistence::Base

    field :status,  type: Integer
    field :body,                   default: ''
    field :headers, type: Hash,    default: {}

    embedded_in :log_entry, class_name: 'Persistence::LogEntry'
    attr_accessible :status, :body, :headers
  end
end
