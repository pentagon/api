module Persistence::LogEntryData
  class Request
    include Persistence::Base

    field :url, default: ''
    field :request_type, default: ''
    field :body, default: ''
    field :headers, type: Hash, default: {}

    embedded_in :log_entry, class_name: 'Persistence::LogEntry'
    attr_accessible :url, :request_type, :body, :headers
  end
end
