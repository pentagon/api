class Persistence::HitlogicCreditsBundle
  include Persistence::Document

  field :amount,              type: Fixnum
  field :payment_order_uri,   type: String
  field :type,                type: String
  field :user_uri,            type: String
  field :valid_to,            type: DateTime

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  belongs_to :payment_order, class_name: 'Persistence::PaymentOrder', foreign_key: :payment_order_uri, primary_key: :uri
  has_many :written_off_credits, class_name: 'Persistence::Hitlogic::CreditsBundleOut', foreign_key: :hitlogic_credits_bundle_id

  attr_accessible :amount, :type, :user_uri, :valid_to, :payment_order_uri

  scope :valid, -> { self.or({valid_to: nil}, {:valid_to.gt => DateTime.now}) }
  scope :unlimited, -> { where(valid_to: nil) }
  scope :limited,   -> { where(:valid_to.ne => nil, :valid_to.gt => DateTime.now).asc(:valid_to) }

  behaves_like :digital_goods

  private_class_method :new

  alias_method :digital_goods_item_id, :type

  class << self
    def types
      {
        bundle_3: {
          price: 100,
          credits: 3,
          title: "HitLogic credits bundle"
        },
        bundle_16: {
          price: 500,
          credits: 16,
          title: "HitLogic credits bundle"
        },
        bundle_34: {
          price: 1000,
          credits: 34,
          title: "HitLogic credits bundle"
        },
        bundle_100: {
          price: 2500,
          credits: 100,
          title: "HitLogic credits bundle"
        },
        bundle_225: {
          price: 5000,
          credits: 225,
          title: "HitLogic credits bundle"
        },
        # subscription_100: {
        #   price: 70,
        #   credits: 100,
        #   valid_period: 1.month,
        #   title: "HitLogic credits subscription"
        # },
      }
    end

    def digital_goods_find_by_id(type, options = {})
      type = type.downcase.to_sym
      new(type: type) if types[type]
    end

    def digital_goods_find_by_ids(types_array, options = {})
      types_array = Array.wrap(types_array).map { |type| type.downcase.to_sym }
      types.keys.select { |type| types_array.include?(type) }.map { |type| new(type: type) }
    end

    def digital_goods_nominal_code
      "4031"
    end
  end

  def data
    self.class.types[type.to_sym]
  end

  def digital_goods_base_price(options = {})
    {amount: data[:price], credits: data[:credits], currency: "GBP"}
  end

  def digital_goods_name
    data[:title]
  end

  def digital_goods_description
    count = data[:credits]
    count == 1 ? "" : "#{count} credits"
  end

  def digital_goods_reporting_description
    description = "#{type.capitalize}"
    description << " (#{digital_goods_description})" if digital_goods_description.present?
    description
  end

  def digital_goods_deliver(payment_order)
    payment_order.update_attributes(item_provider_response: data)
    result = self.update_attributes({
      amount: (payment_order.quantity * data[:credits]),
      payment_order_uri: payment_order.uri,
      user_uri: payment_order.user_uri,
      valid_to: (data[:valid_period] ? DateTime.now + data[:valid_period] : nil)
    })
    {success: result, errors: self.errors.full_messages}
  end

  def amount_left
    @amount_left ||= amount - written_off_credits.sum(:amount)
  end

  def write_of total_creadits, report_id=nil
    written_off_credits.create( amount: amount_left - total_creadits >= 0 ? total_creadits : amount_left, user_uri: user_uri, report_id: report_id).amount
  end

end
