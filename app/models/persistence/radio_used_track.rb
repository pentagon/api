class Persistence::RadioUsedTrack
  include Persistence::Document

  field :album_uri,   type: String
  field :artist_uri,  type: String
  field :station_uri, type: String
  field :track_uri,   type: String

  attr_accessible :album_uri, :artist_uri, :station_uri, :track_uri

  def self.composite_ids
    pluck(:track_uri, :album_uri, :artist_uri).map { |item| item.join("|") }
  end

end
