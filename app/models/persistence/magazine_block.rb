class Persistence::MagazineBlock
  include Persistence::Document

  field :title, type: String
  field :description, type: String
  field :cover, type: String
  field :action_link_caption, type: String
  field :action_link_class, type: String
  field :button_url, type: String
  field :button_text, type: String
  field :position, type: Integer, default: 0
  field :type, type: String, default: 'track'
  field :feed, type: Array, default: []
  field :method_type, type: Integer, default: 1
  field :randomized, type: Boolean, default: false
  field :fetch_realtime, type: Boolean, default: false

  embeds_many :items_gb, class_name: 'Persistence::MagazineBlockItem'
  embeds_many :items_us, class_name: 'Persistence::MagazineBlockItem'
  embeds_many :items_ca, class_name: 'Persistence::MagazineBlockItem'

  attr_accessible :title, :description, :cover, :action_link_caption, :action_link_class, :button_url, :button_text, :position,
      :type, :feed, :method_type, :randomized, :fetch_realtime

  attr_accessor :items

end
