class Persistence::TestRadioTrackReaction
  include Persistence::Document
  BOUNDS = 1..5

  field :station_uri, type: String
  field :track_uri,   type: String
  field :user_uri,    type: String
  field :value,       type: Fixnum

  belongs_to :station, {
    class_name: 'Persistence::TestRadioStation',
    foreign_key: :station_uri,
    primary_key: :uri
  }
  attr_accessible :station_uri, :track_uri, :user_uri, :value
  validates :value, inclusion: { in: BOUNDS }

  def self.average_ratings_per_track(selection = nil)
    selection ||= all
    map = <<-JS
      function() {
        emit(this.track_uri, this.value);
      }
    JS
    reduce = <<-JS
      function(key, values) {
        var sum = values.reduce(function(prev, current) {
          return prev + current;
        }, 0);
        return sum / values.length
      }
    JS
    Hash[selection.map_reduce(map, reduce).out(inline: true).map { |group| [group['_id'], group['value'].to_i] }]
  end
end
