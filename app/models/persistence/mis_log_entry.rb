class Persistence::MisLogEntry
  include Mongoid::Document
  include Mongoid::Timestamps
  store_in collection: name.demodulize.underscore.pluralize.to_sym

  field :job
  field :tube
  field :type
  field :environment
  field :body,        type: Hash, default: {}

  index({job: 1})

  attr_accessible :job, :tube, :type, :environment, :body
end
