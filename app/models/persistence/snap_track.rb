class Persistence::SnapTrack
  include Persistence::Document
  include ::Utils::CarrierWave::Glue
  include HasStoredImage

  field :user_uri,      type: String, default: ''
  field :track_uri,     type: String, default: ''
  field :short_url,     type: String, default: ''
  field :geo_location,  type: Array,  default: []
  field :md5sum,        type: String, default: ''
  field :track,         type: Hash

  index({geo_location: '2d'}, {})

  has_attachment :image, ::SnapTrack::ImageUploader
  has_attachment :preview, ::SnapTrack::PreviewUploader
  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  attr_accessible :user_uri, :track_uri, :geo_location, :md5sum
  after_create :get_short_url

  class << self
    def create_from_params params
      file = extract_image_to_file params[:image]
      preview = extract_image_to_file params[:preview], 'preview_'
      md5 = Digest::MD5.file(file).hexdigest
      res = find_or_create_by md5sum: md5, user_uri: params[:user_uri] do |r|
        r.image = file
        r.preview = preview
        r.track_uri = params[:track_uri]
        r.geo_location = params[:geo_location]
        r.created_at = DateTime.parse(params[:date]) rescue DateTime.now
        r.updated_at = DateTime.parse(params[:date]) rescue DateTime.now
      end
      res.touch
      res
    end

    def extract_image_to_file buffer, prefix = ''
      if buffer
        file = Tempfile.new ["#{prefix}snap_track_#{DateTime.now.to_i}", '.png']
        file.write Base64.decode64 buffer
        file.close
        file
      end
    end
  end

  def page_url
    File.join Settings.mobile.snap_track_base_url, id
  end

  def image_url
    File.join Settings.storage_url, uri.split(':')[0..1], image.filename
  end

  def preview_url
    File.join Settings.storage_url, uri.split(':')[0..1], preview.filename
  end

  def short_url
    self['short_url'].blank? ? get_short_url : super
  end

  def get_short_url
    res = Googl.shorten(page_url).short_url rescue ''
    update_attribute :short_url, res unless res.blank?
    res
  end

  def as_json options = {}
    {
      success: true,
      share_url: page_url,
      short_url: short_url
    }
  end
end
