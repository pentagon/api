class Persistence::HitMakerReport
  include Persistence::Document

  HSS_CATEGORIES = {
    diamond: 950..1000,
    platinum: 850..949,
    golden: 750..849,
    silver: 650..749,
    entry: 0..649
  }

  field :user_uri,                    default: ''
  field :track_uri,                   default: ''
  field :territory,    type: String,  default: ''
  field :version,      type: Integer, default: 1
  field :is_private,   type: Boolean, default: false
  field :is_processed, type: Boolean, default: false
  field :has_error,    type: Boolean, default: false
  field :error_message
  field :data,         type: Hash,    default: {}
  field :is_new,       type: Boolean, default: true

  index({user_uri: 1})
  index({track_uri: 1})
  index({is_processed: 1})

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  belongs_to :track, class_name: 'Persistence::Track', foreign_key: :track_uri, primary_key: :uri
  has_many :data_modules, class_name: 'Persistence::Hitlogic::DataModule', dependent: :destroy
  belongs_to :data_set, class_name: 'Persistence::Hitlogic::DataSet'

  scope :unprocessed, where(is_processed: false)
  scope :by_user, ->(user_uri){where user_uri: user_uri}
  scope :public, where(is_private: false)

  accepts_nested_attributes_for :data_modules
  attr_accessible :user_uri, :track_uri, :territory, :is_new, :data_modules_attributes

  scope :accessible_for, -> user {
    self.or({is_private: false, is_processed: true},
    { user_uri: user.try(:uri) })
  }

  alias_method :private?, :is_private?
  alias_method :processed?, :is_processed?

  class << self
    def get_mapping_for_region(region = 'hss3')
      {
        bpm:          'bpm',
        color:        'color',
        energy:       'energy',
        genre_cloud:  'genre_cloud',
        hss_rating:   "#{region}.hss_rating",
        hss_score:    "#{region}.hss_score",
        hss_avg_year: "#{region}.hss_avg_year",
        similar_hits: "#{region}.similarHits.hits"
      }
    end
  end

  get_mapping_for_region.keys.each do |f|
    define_method(f) {data["#{f}"]}
    define_method("#{f}=") {|val| data["#{f}"] = val}
  end

  def hss_category
    HSS_CATEGORIES.detect { |_,v| v.include?(hss_score.to_i) }.try(:first).to_s.capitalize if hss_score.to_i > 0
  end
end
