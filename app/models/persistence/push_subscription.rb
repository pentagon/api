class Persistence::PushSubscription
  include Persistence::Base
  include Mongoid::Timestamps

  field :user_uri,                    type: String
  field :app_name,                    type: String
  field :app_platform,                type: String
  field :app_version,                 type: String
  field :device_id,                   type: String
  field :device_token,                type: String
  field :device_locale,               type: String,  default: 'en'
  field :device_time_zone,            type: String,  default: 'Europe/London'

  attr_accessible :user_uri, :app_name, :app_platform, :app_version, :device_id, :device_locale, :device_token,
    :device_time_zone

  index({user_uri: 1})
  index({app_name: 1})
  index({app_platform: 1})
  index({app_version: 1})
  index({device_id: 1}, {unique: true})
  index({device_token: 1}, {unique: true})
end
