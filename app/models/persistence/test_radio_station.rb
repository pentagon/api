class Persistence::TestRadioStation
  include Persistence::Document

  field :algoritm_version, type: String
  field :environment, type: String
  field :radio_queue, type: Array, default: []
  field :seed_track_uri, type: String

  attr_accessible :algoritm_version, :environment, :radio_queue, :seed_track_uri

  def reactions(params = {})
    Persistence::TestRadioTrackReaction.where(params.merge({station_uri: uri}))
  end

  def average_ratings_per_track
    Persistence::TestRadioTrackReaction.average_ratings_per_track(reactions)
  end

end
