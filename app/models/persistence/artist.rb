require 'tire/queries/fuzzy_like_this'

class Persistence::Artist
  include Persistence::Document
  include HasStoredImage
  include ChartPosition
  # Search options
  include Tire::Model::Search
  index_name 'unsigned'
  document_type 'artist'

  field :stage_name,                  default: ''
  field :user_uri,                    default: ''
  field :biography,                   default: ''
  field :artist_roles, type: Array,   default: []

  index({user_uri: 1})

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  has_many :tracks, class_name: 'Persistence::Track', foreign_key: :artist_uri, primary_key: :uri
  has_many :albums, class_name: 'Persistence::Album', foreign_key: :artist_uri, primary_key: :uri
  attr_accessible :stage_name, :biography, :artist_roles, :user_uri, :source

  scope :by_user, ->(user_uri){where user_uri: user_uri}

  mapping do
    indexes :id,        index: :not_analyzed
    indexes :title,     analyzer: 'standard'
    indexes :image_url, index: :not_analyzed
    indexes :user_uri,  index: :not_analyzed
    indexes :type,      index: :not_analyzed
    indexes :additional_fields do
      indexes :created_at,  type: 'date', index: :not_analyzed
      indexes :genres,      type: 'string', analyzer: 'keyword'
      indexes :country,     index: :not_analyzed
    end
  end

  after_save :update_searching_index
  after_destroy do |a|
    a.tire.index.remove a
    a.albums.destroy_all
    a.tracks.orphan.destroy_all
  end

  class << self
    def search params
      page_num = params[:page].to_i.zero? ? 1 : params[:page].to_i
      prev_page_num = params[:page].to_i > 0 ? params[:page].to_i.pred : 0
      per_page = page_num.eql?(1) ? 25 : 15
      search_params = {
        load: false,
        from: (page_num > 1 ? (per_page * prev_page_num + 10) : 0),
        per_page: per_page
      }

      tire.search(search_params) do
        if params[:search].present? || params[:country].present? || params[:genre].present?
          query do
            boolean do
             must { match 'additional_fields.genres', params[:genre] } if params[:genre].present?
             must { fuzzy_like_this_field :title, params[:search], max_query_terms: 2} if params[:search].present?
             must { match 'additional_fields.country' , params[:country] } if params[:country].present?
            end
          end
        else
          query {all}
        end
        filter :exists, { field: 'additional_fields.user' }
        filter :exists, { field: 'additional_fields.stage_name' }
        # facet('countries') { terms 'additional_fields.country' }
        sort { by 'additional_fields.created_at', :desc }
      end
    end
  end

  def new_track params = {}
    allowed_properies_names = self.class.attribute_names.map &:to_sym
    Persistence::Track.new((params.symbolize_keys!.slice *allowed_properies_names).merge(
      user_uri: self.user_uri, artist_uri: self.uri))
  end

  def title
    (display_name rescue nil).to_s
  end

  def to_indexed_json
    to_indexed_hash.to_json
  end

  def to_indexed_hash
    {
      id: uri,
      title: title,
      type: 'artist',
      image_url: (image_url rescue nil),
      user_uri: user_uri,
      rights: rights,
      additional_fields: {
        genres: (genres.join(' ') rescue []),
        country: (country rescue nil),
        stage_name: (display_name rescue nil),
        biography: (biography rescue nil),
        artist_roles: (artist_roles rescue []),
        slug: (slug rescue nil),
        created_at: created_at,
        user: {
          uri: (user.uri rescue nil),
          artist_name: (user.artist_name rescue nil)
        }
      }
    }
  end

  def country
    user.contact_info.country
  end

  def slug
    user.mongoid_slug
  end

  def image_url
    user.try :image_url
  end

  def artist_name
    user.artist_name
  end

  def rights
    {explicit: explicit?}
  end

  def update_searching_index
    if tracks.count.zero?
      tire.index.remove self
    else
      tire.update_index
      albums.map &:update_searching_index
      tracks.orphan.indexable.map &:update_searching_index
    end
  end

  def genres
    @genres ||= tracks.pluck(:genre).compact.uniq.reject &:blank?
  end

  def sorted_genres
    genres = tracks.inject(Hash.new(0)) { |h, track| h[track.genre] += 1; h}
    ary = genres.sort_by(&:last).reverse
    @sorted_genres ||= Hash[ary].keys.reject &:blank?
  end

  def display_name
    @display_name ||= stage_name.blank? ? user.try(:full_name) : stage_name rescue ''
  end

  def artist_roles= value
    new_value = case
      when value.is_a?(Array)  then value
      when value.is_a?(String) then value.split(',').map &:strip
    end
    self['artist_roles'] = new_value
  end

  def fallback_image_url
    @fallback_image_url ||= (user.image_url if user).to_s
  end

  def as_json opts = {}
    {id: uri}.update super(except: [:_id, :uri])
  end

  def galleries
    Persistence::Gallery.where gallerieable_uri: user_uri
  end

  def public_galleries
    galleries.map &:images
  end

  def explicit?
    SwearDetector.includes_swear? [stage_name, albums.map(&:title), tracks.map(&:title)].flatten.compact.join(' ')
  end
end
