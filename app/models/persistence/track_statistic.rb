class Persistence::TrackStatistic
  include Persistence::Statistic

  ALLOWED_PARAMS = %w(user_ip track_uri album_uri playlist_id duration on_second listened_in_seconds user_uri
    user_datetime source_page listen_source playlist_type region_name city country_code latitude longitude)

  field :track_uri,           type: String,     default: ''
  field :artist_uri,          type: String,     default: ''
  field :album_uri,           type: String,     default: ''
  field :owner_uri,           type: String,     default: ''
  field :genre,               type: String,     default: ''
  field :subgenre,            type: String,     default: ''
  field :source_page,         type: String,     default: ''
  field :playlist_type,       type: String,     default: ''
  field :playlist_id,         type: String,     default: ''
  field :listen_in_seconds,   type: Integer,    default: 0
  field :listen_in_percents,  type: Integer,    default: 0
  field :client_id,           type: String,     default: 'WEB'

  scope :by_genre,    ->(genre) {where(genre: genre)}
  scope :with_genre,  ->{where(:genre.nin => ["", nil])}
  scope :with_artist, ->{where(:artist_uri.nin => ["", nil])}
  scope :unsigned,    ->{where(:track_uri.nin => [/medianet/])}
  scope :labeled,     ->{where(:track_uri.in => [/medianet/])}

  attr_accessible :track_uri, :artist_uri, :genre, :subgenre, :source_page, :playlist_type, :client_id,
    :playlist_id, :listen_in_percents, :listen_in_seconds, :owner_uri, :album_uri

  index({type: 1, user_uri: 1})
  index({type: 1, playlist_id: 1})
  index({track_uri: 1})
  index({artist_uri: 1})
  index({created_at: 1})


  class << self
    def create_or_update_from_params params
      args = prepare_params params
      if params[:id]
        record = find(params[:id])
        record.update_attributes(args) if record
      else
        record = new args
      end
      record.save
      record
    end

    def load_stat_from_params params
      case
      when params[:kind].eql?('map') then load_listen_map params[:id]
      when params[:kind].eql?('graph') then load_stat_graph params[:id]
      end
    end

    def charts options = {}
      query = if options[:genre].present?
        self.by_genre(options[:genre])
      else
        self.with_genre.with_artist
      end
      query = case options[:source]
        when "unsigned" then query.unsigned
        when "labeled" then query.labeled
        else query
      end
      mr_options = options[:mr_options] || {}
      query = case options[:mode]
        when "track" then query.mr_track(mr_options)
        when "artist" then query.mr_artist(mr_options)
        else query
      end
      query.sort(value: -1).to_a
    end

    def latest_played_tracks_uris playlist_id, size = 5
      self.where(playlist_id: playlist_id).sort(created_at: -1).limit(size).pluck(:track_uri)
    end

    def latest_played_tracks_uris_since playlist_id, time, size = 1000
      self.where(playlist_id: playlist_id).where(:created_at.gte => time).limit(size).pluck(:track_uri)
    end

    def load_listen_map track_uri
      self.where(:track_uri => track_uri, :location.ne => []).mr_track(mode: 'geodata').to_a
    end

    def load_stat_graph track_uri
      self.where(:track_uri => track_uri).mr_track(mode: 'date').to_a
    end

    def mr_track options = {}
      reduce = "function(key, values){ return Array.sum(values); }"
      map = case options[:mode]
        when 'geodata' then "function(){ emit({track_uri: this.track_uri, location: this.location}, 1);}"
        when 'date' then "function(){ emit({track_uri: this.track_uri, year: NumberInt(this.created_at.getFullYear()), month: NumberInt(this.created_at.getMonth()+1), day: NumberInt(this.created_at.getDay()+1)}, 1); }"
        else "function(){ emit(this.track_uri, 1); }"
      end
      collection = options[:mode] ? "statistic_by_track_#{options[:mode]}" : "statistic_by_track"
      map_reduce(map, reduce).out(replace: collection).send(:documents)
    end

    def mr_artist options = {}
      reduce = "function(key, values){ return Array.sum(values); }"
      map = case options[:mode]
        when 'geodata' then "function(){ emit({artist_uri: this.artist_uri, location: this.location}, 1);}"
        when 'date' then "function(){ emit({artist_uri: this.artist_uri, year: NumberInt(this.created_at.getFullYear()), month: NumberInt(this.created_at.getMonth()+1), day: NumberInt(this.created_at.getDay()+1)}, 1); }"
        else "function(){ emit(this.artist_uri, 1); }"
      end
      collection = options[:geodata] ? "statistic_by_artist_geodata" : "statistic_by_artist"
      map_reduce(map, reduce).out(replace: collection).send(:documents)
    end

    def agr_charts(genre = nil)
      exist_artist = Rails.cache.fetch("artists_uri_for_chart_position", expires_in: ChartPosition::UPDATE_PERIOD) do |variable|
        Persistence::Artist.pluck(:uri)
      end
      match_query = if genre
                      {"$match"=>{artist_uri:{"$in" => exist_artist}, genre: genre }}
                    else
                      {"$match"=>{artist_uri:{"$in" => exist_artist }}}
                    end
      collection.aggregate([ match_query,{"$group"=>{"_id"=>"$artist_uri", "count"=>{"$sum"=>1}}},{"$sort"=>{"count"=>-1}}])
    end

    private

    def prepare_params params
      args = params.slice *ALLOWED_PARAMS
      if args[:latitude].present? and args[:longitude].present?
        new_args = {
          location: [args[:latitude], args[:longitude]],
          region_code: args[:region_name]
        }
        args = args.except('latitude', 'longitude', 'region_name').merge new_args
      end
      new_args = {
        listen_in_percents: (params[:on_second].to_f / params[:duration].to_i * 100).to_i,
        listen_in_seconds: params[:listened_in_seconds].to_i
      }
      args = args.except('on_second', 'duration', 'listened_in_seconds').merge new_args
      if (t = Track.fetch params[:track_uri]).present?
        args.update artist_uri: t.artist_uri, album_uri: t.album_uri, genre: t.genre, subgenre: t.subgenre, owner_uri: t.try(:user_uri).to_s
      end
      args
    end
  end
end
