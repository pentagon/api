require 'going_postal'
module Persistence::UserData
  class ContactInfo
    include Persistence::EmbeddedDocument
    include Persistence::Helpers

    field :phone,            default: ''
    field :site_url,         default: ''
    field :country,          default: ''
    field :state,            default: ''
    field :city,             default: ''
    field :street,           default: ''
    field :address,          default: ''
    field :postal_code,      default: ''
    field :facebook_slug,    default: ''
    field :twitter_slug,     default: ''
    field :soundcloud_slug,  default: ''


    embedded_in :user, class_name: 'Persistence::User'

    embeds_many :messenger_infos, class_name: 'Persistence::UserData::ContactInfoData::MessengerInfo'
    accepts_nested_attributes_for :messenger_infos, autosave: true, allow_destroy: true

    attr_accessible :messenger_infos_attributes, :phone, :site_url, :country, :state, :city, :street, :address,
      :postal_code, :facebook_slug, :soundcloud_slug, :twitter_slug

    validate :country_valid?, if: 'do_validation?'
    validate :city_valid?, if: 'do_validation?'
    validate :state_valid?, if: 'do_validation?'
    validate :postal_code_valid?, if: 'do_validation?'


    def do_validation?
      changed? && !new_record?
    end

    def force_valid?
      %w(country city state postal_code).each{ |m| send "#{m}_valid?" }
      errors.size == 0
    end

    def country_valid?
      if country.present?
        errors.add :country, :invalid unless Persistence::Helpers::COUNTRIES_MAP.keys.map(&:to_s).include? country.upcase
      end
    end

    def city_valid?
      if country.present?
        errors.add :city, :blank if city.blank?
      end
    end

    def state_valid?
      if country.present?
        return true unless Persistence::Helpers::COUNTRIES_STATES.include? country.upcase.to_sym
        errors.add :state, :invalid unless Persistence::Helpers::COUNTRIES_STATES[country.upcase.to_sym].keys.map(&:to_s).include? state.upcase
      end
    end

    def postal_code_valid?
      if country.present?
        errors.add :postal_code, :invalid unless GoingPostal.valid_postcode? postal_code, country
      end
    end

    def full_address
      res = []
      res << country_name(country) unless country.blank?
      res << state.strip.chomp(',')
      res << city.strip.chomp(',')
      res << street.strip.chomp(',')
      res << address.strip.chomp(',')
      res.reject(&:blank?).join ', '
    end
  end
end
