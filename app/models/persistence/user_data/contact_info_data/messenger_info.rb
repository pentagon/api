module Persistence::UserData::ContactInfoData
  class MessengerInfo
    include Persistence::EmbeddedDocument

    attr_accessible :messenger_type, :number

    field :messenger_type, default: ''
    field :number
    embedded_in :contact_info, class_name: 'Persistence::UserData::ContactInfo'

    validates :number, presence: true

    MESSENGER_TYPES = %w(AIM GTALK ICQ MSN SKYPE YAHOO) unless defined?(MESSENGER_TYPES)
  end
end
