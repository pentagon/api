module Persistence::UserData
  class SocialAuthentication
    include Persistence::EmbeddedDocument

    field :provider,   type: String, default: ''
    field :uid,        type: String, default: ''

    embedded_in :user, class_name: 'Persistence::User'

    attr_accessible :provider, :uid

    index({uid: 1},         {unique: true})
    validates :uid, uniqueness: {scope: :provider}, if: :provider?
  end
end
