module Persistence::UserData
  class BillingAddress
    include Persistence::EmbeddedDocument

    field :address,     default: ''
    field :city,        default: ''
    field :country,     default: ''
    field :postal_code, default: ''
    field :state,       default: ''

    embedded_in :user, class_name: 'Persistence::User'

    attr_accessible :address, :city, :country, :postal_code, :state
  end
end
