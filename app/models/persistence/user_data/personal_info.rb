module Persistence::UserData
  # Personal info holder
  #
  # nick_name        String  vasya.pupkin
  # first_name       String  Vasiliy
  # last_name        String  Pupkin
  # avatar_uri       String  url for image in bucket
  # gender           Int     0 => female, 1 => male
  class PersonalInfo
    include Persistence::EmbeddedDocument
    field :nick_name,       default: ''
    field :first_name,      default: ''
    field :last_name,       default: ''
    field :gender,          type: Integer
    field :date_of_birth,   type: Date
    field :biography,       default: ''
    field :place_of_birth,  default: ''
    field :school_list,     type: Array, default: []
    field :birth_country,         default: ''
    field :birth_city,         default: ''
    field :birth_state,         default: ''

    embedded_in :user, class_name: 'Persistence::User'
    attr_accessible :nick_name, :first_name, :last_name, :gender, :date_of_birth, :biography,
      :birth_country, :birth_city, :birth_state

    # validates_date :date_of_birth, allow_blank: true
    validates :gender,
      inclusion: {in: [1, 0], message: I18n.t('models.user.gender_invalid_error')},
      allow_nil: true

    GENDER_MAP = {
      '0'       => 0,
      '1'       => 1,
      'female'  => 0,
      'male'    => 1
    }.freeze unless defined?(GENDER_MAP)

    def gender=(value)
      value = GENDER_MAP[value.downcase] if value.is_a?(String)
      self[:gender] = value
    end

    def name
      [first_name, last_name].reject(&:blank?).join(' ')
    end

    def name=(value)
      first_name, last_name = value.split
      self.first_name = first_name
      self.last_name = last_name
      name
    end
  end
end
