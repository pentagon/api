class Persistence::Report
  include Persistence::Document
  include Tire::Model::Search
  index_name 'unsigned'
  document_type 'report'
  after_save :update_searching_index

  field :user_uri,                      default: ''
  field :hit_score,      type: Float,   default: 0
  field :color_tune,                    default: ''
  field :map,            type: Array,   default: []
  field :genre_cloud,    type: Array,   default: []
  field :listeners,      type: Integer, default: 0
  field :similar_tracks, type: Array,   default: []
  field :track_uri,                     default: ''
  field :energy_mood,    type: Float,   default: ''
  field :is_private,     type: Boolean, default: false
  field :is_pending,     type: Boolean, default: false
  # What HSS algorithm versoin used? (2 - for HSS2 (UK), 3 - for HSS3 (US))
  field :hss_version,    type: Fixnum, default: 3
  field :last_error

  index({user_uri: 1},  {unique: true})
  index({track_uri: 1}, {unique: true})

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  belongs_to :track, class_name: 'Persistence::Track', foreign_key: :track_uri, primary_key: :uri

  scope :pending, where(is_pending: true)
  scope :active, where(is_pending: false, :last_error => nil)
  scope :failed, where(is_pending: false, :last_error.ne => nil)
  scope :hss2, where(hss_version: 2)
  scope :hss3, where(hss_version: 3)

  scope :by_user, ->(user_uri){ where(user_uri: user_uri) }
  scope :by_hss_version, ->(hss_version){ where(hss_version: hss_version) }

  validates :user_uri, :track_uri, presence: true
  validates :hss_version, presence: true, inclusion: {in: [2, 3]}

  alias_method :private?, :is_private?
  alias_method :pending?, :is_pending?

  attr_accessible :last_error

  class Error < ::RuntimeError
  end

  #scopes
  scope :accessible_for, -> user {
    self.or({is_private: false, is_pending: false},
    { user_uri: user.try(:uri) })
  }

  def tag
    self.class.name
  end

  def has_error?
    last_error.present?
  end

  def failed?
    not pending? and has_error?
  end

  def processed?
    not pending? and not has_error?
  end

  def enqueueable?
    failed?
  end

  def available?
    track.present?
  end

  def to_indexed_json
    to_indexed_hash.to_json
  end

  def to_indexed_hash params = {}
    hit_score_rounded = hit_score.try(:round)
    {
      tokens: hit_score_rounded.to_s.split(''),
      id: uri,
      uri: uri,
      value: hit_score_rounded,
      hit_score: hit_score_rounded,
      is_pending: is_pending,
      is_private: is_private,
      source: source,
      track: track ? {
        id: track.uri,
        uri: track.uri,
        title: track.title,
        image_url: track.image_url,
        music_url: track.music_url
      } : {} ,
      artist: track && track.artist ? {
        id: track.artist.uri,
        uri: track.artist.uri,
        name: track.artist.display_name
      } : {}
    }
  end

  def update_searching_index
    tire.update_index
  end

  def hss_category
    case
      when hit_score <= 150 then 'Unique'
      when hit_score < 250 then 'Bronze/Low'
      when hit_score < 500 then 'Silver/Medium'
      when hit_score < 750 then 'Gold/High'
      else 'Platinum/Extra High'
    end
  end

  def hitscore_crown
    @hitscore_crown ||= ActiveSupport::StringInquirer.new(/\w+/.match(hss_category)[0].downcase)
  end

  def genre_probability
    genre_cloud.map do |genre|
      genre.merge('probability' => (genre['probability'].to_f*100).round)
    end
  end
end
