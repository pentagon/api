class Persistence::SlmUserTrackFeedback
  include Persistence::Document

  field :user_uri,       type: String,  default: ''
  field :track_uri,      type: String,  default: ''
  field :status,         type: String,  default: ''

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  attr_accessible :track_uri, :user_uri, :status

  index({user_uri: 1})
end
