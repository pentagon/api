module Persistence::PaymentOrderData
  class BillingAddress
    include Persistence::EmbeddedDocument

    field :address,     default: ''
    field :city,        default: ''
    field :country,     default: ''
    field :postal_code, default: ''
    field :state,       default: ''

    embedded_in :payment_order, class_name: 'Persistence::PaymentOrder'

    attr_accessible :address, :city, :country, :postal_code, :state
  end
end
