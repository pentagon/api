require 'weakref'

module Persistence::MongoidExt
  extend ActiveSupport::Concern

  module ClassMethods
    def each_by(by = 100, &block)
      idx = 0
      while ((results = clone.limit(by).skip(idx)) && results.any?)
        results.each do |result|
          yield result
        end
        idx += by
      end
      self
    end

    def in_batches_by(by = 100, &block)
      idx = 0
      while (results = clone.limit(by).skip(idx)).any?
        results = WeakRef.new results
        yield results
        idx += by
        GC.start
      end
      self
    end
  end
end
