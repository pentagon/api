class Persistence::ShareLink
  include Persistence::Document

  field :track_uri,        type: String,  default: ''
  field :user_uri,         type: String,  default: ''
  field :name,             type: String,  default: ''
  field :share_hash,       type: String,  default: Proc.new { SecureRandom.hex }
  field :is_downloadable,  type: Boolean, default: true
  field :track_is_deleted, type: Boolean, default: false

  attr_accessible :name, :track_uri, :is_downloadable

  belongs_to :track, class_name: 'Persistence::Track', foreign_key: :track_uri, primary_key: :uri
  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri

  def to_key
    [self.class.name, share_hash]
  end

  def mark_deleted
    update_attribute :track_is_deleted, true
  end
end
