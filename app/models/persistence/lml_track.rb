class Persistence::LmlTrack
  include Persistence::Document
  include Mongoid::Timestamps

  STATUSES = {
    :ok => 0,
    :error => 1,
    :not_analyzed => 2,
    :pending => 3
  }

  field :title,        type: String,     default: ''
  field :user_uri,     type: String
  field :library_uri,  type: String
  field :track_metadata,     type: Hash, default: {}
  field :analysis,     type: Hash, default: {}
  field :fingerprint,  type: Hash, default: {}
  field :raw,          type: Hash, default: {}

  index({user_uri: 1})
  index({library_uri: 1})

  attr_accessible :user_uri, :library_uri, :title, :track_metadata, :analysis, :fingerprint, :uri, :raw

  def analyzed?
    analysis_status = analysis["an80"] || {}
    not [analysis_status["an31"], analysis_status["fgp"]].include?(STATUSES[:not_analyzed])
  end
end
