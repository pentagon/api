module Persistence::Base
  extend ActiveSupport::Concern

  included do
    include Mongoid::Document
    include Mongoid::Callbacks
    include Persistence::MongoidExt
    store_in collection: name.demodulize.underscore.pluralize.to_sym
    attr_accessible :source
  end

  module ConfigurationMethods
    def default_source
      $default_source
    end

    # Accepts default value for 'source' field in documents from config.
    # cattr_accessor :default_source
    def default_source=(value)
      $default_source = value
    end
    alias_method :source, :default_source=

    # Conviniance method for configuration
    # @return [Base]
    def config
      yield self
    end
  end

  extend ConfigurationMethods

  def to_s
    '%s (%s)' % [self.class.name, id]
  end

  # Generates uniq ID based on +source+, +type+ and timestamp
  # @return [String]
  def generate_uri
    [source.presence || default_source.presence, self.class.name.demodulize.underscore,
      (DateTime.now.to_f * 1000000 + rand(1000)).to_i].reject(&:blank?).join(':')
  end

  def default_source
    source = Persistence::Base.default_source if source.blank?
    if source.blank?
      raise "The default_source not defined for application. Read more here: https://github.com/wetunein/tunehog-persistence#initializer."
    end
    source
  end

  def attributes_for_clone
    clone_document
  end
  protected :attributes_for_clone
end
