class Persistence::Video
  include Persistence::Document

  field :user_uri,                default: ''
  field :url,                     default: ''
  field :video_id,                default: ''
  field :title,                   default: ''
  field :description,             default: ''
  field :preview_img,             default: ''
  field :track_uris, type: Array, default: []

  validates :video_id, presence: true
  attr_accessible :video
  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  attr_accessible :title, :description, :video_id, :url, :track_uris, :preview_img
  scope :by_user, ->(user_uri){where user_uri: user_uri}

  def tracks
    Persistence::Track.any_in uri: track_uris
  end

  def tracks= values
    tracks.each {|v| v.pull :video_uris, uri}
    values.each {|v| v.add_to_set :video_uris, uri}
    set :track_uris, tracks.map(&:uri)
  end

  def track_uris= values
    tracks.each {|v| v.pull :video_uris, uri}
    trks = Persistence::Track.any_in uri: values
    set :track_uris, trks.map(&:uri)
    tracks.each {|v| v.add_to_set :video_uris, uri}
  end

  def embed_url params = {}
    ["http://www.youtube.com/embed/#{video_id}?wmode=transparent", params.to_query].reject(&:blank?).join('&')
  end

  def image_preview style = 'default'
    # hqdefault
    "http://i2.ytimg.com/vi/#{self.video_id}/#{style}.jpg"
  end

  def video=(value)
    self.video_id = value
  end
end
