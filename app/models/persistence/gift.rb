class Persistence::Gift
  include Persistence::Document
  alias_method :id, :uri

  GIFT_STATUSES = %w(temporary sent read)
  field :user_uri,             type: String, default: ''
  field :user_country,         type: String, default: ''
  # gifts, cupidgifts, etc.
  field :source,               type: String, default: ''
  field :secret,               type: String, default: ''
  field :pack_id,              type: Fixnum, default: 1

  field :status,               type: String, default: 'temporary'
  field :sent_at,              type: DateTime
  field :read_at,              type: DateTime

  field :sender_name,          type: String, default: ''
  field :recipient_name,       type: String, default: ''
  field :recipient_email,      type: String, default: ''
  field :title,                type: String, default: ''
  field :message,              type: String, default: ''
  field :template_id,          type: Fixnum
  field :picture_uri,          type: String, default: ''

  belongs_to :picture, class_name: 'Gifts::Picture', foreign_key: :picture_uri, primary_key: :uri
  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri

  index({user_uri: 1}, {background: true})

  validates :status, inclusion: {in: GIFT_STATUSES}
  validates_presence_of :user_uri, :user, :user_country

  attr_accessible :message, :pack_id, :read_at, :recipient_name, :recipient_email,
    :sender_name, :sent_at, :source, :status, :title, :user_country, :user_uri, :template_id, :picture_uri


  validates :recipient_email, presence: true, format: { with: /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i }
  validate :same_recipient_email

  GIFT_STATUSES.each do |t|
    define_method("#{t}?") do
      status.eql? t.to_s
    end
  end

  behaves_like :digital_goods


  def payment_order
    @payment_order ||= Persistence::PaymentOrder.find_by(item_type: gift_type, item_uri: uri)
  end

  def payment_order_id
    payment_order.try :uri
  end

  def template
    @template ||= Gifts::Template.find(template_id)
  end

  def successfully_paid?
    payment_order && payment_order.successful?
  end

  def pack
    @pack ||= Gifts::Pack.find(pack_id)
  end

  def read
    self.update_attributes(status: "read", read_at: DateTime.now)
  end

  def gift_type
    self.class.digital_goods_type
  end

  def mixtape?
    false
  end

  def source_gifts?
    source == 'gifts'
  end

  def source_webastro?
    source == 'astro'
  end

  def digital_goods_deliver(payment_order)
    GiftsMailer.main(self).deliver if source_gifts?
    result = update_attributes(status: "sent", sent_at: DateTime.now)
    {success: result}
  end

  def same_recipient_email
    errors.add :base, "You can't send gift to yourself" if recipient_email == user.email
  end
end
