class Persistence::SearchStatistic
  include Persistence::Statistic

  ALLOWED_PARAMS = %w(user_ip user_datetime source_page search_term search_type region_name city country_code latitude longitude)

  field :search_term, type: String, default: ''
  field :search_type, type: String, default: ''

  attr_accessible :user_ip, :user_uri, :search_type, :search_term

  class << self
    def create_from_params params
      args = prepare_params params
      record = new(args)
      record.save
      record
    end

    private

    def prepare_params params
      args = params.slice *ALLOWED_PARAMS
      if args[:latitude].present? and args[:longitude].present?
        new_args = {
          location: [args[:latitude], args[:longitude]],
          region_code: args[:region_name]
        }
        args = args.except('latitude', 'longitude', 'region_name').merge new_args
      end
      args
    end
  end
end
