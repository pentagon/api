class Persistence::StartuneReport
  include Persistence::Document
  STARTUNE_REPORT_STATUSES = %w(PROCESSING COMPLETE ERROR)

  field :order_number,    type: String
  field :report_file,     type: String
  field :report_type,     type: String
  field :status,          type: String, default: 'PROCESSING'
  field :transaction_id,  type: String
  field :user_uri,        type: String
  field :email,           type: String

  attr_accessible :order_number, :report_file, :report_type, :status, :transaction_id, :user_uri,
    :email
end
