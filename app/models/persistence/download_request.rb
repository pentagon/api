class Persistence::DownloadRequest
  include Persistence::Document

  field :user_uri,          type: String,   default: ''
  field :track_uri,         type: String,   default: ''
  field :is_downloaded,     type: Boolean,  default: false
  field :download_date,     type: Date,     default: Proc.new { DateTime.now }
  field :contact_info,      type: Hash,     default: {} # Compatibility attribute. TODO: Remove it later
  field :billing_address,   type: Hash,     default: {}
  field :secret,            type: String,   default: ''
  field :payment_order_uri, type: String
  field :ip_address,        type: String

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  belongs_to :payment_order, class_name: 'Persistence::PaymentOrder', foreign_key: :payment_order_uri, primary_key: :uri

  attr_accessible :user_uri, :track_uri, :billing_address, :payment_order_uri

  scope :not_downloaded, -> { where(is_downloaded: false) }

  before_create :prepare

  validates_presence_of :track_uri, :user_uri, :user, :billing_address

  def self.find_for_download(secret)
    not_downloaded.find_by secret: secret
  end

  # Compatibility method.
  # TODO: Remove it later
  def billing_address
    attributes["billing_address"].presence || contact_info
  end

  def track
    @track ||= Track.fetch(track_uri)
  end

  def download_url
    "#{Settings.accounts_url}/download/#{secret}"
  end

  def purchase_get_url
    return {success: false, error: 'Track not found.'} unless track && track.rights.purchase?
    filename = "#{track.artist_name}-#{track.title}.mp3".gsub(/^.*(\\|\/)/, '').gsub(/[^0-9A-Za-z.\-]/, '_')
    if track.unsigned? || !Settings.medianet_api.use_full_tracks
      return {success: true, url: track.music_url, filename: filename}
    else
      medianet_id = Track.get_id_from_uri(track_uri)
      price = track.digital_goods_price(country: billing_address["country"])
      price_amount = price[:amount].to_f / 100
      result = ::Medianet::Base.purchase_use_balance(medianet_id, payment_order.ip_address, price_amount, billing_address)
      payment_order.update_attributes item_provider_response: result
      if result['Success']
        {success: true, url: result['Order']['Items'][0]['DownloadLocations'][0]['Location'], filename: filename, result: result}
      else
        {success: false, error: result['Error'], result: result}
      end
    end
  end

  def set_downloaded(download_ip_address = nil)
    self.is_downloaded = true
    self.download_date = DateTime.now
    self.ip_address = download_ip_address if download_ip_address.present?
    self.save
  end

private

  def prepare
    self.secret = Digest::MD5.hexdigest(DateTime.now.to_s + Api::Application.config.secret_token)
  end

end
