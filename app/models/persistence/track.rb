class Persistence::Track
  include Persistence::Document
  include ::Utils::CarrierWave::Glue
  has_attachment :image, Tmm::ImageUploader, extensions: :crop
  include HasStoredImage
  include Tire::Model::Search
  include Persistence::Helpers

  TRACK_SEARCH_AFFECTING_ATTRS = %w(title genre duration release_date is_private is_merchantable is_blocked is_explicit)

  index_name 'unsigned'
  document_type 'track'

  include Mongoid::Slug
  slug :title, :scope => :user_uri, reserve: ['new', 'edit', 'delete', 'create', 'destroy']

  field :user_uri,                             default: ''
  field :title,                                default: ''
  field :artist_uri,                           default: ''
  field :album_uri,                            default: ''
  field :genre,                                default: ''
  field :subgenre,                             default: ''
  field :duration,             type: Integer,  default: 0
  field :description,                          default: ''
  field :release_date,         type: DateTime, default: ->{Time.now}
  field :is_private,           type: Boolean,  default: false
  field :is_merchantable,      type: Boolean,  default: false
  field :is_blocked,           type: Boolean,  default: false
  field :track_status,         type: Integer,  default: 0
  field :lyrics,                               default: ''
  field :is_deleted,           type: Boolean,  default: false
  field :file_size,            type: Integer,  default: 0
  field :video_uris,           type: Array,    default: []
  field :is_explicit,          type: Boolean,  default: false
  field :infringement_info,    type: Hash,     default: {}
  field :is_registered_in_mis, type: Boolean,  default: false
  field :mis_registering_info, type: Hash,     default: {is_processed: false, last_error: '', last_action: ''}
  field :analysis,             type: Hash,     default: {}
  field :id3,                  type: Hash,     default: {}


  index({album_uri: 1})
  index({artist_uri: 1})
  index({user_uri: 1})
  index({is_deleted: 1})
  index({is_private: 1})
  index({is_blocked: 1})
  index({genre: 1})
  index({duration: 1})

  mapping do
    indexes :id,            type:   'string',   index:  :not_analyzed
    indexes :title,         type:   'string'
    indexes :energy,        type:   'float'
    indexes :bpm,           type:   'float'
    indexes :genre,         type:   'string',   index:  :not_analyzed
    indexes :release_date,  type:   'date',     include_in_all: false
    indexes :duration,      type:   'long'
    indexes :song_uri,      type:   'string',   index:  :not_analyzed
    indexes :type,          type:   'string',   index:  :not_analyzed
    indexes :item_number,   type:   'long'
    indexes :rights do
      indexes :available,   type:   'string',   index:  :not_analyzed
      indexes :explicit,    type:   'boolean'
      indexes :hit,         type:   'short'
      indexes :mis_online,  type:   'boolean'
      indexes :preview,     type:   'string',   index:  :not_analyzed
      indexes :public,      type:   'boolean'
      indexes :purchase,    type:   'string',   index:  :not_analyzed
      indexes :radio,       type:   'string',   index:  :not_analyzed
    end
    indexes :artist do
      indexes :id,          type:   'string',   index:  :not_analyzed
      indexes :title,       type:   'string'
    end
    indexes :album do
      indexes :id,          type:   'string',   index:  :not_analyzed
      indexes :title,       type:   'string'
    end
    indexes :image_url,     type:   'string',   index:  :not_analyzed
    indexes :music_url,     type:   'string',   index:  :not_analyzed
  end

  # Accessors
  attr_accessor :content_type, :track_source_file, :track_source_file_ext, :track_tmp_file

  embeds_many :block_infos, class_name: 'Persistence::TrackData::BlockInfo'
  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  belongs_to :artist, class_name: 'Persistence::Artist', foreign_key: :artist_uri, primary_key: :uri
  belongs_to :album, class_name: 'Persistence::Album', foreign_key: :album_uri, primary_key: :uri
  has_many :share_links, class_name: 'Persistence::ShareLink', foreign_key: :track_uri, primary_key: :uri
  has_many :hit_maker_reports, class_name: 'Persistence::HitMakerReport', foreign_key: :track_uri, primary_key: :uri
  has_many :reports, class_name: 'Persistence::Report', foreign_key: :track_uri, primary_key: :uri
  has_many :data_sets, class_name: 'Persistence::Hitlogic::DataSet', foreign_key: :track_uri, primary_key: :uri

  attr_accessible :title, :genre, :subgenre, :description,  :release_date, :is_private, :is_merchantable,
    :lyrics, :artist_uri, :duration, :user_uri, :video_uris, :videos, :album_uri, :track_status, :is_explicit,
    :id3

  default_scope     ->{where is_deleted: false}
  scope :orphan,    ->{where album_uri: {"$in" => [nil, '']}}
  scope :pblc,      ->{where is_private: false, :duration.gt => 0, is_blocked: false}
  scope :prvt,      ->{where is_private: true}
  scope :indexable, ->{where is_deleted: false, :duration.gt => 0, is_blocked: false}


  # Validations
  validates :title, presence: true, unless: :new_record?
  # validates :genre, inclusion: {in: (I18n::t('genres_list').keys.map &:to_s)}, allow_blank: true
  validate  :attachment_validation
  validates_date :release_date
  validate  :copyright_interaction

  # Callbacks
  before_save :store_track, if: :new_record?
  after_destroy {|t| t.tire.index.remove t}

  after_save do |t|
    t.update_searching_index if (TRACK_SEARCH_AFFECTING_ATTRS & t.changed_attributes.keys).any?
  end

  after_validation do |t|
    if t.errors.has_key? :release_date
      t.errors.delete :release_date
      t.errors.add :release_date, 'is invalid'
    end
  end

  # Scopes
  scope :accessible_for, -> user {
    self.or({is_private: false, is_blocked: false, :genre.exists => true, :genre.ne => ''},
    { user_uri: user.try(:uri) }).and(:duration.gt => 0)
  }

  SUPPORTED_FORMATS = %w|audio/x-wav audio/basic audio/L24 audio/mp3 audio/mp4 audio/mpeg audio/ogg audio/vorbis
    audio/vnd.rn-realaudio audio/vnd.wave audio/webm application/octet-stream|

  alias_method :blocked?,   :is_blocked
  alias_method :deleted?,   :is_deleted
  alias_method :private?,   :is_private
  alias_method :can_sold?,  :is_merchantable

  # Class methods definitions
  class << self
    def chart genre = nil, paginate = true
      Rails.cache.fetch("track_genre_chart_#{genre}#{"_full" unless paginate}", expires_in: 60.minutes) do
        keys = Persistence::TrackStatistic.charts(genre: genre, source: "unsigned", mode: "track").map { |x| x["_id"] }
        res = paginate ? Kaminari.paginate_array(keys) : keys
      end
    end

    def charts genre = nil
      all
    end

    def find_via_share_hash hash
      share_link = Persistence::ShareLink.find_by share_hash: hash
      track = share_link.try :track
      track.try(:deleted?) ? nil : track
    end
  end

  def to_key; [self.uri] end
  def purchasable?; can_sold? and ready? end
  def shared?; share_links.any? end
  def ready?; not (blocked? or genre.blank?) and duration.to_i > 0 end
  def can_sold?; is_merchantable && !private? end
  def unblocked; not blocked? end
  def not_deleted?; not is_deleted? end
  def public?; not private? end
  def orphan?; album_uri.blank? end
  def has_album?; not orphan? end
  def artist_name; artist.stage_name end

  def destroy options = {}
    result = update_attribute(:is_deleted, true)
    share_links.each(&:mark_deleted) if respond_to?(:share_links)
    result
  end

  def videos
    Persistence::Video.any_in uri: video_uris
  end

  def videos= values
    videos.each {|v| v.pull :track_uris, uri}
    set :video_uris, videos.map(&:uri)
    videos.each {|v| v.add_to_set :track_uris, uri}
  end

  def video_uris= values
    videos.each {|v| v.pull :track_uris, uri}
    vds = Persistence::Video.any_in uri: values
    set :video_uris, vds.map(&:uri)
    videos.each {|v| v.add_to_set :track_uris, uri}
  end

  def block! args = {}
    args.symbolize_keys!
    args.slice! *[:code, :reason, :blocked_by, :requested_by]
    args.reverse_merge! code: '', reason: '', blocked_by: 'system', requested_by: 'system'
    self.is_blocked = true
    block_infos << Persistence::TrackData::BlockInfo.new(args)
    save validate: false
  end

  def chart_position
    chart_position = self.class.chart(nil, false).index { |t| t == self.uri }
    chart_position += 1 if chart_position.present?
    chart_position
  end

  def genre_chart_position
    genre_chart_position = self.class.chart(self.genre, false).index { |t| t == self.uri }
    genre_chart_position += 1 if genre_chart_position.present?
    genre_chart_position
  end

  def number_of_plays
    Persistence::TrackStatistic.where(track_uri: uri).count
  end

  def release_date=(value)
    super (value.to_f.to_s.eql?(value.to_s) || value.to_i.to_s.eql?(value.to_s)) ? Time.at(value.to_i) : value
  end

  def store_track
    return true if Rails.env.test?
    # Move file to appropriate destination
    tag = self.class.name
    Rails.logger.info "[#{tag}] Track: '#{uri}':: Gonna create catalog '#{File.dirname(track_full_path)}' for file '#{track_full_path}'"
    FileUtils.mkpath File.dirname(track_full_path), verbose: true
    Rails.logger.info "[#{tag}] Track: '#{uri}':: Gonna move file '#{track_source_file}' into '#{track_full_path}'"
    FileUtils.cp track_source_file, track_full_path, verbose: true
    Rails.logger.info "[#{tag}] Track: '#{uri}':: Changing access mode for file '#{track_full_path}'"
    FileUtils.chmod 0664, track_full_path, verbose: true
    Rails.logger.info "[#{tag}] Track: '#{uri}':: Changing access mode for catalog '#{File.dirname(track_full_path)}'"
    #FileUtils.chmod 0775, File.dirname(track_full_path), verbose: true

    # Extract info
    extract_meta_from_track_file
    extract_image_from_track_file
    Rails.logger.info "[#{tag}] Track: '#{uri}':: done!'"
    true
  rescue Exception => e
    Rails.logger.error e
    Rails.logger.error "store_track failed"
  end

  def track_relative_path
    (File.join uri.split ':') + track_source_file_ext.to_s
  end

  # TODO update it to work properly
  def track_full_path
    File.join Settings.storage_path, track_relative_path
  end

  def track_path_without_ext
    cur_extension = File.extname track_full_path
    file_path = track_full_path.gsub(cur_extension, "")
  end

  def available_formats
    Dir["#{track_path_without_ext}.*"].map {|f| File.extname(f) }.reject &:blank?
  end

  def all_files
    Dir["#{track_path_without_ext}.*"]
  end

  def total_size
    all_files.sum { |f| File.size f }
  end

  def attachment_validation
    if track_source_file
      if File.exist?(track_source_file)
        file_size = File.size(track_source_file)
        self.errors.add :track, "File cannot be empty" unless file_size > 0
        #self.errors.add :track, I18n.t('messages.errors.out_disk_quota') if
        #  (Persistence::User.find_by(uri: user_uri).total_tracks_size + file_size) > Settings.tracks.available_space.to_i
      else
        self.errors.add :track, I18n.t('messages.errors.file_not_found')
      end
    end
    self.errors.add :track, I18n.t('messages.errors.invalid_format_file') if
      (content_type and SUPPORTED_FORMATS.exclude?(content_type))
  end

  def accessible_for? user_id
    public? and ready? or owned_by? user_id
  end

  def downloadable_via_hash? hash
    share_link = share_links.find_by share_hash: hash
    share_link.present? and share_link.is_downloadable
  end

  # TODO: provide a link for track file downloading
  def get_for_download share_hash = '', user_ip = '', stat_user_uri = ''
    register_download share_hash, user_ip, stat_user_uri unless share_hash.blank?
    fetch_attachment 'track.mp3' if attachment_url('track.mp3').present?
  end

  def register_download share_hash, user_ip, stat_user_uri
    Api::Statistic::Track.create track_uri: id, artist_uri: artist_uri, owner_uri: user_uri,
      user_uri: stat_user_uri, user_ip: user_ip, type: 'track_statistic_download',
      genre: genre, subgenre: subgenre, created_at: DateTime.now.to_i, share_link_hash: share_hash
  rescue Exception => e
    Rails.logger.debug "Failed to store download stat for track: #{id}, share_link: #{share_hash} :: #{e}"
  end

  def register_share_link_opened share_hash, user_ip, stat_user_uri
    Api::Statistic::Track.create track_uri: id, artist_uri: artist_uri, owner_uri: user_uri,
      user_uri: stat_user_uri, user_ip: user_ip, type: 'track_statistic_open',
      genre: genre, subgenre: subgenre, created_at: DateTime.now.to_i, share_link_hash: share_hash
  rescue Exception => e
    Rails.logger.debug "Failed to store open stat for track: #{id}, share_link: #{share_hash} :: #{e}"
  end

  def track_downloadable_name
    "#{artist_name} - #{title}".first(60) + '.' + available_formats.first
  end

  def owned_by? user
    user.is_a?(String) ? self['user_uri'].eql?(user) : self['user_uri'].eql?(user.try :id)
  end

  def create_share_link_by_params! share_link_params
    share_links.create({ user_uri: user_uri }.merge(share_link_params))
  end

  def remove_share_link_by_id! share_link_uri
    share_link = share_links.find_by uri: share_link_uri
    share_link.destroy
    share_link
  end

  def stats_map
    # we have an array of rows like that:
    # [{"id"=>"b01651ae61f7cde765b4c6b25f2b6fb3",
    #  "key"=>["rrmusic:track:42861", -33.86150000000001, 151.20549999999997], "value"=>1}, ...]
    # and we have to come up with array like:
    #  [{:lat=>12.983300000000014, :lng=>77.58330000000001}, ...]
    Persistence::TrackStatistic.load_listen_map(self.uri).collect do |s|
      {lat: s['_id']['location'].first, lng: s['_id']['location'].last}
    end
  end

  def listen_graph
    Persistence::TrackStatistic.load_stat_graph self.uri
  end

  def extract_image_from_track_file
    Rails.logger.info "[#{self.class.name}] Track: '#{uri}':: Gonna extract image for '#{track_full_path}'"
    avail_cover_images = %w|OTHER ICON OTHER_ICON FRONT_COVER BACK_COVER LEAFLET MEDIA LEAD_ARTIST ARTIST
      CONDUCTOR BAND COMPOSER LYRICIST RECORDING_LOCATION DURING_RECORDING DURING_PERFORMANCE VIDEO
      BRIGHT_COLORED_FISH ILLUSTRATION BAND_LOGO PUBLISHER_LOGO|
    Dir.mktmpdir do |dir|
      system "eyeD3 -i #{dir} #{track_full_path}"
      file = Dir.entries(dir).detect {|f| avail_cover_images.include? f.split('.').first}
      self.image = open("#{dir}/#{file}") if File.exist?("#{dir}/#{file}")
    end
  rescue
    Rails.logger.info "[#{self.class.name}] Track: '#{uri}':: Cannot extract image from '#{track_full_path}' during track creation! "
  end

  def extract_meta_from_track_file
    Rails.logger.info "[#{self.class.name}] Track: '#{uri}':: Gonna run soxi for '#{track_full_path}'"
    if (meta = `soxi -a #{track_full_path}`).present?
      data        = Hash[meta.split("\n").map{|str| str.split("=")}]
      self.id3    = data if data.present?
      self.title  = data['Title'] if title.blank? && data['Title'].present?
      self.genre  = data['Genre'] if I18n.t('genres_list').values.include? data['Genre']
    end
    self.duration = `soxi -D #{track_full_path}`.to_i
  rescue
    Rails.logger.info "[#{self.class.name}] Track: '#{uri}':: Cannot extract Meta from '#{track_local_path}' during track creation!"
  end

  def track_errors
    # TODO: decide, where error messages should come from
    (block_infos.map(&:reason) << ("Genre can't be blank" if genre.blank?)).compact.reject(&:blank?)
  end

  def music_url
    File.join(::Settings.storage_url, uri.split(':')) + '.' + 'mp3'
  end

  def musicmanager_url
    File.join(::Settings.musicmanager_url, 'users', user.mongoid_slug, 'music', slug)
  end

  def track_file_path
    File.join(::Settings.storage_path, uri.split(':')) + '.' + 'mp3'
  end

  def waveform_url
    "#{Settings.track_wave_base_url}/#{id}/wave"
  end

  def waveform_json
    "#{Settings.track_wave_base_url}/#{id}/wavejson"
  end

  # TODO take bpm value from a recommendation or update a track after its recommendation arrives
  def bpm
    0
  end

  def to_indexed_json
    to_indexed_hash.to_json
  end

  def to_indexed_hash
    {
      id: uri,
      title: title,
      song_uri: '',
      type: 'track',
      duration: duration,
      label: 'TH Unsigned',
      genre: genre,
      item_number: 0,
      image_url: image_url,
      music_url: music_url,
      release_date: release_date.try {|d| d.strftime("%Y-%m-%d")},
      color: {name: '', value: 0},
      color_tune: {white: 0, yellow: 0, grey: 0, red: 0, purple: 0, orange: 0, green: 0, blue: 0, black: 0},
      genre_cloud: [],
      energy: 0,
      bpm: bpm,
      rights: rights,
      artist: {id: artist.try(:uri), title: artist.try(:title)},
      album: {id: album.try(:uri), title: album.try(:title)},
      popularity: popularity_hash
    }
  end

  def rights
    {
      available: (ready? ? countries_list : []),
      explicit: is_explicit,
      hit: 0,
      mis_online: (ready? and %w(uplaya rrmusic).include? source),
      preview: (ready? ? countries_list : []),
      public: !private?,
      purchase: (purchasable? ? countries_list : []),
      radio: (ready? ? countries_list : [])
    }
  end

  def popularity_hash
    {
      mn_popularity: 0,
      popularity: 0,
      hits_appearance: 0,
      uk_popularity: 0,
      uk_hits_appearance: 0,
      us_popularity: 0,
      us_hits_appearance: 0
    }
  end

  def update_searching_index
    deleted? ? tire.index.remove(self) : tire.update_index
  end

  def destroy
    if album
      album.tracks_order.delete uri
      album.save
    end
    self.is_deleted = true
    save validate: false
    share_links.each {|sl| sl.udpate_attribute :track_is_deleted, true}
    self
  end

  def artist_name
    artist.display_name if artist
  end

  def fallback_image_url
    @fallback_image_url ||= [album.try(:image_url), user.try(:image_url)].compact.reject {|r| r =~ /default\//}.first
  end

  def default_image_file_name
    number = uri.gsub('0','').last
    "track_default_cover_#{number}.png"
  end

  def explicit?
    SwearDetector.includes_swear?(title) or !!album.try(:explicit?)
  end

  def request_fgp
    ::FgpProcessor.enqueue_track self
  end

  def infringement?
    infringement_info["track_uri"].present? && !infringement_info["track_uri"].eql?('NONE')
  end

  def copyright_interaction
    return true unless infringement?
    errors.add :base, "You can't change security for infringement track" if source =~ /hitlogic|hitmaker/ && is_private_changed?
  end
end
