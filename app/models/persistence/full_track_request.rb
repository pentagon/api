require 'maxmind_geoip'

class Persistence::FullTrackRequest
  include Persistence::Document
  store_in session: "reporting_data"

  FALLBACK_TRACK_DURATION = 270

  field :agent,         type: String
  field :artist_name,   type: String
  field :artist_uri,    type: String
  field :country,       type: String,   default: 'unknown'
  field :environment,   type: String
  field :ip_address,    type: String
  field :response,      type: Hash,     default: {}
  field :station_uri,   type: String
  field :success,       type: Boolean
  field :duration,      type: Fixnum,   default: FALLBACK_TRACK_DURATION
  field :track_name,    type: String
  field :track_uri,     type: String
  field :type,          type: String
  field :user_uri,      type: String
  field :user_email,    type: String

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  belongs_to :station, class_name: 'Persistence::Station', foreign_key: :station_uri, primary_key: :uri

  attr_accessible :agent, :artist_name, :artist_uri, :country, :duration,
    :environment, :ip_address, :response, :station_uri, :success, :track_name,
    :track_uri, :type, :user_uri, :user_email

  index created_at: -1
  index country: 1

  @@ips = {}

  class << self

    def get_countries
      distinct(:country)
    end

    def load_from_params(params = {})
      allowed_params = [:user_uri, :type, :success, :environment, :country]
      where_params = params.slice(*allowed_params).reject { |_, value| value.blank? && value != false }
      view = all.where(where_params)
      view = view.gte(created_at: Time.parse(params[:date_from])) if params[:date_from].present?
      view = view.lte(created_at: Time.parse(params[:date_till]).end_of_day) if params[:date_till].present?
      view.desc :created_at
    end

    def country_for_ip(ip_address)
      @@ips[ip_address] ||= MaxmindGeoip.get_geolocation_from_ip(ip_address)[:country_code].try(:downcase) || 'unknown'
    end

    def aggregated_counts_by_params(params = {})
      view = load_from_params(params)
      results = {}
      reduce = %Q{
        function(key, values) {
          return Array.sum(values);
        }
      }
      [:agent, :type, :success].each do |field|
        unless where_params.include? field
          map = %Q{
            function() {
              var key;
              key = this.#{field};
              if (key == null) {
                key = "";
              }
              emit(key, 1);
            }
          }
          results[field] = view.map_reduce(map, reduce).out(inline: true)
        end
      end
      results
    end
  end

  # TODO remove this after previous stat merge to new storage
  # Overrides relation in order to fetch a track from ElasticSearch
  def track
    @track ||= Track.fetch track_uri
  end
end
