class Persistence::RadioTrackReaction
  include Persistence::Document
  ALLOWED_REACTIONS = %w(like dislike)

  field :track_uri,     type: String,  default: ''
  field :station_uri,   type: String,  default: ''
  field :reaction_type, type: String,  default: ''

  belongs_to :station, class_name: 'Persistence::Station', foreign_key: :station_uri, primary_key: :uri
  attr_accessible :track_uri, :station_uri, :reaction_type
  validates :reaction_type, inclusion: { in: ALLOWED_REACTIONS }

  # Overrides relation in order to fetch a track from ElasticSearch
  def track
    @track ||= Track.fetch track_uri
  end

end
