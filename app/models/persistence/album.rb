class Persistence::Album
  include Persistence::Document
  include Tire::Model::Search
  include Persistence::Helpers

  field :title,         type: String,  default: ''
  field :artist_uri,    type: String,  default: ''
  field :genre,         type: String,  default: ''
  field :release_date,  type: Date,    default: -> { Time.now }
  field :is_album,      type: Boolean, default: true
  field :tracks_order,  type: Array,   default: []

  belongs_to :artist, class_name: 'Persistence::Artist', foreign_key: :artist_uri, primary_key: :uri
  has_many :tracks, dependent: :nullify, class_name: 'Persistence::Track', foreign_key: :album_uri, primary_key: :uri
  attr_accessible :title, :genre, :release_date, :is_album, :artist_uri

  include ::Utils::CarrierWave::Glue
  has_attachment :image, Tmm::ImageUploader, extensions: :crop
  include HasStoredImage

  attr_accessible :tracks_order
  attr_accessor :nullify_track_ids

  include Mongoid::Slug
  slug :title, :scope => :user_uri, reserve: ['new', 'edit', 'delete', 'create', 'destroy']

  index_name 'unsigned'
  document_type 'album'

  mapping do
    indexes :id,            index: :not_analyzed
    indexes :title,         analyzer: 'standard'
    indexes :release_date,  type: 'date', include_in_all: false
    indexes :image_url,     index: :not_analyzed
    indexes :duration,      index: :not_analyzed
    indexes :type,          index: :not_analyzed
    indexes :artist do
      indexes :id, index: :not_analyzed
      indexes :title, analyzer: 'standard'
    end
  end

  scope :track_sets, where(is_album: false)
  scope :albums, where(is_album: true)
  scope :by_artist, ->(artist_uri){ where(artist_uri: artist_uri) }

  validates :artist_uri, :release_date, :title, presence: true
  validates_date :release_date

  after_save :update_searching_index
  after_destroy do |a|
    a.tire.index.remove a
    trks = a.tracks.to_a
    a.tracks.update_all album_uri: nil
    trks.each &:update_searching_index
  end

  alias_method :album?, :is_album

  # Quick fix, need to move to some base method
  after_validation do |a|
    if a.errors.has_key? :release_date
      a.errors.delete :release_date
      a.errors.add :release_date, 'is invalid'
    end
  end

  def update_tracks_order track_uri
    if !track_uri.nil?
      tracks_order.unshift track_uri
    else
      @track_ids ||= tracks.pluck(:uri)
      _tracks_order = tracks_order
      self.tracks_order = _tracks_order & @track_ids
    end
    save!
  end

  def release_date=(value)
    super (value.to_f.to_s.eql?(value) || value.to_i.to_s.eql?(value)) ? Time.at(value.to_i) : value
  end

  def nullify_track_ids= val
    nullify_tracks val.compact.reject(&:blank?)
  end

  def nullify_tracks val
    tracks.any_in(uri: val).map { |a| a.unset(:album_uri) }
  end
  protected :nullify_tracks

  def artist_name
    artist.try :display_name
  end

  def duration
    tracks.pluck(:duration).sum
  end

  def artist_name
    artist.try :display_name
  end

  def owner
    artist.user if artist
  end

  def genres
    tracks.pluck(:genre).uniq.reject &:blank?
  end

  def duration
    tracks.pblc.pluck(:duration).sum
  end

  def to_indexed_json
    to_indexed_hash.to_json
  end

  def to_indexed_hash
    {
      id: uri,
      title: title,
      type: 'album',
      release_date: release_date.strftime("%Y-%m-%d"),
      image_url: image_url,
      tracks_count: tracks.pblc.count,
      duration: duration,
      genre: genres.join(','),
      label: 'TH Unsigned',
      rights: rights,
      artist: {id: artist.try(:uri), title: artist.try(:title)}
    }
  end

  def rights
    {
      purchase: (!!tracks.pblc.count.nonzero? ? countries_list : []),
      available: (!!tracks.pblc.count.nonzero? ? countries_list : []),
      explicit: explicit?
    }
  end

  def to_key
    [self.uri]
  end

  def update_searching_index
    if tracks.count.zero?
      tire.index.remove(self)
    else
      tire.update_index
      tracks.indexable.each &:update_searching_index
    end
  end

  def add_track track
    self.add_tracks Array.wrap(track)
  end

  def add_tracks trks
    raise ArgumentError unless trks.is_a? Array
    self.tracks = []
    self.tracks << trks.map {|t| t.is_a?(String) ? Track.find_by(uri: t) : t}
  end

  def remove_track track
    remove_tracks [track]
  end

  def remove_tracks tracks
    raise ArgumentError unless tracks.is_a? Array
    tracks = tracks.map do |track|
      track.is_a?(String) ? Persistence::Track.find_by(uri: track) : track
    end
    tracks_to_remove = tracks.select do |track|
      track_uris.include? track.uri
    end
    nullify_tracks tracks_to_remove.map(&:uri)
  end

  def fallback_image_url
    @fallback_image_url ||= (artist.image_url if artist).to_s
  end

  def explicit?
    SwearDetector.includes_swear? title
  end
end
