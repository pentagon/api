require 'maxmind_geoip'
require 'going_postal'

class Persistence::PaymentOrder
  include Persistence::Document
  include Mongoid::Paranoia

  class PaymentError < StandardError
    def payments_message_code
      self.class.name.underscore.split("/").last
    end
  end

  ItemProviderError = Class.new(PaymentError)
  ItemNotAvailableForPurchase = Class.new(ItemProviderError)
  PaymentProviderError = Class.new(PaymentError)
  PaymentProviderFundingFailureError = Class.new(PaymentProviderError)
  InvalidPaymentArguments = Class.new(PaymentError)
  InvalidBillingAddress = Class.new(InvalidPaymentArguments)
  InternalPaymentError = Class.new(PaymentError)

  # "Internal" => "External" statuses mapping
  # See: http://stackoverflow.com/questions/2433329/possible-payment-status-values
  STATUSES = {
    'canceled_reversal' => 'Canceled_Reversal',
    'completed'         => 'Completed',
    'created'           => 'Created',
    'denied'            => 'Denied',
    'expired'           => 'Expired',
    'failed'            => 'Failed',
    'pending'           => 'Pending',
    'refunded'          => 'Refunded',
    'reversed'          => 'Reversed',
    'processed'         => 'Processed',
    'voided'            => 'Voided'
  }
  STATUS_TEMPORARY = 'temporary'
  STATUS_CANCELLED = 'cancelled'
  PAYMENT_PROVIDERS = %w(PAYPAL)
  ITEM_PROVIDERS = %w(MEDIANET TUNEHOG)

  field :agent,                           type: String
  field :custom_options,                  type: Hash,   default: {}
  field :department_code,                 type: String
  field :discount_code,                   type: String
  field :increment_code,                  type: Fixnum, default: ->{ generate_incremental_code }
  field :ip_address,                      type: String
  field :item_provider_name,              type: String
  field :item_provider_item_id,           type: String
  field :item_provider_item_info,         type: Hash,   default: {}
  field :item_provider_response,          type: Hash,   default: {}
  field :item_title,                      type: String  # human readable title of an item to reflect in transaction
  field :item_type,                       type: String
  field :item_uri,                        type: String
  field :nominal_code,                    type: String
  field :payment_provider_name,           type: String, default: 'PAYPAL'
  field :payment_provider_order_info,     type: Hash,   default: {}
  field :payment_provider_payment_id,     type: String
  field :payment_provider_payment_token,  type: String
  field :payment_provider_response,       type: Hash,   default: {}
  field :price_currency,                  type: String
  field :price_amount,                    type: Fixnum  # in cents
  field :purchase_country,                type: String
  field :quantity,                        type: Fixnum, default: 1
  field :refunded_payment_id,             type: String
  field :status,                          type: String
  field :user_uri,                        type: String

  index({created_at: -1})

  attr_accessible(
    :agent, :billing_address, :custom_options, :created_at, :department_code,
    :discount_code, :ip_address, :item_provider_name, :item_provider_item_id,
    :item_provider_item_info, :item_provider_response, :item_title, :item_type,
    :item_uri, :nominal_code, :payment_provider_name,
    :payment_provider_order_info, :payment_provider_payment_id,
    :payment_provider_payment_token, :payment_provider_response,
    :price_currency, :price_amount, :purchase_country, :quantity,
    :refunded_payment_id, :status, :user_uri
  )

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri

  embeds_one :billing_address,  autobuild: true, class_name: 'Persistence::UserData::BillingAddress'
  accepts_nested_attributes_for :billing_address

  validates :status, inclusion: {in: (STATUSES.keys + [STATUS_TEMPORARY, STATUS_CANCELLED])}, allow_nil: true
  validates :item_uri, :item_type,
    :payment_provider_name,
    :item_provider_name, :item_provider_item_id,
    :price_currency, :price_amount, :quantity,
    presence: true
  validates :payment_provider_payment_id, presence: true, unless: :unprocessed?
  validates :quantity, numericality: {only_integer: true, greater_than: 0}
  validates :price_amount, numericality: {only_integer: true, greater_than: 0}, unless: :refund?
  validates :price_amount, numericality: {only_integer: true, less_than: 0}, if: :refund?
  validate :digital_goods_custom_validate

  scope :outdated, -> { where(status: STATUS_TEMPORARY, payment_provider_payment_id: nil, :created_at.lte => Time.now.yesterday) }
  scope :processed, -> { nin(status: [STATUS_TEMPORARY, STATUS_CANCELLED]) }
  scope :successful, -> { self.in(status: %w[canceled_reversal completed processed]) }

  class << self
    def logger
      @@logger ||= Logger.new(File.join(Rails.root, "log", "payment_orders.log"))
    end

    def process_ipn(ipn)
      @@logger.info "IPN received:\n#{ipn.params}"
      id = ipn.params["parent_txn_id"] || ipn.params["txn_id"]
      if id.present? && !ipn.params["recurring_payment_id"].present?
        payment = self.find_by(payment_provider_payment_id: id)
        if payment.present?
          if ipn.params["parent_txn_id"] && ipn.params['payment_status'] == STATUSES['refunded']
            @@logger.info "Proccessing refund for: #{payment.uri}"
            payment.process_refund(ipn.params["txn_id"])
          else
            payment.refresh_status
          end
        else
          @@logger.warn "Payment not found."
        end
      elsif ipn.params["recurring_payment_id"].present?
        id = ipn.params["recurring_payment_id"]
        subscription = ::Persistence::Subscription.find_by(payment_prov_id: id)
        if subscription
          subscription.process_ipn(ipn)
          @@logger.info "Proccessing payment for subscription: #{subscription.uri}"
        else
          @@logger.warn "Subscription not found."
        end
      else
        @@logger.error "Unknown IPN error."
      end
    end

    def load_from_params(params = {})
      view = all
      view = view.where(user_uri: params[:user_uri]) if params[:user_uri].present?
      view = view.where(discount_code: params[:discount_code]) if params[:discount_code].present?
      view = view.where(department_code: params[:department_code]) if params[:department_code].present?
      view = view.where(nominal_code: params[:nominal_code]) if params[:nominal_code].present?
      if params[:status].present?
        view = view.where(status: params[:status])
      elsif params[:statuses].present?
        view = view.in(status: params[:statuses])
      end
      if params[:item_type].present?
        view = view.where(item_type: params[:item_type])
      elsif params[:item_types].present?
        view = view.in(item_type: params[:item_types])
      end
      if params[:no_refunds].present?
        view = view.where(refunded_payment_id: nil)
      end
      view = params[:ascending] ? view.asc(:created_at) : view.desc(:created_at)
      view = view.gte(created_at: Time.parse(params[:date_from])) if params[:date_from].present?
      view = view.lte(created_at: Time.parse(params[:date_till]).end_of_day) if params[:date_till].present?
      view = view.limit params[:limit].to_i if params[:limit].present?
      view
    end

    def aggregate_price(selection = nil)
      selection ||= all
      map = <<-JS
        function() {
          emit(this.price_currency, this.price_amount);
        }
      JS
      reduce = <<-JS
        function(key, values) {
          return values.reduce(function(prev, current) {
            return prev + current;
          }, 0);
        }
      JS
      Hash[selection.map_reduce(map, reduce).out(inline: true).map { |group| [group['_id'], group['value'].to_i] }]
    end

    def create_temporary_payment(item, user, params = {})
      return false unless item.acts_like? :digital_goods
      params = params.with_indifferent_access
      params[:custom_options] ||= {}
      billing_address = user.billing_address_hash(params)
      if params[:discount_code].present?
        modifier = ::Persistence::PaymentPriceModifier.find_by({
          code: params[:discount_code].to_s.upcase
        })
        params[:discount_code] = nil unless modifier && modifier.applicable_to?(item)
      end
      price = item.digital_goods_price(params)
      quantity = params[:quantity].to_i
      quantity = 1 if quantity < 1
      self.create({
        agent: params[:agent].presence || 'UNDEFINED',
        billing_address: billing_address,
        custom_options: params[:custom_options].merge({
          "cancel_url" => params[:cancel_url],
          "error_url" => params[:error_url],
          "mobile" => params[:mobile],
          "popup" => params[:popup],
          "success_url" => params[:success_url],
        }),
        department_code: params[:department_code].presence || "000",
        discount_code: params[:discount_code],
        ip_address: params[:ip_address],
        item_provider_item_id: item.digital_goods_item_provider_item_id,
        item_provider_item_info: item.digital_goods_item_provider_item_info(country: params[:country]),
        item_provider_name: item.digital_goods_item_provider_name,
        item_title: item.digital_goods_name,
        item_type: item.class.digital_goods_type,
        item_uri: item.digital_goods_item_id,
        nominal_code: item.class.digital_goods_nominal_code,
        payment_provider_name: 'PAYPAL',
        price_amount: (price[:amount] * quantity),
        price_currency: price[:currency],
        purchase_country: params[:country],
        quantity: quantity,
        status: STATUS_TEMPORARY,
        user_uri: user.uri,
      })
    end

    def create_temporary_payment!(item, user, params = {})
      raise InvalidPaymentArguments unless item.acts_like? :digital_goods
      payment = create_temporary_payment item, user, params
      unless payment.persisted?
        raise InvalidBillingAddress if payment.errors[:billing_address].any?
        @@logger.error "Temporary payment save error:\n#{payment.errors.messages}"
        raise InternalPaymentError
      end
      payment
    end

    def check_billing_address(address = {})
      address = address.with_indifferent_access
      errors = {}

      # Validate country
      if address[:country].present?
        address[:country] = address[:country].upcase.to_sym
        unless valid_country_states.keys.include? address[:country]
          errors[:country] = :invalid
        end
      else
        errors[:country] = :blank
      end

      # Validate city and postal code
      errors[:city] = :blank if address[:city].blank?
      errors[:postal_code] = :invalid unless GoingPostal.valid_postcode? address[:postal_code], address[:country]

      # Validate state
      country_states = Persistence::Helpers::COUNTRIES_STATES.with_indifferent_access
      unless errors[:country]
        states = valid_country_states[address[:country]][:states]
        address[:state] = address[:state].upcase.to_sym if address[:state]
        if states.is_a? Hash
          if address[:state].blank?
            errors[:state] = :blank
          elsif states.keys.exclude? address[:state]
            errors[:state] = :invalid
          end
        end
      end

      errors
    end

    def valid_country_states
      valid_countries = [:GB, :US, :CA]
      countries = Persistence::Helpers::COUNTRIES_MAP
      country_states = Persistence::Helpers::COUNTRIES_STATES
      Hash[valid_countries.map { |country| [country, {title: countries[country], states: country_states[country] || true}]}]
    end
  end

  logger.formatter = proc do |severity, timestamp, progname, msg|
    "\n[#{severity.first}][#{timestamp}] #{String === msg ? msg : msg.inspect}\n"
  end

  delegate :logger, to: 'self.class'

  def purchase_country
    attributes["purchase_country"] || MaxmindGeoip.get_geolocation_from_ip(ip_address)[:country_code].try(:downcase)
  end

  def item_class
    item_type.gsub("|", "/").camelize.constantize
  rescue NameError
    nil
  end

  def item
    return nil unless item_type && item_uri
    unless @item
      if item_class
        @item = item_class.digital_goods_find_by_id(item_uri, country: purchase_country)
      end
    end
    @item
  end

  def fetch_payment_provider_transaction_details(payment_id = nil)
    payment_id = payment_provider_payment_id if payment_provider_payment_id.present?
    @transaction_details ||= paypal_gateway.transaction_details(payment_id)
    @transaction_details.params.merge("success" => @transaction_details.success?)
  end

  def successful?
    status.in? ['canceled_reversal', 'completed', 'processed']
  end

  def refund?
    refunded_payment_id.present?
  end

  def unprocessed?
    temporary? || cancelled?
  end

  def temporary?
    status == STATUS_TEMPORARY
  end

  def cancelled?
    status == STATUS_CANCELLED
  end

  def purchase_redirect_url(confirm_purchase_url, confirm_cancel_url)
    raise InternalPaymentError unless temporary?
    gateway_options = {
      items: paypal_order_items,
      currency: price_currency,
      return_url: confirm_purchase_url,
      cancel_return_url: confirm_cancel_url
    }
    gateway_options[:no_shipping] = true if custom_options['popup']
    response = paypal_gateway.setup_purchase(price_amount, gateway_options)
    unless response.success?
      @@logger.error "Setup purchase error for #{item_type} #{item_uri}:\n#{response.params}"
      Rails.logger.error "[PaymentOrder] setup purchase error: #{response.params}"
      raise PaymentProviderError
    end
    paypal_gateway.redirect_url_for(response.token, mobile: !!custom_options['mobile'])
  end

  def invoice_id
    "#{department_code}-#{nominal_code}-TH#{increment_code}"
  end

  def funding_error_recover_redirect_url(token)
    paypal_gateway.redirect_url_for(token, mobile: !!custom_options['mobile'])
  end

  def deliver_item
    if item
      item.digital_goods_deliver(self)
    else
      {success: false, errors: ["Item #{item_type} | #{item_uri} not found."]}
    end
  end

  def do_purchase(token, payer_id)
    return self if payment_provider_payment_id.present?
    raise InternalPaymentError unless item.acts_like? :digital_goods
    options = {
      items: paypal_order_items,
      currency: price_currency,
      token: token,
      invoice_id: invoice_id,
      payer_id: payer_id
    }
    response = paypal_gateway.purchase(price_amount, options)
    @@logger.info "Commit payment (#{self.uri}) for #{item.class.digital_goods_type} #{item.digital_goods_item_id}."
    unless response.success?
      case response.params['ErrorCode'].to_i
      when 10415
        @@logger.info "Payment already completed:\n#{response.params}"
        return self
      when 10486
        @@logger.info "Payment provider funding failure:\n#{response.params}"
        raise PaymentProviderFundingFailureError
      else
        @@logger.error "Payment provider error:\n#{response.params}"
        raise PaymentProviderError
      end
    end
    payment_id = response.params['transaction_id']
    result = self.update_attributes({
      payment_provider_order_info: fetch_payment_provider_transaction_details(payment_id),
      payment_provider_payment_id: payment_id,
      payment_provider_payment_token: response.params['token'],
      payment_provider_response: response.params,
      status: STATUSES.key(response.params['payment_status'])
    })
    unless result
      @@logger.error "Payment update error:\n#{self.errors.messages}"
      raise InternalPaymentError
    end
    self
  end

  def process_refund(payment_provider_id)
    refund_payment = self.class.create({
      agent: agent,
      billing_address: billing_address,
      department_code: department_code,
      discount_code: discount_code,
      item_provider_name: item_provider_name,
      item_provider_item_id: item_provider_item_id,
      item_provider_item_info: item_provider_item_info,
      item_title: item_title,
      item_type: item_type,
      item_uri: item_uri,
      nominal_code: nominal_code,
      payment_provider_name: payment_provider_name,
      payment_provider_payment_id: payment_provider_id,
      price_amount: -price_amount,
      price_currency: price_currency,
      refunded_payment_id: uri,
      quantity: quantity,
      user_uri: user_uri,
    })
    refund_payment.update_attributes({
      payment_provider_order_info: refund_payment.fetch_payment_provider_transaction_details,
    })
    refund_payment.refresh_status
  end

  def process_cancel
    if temporary?
      @@logger.info "Payment #{uri} canceled."
      self.status = STATUS_CANCELLED
      self.save
    else
      @@logger.warn "Payment #{uri} was already canceled."
    end
  end

  def refresh_status
    response = fetch_payment_provider_transaction_details
    if response["success"]
      self.status = STATUSES.key response["payment_status"]
      self.save
    end
  end

  private

  def generate_incremental_code
    self.class.unscoped.count + 1
  end

  def paypal_order_items
    hash = {
      name: item.digital_goods_name,
      amount: item.digital_goods_price(country: purchase_country, discount_code: discount_code)[:amount],
      description: item.digital_goods_description,
      number: 1,
      quantity: quantity
    }
    hash[:category] = 'Digital' if custom_options['popup']
    [hash]
  end

  def digital_goods_custom_validate
    item.digital_goods_validate(self) if item
  end

  def paypal_gateway
    environment = Settings.paypal.use_sandbox ? "sandbox" : "production"
    @paypal_gateway ||= {}
    type = custom_options["popup"] ? :popup : :normal
    unless @paypal_gateway[type]
      account_name = item_class.digital_goods_use_payment_account
      options = {
        login:      Settings.paypal[environment][account_name]["login"],
        password:   Settings.paypal[environment][account_name]["password"],
        signature:  Settings.paypal[environment][account_name]["signature"]
      }
      @paypal_gateway[type] = if custom_options["popup"]
        ActiveMerchant::Billing::PaypalDigitalGoodsPatchedGateway.new options
      else
        ActiveMerchant::Billing::PaypalExpressGateway.new options
      end
    end
    @paypal_gateway[type]
  end
end
