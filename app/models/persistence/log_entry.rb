module Persistence::LogEntry
  extend Penetrator::Concern
  HTTP_METHODS = %w(GET POST PUT DELETE PATCH HEAD OPTIONS)

  included do
    include Persistence::Base
    include Mongoid::Timestamps

    field :event_type,                        default: ''
    field :event_scope,                       default: ''
    field :location,                          default: ''
    field :user_uri,                          default: ''
    field :event_metadata,    type: Hash,     default: {}
    field :timestamp,         type: DateTime, default: -> { Time.now.utc }

    scope :by_event_type, ->(event_type){ where(event_type: event_type.ci) }
    scope :by_event_scope, ->(event_scope){ where(event_scope: event_scope.ci) }

    embeds_one :request,  autobuild: true, class_name: 'Persistence::LogEntryData::Request'
    embeds_one :response, autobuild: true, class_name: 'Persistence::LogEntryData::Response'

    accepts_nested_attributes_for :request, :response

    attr_accessible :event_type, :event_scope, :location, :user_uri, :event_metadata, :timestamp,
                    :request_attributes, :response_attributes

    # Very simple validation just to skip saving invalid document
    def is_valid?
      event_type.present? and
      event_scope.present? and
      timestamp.present? and
      request.url.present? and
      HTTP_METHODS.include?(request.request_type) and response.status.present?
    end
  end
end
