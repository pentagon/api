class Persistence::Subscription

  # TODO: Refactor this into interactions.

  include Persistence::Document
  include Mongoid::Paranoia
  extend ActionView::Helpers::TextHelper # Need pluralize

  field :agent,               type: String,   default: ''
  field :country,             type: String
  field :currency,            type: String
  field :custom_options,      type: Hash,     default: {}
  field :department_code,     type: String,   default: '000'
  field :is_dummy,            type: Boolean,  default: false
  field :is_internal_trial,   type: Boolean,  default: false
  field :notification_status, type: String
  field :outstanding_balance, type: Float,    default: 0
  field :payment_prov_id,     type: String
  field :price,               type: Float,    default: 0
  field :repeat_frequency,    type: Fixnum
  field :repeat_period,       type: String,   default: 'month'
  field :status,              type: String,   default: 'active'
  field :trial_cycles,        type: Fixnum
  field :trial_frequency,     type: Fixnum
  field :trial_period,        type: String
  field :trial_price,         type: Fixnum
  field :type,                type: String
  field :user_uri,            type: String

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri

  attr_accessible :agent, :country, :currency, :custom_options,
    :department_code, :is_dummy, :is_internal_trial, :payment_prov_id, :price,
    :repeat_frequency, :repeat_period, :status, :trial_cycles, :trial_frequency,
    :trial_period, :trial_price, :type, :user_uri, :notification_status

  behaves_like :digital_goods

  REPEAT_PERIODS = {day: 'Day', week: 'Week', semi_month: 'SemiMonth', month: 'Month', year: 'Year'}
  MAX_PERIOD_FREQUENCY = {day: 365, week: 52, semi_month: 1, month: 12, year: 1}
  STATUSES = [:active, :cancelled, :suspended, :pending, :compatibility]

  @@logger = Logger.new(File.join(Rails.root, "log", "subscriptions.log"))
  @@logger.formatter = proc do |severity, timestamp, progname, msg|
    "\n[#{severity.first}][#{timestamp}] #{String === msg ? msg : msg.inspect}\n"
  end

  validates :user_uri, presence: true
  with_options unless: :internal? do |subscription|
    subscription.validates :payment_prov_id, presence: true
    subscription.validates :repeat_period, inclusion: {in: REPEAT_PERIODS.with_indifferent_access.keys}
    subscription.validates :repeat_frequency, numericality: {
      only_integer: true,
      greater_than: 0,
      less_than_or_equal_to: lambda { |record| MAX_PERIOD_FREQUENCY[record.repeat_period.to_sym] }
    }
  end

  after_create :refresh_status, unless: :internal?

  STATUSES.each { |s| define_method("#{s}?") { status.eql?(s.to_s) } }

  def internal?
    is_dummy || is_internal_trial
  end

  def price_cents
    (price * 100).to_i
  end

  def tracks_limit_reached
    return false if is_dummy || custom_options["labeled_tracks_limit"].blank?
    tracks_limit = custom_options["labeled_tracks_limit"].to_i + custom_options["bonus_track_requests"].to_i
    custom_options["track_requests"].to_i >= (tracks_limit * repeat_frequency)
  end

  def last_pay_date
    @last_payment ||= ::Persistence::PaymentOrder.successful.where(item_uri: uri).desc(:created_at).first
    @last_payment.created_at if @last_payment
  end

  def trial_length
    if is_internal_trial && trial_frequency.present? && trial_period.present?
      trial_frequency.to_i.send(trial_period)
    else
      0
    end
  end

  def next_payment_date
    (last_pay_date || created_at) + duration_paid
  end

  def self.digital_goods_find_by_id(uri, options = {})
    find_by(uri: uri)
  end

  def self.digital_goods_nominal_code
    "4000"
  end

  def self.load_from_params(params = {})
    view = all
    view = view.where(user_uri: params[:user_uri]) if params[:user_uri].present?
    if params[:type].present?
      view = view.where(type: params[:type])
    elsif params[:types].present?
      view = view.in type: params[:types]
    end
    view = view.where(is_internal_trial: params[:is_internal_trial]) unless params[:is_internal_trial].nil?
    if params[:status].present?
      included_statuses = params[:status].keys.select { |status| ['1', 1, 'true', true].include? params[:status][status] }
      excluded_statuses = params[:status].keys.select { |status| ['0', 0, 'false', false].include? params[:status][status] }
      view = view.in status: included_statuses unless included_statuses.blank?
      view = view.nin status: excluded_statuses unless excluded_statuses.blank?
    elsif params[:statuses].present?
      view = view.in status: params[:statuses] unless params[:statuses].blank?
    end
    view = view.gte(created_at: Time.parse(params[:date_from])) if params[:date_from].present?
    view = view.lte(created_at: Time.parse(params[:date_till]).end_of_day) if params[:date_till].present?
    view = params[:ascending] ? view.asc(:created_at) : view.desc(:created_at)
    view = view.limit params[:limit].to_i if params[:limit].present?
    view
  end

  def self.get_latest_of_type_for_user(type, user_uri)
    self.where(user_uri: user_uri, type: type).desc(:created_at).limit(1).entries.first
  end

  def digital_goods_base_price(options = {})
    {amount: price_cents, currency: currency}
  end

  def digital_goods_reporting_description
    "#{type.capitalize}"
  end

  def usable?
    if status == 'active' || is_dummy
      is_internal_trial ? (Time.now - trial_length) < created_at : true
    elsif status == 'cancelled' && last_pay_date.present?
      (Time.now - duration_paid) < last_pay_date
    elsif status == 'compatibility'
      (Time.now - duration_paid) < created_at
    else
      false
    end
  end

  # TODO remove it after moving to intreraction accomplished
  def cancel(message = nil)
    success = if internal?
      true
    else
      response = self.class.paypal_gateway.cancel_recurring(payment_prov_id, note: message)
      response.success?
    end
    if success
      process_cancel
      case type
      when 'radio'
        RadioMailer.subscription_cancelled(self).deliver
      when 'astro'
        AstroMailer.subscription_cancelled(self).deliver
      when 'startune'
        StartuneMailer.subscription_cancelled(self).deliver
      end
    end
    success
  end

  # TODO remove it after moving to intreraction accomplished
  def bill_outstanding_amount
    unless cancelled?
      response = self.class.paypal_gateway.bill_outstanding_amount(payment_prov_id)
      response.success?
    end
  end

  def refresh_status
    return false if payment_prov_id.blank?
    response = fetch_status
    self.status = case response.params['profile_status']
      when 'ActiveProfile' then 'active'
      when 'SuspendedProfile' then 'suspended'
      when 'PendingProfile' then 'pending'
      when 'CancelledProfile' then 'cancelled'
    end
    self.outstanding_balance = response.params['outstanding_balance'].to_f
    self.save
  end

  def fetch_status
    self.class.paypal_gateway.status_recurring(payment_prov_id)
  end

  # TODO remove it after moving to intreraction accomplished
  def self.auth_redirect_url(subscription_template, return_url, cancel_url, mobile = false)
    response = paypal_gateway.setup_authorization(0,
      billing_agreement: {
        type: 'RecurringPayments',
        description: recurring_profile_description(subscription_template)
      },
      return_url: return_url,
      cancel_return_url: cancel_url
    )
    paypal_gateway.redirect_url_for(response.token, mobile: !!mobile)
  end

  # TODO remove it after moving to intreraction accomplished
  def self.setup_profile(subscription_template, token)
    profile_options = {
      description: recurring_profile_description(subscription_template),
      start_date: Time.now,
      period: REPEAT_PERIODS[subscription_template.repeat_period.to_sym],
      frequency: subscription_template.repeat_frequency,
      currency: subscription_template.currency,
      max_failed_payments: 1,
      auto_bill_outstanding: true,
      token: token
    }
    unless subscription_template.trial_price.nil?
      profile_options.merge!({
        trial_period: REPEAT_PERIODS[subscription_template.trial_period.to_sym],
        trial_frequency: subscription_template.trial_frequency,
        trial_cycles: subscription_template.trial_cycles,
        trial_amount: subscription_template.trial_price.to_i
      })
    end
    response = paypal_gateway.recurring(subscription_template.price_cents.to_i, nil, profile_options)
    @@logger.error("Setup profile error:\n#{response.params}") unless response.success?
    response.success? ? response.params['profile_id'] : nil
  end

  def process_ipn(ipn)
    @@logger.info("IPN received:\n#{ipn.params}")
    case ipn.params['txn_type']
    when 'recurring_payment'
      process_payment(ipn.params)
    when 'recurring_payment_profile_cancel'
      process_cancel
    when 'recurring_payment_suspended', 'recurring_payment_suspended_due_to_max_failed_payment'
      process_suspend
    when 'recurring_payment_skipped'
      @@logger.info "Payment skipped: #{uri}"
      unless last_pay_date.present?
        @@logger.info 'Initial payment skipped. Cancelling the subscription.'
        cancel 'Initial payment failed.'
      end
    when 'recurring_payment_outstanding_payment'
      process_outstanding_balance_payment(ipn.params)
    when 'recurring_payment_outstanding_payment_failed'
      #TODO: Do something useful
    else
      @@logger.error "Unknown IPN type: #{ipn.params['txn_type']}"
    end
    refresh_status
  end

  private

  def duration_paid
    return 100.years if is_dummy
    if self.repeat_period == 'semi_month'
      15.days
    else
      self.repeat_frequency.to_i.send self.repeat_period
    end
  end

  def process_payment(params)
    if last_pay_date.present?
      case type
      when 'astro'
        AstroMailer.subscription_payment_processed(self).deliver
      when 'startune'
        StartuneMailer.subscription_payment_processed(self).deliver
      end
    end

    date = Time.strptime(params['payment_date'], "%H:%M:%S %b %e, %Y %Z") rescue Time.now
    payment = ::Persistence::PaymentOrder.find_by payment_provider_payment_id: params['txn_id']
    payment ||= ::Persistence::PaymentOrder.create({
      billing_address: user.try(:billing_address_hash),
      purchase_country: country,
      department_code: department_code.presence || "000",
      item_provider_name: 'TUNEHOG',
      item_provider_item_id: uri,
      item_provider_item_info: fetch_status.params,
      item_title: 'Recurring payment',
      item_type: 'persistence/subscription',
      item_uri: uri,
      nominal_code: self.class.digital_goods_nominal_code,
      payment_provider_name: 'PAYPAL',
      payment_provider_payment_id: params['txn_id'],
      payment_provider_response: params,
      price_amount: (params['amount'].to_f * 100).to_i,
      price_currency: params['mc_currency'],
      quantity: 1,
      status: ::Persistence::PaymentOrder::STATUSES.key(params['payment_status']),
      user_uri: user_uri
    })
    payment.update_attributes({
      agent: 'PAYPAL',
      created_at: date,
      payment_provider_order_info: payment.fetch_payment_provider_transaction_details,
    })

    if payment.successful?
      if %w(radio astro).include?(type)
        custom_options["bonus_track_requests"] = 0
        custom_options["track_requests"] = 0
      end
      self.save
    else
      false
    end
  end

  def process_outstanding_balance_payment(params)
    if params['payment_status'] == 'Completed'
      process_payment params
      if suspended? && params['outstanding_balance'].to_f == 0
        self.class.paypal_gateway.reactivate_recurring(payment_prov_id)
      end
    end
  end

  # TODO remove it after moving to intreraction accomplished
  def process_cancel
    self.status = 'cancelled'
    self.save
  end

  def process_suspend
    case type
    when 'astro'
      AstroMailer.subscription_suspended(self).deliver
    when 'startune'
      StartuneMailer.subscription_suspended(self).deliver
    end
    if self.status != 'cancelled'
      self.status = 'suspended'
      self.save
    else
      false
    end
  end

  def self.recurring_profile_description(subscription_template)
    description = "#{subscription_template.title}: #{subscription_template.price_cents.to_f / 100} #{subscription_template.currency} per #{pluralize(subscription_template.repeat_frequency, subscription_template.repeat_period)}."
    unless subscription_template.trial_price.nil?
      length = (subscription_template.trial_frequency * subscription_template.trial_cycles).to_i
      length_string = pluralize(length, subscription_template.trial_period)
      description << " \n"
      description << if subscription_template.trial_price == 0
        price_string = "free"
        "#{length_string} #{price_string} trial."
      elsif subscription_template.trial_cycles == 1
        price_string = "#{subscription_template.trial_price.to_f / 100} #{subscription_template.currency}"
        "#{price_string} #{length_string} trial."
      else
        price_string = "#{subscription_template.trial_price.to_f / 100} #{subscription_template.currency} per #{pluralize(subscription_template.trial_frequency, subscription_template.trial_period)}"
        "#{price_string} #{length_string} trial."
      end
    end
    description
  end

  @@paypal_gateway = nil
  def self.paypal_gateway
    environment = Settings.paypal.use_sandbox ? "sandbox" : "production"
    account_name = digital_goods_use_payment_account
    unless @@paypal_gateway
      options = {
        login:      Settings.paypal[environment][account_name]["login"],
        password:   Settings.paypal[environment][account_name]["password"],
        signature:  Settings.paypal[environment][account_name]["signature"]
      }
      @@paypal_gateway = ActiveMerchant::Billing::PaypalExpressGateway.new(options)
    end
    @@paypal_gateway
  end

end
