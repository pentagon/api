class Persistence::PaymentPriceModifier
  include Persistence::Document
  include Mongoid::Paranoia

  AVAILABLE_OPERATIONS = [:substract_value, :substract_percent, :use_value]

  field :code,                type: String
  field :description,         type: String
  field :digital_goods_type,  type: String
  field :digital_goods_id,    type: String
  field :max_use_count,       type: Fixnum
  field :name,                type: String
  field :operation,           type: String
  field :valid_date_from,     type: Date
  field :valid_date_till,     type: Date
  field :value,               type: Float

  attr_accessible :code, :description, :digital_goods_type, :digital_goods_id,
    :max_use_count, :name, :operation, :valid_date_from, :valid_date_till, :value

  validates :code, uniqueness: true
  validates :code, :value, presence: true
  validates :operation, inclusion: {in: AVAILABLE_OPERATIONS.map(&:to_s)}
  validates :value, numericality: {only_integer: true, greater_than: 0}
  validates :value, numericality: {less_than: 100}, if: :operation_substract_percent?

  before_validation :normalize_attributes

  def self.load_from_params(params = {})
    view = all
    view = view.where(item_type: params[:item_type]) if params[:item_type].present?
    view = params[:ascending] ? view.asc(:created_at) : view.desc(:created_at)
    view = view.limit params[:limit].to_i if params[:limit].present?
    view
  end

  AVAILABLE_OPERATIONS.each do |op|
    define_method("operation_#{op}?") {operation.to_sym == op}
  end

  def date_valid?
    today = Date.today
    if valid_date_till.present? && valid_date_from.present?
      today.between?(valid_date_from, valid_date_till)
    elsif valid_date_till
      today <= valid_date_till
    elsif valid_date_from
      today >= valid_date_from
    else
      true
    end
  end

  def usage_limit_reached?
    return false unless max_use_count
    ::Persistence::PaymentOrder.processed.where(discount_code: code).count >= max_use_count
  end

  def usable?
    date_valid? && !usage_limit_reached?
  end

  def calculate_price(original_price)
    case operation.to_sym
    when :substract_value     then (original_price - value).to_i
    when :substract_percent   then (original_price * (1 - value / 100)).round
    when :use_value           then value.to_i
    end
  end

  def applicable_to?(item)
    return false unless usable? && item.acts_like?(:digital_goods)
    type_matches = digital_goods_type.nil? || (digital_goods_type == item.class.digital_goods_type)
    id_matches = digital_goods_id.nil? || (digital_goods_type && item.digital_goods_item_id == digital_goods_id)
    type_matches && id_matches
  end

protected

  def normalize_attributes
    self.max_use_count = nil if self.max_use_count.to_i <= 0
    self.digital_goods_type = nil if self.digital_goods_type.blank?
    self.digital_goods_id = nil if self.digital_goods_id.blank?
    self.valid_date_from = nil if self.valid_date_from.blank?
    self.valid_date_till = nil if self.valid_date_till.blank?
    self.code = self.code.strip.upcase if self.code.present?
  end

end
