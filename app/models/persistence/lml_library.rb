class Persistence::LmlLibrary
  include Persistence::Document

  field :user_uri,      type: String, default: ''
  field :name,          type: String, default: ''
  field :library_hash,  type: String, default: ''
  field :location,      type: Hash,   default: {'ip_address' => '', 'port' => 0}

  index({user_uri: 1})
  index({'location.ip_address' => 1})

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri
  attr_accessor :ip_address, :port
  attr_accessible :name, :library_hash, :user_uri, :ip_address, :port

  def port
    self.location['port']
  end

  def port= value
    self.location['port'] = value.is_a?(String) ? value.to_i : value
  end

  def ip_address
    self.location['ip_address']
  end

  def ip_address= value
    self.location['ip_address'] = value
  end
end
