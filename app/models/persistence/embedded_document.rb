module Persistence::EmbeddedDocument
  extend ActiveSupport::Concern

  included do
    include Persistence::Base
    include Mongoid::Validations
    include Mongoid::Timestamps
  end
end
