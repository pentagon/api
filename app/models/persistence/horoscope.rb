class Persistence::Horoscope
  include Persistence::Document

  field :date_from,   type: String
  field :date_to,     type: String
  field :type,        type: String, default: 'daily'
  field :response,    type: String
  field :data,        type: Hash,   default: {}
  field :tracks_hash, type: Hash,   default: {}
  field :consumer,    type: String

  index({date_from: 1})
  index({date_to: 1})
  index({type: 1})

  attr_accessible :date_from, :date_to, :type, :response, :data, :consumer
end
