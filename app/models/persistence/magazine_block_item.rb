class Persistence::MagazineBlockItem
  include Persistence::Document

  field :uri, type: String
  field :url, type: String
  field :cover, type: String
  field :name, type: String
  field :name_url, type: String
  field :subname, type: String
  field :subname_url, type: String
  field :user_score, type: Integer
  field :wishlist, type: Boolean

  embedded_in :block, class_name: 'Persistence::MagazineBlock'

  attr_accessible :uri, :url, :cover, :name, :name_url, :subname, :subname_url, :user_score, :wishlist
end
