class ApiBase
  include Tire::Model::Search
  include Tire::Model::Persistence
  index_name 'labeled'
  include HasStoredImage
  include UriUtils

  class << self
    def fetch ids, must_filters = [], must_not_filters = []
      fetch_by_size ids, Array.wrap(ids).count, must_filters, must_not_filters
    end

    def fetch_by_size ids, size, must_filters = [], must_not_filters = []
      return nil unless ids
      ids_to_fetch = Array.wrap ids
      return [] unless ids_to_fetch.any?
      res = Tire::Search::Search.new('labeled,unsigned', wrapper: self) do |s|
        s.query {|query| query.ids ids_to_fetch, document_type}
        s.filter :bool, must: must_filters if must_filters.any?
        s.filter :bool, must_not: must_not_filters if must_not_filters.any?
        s.size size
      end
      ids.respond_to?(:any?) ? res.results.sort_by {|i| ids_to_fetch.index i.id} : res.results.first
    end

    def fetch_by_size_from_index ids, size, must_filters = [], must_not_filters = [], index
      return nil unless ids
      ids_to_fetch = Array.wrap ids
      return [] unless ids_to_fetch.any?
      res = Tire::Search::Search.new(index, wrapper: self) do |s|
        s.query {|query| query.ids ids_to_fetch, document_type}
        s.filter :bool, must: must_filters if must_filters.any?
        s.filter :bool, must_not: must_not_filters if must_not_filters.any?
        s.size size
      end
      ids.respond_to?(:any?) ? res.results.sort_by {|i| ids_to_fetch.index i.id} : res.results.first
    end

    def fetch_from_index ids, must_filters = [], must_not_filters = [], index = nil
      index_str = index || 'labeled,origin_soundcloud,unsigned'
      fetch_by_size_from_index ids, Array.wrap(ids).count, must_filters, must_not_filters, index_str
    end

  end

  def source_index
    @attributes['_index']
  end

  def present?
    @id.present?
  end

  def blank?
    @id.blank?
  end
end
