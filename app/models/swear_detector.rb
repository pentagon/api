class SwearDetector
  def self.includes_swear? title
    swear_map = YAML.load_file File.join Rails.root, 'config', 'swear_words.yml'
    prepared = title.split(/\W|_/).reject &:blank?
    return false if prepared.blank?
    (prepared & swear_map.values.flatten).any? or
      swear_map['partial'].select {|s| prepared.map {|w| w.include? s}.uniq.any?}.any?
  end
end
