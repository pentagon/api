class Mixtape < Persistence::Gift
  include ActionView::Helpers::TextHelper # Need pluralize

  field :track_uris,           type: Array,   default: []
  field :mixtape_played_count, type: Fixnum,  default: 0
  field :cassette_id,          type: Fixnum
  field :preset_id,            type: Fixnum
  field :mixtape_title,        type: String,  default: ''

  attr_accessible :track_uris, :cassette_id, :mixtape_title, :preset_id
  validates_presence_of :track_uris

  def cassette
    @cassette ||= Gifts::Cassette.find(cassette_id)
  end

  def tracks
    @tracks ||= Track.fetch track_uris
  end

  def preset
    @preset ||= Gifts::Preset.find(preset_id)
  end

  def mixtape?
    true
  end

  def digital_goods_name
    "Tunehog digital mixtape gift"
  end

  def digital_goods_description
    pluralize(track_uris.count, "track")
  end

  def digital_goods_reporting_description
    digital_goods_description
  end

  def digital_goods_base_price(options = {})
    country = options[:country].presence.try(:downcase) || "gb"
    amount = self.class.prices.select { |range| range.include?(track_uris.length) }.values.first
    currency = self.class.currency(country)
    {amount: amount, currency: currency}
  end

  def digital_goods_deliver(payment_order)
    if source_webastro?
      AstroMailer.mixtape_sent(self).deliver
      AstroMailer.mixtape_received(self).deliver
    end
    super
  end

  class << self

    def digital_goods_nominal_code
      "4010"
    end

    def currency(country = "gb")
      case country
      when "us" then "USD"
      when "gb" then "GBP"
      else "GBP"
      end
    end

    def prices
      {
        1..20   => 299,
        21..40  => 399,
        41..60  => 499
      }
    end

    def digital_goods_find_by_id(id, options = {})
      find_by(uri: id)
    end
  end
end
