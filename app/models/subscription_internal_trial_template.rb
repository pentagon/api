class SubscriptionInternalTrialTemplate < SubscriptionTemplate
  SELECT_PARAMS = [:id, :type, :country, :trial_period, :trial_frequency]

  class << self

    def all(include_invalid = nil)
      storage.map { |template| new(template) }
    end

    def find(type, country)
      arguments =
      select(type: type, country: country).first
    end

  private

    def storage_file
      File.join(Rails.root, 'config', 'subscription_internal_trials.yml')
    end

  end
end
