module UriUtils
  extend ActiveSupport::Concern

  LABELED_SOURCES  = %w(medianet)

  module ClassMethods
    def get_source_from_uri uri
      unless uri.blank?
        source = uri.split(':')[0]
        LABELED_SOURCES.include?(source) ? 'labeled' : 'unsigned'
      end
    end

    def get_type_from_uri uri
      uri.split(':').second
    end

    def get_id_from_uri uri
      uri.split(':').last
    end
  end

  def labeled?
    self.class.get_source_from_uri(id) == 'labeled'
  end

  def unsigned?
    self.class.get_source_from_uri(id) == 'unsigned'
  end
end
