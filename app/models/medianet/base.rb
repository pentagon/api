require 'maxmind_geoip'

class MedianetError < Persistence::PaymentOrder::ItemProviderError
  def payments_message_code
    "item_provider_error"
  end
end

class Medianet::Base
  include Singleton

  CountryCodeEmptyError = Class.new(MedianetError)
  GetTrackInfoError = Class.new(MedianetError)
  GetPriceError = Class.new(GetTrackInfoError)

  def initialize
    medianet_settings = ::Settings.medianet_api.environments[Settings.medianet_api.use_environment]
    @domain = medianet_settings['domain']
    @api_key = medianet_settings['key']
    @secret = medianet_settings['secret']
  end

  def radio_get_media_location(track_id, ip_address)
    geo_info = MaxmindGeoip.get_geolocation_from_ip ip_address
    country_code = geo_info[:country_code].to_s.downcase
    country_code = Settings.medianet_api.country_codes.fallback unless Settings.medianet_api.country_codes.allowed.include? country_code
    params = {
      'TrackID'   => track_id,
      'UserIP'    => ip_address,
      'cc'        => country_code,
      'Protocol'  => 'http',
      'AssetCode' => '014'
    }
    send_request 'Radio.GetMediaLocation', query: params, sign: true, https: true
  end

  def purchase_use_balance(track_or_album_id, ip_address, price_amount, billing_address_data)
    query = {'UserIP' => ip_address, 'cc' => billing_address_data['country'].to_s.downcase}
    post_data = {
      'TotalCharge' => price_amount,
      'Price' => price_amount,
      'Items' => [
        {
          'MnetId' => track_or_album_id,
          'ItemType' => 'Track',
          'Format' => 'MP3',
          'Tax' => 0,
          'Price' => price_amount
        }
      ],
      'User' => {
        'BillingAddress' => {
          'City' => billing_address_data['city'],
          'Country' => billing_address_data['country'],
          'PostalCode' => billing_address_data['postal_code'],
          'State' => billing_address_data['state']
        }
      }
    }
    send_request 'purchase.useBalance', query: query, http_method: :post, sign: true, https: true, post_data: post_data
  end

  def get_track_info(id, country_code = 'gb')
    @track_info_cache ||= {}
    @track_info_cache[country_code] ||= {}
    @track_info_cache[country_code][id] ||= send_request 'track.get', query: {'MnetId' => id, 'cc' => country_code.downcase}
    raise GetTrackInfoError unless @track_info_cache[country_code][id]['Success']
    @track_info_cache[country_code][id]
  end

  def get_price(id, country_code = 'gb')
    raise CountryCodeEmptyError if country_code.blank?
    track_info = get_track_info id, country_code
    raise GetPriceError unless track_info['Track']['PriceTag']
    {
      amount: track_info['Track']['PriceTag']['Amount'].to_f,
      currency: track_info['Track']['PriceTag']['Currency'],
      fixed: track_info['Track']['PriceTag']['IsSetPrice'],
      publishing_cost: track_info['Track']['PriceTag']['PublishingCost'].to_f,
      wholesale_price: track_info['Track']['PriceTag']['WholesalePrice'].to_f
    }
  end

  private

  def send_request(api_method, opts = {})
    opts.reverse_merge! query: {}, http_method: :get, sign: false, https: false, post_data: {}
    url = "#{opts[:https] ? 'https' : 'http'}://#{@domain}"
    @parameters = {method: api_method, format: 'JSON', apiKey: @api_key}.merge opts[:query]
    @parameters.update signature: generate_signature if opts[:sign]
    Rails.logger.info "Sending request to MN via #{url} of '#{@parameters}'"
    resp = if opts[:http_method] == :get
      RestClient.get url, {accept: :json, content_type: :json, params: @parameters}
    elsif opts[:http_method] == :post
      RestClient.post [url, parameters_string].join('?'), opts[:post_data].to_json, {accept: :json, content_type: :json}
    end

    resp = JSON.parse resp
    Rails.logger.info resp
    resp['Success'] = false unless resp.has_key?('Success')
    resp
  rescue => e
    Rails.logger.info e.message
    Rails.logger.info e.backtrace.join "\n"
    {'Success' => false, exception: e.message}
  end

  def parameters_string
    @parameters.collect { |k, v| "#{k.to_s}=#{CGI::escape(v.to_s)}" }.join('&')
  end

  def generate_signature
    OpenSSL::HMAC::hexdigest 'md5', @secret, parameters_string
  end

  def self.method_missing method, *args
    instance.send method, *args
  end
end
