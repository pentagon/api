# TODO review this
#
# def feedback_of_user user_uri
#   return user_feedback if user_feedback
#   # TODO: take into account type of service: radio, player & etc
#   (Persistence::UserTrackFeedback.find_by track_uri: id, user_uri: user_uri).try :status
# end
#
# def as_json_for_player opts = {}
#   opts ||= {}
#   res = {
#     id: id,
#     track_url: music_url,
#     cover_url: image_url,
#     artist_name: artist.title || '',
#     artist_uri: artist.id,
#     album_uri: album_uri,
#     track_name: title || '',
#     duration: duration,
#     link_to_track_page: show_page_url,
#     link_to_artist_page: artist_show_page_url,
#     allowed: allowed,
#     rights: rights.try(:to_hash) || TRACK_DEFAULT_RIGHTS
#   }
#   res[:status] = opts[:user_uri].present? ? feedback_of_user(opts[:user_uri]) : ''
#   res
# end

class UserDnaTrackMap
  class << self
    def for_user user_uri, catalog = 'gb'
      user_feedback = Persistence::UserTrackFeedback.liked_user_tracks(user_uri) + Persistence::UserTrackFeedback.disliked_user_tracks(user_uri)
      dna = Persistence::UserDna.get_last_for_user user_uri
      dna.similar_users_map.inject({}) do |a,(k,v)|
        track_uris = Persistence::UserTrackFeedback.liked_track_uris_for_users(v)
        track_uris = track_uris - user_feedback
        track_uris = track_uris.sort_by {|t| track_uris.count t}.uniq.reverse
        a[k] = Track.fetch(track_uris).map {|track| track.id}
        a
      end.values.flatten.uniq if dna
    end

    def random_without_user_tracks user_uri, count = 10
      user_track_ids = UserDnaTrackMap.for_user(user_uri) or []
      random_ids = Persistence::Track.skip(rand(Persistence::Track.count)).limit(50).pluck(:uri).shuffle.first(count.to_i)
      random_ids = random_ids - user_track_ids
      tracks = Track.fetch random_ids
      tracks.map {|t| t.as_json_for_player(user_uri: user_uri)}
    end

    def get_tracks_for_user user_uri, per_page = 10, page = 1
      ids = Kaminari.paginate_array(UserDnaTrackMap.for_user user_uri).page(page).per(per_page)
      tracks = Track.fetch ids
      tracks.map {|t| t.as_json_for_player(user_uri: user_uri)}.pesence || random_without_user_tracks(user_uri, per_page)
    end
  end
end
