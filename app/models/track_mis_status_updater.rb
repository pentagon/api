# Query format:
#
# {
#   "track_uri":"randr:track:uyhereruybner723123sDDwe",
#   "metadata": {
#     "track_url":"http://staging.kiosk.com/rrmusic/1231412.mp3",
#     "track_title":"Fake title",
#     "genre":25,
#     "artist_uri":"randr:artist:47558",
#     "year":2013,
#     "album_uri":"randr:album:1234"
#   }
# }

class TrackMisStatusUpdater
  class << self
    def register_track track
      query = {track_uri: track.uri, metadata: metadata_for_track(track)}
      MisService.enqueue MisCommand::RegisterTrack.new name, :process_mis_response, query
    end

    def check_track_status track
      query = {track_uri: track.uri}
      MisService.enqueue MisCommand::GetTrackStatus.new name, :process_mis_response, query
    end

    def unregister_track track
      query = {track_uri: track.uri}
      MisService.enqueue MisCommand::UnregisterTrack.new name, :process_mis_response, query
    end

    def update_track track
      query = {track_uri: track.uri, metadata: metadata_for_track(track)}
      MisService.enqueue MisCommand::UpdateTrack.new name, :process_mis_response, query
    end

    def metadata_for_track track
      {
        track_title: track.title,
        artist_uri: track.artist_uri,
        album_uri: track.album_uri,
        year: track.release_date.year,
        track_url: track.music_url
      }
    end

    def process_mis_response response
      new response
    end
  end

  def initialize response
    track_id = response['track_uri']
    track = Persistence::Track.find_by uri: track_id
    raise Exception.new "[#{self.class.name} :: #{Time.now}] cannot fetch track with uri: '#{track_id}'" unless track
    last_action = response['action']
    last_error = response['status'].eql?('Error') ? response['message'] : ''
    info = {is_processed: true, last_error: last_error, last_action: last_action}
    track.mis_registering_info = info
    unless response['status'].eql? 'Error'
      track.is_registered_in_mis = case last_action
        when 'add' then true
        when 'check' then response['results']['status'].eql? 'existing'
        when 'delete' then false
        when 'update' then true
      end
    else
      Rails.logger.error "[#{self.class.name} :: #{Time.now}] Error occured while registering track: '#{track_id}'"
    end
    track.save
  end
end
