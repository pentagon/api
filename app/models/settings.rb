class Settings < Settingslogic
  source "#{Rails.root}/config/settings.yml"
  namespace Rails.env
  load! if Rails.env.development?
  self.ips.map! { |a| IPAddr.new(a) }
end
