class Artist < ApiBase
  document_type 'artist'

  property :_id
  property :albums
  property :additional_fields
  property :image_url
  property :popularity
  property :popularity_rank
  property :rights
  property :title
  property :tracks
  property :type
  property :user_uri
end
