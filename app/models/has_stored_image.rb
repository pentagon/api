module HasStoredImage
  extend ActiveSupport::Concern

  def image_url
    image_url_from_uri uri, true
  end

  def image_url_from_uri object_uri, defaults_to_class = false
    path = object_uri.split(':')
    fallback = fallback_image_url if respond_to? :fallback_image_url
    get_from_model(path) || detect_image_on(path) || fallback || default_image(path, defaults_to_class)
  end

  private
  def detect_image_on path
    %w(.jpg .png .jpeg).map do |ext|
      [File.join(Settings.storage_path, path) + ext,  File.join(Settings.storage_url, path) + ext]
    end.detect {|f| File.exists?(f.first)}.try(:last) 
  end

  def get_from_model path 
    img = self['image']
    return nil if img.nil?
    file_path = File.join(Settings.storage_path, path.first(2), img)
    return nil unless File.exists?(file_path)
    File.join(Settings.storage_url, path.first(2), img)
  end

  def default_image path, defaults_to_class
    if defaults_to_class
      if respond_to? :default_image_file_name
        File.join Settings.storage_url, 'default', default_image_file_name
      else
        File.join Settings.storage_url, 'default', "#{self.class.name.to_s.demodulize.downcase}.png"
      end
    else
      File.join Settings.storage_url, path.first(2), "#{path.last}.jpg"
    end
  end
end

