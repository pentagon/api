class Album < ApiBase
  document_type 'album'

  property :_id
  property :artist
  property :charts
  property :contributors
  property :duration
  property :genre
  property :image_url
  property :label
  property :popularity
  property :popularity_rank
  property :release_date
  property :rights
  property :title
  property :tracks
  property :type
  property :tracks_count

  def release_year
    Time.parse(release_date).year
  end

  def as_json opts = {}
    super(opts.update(root: false))
  end
end
