class LogEntry
  include Persistence::LogEntry
  include Tire::Model::Search
  include Tire::Model::Callbacks

  index_name 'logger'
  document_type 'log_entry'

  class << self
    def load_from_params params
      view_filters = %w(event_scope event_type)
      accepted_filters = view_filters.reject { |f| params[f].nil? }
      keys = accepted_filters.map { |f| params[f] }
      if accepted_filters.empty?
        all
      elsif accepted_filters.length == 1
        where accepted_filters.first => keys.first
      else
        where Hash[*accepted_filters.zip(keys).flatten]
      end
    end
  end
end
