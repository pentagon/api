module Radio::RadioRequest
  extend ActiveSupport::Concern

  def default_commercial_filters
    {
      'timeRange' => {'future' => 0, 'present' => 60},
      'genre' =>     {'same' => 100},
      'hss' =>       {'superHit' => 100}
    }
  end

  def default_free_filters
    {
      'timeRange' => {'future' => 100},
      'genre' =>     {},
      'hss' =>       {'superHit' => 0}
    }
  end

  class Query
    attr_accessor :filters, :priority, :station_id, :requests, :subuniverse_uri, :command_class

    def initialize
      @requests = []
    end

    def add_request_data data
      @requests.push *Array.wrap(data)
    end

    def as_hash
      {
        action: 'station:update',
        popularity_filter: false,
        priority: @priority,
        station_id: @station_id,
        requests: @requests,
        subuniverse_uri: @subuniverse_uri
      }
    end

    def exec_async handling_class, handling_method
      MisService.enqueue command_class.new handling_class, handling_method, as_hash
    end

    def exec_sync handling_class, handling_method, seconds_to_wait = 6, poll_delay = 0.1
      # assume that job is not completed if its job_id exists in RedisDb
      retries = seconds_to_wait / poll_delay
      job_id = MisService.enqueue command_class.new handling_class, handling_method, as_hash, 1
      loop do
        if RedisDb.client.exists job_id
          sleep poll_delay
          (retries -= 1) > 0 ? next : raise(SyncMisRequestTimeout.new)
        end
        break
      end
    end
  end

  def create_query &block
    query = Query.new
    yield query
    query
  end
end
