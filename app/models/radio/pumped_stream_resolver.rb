class Radio::PumpedStreamResolver < Radio::StreamResolver
  attr_accessor :subscription

  def initialize(params = {})
    super
    subscription = params[:subscription].presence
    subscription = ::Persistence::Subscription.find_by(uri: subscription) if subscription.is_a?(String)
  end

  def after_stream_fetch_callback
    increment_tracks_counter_on_subscription if labeled_track?
  end

  private

  def increment_tracks_counter_on_subscription
    if subscription && subscription.usable?
      subscription.custom_options["track_requests"] ||= 0
      subscription.custom_options["track_requests"] += 1
      subscription.save
    end
  end
end
