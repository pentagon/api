class Radio::PoolManager

  class QueueExhausted < StandardError
    def initialize station_id
      super "queue is exhausted for station: '#{station_id}'"
    end
  end

  include Radio::RadioRequest
  attr_accessor :queue_mgr, :station, :command_class, :filters

  def initialize station
    @station = station
    @queue_mgr = Radio::QueueManager.new @station.uri
    @filters = @station.is_free ? default_free_filters : default_commercial_filters
  end

  def pick_next_track_id
    track_id = queue_mgr.extract_track
    raise QueueExhausted.new station.uri unless track_id
    track_id
  end

  def command_class
    @command_class || MisCommand::GetRadioData
  end

  # TODO remove it after Jesus gets ready
  def get_subuniverse_uri
    return Settings.radio.mis_api.subuniverse_uris.free if station.is_free
    Settings.radio.mis_api.subuniverse_uris.commercial[station.country] ||
      Settings.radio.mis_api.subuniverse_uris.commercial.other
  end

  def send_sync_mis_request &block
    res = create_query do |r|
      r.command_class = self.command_class
      r.filters = filters
      r.station_id = station.uri
      r.priority = Settings.radio.mis_api.job_priority
      r.subuniverse_uri = get_subuniverse_uri
      yield r if block_given?
    end
    res.exec_sync Radio::QueueManager, :process_response if res.requests.any?
  end

  def send_async_mis_request &block
    res = create_query do |r|
      r.command_class = self.command_class
      r.filters = filters
      r.station_id = station.uri
      r.priority = Settings.radio.mis_api.job_priority
      r.subuniverse_uri = get_subuniverse_uri
      yield r if block_given?
    end
    res.exec_async Radio::QueueManager, :process_response if res.requests.any?
  end
end
