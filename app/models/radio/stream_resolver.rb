class Radio::StreamResolver
  include UriUtils
  attr_accessor :agent, :headers, :ip_address, :station_uri, :track, :track_uri, :user_uri, :user,
    :user_country, :response

  def initialize args
    @user = args[:user]
    @user_uri = args[:user_uri].presence || args[:user].try(:uri)
    @user_country = args[:user_country]
    @track = args[:track]
    @track_uri = args[:track_uri].presence || args[:track].try(:id)
    @ip_address = args[:ip_address]
    @agent = args[:agent].presence
    @station_uri = args[:station_uri] || ''
    @track = args[:track]
    @headers = args[:headers].presence || {}
  end

  def get_radio_location
    @response = {success: true, location: track.music_url}
    if labeled_track? && Settings.medianet_api.use_full_tracks
      @response = get_medianet_radio_location
      register_track_request
    end
    after_stream_fetch_callback
    response
  end

  def after_stream_fetch_callback
    # should be overridden in descendants
  end

  def register_track_request
    create_params = {
      agent: agent,
      artist_name: track.try(:artist).try(:title),
      artist_uri: track.try(:artist).try(:id),
      country: user_country,
      environment: Rails.env.to_s,
      ip_address: ip_address,
      response: response,
      station_uri: station_uri,
      success: response[:success],
      track_duration: track.duration,
      track_name: track.title,
      track_uri: track_uri,
      type: track_type,
      user_email: user.try(:email),
      user_uri: user_uri
    }
    Rails.logger.info "[#{self.class.name}] creating full_track_request from params: '#{create_params}'"
    Persistence::FullTrackRequest.create create_params
  end

  private

  def user
    @user ||= Persistence::User.find_by uri: user_uri
  end

  def agent
    @agent ||= (headers['HTTP_X_RADIO_AGENT'] || headers['X-Radio-Agent']).to_s
  end

  def medianet_id
    track_uri.split(':').last
  end

  def track
    @track ||= Track.fetch track_uri
  end

  def track_type
    self.class.get_source_from_uri track_uri
  end

  def labeled_track?
    track_type == 'labeled'
  end

  def get_medianet_radio_location
    response = Medianet::Base.radio_get_media_location medianet_id, ip_address
    success = !!response['Success'] && !!response['Location']
    url = response['Location'] if success
    error_message = response['Error'] unless success
    unless success
      Rails.logger.error "[#{self.class.name}] Cannot fetch #{self.track_uri} location from Medianet."
      Rails.logger.error "[#{self.class.name}] Track: '#{self.track_uri}', station: '#{self.station_uri}', ip: #{ip_address} :: #{response}"
    end
    {success: success, error: error_message, location: url}
  end
end
