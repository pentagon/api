class Radio::QueueManager
  class << self
    attr_accessor :logger

    def configure
      yield self
    end

    def process_response response
      if response['status'].eql? 'success'
        station_id = response['station_id']
        if response['playlist'].blank?
          error_msg = "Empty playlist for station_id: '#{station_id}', response: '#{response}'"
          logger.error error_msg
          raise InteractionErrors::WrongArgument.new error_msg
        else
          mgr = new station_id
          mgr.store_tracks_from_response_playlist response['playlist']
        end
      else
        raise "Failed response: #{response}"
      end
    end
  end

  delegate :logger, to: 'self.class'

  def initialize station_id
    @station_id = station_id
  end

  def store_tracks_from_response_playlist playlist
    RedisDb.client.pipelined do
      playlist.collect {|t| t['uri']}.each {|id| RedisDb.client.sadd redis_key, id}
    end
  end

  def extract_track
    RedisDb.client.spop redis_key
  end

  def redis_key
    "radio_station_pool_#{@station_id}"
  end
end
