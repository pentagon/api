class Track < ApiBase
  document_type 'track'
  behaves_like :digital_goods

  property :_id
  property :album
  property :artist
  property :bpm
  property :charts
  property :color
  property :color_tune
  property :contributors
  property :duration
  property :energy
  property :genre
  property :genre_cloud
  property :hss_uk
  property :hss_us
  property :image_url
  property :isrc
  property :item_number
  property :label
  property :music_url
  property :popularity
  property :release_date
  property :rights
  property :song_uri
  property :subgenre
  property :suggestions
  property :title
  property :track_md5
  property :type
  property :user_uri

  TRACK_DEFAULT_RIGHTS = {available: false, preview: false, purchase: false, stream: false, mis_online: false,
    explicit: false, radio: false}

  class << self
    def digital_goods_find_by_id(id, options = {})
      track = fetch(id)
      country = options[:country].presence || "gb"
      if track
        unless track.rights[:purchase].include?(country)
          raise ::Persistence::PaymentOrder::ItemNotAvailableForPurchase
        end
      end
      track
    end

    def digital_goods_nominal_code
      "4020"
    end
  end

  def album_name
    album.title
  end

  def album_uri
    album.id
  end

  def artist_name
    artist.title
  end

  def artist_uri
    artist.id
  end

  def release_year
    Time.parse(release_date).year
  end

  def show_page_url
    unsigned? ? File.join(::Settings.track_show_page_url, id) : ''
  end

  def artist_show_page_url
    (unsigned? && artist.id) ? File.join(::Settings.artist_show_page_url, artist.id) : ''
  end

  def waveform_base
    "#{::Settings.track_wave_base_url}/#{id}"
  end

  def get_discovery_price(country)
    price = digital_goods_price(country: country)
    if price
      {
        'Amount' => price[:amount].to_f / 100,
        'Currency' => price[:currency]
      }
    end
  rescue Exception => e
    nil
  end

  def digital_goods_name
    "#{title}, by #{artist_name}"
  end

  def digital_goods_reporting_description
    digital_goods_name
  end

  def digital_goods_base_price(options = {})
    country = options[:country].presence.try(:downcase) || "gb"
    unless rights.purchase.include?(country) && ["gb", "us"].include?(country)
      raise ::Persistence::PaymentOrder::ItemNotAvailableForPurchase.new "Track is not available for purchase in #{country}."
    end
    amount, currency = if labeled?
      @medianet_price ||= ::Medianet::Base.get_price(digital_goods_item_provider_item_id, country)
      converted_amount = (@medianet_price[:amount] * 100).to_i
      if @medianet_price[:fixed]
        [converted_amount, @medianet_price[:currency]]
      else
        case country
          when "gb" then [99, "GBP"]
          when "us" then [129, "USD"]
        end
      end
    else
      case country
        when "gb" then [99, "GBP"]
        when "us" then [99, "USD"]
      end
    end
    {amount: amount, currency: currency}
  rescue Medianet::Base::GetPriceError
    Rails.logger.error "Track '#{id}' is not available for purchase. Purchase rights inconsistency."
    raise
  end

  def digital_goods_item_provider_name
    labeled? ? 'MEDIANET' : 'TUNEHOG'
  end

  def digital_goods_item_provider_item_info(options = {})
    @digital_goods_item_provider_item_info ||= if labeled?
      ::Medianet::Base.get_track_info digital_goods_item_provider_item_id, options[:country]
    else
      {}
    end
  end

  def digital_goods_item_provider_item_id
    labeled? ? self.class.get_id_from_uri(id) : id
  end

  def digital_goods_validate(payment_order)
    # Gather errors
    errors_hash = payment_order.class.check_billing_address(payment_order.billing_address.attributes)
    errors_hash.each do |attribute, message|
      payment_order.billing_address.errors.add(attribute, message)
    end

    # Hack to allow superusers purchase from invalid country
    unless payment_order.billing_address["country"] == payment_order.purchase_country
      if payment_order.user.superuser?
        payment_order.purchase_country = payment_order.billing_address["country"]
      else
        payment_order.billing_address.errors[:country] ||= :does_not_match
      end
    end

    # Summarize validations
    if payment_order.billing_address.errors.any?
      payment_order.billing_address.errors.full_messages.each do |message|
        payment_order.errors.add :billing_address, message
      end
    end
  end

  def digital_goods_deliver(payment_order)
    download_request = Persistence::DownloadRequest.create({
      billing_address: payment_order.billing_address.attributes,
      payment_order_uri: payment_order.uri,
      track_uri: id,
      user_uri: payment_order.user_uri
    })
    if download_request.persisted?
      mailer = case payment_order.department_code
      when "500" then AstroMailer
      when "505" then StartuneMailer
      else
        TrackPurchaseMailer
      end
      mailer.purchased_track(download_request).deliver
      {success: true}
    else
      {success: false, errors: download_request.errors.full_messages}
    end
  end
end
