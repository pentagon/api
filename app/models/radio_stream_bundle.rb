class RadioStreamBundle

  include ActionView::Helpers::TextHelper # Need pluralize

  behaves_like :digital_goods

  attr_reader :type, :subtype
  private_class_method :new

  class << self
    def types
      {
        astro: {
          general: {price: 100, count: 172, hours: 10}
        },
        startune: {
          general: {price: 100, count: 172, hours: 10}
        }
      }
    end

    # Type should have "type:subtype" format, e.g. "astro:gold"
    def digital_goods_find_by_id(type, options = {})
      type, subtype = type.split(":").map { |str| str.downcase.to_sym }
      new(type, subtype) if types[type] && types[type][subtype]
    end

    def digital_goods_find_by_ids(types_array, options = {})
      types_array = Array.wrap(types_array).map { |type| type.downcase.to_sym }
      types.keys.select { |type| types_array.include?(type) }.map { |type| new(type) }
    end

    def digital_goods_nominal_code
      # TODO: Ask Julia or Scott for actual code.
      "0000"
    end
  end

  def initialize(type, subtype)
    @type = type.to_sym
    @subtype = subtype.to_sym
  end

  def digital_goods_item_id
    "#{type}:#{subtype}"
  end

  def data
    self.class.types[type][subtype]
  end

  def digital_goods_base_price(options = {})
    country = options[:country].presence || 'gb'
    currency = case country
    when 'gb' then 'GBP'
    when 'us' then 'USD'
    else 'GBP'
    end
    {amount: data[:price], currency: currency}
  end

  def digital_goods_name
    "Radio streaming bundle"
  end

  def digital_goods_description
    pluralize(data[:hours], "hour")
  end

  def digital_goods_reporting_description
    digital_goods_description
  end

  def digital_goods_deliver(payment_order)
    payment_order.update_attributes(item_provider_response: data)
    subscription = ::Persistence::Subscription.get_latest_of_type_for_user(type, payment_order.user_uri)
    return {success: false, errors: ["Subscription is not usable"]} unless subscription.usable?
    subscription.custom_options["bonus_track_requests"] ||= 0
    subscription.custom_options["bonus_track_requests"] += data[:count]
    result = subscription.save
    if result && @type == 'astro'
      AstroMailer.stream_bundle_purchased(payment_order.user, data[:hours]).deliver
    end
    {success: result, errors: subscription.errors.full_messages}
  end

end
