class GenreFeed
  def self.load
    @@genres ||= JSON.parse File.read File.join Rails.root, 'config', 'genres.json'
  end
end
