class FgpProcessor
  class << self
    def enqueue_track track
      query = {}
      md5sum = `md5sum #{track.track_file_path}`.split(' ').first
      if md5sum
        `mad3 --fgp1 #{track.track_file_path}`
        # fgp service expects array or tracks to verify
        query[:tracks] = [{track_uri: track.uri, track_md5: md5sum}]
        MisService.enqueue MisCommand::GetFgpInfo.new name, :process_results, query
      end
    end

    def process_results data
      if data['results']['status'].eql? 'OK'
        data['results']['tracks'].each do |r|
          if (track = ::Persistence::Track.find_by uri: r['track_uri'])
            track.apply_fgp_results fetch_track_info r['found_track_uri']
          else
            Rails.logger.error "[#{name} :: #{Time.now}] failed to fetch track '#{t['track_uri']}' for result: '#{r}'"
          end
        end
      end
    end

    def fetch_track_info track_uri
      res = HashWithIndifferentAccess.new track_uri: track_uri
      track = Track.fetch track_uri
      res.update title: track.title, artist_title: track.artist.title if track
      res
    end
  end
end
