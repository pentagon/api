class Song < ApiBase
  index_name 'origin_song'
  document_type 'song'

  property :_id
  property :md5
  property :tracks
end
