class ApiStatus
  class << self
    def get short = true
      status = {service: 'Tunehog API', status: 'OK'}
      status.update deploy_timestamp
      status.update deployer_stamp
      status.update revision: revision_info
      status.update hostname: Socket.gethostname.split(".").first
      unless short
        status.update beanstalkd: beanstalk_status
        status.update registered_mis_jobs: mis_jobs
      end
      status
    end

    def beanstalk_status
      bt = Beaneater::Pool.new [MisSettings.beanstalk]
      res = {beanstalk: bt.connections.map(&:host)}
      res[:jobs] = {}
      MisSettings.job_tubes.each_pair {|k, v| res[:jobs][k] = bt.tubes[v].stats rescue {}}
      res[:callbacks] = {}
      MisSettings.callback_tubes.each_pair {|k, v| res[:callbacks][k] = bt.tubes[v].stats rescue {}}
      bt.close
      res
    end

    def deploy_timestamp
      res = if File.exists?(file = File.join(Rails.root, 'data', 'deploy.dat'))
        {deployed_at: open(file).read.strip}
      end or {}
    end

    def deployer_stamp
      res = if File.exists?(file = File.join(Rails.root, 'data', 'deployer.dat'))
        {deployer: open(file).read.strip}
      end or {}
    end

    def revision_info
      revision_file = Rails.root.join('data', 'revision.json')
      return {} unless File.exists?(revision_file)
      revision_info = MultiJson.decode File.readlines(revision_file.to_s).first
      revision_info.merge! link: [::Settings.commit_base_url, revision_info['commit']].join('/').gsub('//', '/')
      revision_info
    end

    def mis_jobs
      keys = RedisDb.client.keys 'mis_job_*'
      res = {total: keys.count, jobs: {}}
      keys.each {|k| res[:jobs][k] = RedisDb.client.get k}
      res
    end
  end
end
