class RefreshToken

  DEFAULT_LIFETIME = 1.month

  include Persistence::Document
  store_in collection: 'oauth2_refresh_tokens'.to_sym

  belongs_to :client, class_name: 'Persistence::Client'
  belongs_to :user, class_name: 'Persistence::User'
  has_many :access_tokens, class_name: '::AccessToken'

  field :token
  field :expires_at, type: Time

  index({token: 1}, {unique: true, background: true})
  index({expires_at: 1}, {background: true})

  validates :expires_at, presence: true
  validates :client, presence: true
  validates :token, presence: true, uniqueness: true

  attr_accessible :access_tokens, :user, :client

  after_initialize :init_token, on: :create, unless: :token?
  after_initialize :init_expires_at, on: :create, unless: :expires_at?

  default_scope where(:expires_at.gte => Time.now.utc)

  def expires_in
    (expires_at - Time.now.utc).to_i
  end

  def expired!
    self.expires_at = Time.now.utc
    self.save!
  end

  private

  def init_token
    self.token = SecureRandom.hex
  end

  def init_expires_at
    self.expires_at = DEFAULT_LIFETIME.from_now
  end

end
