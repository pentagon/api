class Ecard < Persistence::Gift

  field :track_uri,            type: String, default: ''
  field :download_request_uri, type: String, default: ''

  belongs_to :download_request, class_name: 'Persistence::DownloadRequest', foreign_key: :download_request_uri, primary_key: :uri
  index({download_request_uri: 1}, {background: true})

  attr_accessible :track_uri
  validates_presence_of :track_uri, :track

  delegate :digital_goods_description, :digital_goods_item_provider_item_info,
    :digital_goods_validate, :digital_goods_reporting_description, to: :track

  def track
    @track ||= Track.fetch track_uri
  end

  def digital_goods_name
    "Tunehog digital ecard gift"
  end

  def digital_goods_base_price(options = {})
    country = options[:country].presence || user_country
    @digital_goods_base_price ||= self.class.digital_goods_base_price_by_track_uri(track_uri, country)
  end

  def digital_goods_deliver(payment_order)
    download_request = Persistence::DownloadRequest.create({
      billing_address: payment_order.billing_address.attributes,
      payment_order_uri: payment_order.uri,
      track_uri: track_uri,
      user_uri: payment_order.user_uri
    })
    if download_request.persisted?
      super(payment_order)
      self.download_request_uri = download_request.uri
      save
      {success: true}
    else
      {success: false, errors: download_request.errors.full_messages}
    end
  end

  class << self

    def digital_goods_nominal_code
      "4021"
    end

    def digital_goods_find_by_id(id, options = {})
      ecard = find_by(uri: id)
      ecard if ::Track.digital_goods_find_by_id(ecard.track_uri, options)
    end

    def digital_goods_base_price_by_track_uri(track_uri, country)
      track = ::Track.digital_goods_find_by_id(track_uri, country: country)
      raise ::Persistence::PaymentOrder::ItemNotAvailableForPurchase unless track
      result = track.digital_goods_base_price(country: country)
      result[:amount] += 50
      integral_part = result[:amount] / 100 * 100
      cents_part = (result[:amount] % 100 < 50) ? 49 : 99
      result[:amount] = integral_part + cents_part
      result
    rescue ::Persistence::PaymentOrder::ItemNotAvailableForPurchase
      raise ::Persistence::PaymentOrder::ItemNotAvailableForPurchase.new("Ecard with this track is not available in your country(#{country})")
    end
  end
end
