class Client < Persistence::Oauth2::Client
  belongs_to :user, class_name: 'Persistence::User'

  has_many :access_tokens#, class_name: 'Devise::Oauth2Providable::AccessToken'
  has_many :refresh_tokens#, class_name: 'Devise::Oauth2Providable::RefreshToken'
  has_many :authorization_codes#, class_name: 'Devise::Oauth2Providable::AuthorizationCode'

  store_in collection: "oauth2_clients".to_sym
end
