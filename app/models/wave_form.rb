require 'quick_magick'
require 'tempfile'
require 'waveform_builder'

class WaveForm
  class ValidationException < Exception; end
  class FileNotFoundException < Exception; end

  class << self
    def make_form_params params
      bc = params[:background_color] || params[:bg] || 'white'
      width, height = 1200, 120
      track_path = pick_file_for_track params[:id]
      raise FileNotFoundException.new unless File.exists? track_path
      if params[:size]
        _, width, height = params[:size].match(/^(\d+)x(\d+)$/).to_a.map(&:to_i)
        raise ValidationException.new 'bad request' unless width.to_i > 0 || height.to_i > 0
      end
      Rails.logger.info "#{name} :: Gonna build WaveForm for params: bg_color: '#{bc}', with: '#{width}', height: '#{height}'"
      WaveformBuilder.build track_path, bc, width, height
    end

    def make_json_from_params params
      json = get_json_from_params params
      json_name = File.join(params[:id].to_s.split ':') + '.json'
      Rails.logger.info "#{name} :: Gonna store WaveForm json"
      File.open(File.join(::Settings.storage_path, json_name), 'w') do |f|
        f.write({waveform: json}.to_json)
      end
      json
    end

    def get_json_from_params params
      track_path = pick_file_for_track params[:id]
      width = params[:width] || 1200
      raise FileNotFoundException.new unless File.exists? track_path
      Rails.logger.info "#{name} :: Gonna build WaveForm json"
      WaveformBuilder.build_json track_path, width
    end

    def pick_file_for_track track_id
      track_full_path = File.join ::Settings.storage_path, track_id.split(':')
      Rails.logger.info "#{name} :: get track path '#{track_full_path}' for track ID: '#{track_id}'..."
      file_name = File.exists?(track_full_path + '.mp3') ? (track_full_path + '.mp3') :
        File.exists?(track_full_path + '.wav') ? (track_full_path + '.wav') : ''
      if file_name.present?
        Rails.logger.info "#{name} :: found file '#{file_name}' for track ID: '#{track_id}'"
        file_name
      else
        Rails.logger.info "#{name} :: failed to pick up mp3 and wav files for track ID: '#{track_id}'. Gonna find all we have..."
        formats = Dir["#{track_full_path}.*"].map {|f| File.extname f}
        Rails.logger.info "#{name} :: Got formats: '#{formats.inspect}'. Returning a file of first format."
        track_full_path + formats.first.to_s
      end
    end
  end
end
