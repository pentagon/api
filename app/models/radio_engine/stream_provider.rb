class RadioEngine::StreamProvider
  attr_accessor :station, :queue_manager, :country

  def initialize(station)
    @station = station
    @country = station.country.eql?('us') ? 'us' : 'uk'
    @queue_manager = RadioEngine::QueueManager.new station, country
  end

  def fetch_next_track(prev_track_id = nil, prev_track_feedback = nil)
    queue_manager.apply_feedback_for_track prev_track_id, prev_track_feedback if prev_track_id
    composite_id = RadioEngine::StreamFiltering.new(@station).filter.first
    if composite_id.blank?
      queue_manager.ask_for_more_tracks
      nil
    else
      store_used_tracks composite_id
      remove_from_pool composite_id
      composite_id.split("|").first
    end
  end

  def remove_from_pool(composite_ids)
    new_pool = @station.pending_tracks_pool - Array.wrap(composite_ids)
    @station.pending_tracks_pool = new_pool
    @station.save
  end

  def store_used_tracks(composite_ids)
    Array.wrap(composite_ids).each do |id|
      ids = id.split("|")
      Persistence::RadioUsedTrack.create({
        album_uri:    ids.second,
        artist_uri:   ids.third,
        station_uri:  @station.uri,
        track_uri:    ids.first,
      })
    end
  end

end

=begin
station = Persistence::Station.find_by uri: uri
RadioEngine::StreamProvider.new(station).fetch_next_track curr_track, curr_track_feedback
=end
