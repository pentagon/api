module RadioEngine::PoolUtils
  extend ActiveSupport::Concern
  REJECTED_POOL_NAME = 'rejected'
  PENDING_POOL_NAME = 'pending'

  attr_accessor :station

  def station_id
    raise StandardError.new 'Method station_id is supposed to be overriden!'
  end

  def station
    @station ||= Persistence::Station.find_by uri: station_id
  end

  def store_to_pending_pool(composite_ids)
    station.add_to_set :pending_tracks_pool, composite_ids
  end

  def store_to_rejections_pool(composite_ids)
    station.add_to_set :rejected_tracks_pool, composite_ids
  end

  def store_to_pool_of_type(pool_type, ids)
    Rails.logger.debug "Storing to pool: '#{pool_type}' IDs: #{ids}"
    if pool_type.eql? REJECTED_POOL_NAME
      store_to_rejections_pool ids
    else
      store_to_pending_pool ids
    end
  end

  def diff_pending_and_rejected
    station.update_attribute :pending_tracks_pool,
      (station.pending_tracks_pool - station.rejected_tracks_pool)
  end
end
