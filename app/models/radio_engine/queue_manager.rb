class RadioEngine::QueueManager
  include RadioEngine::PoolUtils

  FEEDBACK_TYPES = %w(listened like dislike)
  FEEDBACK_POOL_MAP = {
    like: RadioEngine::PoolUtils::PENDING_POOL_NAME,
    dislike: RadioEngine::PoolUtils::REJECTED_POOL_NAME
  }.with_indifferent_access

  attr_accessor :pool_manager, :station

  def initialize(station, country)
    @station = station
    @pool_manager = RadioEngine::PoolManager.new @station.uri, country
  end

  def station_id
    @station.uri
  end

  def apply_feedback_for_track(track_id, feedback)
    if RadioEngine::QueueManager::FEEDBACK_TYPES.include? feedback
      if %w(like dislike).include? feedback
        ::Persistence::RadioTrackReaction.create({
          reaction_type: feedback,
          station_uri: station_id,
          track_uri: track_id,
        })
        pool_manager.request_related_to track_id, FEEDBACK_POOL_MAP[feedback]
      end
    else
      # raise error?
    end
  end

  def ask_for_more_tracks
    station.seed_track_uris.each do |i|
      pool_manager.request_related_to i, RadioEngine::PoolUtils::PENDING_POOL_NAME
    end
  end
end
