class RadioEngine::StreamFiltering

  # TODO: Implement permanent reject pool

  ARTIST_MAX_USAGE_COUNT = 2
  SAME_ARTIST_MIN_PROXIMITY = 7
  SEED_TRACK_MIN_POSITION = 2
  SEED_TRACK_MAX_POSITION = 10
  CONSECUTIVE_LABELED_TRACKS_MAX = 2
  CONSECUTIVE_UNSIGNED_TRACKS_MAX = 1

  def initialize(station)
    @station = station
    @composite_ids = station.pending_tracks_pool.compact
    @logger = Logger.new(File.join(Rails.root, "log", "new_radio.log"))
    @logger.formatter = proc do |severity, timestamp, progname, msg|
      "[#{severity.first}][#{timestamp}] #{String === msg ? msg : msg.inspect}\n"
    end
  end

  def filter
    @logger.info "Station filtering start: #{@station.uri}"
    reject_used_tracks
    reject_n_times_used_artists
    reject_close_tracks_from_same_artists
    include_seed_track_within_bounds
    unless @station.is_free
      reject_seed_albums
      reject_used_albums
      reject_consecutive_single_or_album_tracks
      reject_non_single_after_unsigned
    end
    @logger.info "Station filtering finished: #{@station.uri}\n"
    @station.save
    @composite_ids.compact
  end

private

  def track_type(track)
    if track.unsigned?
      'emerging'
    elsif track.rights[:hit].to_i == 1
      'single'
    else
      'album'
    end
  end

  def transformed_id(composite_id)
    ids_array = composite_id.split("|")
    {
      album:  ids_array.second,
      artist: ids_array.third,
      track:  ids_array.first,
    }
  end

  def used_tracks
    Persistence::RadioUsedTrack.where(station_uri: @station.uri).desc(:created_at)
  end

  def reject_used_tracks
    track_uris = @composite_ids.map { |id| transformed_id(id)[:track] }
    used_composite_ids = used_tracks.in(track_uri: track_uris).composite_ids
    @composite_ids -= used_composite_ids
    @station.pending_tracks_pool -= used_composite_ids
    @logger.info "Reject used tracks:\n#{used_composite_ids}" unless used_composite_ids.blank?
    self
  end

  def reject_used_albums
    album_uris = @composite_ids.map { |id| transformed_id(id)[:album] }
    used_album_uris = used_tracks.in(album_uri: album_uris).pluck(:album_uri)
    @composite_ids.reject! do |id|
      if used_album_uris.include?(transformed_id(id)[:album])
        @station.pending_tracks_pool -= [id]
        @logger.info "Reject used album: #{id}"
        true
      end
    end
    self
  end

  def reject_seed_albums
    if @station.created_at >= 1.hour.ago
      @composite_ids.reject! do |id|
        if @station.seed_tracks.map(&:album_uri).include?(transformed_id(id)[:album])
          @station.pending_tracks_pool -= [id]
          @logger.info "Reject same album as seed: #{id}"
          true
        end
      end
    end
    self
  end

  def reject_n_times_used_artists
    artist_uris = @composite_ids.map { |id| transformed_id(id)[:artist] }
    used_artist_uris = used_tracks.gte(created_at: 1.hour.ago)
      .in(artist_uri: artist_uris).pluck(:artist_uri)
    @composite_ids.reject! do |id|
      artist_uri = transformed_id(id)[:artist]
      artist_is_used = used_artist_uris.include?(artist_uri)
      artist_used_twice = used_artist_uris.count(artist_uri) >= ARTIST_MAX_USAGE_COUNT
      if artist_is_used && artist_used_twice
        @logger.info "Reject #{ARTIST_MAX_USAGE_COUNT} times used artist: #{id}"
        true
      end
    end
    self
  end

  def reject_close_tracks_from_same_artists
    artist_uris = @composite_ids.map { |id| transformed_id(id)[:artist] }
    used_artist_uris = used_tracks.limit(SAME_ARTIST_MIN_PROXIMITY).pluck(:artist_uri)
    @composite_ids.reject! do |id|
      if used_artist_uris.include?(transformed_id(id)[:artist])
        @logger.info "Reject same artist within #{SAME_ARTIST_MIN_PROXIMITY} tracks: #{id}"
        true
      end
    end
    self
  end

  def include_seed_track_within_bounds
    if @station.seed_track_uris.length == 1
      seed_track = @station.seed_tracks.first
      seed_track_id = "#{seed_track.id}|#{seed_track.album_uri}|#{seed_track.artist_uri}"
      unless used_tracks.pluck(:track_uri).include?(seed_track.id)
        random_factor = Random.rand(SEED_TRACK_MAX_POSITION - 1) <= used_tracks.count
        within_bounds = used_tracks.count.between?(SEED_TRACK_MIN_POSITION - 1, SEED_TRACK_MAX_POSITION - 1)
        if within_bounds && random_factor
          @logger.info "Added seed track within [#{SEED_TRACK_MIN_POSITION}, #{SEED_TRACK_MAX_POSITION}] tracks: #{seed_track_id}"
          @composite_ids.unshift(seed_track_id)
        end
      end
    end
    self
  end

  def reject_consecutive_single_or_album_tracks
    return self if used_tracks.length < CONSECUTIVE_LABELED_TRACKS_MAX
    previous_tracks_uris = used_tracks.limit(CONSECUTIVE_LABELED_TRACKS_MAX).pluck(:track_uri)
    previous_tracks = Track.fetch(previous_tracks_uris)
    tracks_pool = Track.fetch(@composite_ids.map { |id| transformed_id(id)[:track] })
    previous_all_single = previous_tracks.all? { |track| track_type(track) == "single" }
    previous_all_album = previous_tracks.all? { |track| track_type(track) == "album" }
    @composite_ids = tracks_pool.reject do |pending_track|
      if track_type(pending_track) == "single"
        if previous_all_single
          @logger.info "Reject more than #{CONSECUTIVE_LABELED_TRACKS_MAX} consecutive single track: #{pending_track.id}"
          true
        end
      elsif track_type(pending_track) == "album"
        if previous_all_album
          @logger.info "Reject more than #{CONSECUTIVE_LABELED_TRACKS_MAX} consecutive album track: #{pending_track.id}"
          true
        end
      end
    end.map { |t| "#{t.id}|#{t.album_uri}|#{t.artist_uri}" }
    self
  end

  def reject_non_single_after_unsigned
    return self if used_tracks.length < CONSECUTIVE_UNSIGNED_TRACKS_MAX
    previous_tracks_uris = used_tracks.limit(CONSECUTIVE_UNSIGNED_TRACKS_MAX).pluck(:track_uri)
    previous_tracks = Track.fetch(previous_tracks_uris)
    tracks_pool = Track.fetch(@composite_ids.map { |id| transformed_id(id)[:track] })
    if previous_tracks.all? { |track| track_type(track) == "emerging" }
      @composite_ids = tracks_pool.reject do |pending_track|
        if track_type(pending_track) != "single"
          @logger.info "Reject #{CONSECUTIVE_UNSIGNED_TRACKS_MAX} consecutive unsigned track: #{pending_track.id}"
          true
        end
      end.map { |t| "#{t.id}|#{t.album_uri}|#{t.artist_uri}" }
    end
    self
  end
end
