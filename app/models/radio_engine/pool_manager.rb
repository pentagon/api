class RadioEngine::PoolManager
  include RadioEngine::PoolUtils

  class << self
    def process_response(response)
      if response['status'].eql? 'OK'
        station_id = response['station_id']
        country = response['region']
        track_ids = response['results']['tracks'].sort {|a, b| a['d'] <=> b['d']}.collect { |t| t['uri'] }
        pool_type = response['pool_type'].eql?(RadioEngine::PoolUtils::REJECTED_POOL_NAME) ?
          RadioEngine::PoolUtils::REJECTED_POOL_NAME : RadioEngine::PoolUtils::PENDING_POOL_NAME
        if track_ids.any?
          mgr = new station_id, country
          composite_ids = Track.fetch(track_ids).map do |track|
            "#{track.id}|#{track.album_uri}|#{track.artist_uri}"
          end
          mgr.store_to_pool_of_type pool_type, composite_ids
          mgr.diff_pending_and_rejected
        else
          Rails.logger.error "[RadioEngine::PoolManager] No track_ids for response: #{response}"
        end
      else
        Rails.logger.error "[RadioEngine::PoolManager] Bad response response: #{response}"
      end
    end
  end

  attr_accessor :station_id, :country

  def initialize(station_id, country)
    @station_id = station_id
    @country = country
    @priority = 1
  end

  def request_related_to(track_id, pool_type)
    query = {track_uri: track_id, region: country, num_tracks_per_subset: 20,  app: 'stream',
      station_id: @station_id, pool_type: pool_type}
    cmd = MisCommand::GetRecommendation.new self.class, :process_response, query, @priority
    MisService.enqueue cmd
  end
end
