module Recommend
  class Product
    def self.top_by_period *args
      options = args.extract_options!
      response = JSON.parse RestClient.post File::join(::Settings.recommend_api_url, "/product/top-by-period"), options
      response['results'] if response['ok']
    rescue
      nil
    end

    def self.get_by_ids options
      response = JSON.parse RestClient.post File::join(::Settings.recommend_api_url, "/product/fetch"), options
      response['results'] if response['ok']
    end
  end
end
