class MisLogger
  include Singleton

  def debug args
    Persistence::MisLogEntry.create args.merge type: 'debug', environment: Rails.env
  end

  def info args
    Persistence::MisLogEntry.create args.merge type: 'info', environment: Rails.env
  end

  def error args
    Persistence::MisLogEntry.create args.merge type: 'error', environment: Rails.env
  end

  def self.method_missing method, *args
    instance.send method, *args
  end
end
