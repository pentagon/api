class FeaturedBlockSeeder
  include Featured::BlockSettings

  def initialize block, type
    @block, @type = block, type
  end

  def seed items
    case @block.method_type.to_i
    when ARTIST_METHOD
      handle_artst_method(items)
    when TRACKS_METHOD
      handle_tracks_method(items)
    when RECOMMEND_METHOD
      handle_recommend_method(items)
    end
  end

  def handle_artst_method items
    Persistence::Artist.in(uri: items).all.each { |a| @block.send(@type).create build_artist_params a }
  end

  def build_artist_params artist
    {
      uri: artist.uri,
      cover: artist.image_url,
      name: artist.display_name,
      name_url: File.join(::Settings.musicmanager_url, 'users', artist.user_uri)
    }
  end

  def handle_tracks_method items
    Track.fetch(items).map { |t| @block.send(@type).create build_track_params t }
  end

  def build_track_params track
    {
      uri: track.id,
      cover: track.image_url,
      name: track.title,
      name_url: (File.join(::Settings.musicmanager_url, 'users', track.artist.user_uri, 'music', track.id) if track.artist.user_uri),
      subname: track.artist.title,
      subname_url: (File.join(::Settings.musicmanager_url, 'users', track.artist.user_uri) if track.artist.user_uri),
      url: File.join(::Settings.storage_url, track.id.split(':')) + '.' + 'mp3'
    }
  end

  def handle_recommend_method items
    items.each { |i| @block.send(@type).create uri: i }
  end

end
