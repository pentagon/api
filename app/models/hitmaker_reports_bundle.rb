class HitmakerReportsBundle

  include ActionView::Helpers::TextHelper # Need pluralize
  behaves_like :digital_goods

  attr_reader :type
  private_class_method :new

  alias_method :digital_goods_item_id, :type

  class << self
    def types
      {
        bronze:   {price: 299,  count: 1},
        silver:   {price: 1199, count: 5},
        gold:     {price: 2399, count: 12},
        platinum: {price: 3599, count: 20}
      }
    end

    def digital_goods_find_by_id(type, options = {})
      type = type.downcase.to_sym
      new(type) if types[type]
    end

    def digital_goods_find_by_ids(types_array, options = {})
      types_array = Array.wrap(types_array).map { |type| type.downcase.to_sym }
      types.keys.select { |type| types_array.include?(type) }.map { |type| new(type) }
    end

    def digital_goods_nominal_code
      "4030"
    end
  end

  def initialize(type)
    @type = type
  end

  def data
    self.class.types[type]
  end

  def digital_goods_base_price(options = {})
    {amount: data[:price], currency: "GBP"}
  end

  def digital_goods_name
    "HitLogic reports bundle"
  end

  def digital_goods_description
    count = data[:count]
    count == 1 ? "1 report" : "#{count} #{'report'.pluralize}"
  end

  def digital_goods_reporting_description
    "#{@type.capitalize} (#{digital_goods_description})"
  end

  def digital_goods_deliver(payment_order)
    payment_order.update_attributes(item_provider_response: data)
    user = payment_order.user
    user.available_reports += data[:count] * payment_order.quantity
    result = user.save
    {success: result, errors: user.errors.full_messages}
  end

end
