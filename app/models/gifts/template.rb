class Gifts::Template  < ActiveJSON::Base
  include ActiveHash::Associations
  include ActiveModel::Serialization
  include ActiveModel::SerializerSupport


  set_root_path File.join(Rails.root, 'config', 'gifts_data')
  set_filename "templates"

  belongs_to :category, class_name: "Gifts::Category"

  def image_url
    Settings.storage_url + '/gifts/templates/' + image_path
  end
end
