class Gifts::Category < ActiveJSON::Base
  include ActiveHash::Associations
  include ActiveModel::Serialization
  include ActiveModel::SerializerSupport

  set_root_path File.join(Rails.root, 'config', 'gifts_data')
  set_filename "categories"

  has_many :templates, class_name: "Gifts::Template"

  def image_url
    File.join Settings.storage_url, 'gifts', 'categories', image_name
  end

  def image_hover_url
    File.join Settings.storage_url, 'gifts', 'categories', image_name_hover
  end
end
