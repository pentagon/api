class Gifts::Pack < ActiveJSON::Base
  include ActiveModel::Serialization
  include ActiveModel::SerializerSupport

  AVAILABLE_IMAGES = %w(small front top)

  set_root_path File.join(Rails.root, 'config', 'gifts_data')
  set_filename "packs"

  def image_url(type = nil)
    type = type ? type + '/' : ''
    Settings.storage_url + "/gifts/packs/#{type}" + image_name
  end

  def image_path(type = nil)
    type = type ? type + '/' : ''
    Settings.storage_path + "/gifts/packs/#{type}" + image_name
  end

  def image_back_url
    Settings.storage_url + "/gifts/packs/back/pack_back.png"
  end

  AVAILABLE_IMAGES.each do |m|
    define_method("image_#{m}_url") { image_url m }
  end

end
