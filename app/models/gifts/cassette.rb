class Gifts::Cassette < ActiveJSON::Base
  include ActiveModel::Serialization
  include ActiveModel::SerializerSupport

  set_root_path File.join(Rails.root, 'config', 'gifts_data')
  set_filename "cassettes"

  def image_url
    Settings.storage_url + '/gifts/cassettes/' + image_name
  end

  def image_large_url
    Settings.storage_url + '/gifts/cassettes/large/' + image_name
  end

end

