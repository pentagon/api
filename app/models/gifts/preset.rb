class Gifts::Preset  < ActiveJSON::Base
  include ActiveHash::Associations
  include ActiveModel::Serialization
  include ActiveModel::SerializerSupport


  set_root_path File.join(Rails.root, 'config', 'gifts_data')
  set_filename "presets"

  belongs_to :template, class_name: "Gifts::Template"

end
