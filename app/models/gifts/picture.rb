class Gifts::Picture
  include Persistence::Document
  include ::Utils::CarrierWave::Glue
  include ::Utils::CarrierWave::Crop

  has_attachment :image, Gifts::PictureUploader, extensions: :crop

  field :user_uri,  type: String, default: ''

  attr_accessible :user_uri

  belongs_to :user, class_name: 'Persistence::User', foreign_key: :user_uri, primary_key: :uri

  def set_raw_image(data)
    is_base64 = data.split(',')[0].include?('base64')
    temp_file = Tempfile.new(['webcam', is_base64 ? ".png": ".jpeg" ], "#{Rails.root}/tmp", encoding: 'ascii-8bit')
    begin
      temp_file.binmode
      temp_file.write(is_base64 ? Base64.decode64(data.split(',')[1]) : data)
      self.image = temp_file
    ensure
      temp_file.close
    end
  end
end
