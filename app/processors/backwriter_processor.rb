class BackwriterProcessor < TorqueBox::Messaging::MessageProcessor
  def on_message(body)
    process_method = case body[:key]
    when 'logger'
      method(:save_log)
    when 'activity'
      method(:save_activity)
    when 'feedback'
      method(:feedback)
    when 'search'
      method(:search)
    else
      method(:bypass)
    end
    if body[:data].is_a? Array
      body[:data].each { |entry_params| process_method.call(entry_params) }
    else
      process_method.call(body[:data])
    end
  rescue Exception => e
    Rails.logger.error "[BackwriterProcessor] :: message: #{body}, exception: #{e}"
  end

  private

  def search hash
    entry = Persistence::SearchStatistic.new(hash)
    entry.save if entry.valid?
  end

  def save_log hash
    entry = LogEntry.new(hash)
    entry.save if entry.is_valid?
  end

  def save_activity hash
    Persistence::Activity.create trackable_uri: hash[:uri], trackable_type: 'Persistence::User', key: hash[:type],
      parameters: hash[:parameters]
  end

  def feedback hash
    unless hash[:source].to_s == 'recommend'
      RestClient.post File::join(::Settings.recommend_api_url, 'notifications', 'feedback'), hash
    end
  end

  def bypass hash
  end

end
