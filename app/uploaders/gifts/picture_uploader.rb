class Gifts::PictureUploader < Utils::CarrierWave::BaseUploader
  process convert: 'jpg'

  def filename
    model.uri.split(':').last + '.jpg'
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  version :original do
    process convert: 'jpg'
    process resize_to_limit: [800, 600]
  end

  version :thumb, :from_version => :original do
    process :crop
    process convert: 'jpg'
    process :resize_to_fill => [215,215]
  end
end
