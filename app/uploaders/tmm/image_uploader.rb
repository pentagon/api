class Tmm::ImageUploader < Utils::CarrierWave::BaseUploader
  process :crop
  process convert: 'jpg'
  # permissions 0666
  # directory_permissions 0777

  def default_url
    model.fallback_image_url.to_s if model.respond_to? :fallback_image_url
  end

  def filename
    @name ||= begin
      if model.image.cached?.present?
        [model.uri.split(':').last, model.updated_at.to_i, 'jpg'].join('.')
      else
        model['image']
      end
    end
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  def errors
    {}
  end
end
