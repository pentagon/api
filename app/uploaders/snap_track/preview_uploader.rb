class SnapTrack::PreviewUploader < Utils::CarrierWave::BaseUploader
  process :crop
  process convert: 'jpg'

  def filename
    'preview_' + model.uri.split(':').last + '.jpg'
  end

  def extension_white_list
    %w(jpg jpeg png)
  end

  def errors
    {}
  end
end
