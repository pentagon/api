class RadioMailer < ActionMailer::Base
  default from: Settings.default_from

  def subscription_confirmation(subscription)
    user = Persistence::User.find_by(uri: subscription.user_uri)
    @username = [user.first_name, user.display_name].reject(&:empty?).first
    @subscription = subscription
    mail to: user.email, subject: 'Confirmation of your tunehog radio subscription'
  end

  def subscription_end_warning(subscription)
    user = Persistence::User.find_by(uri: subscription.user_uri)
    @username = [user.first_name, user.display_name].reject(&:empty?).first
    @subscription = subscription
    mail to: user.email, subject: 'Your tunehog Radio subscription will auto renew in 5 days'
  end

  def subscription_is_over(subscription)
    user = Persistence::User.find_by(uri: subscription.user_uri)
    @username = [user.first_name, user.display_name].reject(&:empty?).first
    @subscription = subscription
    mail to: user.email, subject: 'Your tunehog Radio subscription is over'
  end

  def subscription_cancelled(subscription)
    user = Persistence::User.find_by(uri: subscription.user_uri)
    @username = [user.first_name, user.display_name].reject(&:empty?).first
    @subscription = subscription
    mail to: user.email, subject: 'Your tunehog Radio subscription is cancelled'
  end

end
