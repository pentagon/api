class MobileMailer < ActionMailer::Base

  default from: Settings.default_from

  def notification params
    user = params[:user]
    @username = user.first_name || user.nick_name
    @message = params[:message]
    mail to: user.email, subject: params[:subject]
  end
end
