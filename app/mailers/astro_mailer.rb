class AstroMailer < ActionMailer::Base
  default from: Settings.webastro_from

  def subscription_confirmation(subscription)
    return unless subscription.user
    set_subscription_parameters(subscription)
    mail to: @user.email, subject: 'Confirmation of your Astro subscription'
  end

  def subscription_cancelled(subscription)
    return unless subscription.user
    set_subscription_parameters(subscription)
    mail to: @user.email, subject: 'Your Astro subscription is cancelled'
  end

  def subscription_payment_processed(subscription)
    return unless subscription.user
    set_subscription_parameters(subscription)
    mail to: @user.email, subject: 'Astro DJ subscription renewal'
  end

  def subscription_suspended(subscription)
    return unless subscription.user
    set_subscription_parameters(subscription)
    mail to: @user.email, subject: 'Astro DJ subscription suspended'
  end

  def stream_bundle_purchased(user, hours)
    @username = get_username(user)
    @hours = hours
    mail to: user.email, subject: "Astro DJ - #{@hours} additional hours purchase"
  end

  def trial_near_expiry(subscription)
    user = subscription.user
    return unless user
    @username = get_username(user)
    mail to: user.email, subject: "Astro DJ: Trial is coming to an end"
  end

  def trial_expired(subscription)
    user = subscription.user
    return unless user
    @username = get_username(user)
    mail to: user.email, subject: "Astro DJ: Trial is coming to an end"
  end

  def mixtape_sent(mixtape)
    @mixtape = mixtape
    @mixtape_url = "#{Settings.web_astro_url}/mixtape/mixtapeSender/?id=#{@mixtape.uri}"
    @sender_name = @mixtape.sender_name.presence || @mixtape.user.display_name
    mail(to: @mixtape.user.email, subject: "You've sent an Astro mixtape gift to #{@mixtape.recipient_name}") do |format|
      format.html { render layout: 'astro_mailer_mixtapes' }
    end
  end

  def mixtape_received(mixtape)
    @mixtape = mixtape
    @mixtape_url = "#{Settings.web_astro_url}/mixtape/mixtapePage/?id=#{@mixtape.uri}"
    @sender_name = @mixtape.sender_name.presence || @mixtape.user.display_name
    mail(to: @mixtape.recipient_email, subject: "#{@sender_name} has sent you an Astro DJ Mixtape!") do |format|
      format.html { render layout: 'astro_mailer_mixtapes' }
    end
  end

  def purchased_track(download_request)
    @username = get_username(download_request.user)
    download_request = download_request
    track = download_request.track
    @track_name = "#{track.title}, by #{track.artist_name}"
    @download_url = download_request.download_url
    mail to: download_request.user.email, subject: 'Purchase confirmation'
  end

private

  def set_subscription_parameters(subscription)
    @subscription = subscription
    @user = @subscription.user
    @username = get_username(@user)
  end

  def get_username(user)
    [user.first_name, user.display_name].reject(&:empty?).first
  end
end
