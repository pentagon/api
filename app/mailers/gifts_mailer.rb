class GiftsMailer < ActionMailer::Base
  default from: Settings.default_from

  def main(gift)
    @gift = gift
    attachments.inline['pack.png'] = File.read(gift.pack.image_path)
    %W(open_gift.png app_store.png google_play.png tunehog_logo.png tw.png fb.png tw_footer.png fb_footer.png).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/gifts_mailer/#{f}"))
    end
    @gifts_url = "#{Settings.gifts_url}/#{@gift.gift_type}/#{@gift.uri}/view"
    @sender_name = @gift.sender_name ? @gift.sender_name : @gift.user.display_name
    subject = @gift.mixtape? ? "#{@sender_name} has sent you a tunehog Mixtape!" : "You've received a tunehog e-card from #{@sender_name}!"
    mail to: @gift.recipient_email, subject: subject
  end

  def to_buyer(card)
    @gift = card
    @sender_name = @gift.user.display_name
    mail to: @gift.user.email, subject: 'Your tunehog gift has been sent.'
  end

end
