class TmmMailer < ActionMailer::Base
  default from: Settings.default_from

  def infringement *args
    @options = args.extract_options!
    @user = @options[:user].presence || Persistence::User.find_by(uri: @options[:user_uri])
    @track = @options[:track].presence || Persistence::Track.find_by(uri: @options[:track_uri])
    raise ArgumentError, "User or Track was not found with URIs '#{@options[:user_uri]}', '#{@options[:track_uri]}'" if @user.nil? || @track.nil?
    mail to: @user.email, subject: 'Important message from tunehog Support'
  end
end
