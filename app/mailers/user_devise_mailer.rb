class UserDeviseMailer < ActionMailer::Base

  def reset_password_instructions(record, token, opts={})
    @token = token
    @edit_password_url = opts.delete(:edit_password_url)
    @resource = record
    mail headers_for(record, opts)
    true
  end

  def headers_for record, options = {}
    headers = {
      :subject  => 'Reset password instructions',
      :to       => record.email,
      :from     => 'noreply@tunehog.com',
      :reply_to => 'noreply@tunehog.com'
    }.merge(options)
    headers
  end

end
