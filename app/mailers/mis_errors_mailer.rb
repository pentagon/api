class MisErrorsMailer < ActionMailer::Base

  default from: 'error_notifications@tunehog.com'

  def empty_playlist_received(data, station)
    @data = data
    @station = station
    mail to: Settings.mis_api.mailer.emails, subject: '[radio] Empty playlist received'
  end

end
