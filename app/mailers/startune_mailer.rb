class StartuneMailer < ActionMailer::Base
  default from: Settings.startune_from

  def subscription_confirmation(subscription)
    return unless subscription.user
    set_mail_attributes(subscription.user)
    mail to: @user.email, subject: 'Confirmation of your Star Tune Premium account'
  end

  def subscription_cancelled(subscription)
    return unless subscription.user
    set_mail_attributes(subscription.user)
    mail to: @user.email, subject: 'Your Star Tune Premium account is cancelled'
  end

  def subscription_payment_processed(subscription)
    return unless subscription.user
    set_mail_attributes(subscription.user)
    mail to: @user.email, subject: 'Star Tune Premium account renewal '
  end

  def subscription_suspended(subscription)
    return unless subscription.user
    set_mail_attributes(subscription.user)
    mail to: @user.email, subject: 'Star Tune Premium account suspended'
  end

  def purchased_track(download_request)
    set_mail_attributes(download_request.user)
    @download_request = download_request
    track = @download_request.track
    @track_name = "#{track.title}, by #{track.artist_name}"
    @download_url = @download_request.download_url
    mail to: @download_request.user.email, subject: 'Purchase confirmation'
  end

  def report_received(email, report_url, product_name)
    @username = email.split('@').try(:first)
    @download_url = report_url
    @product_name = product_name
    mail to: email, subject: 'Report received'
  end

private

  def set_mail_attributes(user)
    @user = user
    @username = [@user.first_name, @user.display_name].reject(&:empty?).first
  end
end
