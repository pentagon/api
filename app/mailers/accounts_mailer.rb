class AccountsMailer < ActionMailer::Base

  default from: Settings.default_from
  DWL_FROM = '"DWL Admin" <noreply@digitalwhitelabel.com>'

  def slm_verification_instructions user
    @user = user
    from = '"Sounds Like Me" <slm@tunehog.com>'
    %W(slm_email_button.png slm_email_logo.png).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/accounts_mailer/#{f}"))
    end
    subject = "Welcome to Sounds Like Me!"
    mail from: from, to: @user.email, subject: subject
  end

  def slm_reset_password_instructions user, token
    @user = user
    @token = token
    from = '"Sounds Like Me" <slm@tunehog.com>'
    subject = 'Sounds Like Me: Reset password instructions'
    %W(slm_email_button.png slm_email_logo.png).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/accounts_mailer/#{f}"))
    end
    mail from: from, to: @user.email, subject: subject
  end

  def snapjam_verification_instructions user
    @user = user
    from = '"SnapJam" <snapjam@tunehog.com>'
    subject = 'Welcome to SnapJam!'
    %w(snapjam_email_getapps.png snapjam_email_getapps.svg snapjam_email_logo.png).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/accounts_mailer/#{f}"))
    end
    mail from: from, to: @user.email, subject: subject
  end

  def snapjam_reset_password_instructions user, token
    @user = user
    @token = token
    from = '"SnapJam" <snapjam@tunehog.com>'
    subject = 'SnapJam: Reset password instructions'
    %w(snapjam_email_getapps.png snapjam_email_getapps.svg snapjam_email_logo.png).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/accounts_mailer/#{f}"))
    end
    mail from: from, to: @user.email, subject: subject
  end

  def startune_verification_instructions user
    @user = user
    from = Settings.startune_from
    subject = 'Welcome to Star Tune! Please confirm your registration.'
    %w(startune_fb.png startune_logo.png startune_top.jpg startune_tw.png).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/accounts_mailer/#{f}"))
    end
    mail from: from, to: @user.email, subject: subject
  end

  def startune_reset_password_instructions user, token
    @user = user
    @token = token
    from = '"Star Tune" <startune@tunehog.com>'
    subject = 'Reset password instructions'
    %w(startune_fb.png startune_logo.png startune_top.jpg startune_tw.png).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/accounts_mailer/#{f}"))
    end
    mail from: from, to: @user.email, subject: subject
  end

  def verification_instructions user, service = 'tunehog'
    @user = user
    @service = service
    case @service
    when 'webastro'
      from = '"Astro" <noreply@astrologydj.com>'
      subject = 'Astro: Confirm your registration'
      @product = 'Astro'
    else
      from = Settings.default_from
      subject = 'Welcome to tunehog!'
      @product = 'tunehog'
    end
    mail from: from, to: @user.email, subject: subject
  end

  def global_verification_instructions user
    @user = user
    from = Settings.default_from
    subject = 'Welcome to tunehog!'
    @product = 'tunehog'
    mail from: from, to: @user.email, subject: subject
  end

  def radio_verification_instructions user
    @user = user
    from = '"tunehog Radio" <radio@tunehog.com>'
    subject = 'Welcome to tunehog Radio!'
    @product = 'tunehog'
    %w(radio-ios_email_getapps.png radio-ios_email_getapps.svg radio-ios_email_logo.png).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/accounts_mailer/#{f}"))
    end
    mail from: from, to: @user.email, subject: subject
  end

  def radio_reset_password_instructions user, token
    @user = user
    @token = token
    from = '"tunehog Radio" <radio@tunehog.com>'
    subject = 'tunehog Radio: Reset password instructions'
    %w(radio-ios_email_getapps.png radio-ios_email_getapps.svg radio-ios_email_logo.png).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/accounts_mailer/#{f}"))
    end
    mail from: from, to: @user.email, subject: subject
  end

  def discovery_verification_instructions user
    @user = user
    from = '"tunehog Discover" <discovery@tunehog.com>'
    subject = 'Welcome to tunehog Discover!'
    @product = 'tunehog'
    %w(discovery-ios_email_getapps.png discovery-ios_email_getapps.svg discovery-ios_email_logo.png discovery-a_email_gp.png).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/accounts_mailer/#{f}"))
    end
    mail from: from, to: @user.email, subject: subject
  end

  def discovery_reset_password_instructions user, token
    @user = user
    @token = token
    from = '"tunehog Discover" <discovery@tunehog.com>'
    subject = 'tunehog Discover: Reset password instructions'
    %w(discovery-ios_email_getapps.png discovery-ios_email_getapps.svg discovery-ios_email_logo.png discovery-a_email_gp.png).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/accounts_mailer/#{f}"))
    end
    mail from: from, to: @user.email, subject: subject
  end

  def myfirstplayer_verification_instructions user
    @user = user
    from = '"My First Player" <myfirstplayer@tunehog.com>'
    subject = 'Welcome to My First Player!'
    @product = 'My First Player'
    %w(first-player_email_gp.png first-player_email_logo.png).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/accounts_mailer/#{f}"))
    end
    mail from: from, to: @user.email, subject: subject
  end

  def myfirstplayer_reset_password_instructions user, token
    @user = user
    @token = token
    from = '"My First Player" <myfirstplayer@tunehog.com>'
    subject = 'My First Player: Reset password instructions'
    %w(first-player_email_gp.png first-player_email_logo.png).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/accounts_mailer/#{f}"))
    end
    mail from: from, to: @user.email, subject: subject
  end

  def astro_verification_instructions user
    @user = user
    from = '"Astro DJ" <noreply@astrologydj.com>'
    subject = 'Astro DJ: Confirm your registration'
    %w(astro_mixtape_email_logo.png astro_mixtape_header_line.png).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/accounts_mailer/#{f}"))
    end
    mail from: from, to: @user.email, subject: subject
  end

  def astro_reset_password_instructions user, token
    @user = user
    @token = token
    from = '"Astro DJ" <noreply@astrologydj.com>'
    subject = 'Astro DJ: Reset password instructions'
    %w(astro_mixtape_email_logo.png astro_mixtape_header_line.png).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/accounts_mailer/#{f}"))
    end
    mail from: from, to: @user.email, subject: subject
  end

  def hitlogic_verification_instructions user
    @user = user
    from = '"Cadenza" <cadenza@tunehog.com>'
    subject = 'Welcome to Cadenza!'
    @product = 'Cadenza'
    %w(cadenza_email_header.png).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/accounts_mailer/#{f}"))
    end
    mail from: from, to: @user.email, subject: subject
  end

  def dwl_verification_instructions user
    @user = user
    from = DWL_FROM
    subject = ' Digital White Label. Email confirmation instructions.'
    %w(dwl_mailer_header.jpg dwl_mailer_black-box.jpg).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/accounts_mailer/#{f}"))
    end
    mail from: from, to: @user.email, subject: subject
  end

  def dwl_reset_password_instructions user, token
    @user = user
    @token = token
    from = DWL_FROM
    subject = 'Digital White Label. Reset password instructions.'
    %w(dwl_mailer_header.jpg dwl_mailer_black-box.jpg).each do |f|
      attachments.inline[f] = File.read(Rails.root.join("public/accounts_mailer/#{f}"))
    end
    mail from: from, to: @user.email, subject: subject
  end
end
