class TrackPurchaseMailer < ActionMailer::Base

  default from: Settings.default_from

  def purchased_track(download_request)
    @download_request = download_request
    mail to: @download_request.user.email, subject: 'Purchase confirmation'
  end

end
