module TokenAuthenticatableTrait
  extend Penetrator::Concern

  included do
    ActiveSupport::Deprecation.warn 'TokenAuthenticatableTrait uses devise :token_authenticatable'

    before_save :ensure_authentication_token
  end

  def ensure_authentication_token
    reset_authentication_token if authentication_token.blank?
  end

  def ensure_authentication_token!
    reset_authentication_token! if authentication_token.blank?
  end

  def reset_authentication_token
    self.authentication_token = self.class.authentication_token
  end

  def reset_authentication_token!
    Rails.cache.delete("user_#{authentication_token}")
    reset_authentication_token
    save(validate: false)
  end

  module ClassMethods

    def authentication_token
      generate_token(:authentication_token)
    end
    # Attempt to find a user by its authentorization_token to reset its
    # password. If a user is found and token is still valid, reset its password and automatically
    # try saving the record. If not user is found, returns a new user
    # containing an error in authentorization_token attribute.
    # Attributes must contain authentorization_token, password and confirmation
    def reset_password_by_authentication_token(attributes={})
      recoverable = find_or_initialize_with_error_by(:authentication_token, attributes[:authentication_token])
      if recoverable.persisted?
        recoverable.reset_password!(attributes[:password], attributes[:password_confirmation])
      end
      recoverable
    end
  end
end
