module TrackableTrait
  extend Penetrator::Concern

  def log_sign_in(parameters={})
    unless TorqueBox::ServiceRegistry.lookup("jboss.messaging.default").nil?
      backwriter_queue.publish({key: 'activity', data: {uri: self.uri, type: 'signed_in', parameters: parameters}})
    end
  end

  def log_sign_out(parameters={})
    unless TorqueBox::ServiceRegistry.lookup("jboss.messaging.default").nil?
      backwriter_queue.publish({key: 'activity', data: {uri: self.uri, type: 'signed_out', parameters: parameters}})
    end
  end

  # event records

  def last_sign_in_event
    activities.where(key: 'signed_in').last
  end

  def last_sign_out_event
    activities.where(key: 'signed_out').last
  end

  # timestamps

  def last_sign_in_at
    return if last_sign_in_event.nil?
    last_sign_in_event.created_at
  end

  def last_sign_out_at
    return if last_sign_out_event.nil?
    last_sign_out_event.created_at
  end

  # IPs

  def last_sign_in_ip
    return if last_sign_in_event.nil?
    last_sign_in_event.parameters[:remote_ip]
  end

  def last_sign_out_ip
    return if last_sign_out_event.nil?
    last_sign_out_event.parameters[:remote_ip]
  end

  private

  def backwriter_queue
    @backwriter_queue ||= TorqueBox::Messaging::Queue.new('/queues/backwriter')
  end
end
