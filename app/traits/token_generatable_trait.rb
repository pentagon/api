module TokenGeneratableTrait
  extend Penetrator::Concern

  module ClassMethods
    # Generate a token checking if one does not already exist in the database.
    def generate_token(field)
      loop do
        token = SecureRandom.urlsafe_base64(15).tr('lIO0', 'sxyz')
        break token unless self.find_by({ field => token })
      end
    end
  end
end
