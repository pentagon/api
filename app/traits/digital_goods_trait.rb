module DigitalGoodsTrait
  extend Penetrator::Concern

  module ClassMethods
    def digital_goods_find_by_id(id, options = {})
      raise 'Not implemented'
    end

    def digital_goods_find_by_ids(ids, options = {})
      ids.map { |id| digital_goods_find_by_id(id, options) }
    end

    def digital_goods_type
      self.name.underscore
    end

    # Defines the digital goods type. Consult Alex Chepurnoy or Scott Cunningham.
    def digital_goods_nominal_code
      "0000"
    end

    def digital_goods_use_payment_account
      "small"
    end
  end

  def acts_like_digital_goods?
    true
  end

  # Product name used in PayPal.
  def digital_goods_name
    attribute = [:title, :name, :full_name].select do |attribute|
      respond_to? attribute
    end.first
    if attribute
      self.send attribute
    else
      self.class.digital_goods_type
    end
  end

  def digital_goods_validate(payment_order)
    # Do nothing. Override if you need custom validation.
  end

  # Should return hash with two items: amount in cents and currency code
  def digital_goods_base_price(options = {})
    # return {amount: 595, currency: "USD"} # $5.95
    raise 'Not implemented'
  end

  def digital_goods_price(options = {})
    price = digital_goods_base_price(options)
    modifier = ::Persistence::PaymentPriceModifier.find_by(code: options[:discount_code].to_s.upcase) if options[:discount_code].present?
    if modifier && modifier.applicable_to?(self)
      price[:original_amount] = price[:amount]
      price[:amount] = modifier.calculate_price(price[:original_amount])
    end
    price
  end

  # Description used in PayPal.
  def digital_goods_description
    ""
  end

  # Description used tunehog Admin.
  def digital_goods_reporting_description
    digital_goods_item_id
  end

  def digital_goods_item_provider_name
    'TUNEHOG'
  end

  def digital_goods_item_provider_item_info(options = {})
    {}
  end

  # This is the ID that will be user for finding the item,
  # passing it to `self.class.digital_goods_find_by_id`.
  # Just because we have all this fucking id/uri mess.
  def digital_goods_item_id
    attribute = [:uri, :id].select { |attribute| respond_to?(attribute) }.first
    self.send(attribute) if attribute
  end

  # Item ID in used by item provider.
  # It’s the same as `digital_goods_item_id` for most items.
  def digital_goods_item_provider_item_id
    digital_goods_item_id
  end

  def digital_goods_payment_redirect_url(return_url, cancel_url, mobile = false)
    ::Persistence::PaymentOrder.payment_redirect_url self, return_url, cancel_url, mobile
  end

  def digital_goods_commit_payment(token, payer_id, ip_address, user_uri)
    ::Persistence::PaymentOrder.commit_payment self, token, payer_id, ip_address, user_uri
  end

  # Should deliver the goods and return result hash
  # {success: true} or {success: false, errors: []}
  def digital_goods_deliver(payment_order)
    {success: true}
  end

end
