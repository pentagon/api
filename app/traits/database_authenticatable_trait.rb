module DatabaseAuthenticatableTrait
  extend Penetrator::Concern

  included do
    ActiveSupport::Deprecation.warn 'DatabaseAuthenticatableTrait uses devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable, :omniauthable'

    attr_reader :password
    attr_accessor :password_confirmation

    attr_accessible :password, :password_confirmation
  end

  def valid_password? password
    return false if encrypted_password.blank?
    bcrypt = ::BCrypt::Password.new(encrypted_password)
    password = ::BCrypt::Engine.hash_secret(password, bcrypt.salt)
    password == encrypted_password
  end

  def password= new_password
    @password = new_password
    self.encrypted_password = ::BCrypt::Password.create(new_password, cost: 10).to_s if @password.present?
  end

end
