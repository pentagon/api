module Oauth2ProvidableTrait
  extend Penetrator::Concern

  included do
    ActiveSupport::Deprecation.warn 'Oauth2ProvidableTrait uses devise :oauth2_providable, :oauth2_password_grantable, :oauth2_refresh_token_grantable, :oauth2_authorization_code_grantable'
    #devise :oauth2_providable, :oauth2_password_grantable, :oauth2_refresh_token_grantable,
    #  :oauth2_authorization_code_grantable

    has_many :clients
    has_many :access_tokens
    has_many :refresh_tokens
    has_many :authorization_codes
  end

  module ClassMethods
    # Tries find user by +provider+ and +uid+ then +email+.
    # Creates new record from +auth+ hash when not found.
    # @param [OmniAuth::AuthHash] auth
    # @param [Persistence::User] signed_in_user
    # @return [User]
    def find_or_create_from_oauth(auth, signed_in_user=nil, &block)
      new_user = false
      if signed_in_user.nil?
        # trying find by provider and uid
        user = find_by(provider: auth.provider, uid: auth.uid)
        # return user unless user.nil?

        # trying find by email
        if auth[:partner].present?
          params = {email: auth.info.email, partner: auth[:partner], partner_tracking_page: auth[:partner_tracking_page], partner_tracking_date: auth[:partner_tracking_date], source: auth[:source]}
          params = params.merge({source: auth[:source]}) if auth[:source].present?
        else
          params = {email: auth.info.email}
          params = params.merge({source: auth[:source]}) if auth[:source].present?
        end
        user = (find_by(email: auth.info.email) or new(params)) if user.blank?
        # return user if user.persisted?
        if user.encrypted_password.blank?
          user.generate_password
          new_user = true
        end
      else
        user = signed_in_user
      end
      user.update_from_oauth_info(auth)
      user.save
      if new_user
        if block_given?
          yield user
        else
          user.generate_verification_token!
          AccountsMailer.verification_instructions(user, user.source).deliver
        end
      end
      user
    end

    def create_from_oauth(auth, additional_params = {})
      if additional_params[:referral_token].present?
        referral = Persistence::Referral.find_or_create(additional_params[:referral_token], additional_params[:remote_ip])
        additional_params[:referral_uri] = referral.uri
        additional_params[:referral_ip] = additional_params[:remote_ip]
      end
      user = new(additional_params.merge(email: auth.info.email).except("referral_token", "remote_ip"))
      user.generate_password
      user.update_from_oauth_info(auth)
      user.add_social_auth(auth)
      user.save
      user
    end

    def find_or_create_from_mobile(provider, data, signed_in_user=nil)
      if signed_in_user.nil?
        # trying find by provider and uid
        user = find_by(provider: provider, uid: data[:id])
        # return user unless user.nil?

        # trying find by email
        user = find_or_initialize_by(email: data[:email]) if user.blank?
        # return user if user.persisted?
        user.generate_password if user.encrypted_password.blank?
      else
        user = signed_in_user
      end
      user.update_from_data(provider, data)
      user.save
      user
    end
  end

  def generate_password
    self.password = SecureRandom.urlsafe_base64(15).tr('lIO0', 'sxyz')[0..7]
    ActiveSupport::Deprecation.warn 'generate_password uses Devise.friendly_token'
    self.password_confirmation = password
  end

  # Updates user document from env['omniauth.auth']
  # @param [OmniAuth::AuthHash] auth
  def update_from_oauth_info(auth)
    info = auth.info
    raw_info = auth.extra.raw_info
    self.social_info = raw_info
    self.provider = auth.provider
    self.uid = auth.uid
    self.email = info.email if email.blank?
    self.personal_info.name = info.name if personal_info.name.blank?
    # self.personal_info.nick_name = info.nickname if personal_info.nick_name.blank?
    self.personal_info.first_name = info.first_name if personal_info.first_name.blank?
    self.personal_info.last_name = info.last_name if personal_info.last_name.blank?
    self.personal_info.gender = raw_info.gender if personal_info.gender.nil?
    case auth.provider
    when 'facebook'
      self.personal_info.biography = raw_info.bio if personal_info.biography.blank?
      self.personal_info.date_of_birth = Timeliness.parse(raw_info.birthday, :date) if personal_info.date_of_birth.blank?
    when 'google_oauth2'
      self.personal_info.biography = raw_info.biography if personal_info.biography.blank?
    end
  end

  def add_social_auth(auth)
    auth = self.social_authentications.build(uid: auth[:uid], provider: auth[:provider])
  end

  # Updates user document from simple data hash
  # @param [String] provider
  # @param [Object::HashWithIndifferentAccess] data
  def update_from_data(provider, data)
    self.social_info = data
    self.provider = provider
    self.uid = data[:id]
    self.email = data[:email] if email.blank?
    self.personal_info.name = data[:name] if personal_info.name.blank?
    # self.personal_info.nick_name = data[:username] if personal_info.nick_name.blank?
    self.personal_info.first_name = data[:first_name] if personal_info.first_name.blank?
    self.personal_info.last_name = data[:last_name] if personal_info.last_name.blank?
    self.personal_info.gender = data[:gender] if personal_info.gender.nil?
    self.personal_info.date_of_birth = Timeliness.parse(data[:birthday], :date) if personal_info.date_of_birth.blank?
  end
end
