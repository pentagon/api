module ReferralableTrait
  extend Penetrator::Concern

  included do
    after_initialize :ensure_referral_token
    after_create :add_to_initiator
    attr_accessor :referral_uri, :referral_ip
    attr_accessible :referral_uri, :referral_ip
  end

  module ClassMethods
    def referral_token
      generate_token(:referral_token)
    end
  end

  def responders
    self.class.in(uri: responder_referrals.map(&:responder_uri))
  end

  # def initiator
  #   self.class.find(initiator_referral.initiator_id)
  # end

  # Generate new referral token (a.k.a. "single access token").
  def reset_referral_token
    self.referral_token = self.class.referral_token
  end

  # Generate new referral token and save the record.
  def reset_referral_token!
    reset_referral_token
    save validate: false
  end

  # Generate referral token unless already exists.
  def ensure_referral_token
    reset_referral_token if referral_token.blank?
  end

  # Generate referral token unless already exists and save the record.
  def ensure_referral_token!
    reset_referral_token! if referral_token.blank?
  end

  def not_responded_count
    responder_referrals.where(responder_uri: nil).count
  end

  def add_to_initiator
    if referral_uri.present? and referral = Persistence::Referral.find_by(uri: referral_uri)
      referral.update_attribute(:responder_uri, uri)
    end
  end
  private :add_to_initiator

  def referral_bonuses
    (responders.count || 0) * Setting.referrals.multiplier.to_i
  end
end
