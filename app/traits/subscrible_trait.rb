module SubscribleTrait
  extend Penetrator::Concern

  def subscribe
    update_attribute :subscribed, true
  end

  def unsubscribe
    update_attribute :subscribed, false
  end

  def unsubscribed?
    not subscribed?
  end
end
